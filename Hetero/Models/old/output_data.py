import mysql.connector
import datetime
from datetime import date, timedelta
import scheduler as cp
from scheduler import *
import input_data as ip_data
import scheduler as ps
#from input_data import *
from itertools import groupby
import master as mp

#from PS_IBP_Model import *

def Output_Update():
    Del_Status = {}
    currenttime = datetime.datetime.now()
    print "Step33:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    for i in ps.current_pos:
        # print i, all_starts_var[i],PlanningHorizon
        if int(ps.all_starts_var[i]) >= int(ip_data.PlanningHorizon) + 1:
            ##            print "yes"
            Del_Status[i] = 0
        else:
            Del_Status[i] = max(ps.active_var[(i1, j, ar, s, alt)] for (i1, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
                                i == i1 and alt <> -1)

    Total_RM_Requirement = {}
    Total_RM_Requirement1 = {}
    ##    Total_RM_Requirement_Alt = {}
    currenttime = datetime.datetime.now()
    print "Step34:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##    for (i,j,ar,s,alt,ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
    ##        if i in current_pos and Input_Product_Qty_var[(i,j,ar,alt,ip)]>0:
    ##            if ip==0 or ip==4:
    ##                print i,alt,ip, Input_Product_Qty_var[(i,j,ar,alt,ip)], active_var[(i, j, ar, s, alt)]

    for p in ip_data.Products:
        if ip_data.Product_Type_ID[p] == 1:
            # Total_RM_Requirement[p] = sum(Dom_IP[(i, j, ip)] for (i, j, ar, s, alt, ip) in PO_TASK_AR_SEQ_ALT_IP_COMB \
            #     if i in current_pos and int(alt) <> -1 and int(ip) == int(p))
            Total_RM_Requirement[p] = sum(ps.Dom_IP[(i, j, ip)] for (i, j, ip) in ps.PO_OP_SFG_Comb if i in ps.current_pos and int(ip) == int(p))
            ##        print p, Total_RM_Requirement[p]

    currenttime = datetime.datetime.now()
    print "Step40:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        print "Inside SDST loop after step 40"
        global SDST_report
        SDST_report = []
        for i in ip_data.SETUP_MACHINES:
            if i[0] in ps.non_overlapping_machines:
                for j in xrange(ps.seq_size[i[0]]):
                    j1 = j + 1
                    if j1 <= ps.seq_size[i[0]] and (i[0], j) in ps.valid_comb and (i[0], j1) in ps.valid_comb and j1 == j + 1 and (int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j1])) in ip_data.SETUP_TIME_NEW_COMB_ALL:
                        if ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j] >= ip_data.SDST[(i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j1]))]:
                            SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j1]), ps.End_Time[i[0], j],
                                                ps.End_Time[i[0], j] + ip_data.SDST[(i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j1]))]))
                        else:
                            SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j1]), ps.End_Time[i[0], j],
                                                ps.Begin_Time[i[0], j1]))

        currenttime = datetime.datetime.now()
        print "Step41:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        if ip_data.Setuptime_with_Holiday_Inbetween == "Enable":
            #      SDST between two lots with a shutdown/holiday in between

            if ip_data.SDST_Split == "Enable":  # "Changeover Split allowed"
                for i in ip_data.SETUP_MACHINES:
                    if i[0] in ps.non_overlapping_machines:
                        for j in xrange(ps.seq_size[i[0]]):
                            j1 = j + 1
                            j2 = j1 + 1
                            if j1 <= ps.seq_size[i[0]] and j2 <= ps.seq_size[i[0]] and (i[0], j) in ps.valid_comb and (
                            i[0], j2) in ps.valid_comb and (i[0], j1) in ps.valid_shutdown_comb and (
                            int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2])) in ip_data.SETUP_TIME_NEW_COMB_ALL:
                                if ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j],
                                                        ps.End_Time[i[0], j] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))
                                elif ps.Begin_Time[i[0], j2] - ps.End_Time[i[0], j1] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j1],
                                                        ps.End_Time[i[0], j1] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))
                                else:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j],
                                                        ps.Begin_Time[i[0], j1]))
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j1],
                                                        ps.End_Time[i[0], j1] \
                                                        + ip_data.SDST[(i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))] - (
                                                        ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j])))
                                    ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                            j11 = j1 + 1
                            j2 = j11 + 1
                            if j1 <= ps.seq_size[i[0]] and j11 <= ps.seq_size[i[0]] and j2 <= ps.seq_size[i[0]] and (
                            i[0], j) in ps.valid_comb and (i[0], j2) in ps.valid_comb and (i[0], j1) in ps.valid_shutdown_comb and (
                            i[0], j11) in ps.valid_shutdown_comb and (
                            int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2])) in ip_data.SETUP_TIME_NEW_COMB_ALL:
                                if ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j],
                                                        ps.End_Time[i[0], j] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))

                                elif ps.Begin_Time[i[0], j11] - ps.End_Time[i[0], j1] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j1],
                                                        ps.End_Time[i[0], j1] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))

                                elif ps.Begin_Time[i[0], j2] - ps.End_Time[i[0], j11] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j11],
                                                        ps.End_Time[i[0], j11] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))

                                elif ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j] + ps.Begin_Time[i[0], j11] - ps.End_Time[
                                    i[0], j1] >= ip_data.SDST[(i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j],
                                                        ps.Begin_Time[i[0], j1]))
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j1],
                                                        ps.End_Time[i[0], j1] \
                                                        + ip_data.SDST[(i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))] - (
                                                        ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j])))
                                else:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j],
                                                        ps.Begin_Time[i[0], j1]))
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j1],
                                                        ps.Begin_Time[i[0], j11]))
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j11],
                                                        ps.End_Time[i[0], j11] \
                                                        + ip_data.SDST[(i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]
                                                        - (ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j])
                                                        - (ps.Begin_Time[i[0], j11] - ps.End_Time[i[0], j1])))
            else:  # "Changeover Split not allowed"
                for i in ip_data.SETUP_MACHINES:
                    if i[0] in ps.non_overlapping_machines:
                        for j in xrange(ps.seq_size[i[0]]):
                            j1 = j + 1
                            j2 = j1 + 1
                            if j1 <= ps.seq_size[i[0]] and j2 <= ps.seq_size[i[0]] and (i[0], j) in ps.valid_comb and (
                            i[0], j2) in ps.valid_comb and (i[0], j1) in ps.valid_shutdown_comb and (
                            int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2])) in ip_data.SETUP_TIME_NEW_COMB_ALL:
                                if ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j],
                                                        ps.End_Time[i[0], j] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))
                                elif ps.Begin_Time[i[0], j2] - ps.End_Time[i[0], j1] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j1],
                                                        ps.End_Time[i[0], j1] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))
                                    ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                            j11 = j1 + 1
                            j2 = j11 + 1
                            if j1 <= ps.seq_size[i[0]] and j11 <= ps.seq_size[i[0]] and j2 <= ps.seq_size[i[0]] and (
                            i[0], j) in ps.valid_comb and (i[0], j2) in ps.valid_comb and (i[0], j1) in ps.valid_shutdown_comb and (
                            i[0], j11) in ps.valid_shutdown_comb and (
                            int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2])) in ip_data.SETUP_TIME_NEW_COMB_ALL:
                                if ps.Begin_Time[i[0], j1] - ps.End_Time[i[0], j] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j],
                                                        ps.End_Time[i[0], j] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))
                                elif ps.Begin_Time[i[0], j11] - ps.End_Time[i[0], j1] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j1],
                                                        ps.End_Time[i[0], j1] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))
                                elif ps.Begin_Time[i[0], j2] - ps.End_Time[i[0], j11] >= ip_data.SDST[
                                    (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]), ps.End_Time[i[0], j11],
                                                        ps.End_Time[i[0], j11] + ip_data.SDST[
                                                            (i[0], int(ps.PO[i[0], j]), int(ps.PO[i[0], j2]))]))

                                ##    if Sequence_Dependent_Setup_Time =="Include":
                                ##      global SDST_report
                                ##      SDST_report =[]
                                ##      for i in SETUP_MACHINES:
                                ##        if i[0] in non_overlapping_machines:
                                ##          for j in xrange(seq_size[i[0]]):
                                ##            for j1 in xrange(seq_size[i[0]]):
                                ##              if (i[0],j) in valid_comb and (i[0],j1) in valid_comb and j1==j+1 and (int(i[0]),int(PO[i[0],j]),int(PO[i[0],j1])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                  if Begin_Time[i[0],j1] - End_Time[i[0],j] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j1]))]:
                                ##                    SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j1]), End_Time[i[0],j],End_Time[i[0],j] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j1]))]))
                                ##                    break
                                ##                  else:
                                ##                    SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j1]), End_Time[i[0],j],Begin_Time[i[0],j1]))
                                ##                    break

                                ##      if SDST_Split == "Allowed":
                                ##          for i in SETUP_MACHINES:
                                ##            if i[0] in non_overlapping_machines:
                                ##              for j in xrange(seq_size[i[0]]):
                                ##                for j1 in xrange(seq_size[i[0]]):
                                ##                  for j2 in xrange(seq_size[i[0]]):
                                ##                    if j2==j1+1 and j1==j+1:
                                ##                      if (i[0],j) in valid_comb and (i[0],j2) in valid_comb and (i[0],j1) in valid_shutdown_comb and (int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                        if Begin_Time[i[0],j1] - End_Time[i[0],j] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j],End_Time[i[0],j] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))
                                ##                        elif Begin_Time[i[0],j2] - End_Time[i[0],j1] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j1],End_Time[i[0],j1] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))
                                ##                        else:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j],Begin_Time[i[0],j1]))
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j1],End_Time[i[0],j1] \
                                ##                                              + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]-(Begin_Time[i[0],j1]-End_Time[i[0],j])))

                                ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                                ##                    for j11 in xrange(seq_size[i[0]]):
                                ##                      if int(j1) == int(j) + 1 and int(j11) == int(j1) + 1 and int(j2) == int(j11) + 1:
                                ##                        if (i[0], j) in valid_comb and (i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (i[0], j11) in valid_shutdown_comb and (int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                          if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            #print 1, j,j1,j11,j2
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                ##                                            End_Time[i[0], j] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##
                                ##                          elif Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            #print 2, j, j1, j11, j2
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                ##                                            End_Time[i[0], j1] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##
                                ##                          elif Begin_Time[i[0], j2] - End_Time[i[0], j11] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            #print 3, j, j1, j11, j2
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11],
                                ##                                            End_Time[i[0], j11] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##
                                ##                          elif Begin_Time[i[0], j1] - End_Time[i[0], j] + Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j], Begin_Time[i[0], j1]))
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1], End_Time[i[0], j1] \
                                ##                             + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))] - (Begin_Time[i[0], j1] - End_Time[i[0], j])))
                                ##                          else:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j], Begin_Time[i[0], j1]))
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1], Begin_Time[i[0], j11]))
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11], End_Time[i[0], j11] \
                                ##                             + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]
                                ##                             - (Begin_Time[i[0], j1] - End_Time[i[0], j])
                                ##                             - (Begin_Time[i[0], j11] - End_Time[i[0], j1])))
                                ##      else:
                                ##           for i in SETUP_MACHINES:
                                ##            if i[0] in non_overlapping_machines:
                                ##              for j in xrange(seq_size[i[0]]):
                                ##                for j1 in xrange(seq_size[i[0]]):
                                ##                  for j2 in xrange(seq_size[i[0]]):
                                ##                    if j2==j1+1 and j1==j+1:
                                ##                      if (i[0],j) in valid_comb and (i[0],j2) in valid_comb and (i[0],j1) in valid_shutdown_comb and (int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                        if Begin_Time[i[0],j1] - End_Time[i[0],j] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j],End_Time[i[0],j] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))
                                ##                        elif Begin_Time[i[0],j2] - End_Time[i[0],j1] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j1],End_Time[i[0],j1] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))

                                ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                                ##                    for j11 in range(seq_size[i[0]]):
                                ##                      if int(j1) == int(j) + 1 and int(j11) == int(j1) + 1 and int(j2) == int(j11) + 1:
                                ##                        if (i[0], j) in valid_comb and (i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (i[0], j11) in valid_shutdown_comb and (int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                          if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                ##                                            End_Time[i[0], j] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##                          elif Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                             SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                ##                                            End_Time[i[0], j1] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##                          elif Begin_Time[i[0], j2] - End_Time[i[0], j11] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11],
                                ##                                            End_Time[i[0], j11] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))

    currenttime = datetime.datetime.now()
    print "Step43:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Processing_Time2 = {}
    Utilization2 = {}
    for i in ip_data.all_machines:
        if i[0] in ps.non_overlapping_machines:
            k1 = 0
            k2 = 0
            Duration = 0
            if ip_data.PlanningBucket == "Seconds":
                Duration = int(ip_data.PlanningHorizon) / 86400
            elif ip_data.PlanningBucket == "Minutes":
                Duration = int(ip_data.PlanningHorizon) / 1440
            elif ip_data.PlanningBucket == "Hours":
                Duration = int(ip_data.PlanningHorizon) / 24

            for i1 in range(0, int(Duration)):
                if ip_data.PlanningBucket == "Seconds":
                    k1 = i1 * 24 * 60 * 60
                    k2 = (i1 + 1) * 24 * 60 * 60
                elif ip_data.PlanningBucket == "Minutes":
                    k1 = i1 * 24 * 60
                    k2 = (i1 + 1) * 24 * 60
                elif ip_data.PlanningBucket == "Hours":
                    k1 = i1 * 24
                    k2 = (i1 + 1) * 24

                temp1 = 0
                temp2 = 0
                temp3 = 0
                temp4 = 0
                for j in range(ps.seq_size[i[0]]):
                    if (int(i[0]), int(j)) in ps.valid_comb:
                        if int(ps.Begin_Time[i[0], j]) >= int(k1) and int(ps.Begin_Time[i[0], j]) <= int(k2) and int(
                                ps.End_Time[i[0], j]) <= int(k2):
                            temp1 = temp1 + ps.End_Time[i[0], j] - ps.Begin_Time[i[0], j]
                        elif ps.Begin_Time[i[0], j] >= k1 and ps.Begin_Time[i[0], j] <= k2 and ps.End_Time[i[0], j] > k2:
                            temp2 = temp2 + k2 - ps.Begin_Time[i[0], j]
                        elif ps.Begin_Time[i[0], j] < k1 and ps.End_Time[i[0], j] >= k1 and ps.End_Time[i[0], j] <= k2:
                            temp3 = temp3 + ps.End_Time[i[0], j] - k1
                        elif ps.Begin_Time[i[0], j] < k1 and ps.End_Time[i[0], j] > k2:
                            temp4 = temp4 + k2 - k1

                if ip_data.Sequence_Dependent_Setup_Time == "Include":
                    for (p, q, r, s, t) in SDST_report:
                        if p == i[0]:
                            if int(s) >= int(k1) and int(s) <= int(k2) and int(t) <= int(k2):
                                temp1 = temp1 + t - s
                            elif s >= k1 and s <= k2 and t > k2:
                                temp2 = temp2 + k2 - s
                            elif s < k1 and t >= k1 and t <= k2:
                                temp3 = temp3 + t - k1
                            elif s < k1 and t > k2:
                                temp4 = temp4 + k2 - k1

                Processing_Time2[i[0], i1] = temp1 + temp2 + temp3 + temp4
                Utilization2[i[0], i1] = float(Processing_Time2[i[0], i1]) * 100 / ps.Total_Shift_Timings[i[0]]
                ##          print "PT: ", i[0], Processing_Time2[i[0],i1], Total_Shift_Timings[i[0]]


                ##    print " ----------------- Material_Profile -------------------------- "
                ##    Material_Status=[]
                ##    TH  = int(PlanningHorizon)
                ##    print Material_Profile
                ##    for i in range(0, TH):
                ##        for (p1,t1) in Material_Profile_Comb:
                ##           if t1 == i:
                ##               print p1, t1, Material_Profile[(p1,t1)]
                ##               Material_Status.append((p1,t1,Material_Profile[(p1,t1)]))
                ##
                ##    for (i,j,b,e) in valid_tasks_sorted_new:
                ##        if i == 0:
                ##            print "PO, Product, Begin, End: ", PO[(i,j)],getProductID(PO[(i,j)]), b, e
                ##
                ##    for (Client_ID, Scenario_ID, PlantID, i, j, m,s, ip, op, ip_qty1,ip_qty2, op_qty,setup, proc, td) in params19:
                ##        if i==0:
                ##            print "iq_qty: ", i, j, m, s, ip, op, ip_qty1, op_qty,
                ##
                ####    print Material_Status

    params05 = [(ip_data.Client_ID, ip_data.Scenario_ID, i, j, ip, op, ps.Dom_IP[(i, j, ip)], ps.Dom_OP[(i, j, op)]) for
                (i, j, ar, alt, ip, op) in ip_data.PO_BOM_COMB if i in ps.current_pos]

    sql05 = "insert into XLPS_SFG_QTY(CLIENT_ID, Scenario_ID, PRODUCTION_ORDER_INDEX, OPERATION_INDEX, INPUT_PRODUCT_INDEX, \
               OUTPUT_PRODUCT_INDEX,INPUT_QTY, OUTPUT_QTY) values(%s,%s,%s,%s,%s,%s,%s,%s)"

    params0 = [(ip_data.Client_ID, ip_data.Scenario_ID, i, j, ar, s, alt, ps.fixed_start_time[(i, j, ar, s, alt)], \
                ps.fixed_end_time[(i, j, ar, s, alt)], ps.active_var[(i, j, ar, s, alt)]) \
               for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
               i in ps.current_pos and (i, j, ar, s, alt) in ps.fixed_decisions and ps.active_var[(i, j, ar, s, alt)] == 1]

    sql0 = "insert into XLPS_SCHEDULE_TIMES(CLIENT_ID, Scenario_ID, PRODUCTION_ORDER_INDEX, OPERATION_INDEX,ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, \
               BEGIN_TIME_INDEX, END_TIME_INDEX,ACTIVE_STATUS) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    ##    if all_pos_iteration == 0:
    ##        for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
    ##            if i in current_pos and active_var[(i,j,ar,s,alt)]==1:
    ##              High_Priority_Comb.append((i,j,ar,s,alt))
    ##              High_Priority_Begin_Time[(i,j,ar,s,alt)] = int(fixed_start_time[(i,j,ar,s,alt)])
    ##              High_Priority_End_Time[(i,j,ar,s,alt)] = int(fixed_end_time[(i,j,ar,s,alt)])
    ##              High_Priority_Active_Status[(i,j,ar,s,alt)] = int(active_var[(i,j,ar,s,alt)])
    ##
    ##        print "High_Priority_Comb: ", High_Priority_Comb
    ##        print "High_Priority_Begin_Time: ", High_Priority_Begin_Time
    ##        print "High_Priority_End_Time: ", High_Priority_End_Time
    ##        print "High_Priority_Active_Status: ", High_Priority_Active_Status

    params1 = [
        (ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, ps.PO[i, j], i, b, e, ps.SeqID[i, j], ps.RouteID[i, j], ps.OperID[i, j], ps.ProdID[i, j], 1)
        for (i, j, b, e) in ps.valid_tasks_sorted_new \
        if int(ps.PO[i, j]) in ps.current_pos]

    ##    for (m,i,j) in Invalid_Timings:
    ##        print m,i,j

    params0_0 = [(ip_data.Client_ID, ip_data.Scenario_ID, m, i, j) for (m, i, j) in ps.Invalid_Timings]
    sql0_0 = "insert into xlps_machine_invalid_ranges(CLIENT_ID, Scenario_ID, MACHINE_INDEX, START_TIME_INDEX, END_TIME_INDEX) values(%s,%s,%s,%s,%s)"
    sql0_01 = 'UPDATE  xlps_machine_invalid_ranges sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)
    sql0_02 = "update xlps_machine_invalid_ranges set Start_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ), interval START_TIME_INDEX Hour) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql0_03 = "update xlps_machine_invalid_ranges set Start_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),   interval START_TIME_INDEX Minute) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql0_04 = "update xlps_machine_invalid_ranges   set Start_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),    interval START_TIME_INDEX Second) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql0_05 = "update xlps_machine_invalid_ranges  set End_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),   interval END_TIME_INDEX hour) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql0_06 = "update xlps_machine_invalid_ranges     set End_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),    interval END_TIME_INDEX Minute) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql0_07 = "update xlps_machine_invalid_ranges  set End_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),  interval END_TIME_INDEX Second) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql1 = "insert into SPPS_SCHEDULE_REPORT(CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX, MACHINE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX,SEQUENCE_ID,ROUTING_ID,OPERATION_INDEX,PRODUCT_INDEX,MODIFIED) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    sql9 = 'UPDATE  SPPS_SCHEDULE_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)
    sql9_1 = 'UPDATE  SPPS_SCHEDULE_REPORT sc inner join XLPS_OPERATION_SOLVER_READY msr on msr.OPERATION_INDEX =sc.OPERATION_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.OPERATION_ID=msr.OPERATION_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)
    sql10 = 'UPDATE  SPPS_SCHEDULE_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id  set sc.PRODUCTION_ORDER_ID=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID = ' + str(ip_data.Scenario_ID)
    sql11 = "update SPPS_SCHEDULE_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ), interval BEGIN_TIME_INDEX Hour) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql11_1 = "update SPPS_SCHEDULE_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),   interval BEGIN_TIME_INDEX Minute) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql11_2 = "update SPPS_SCHEDULE_REPORT   set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),    interval BEGIN_TIME_INDEX Second) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql12 = "update SPPS_SCHEDULE_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),   interval END_TIME_INDEX hour) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql12_1 = "update SPPS_SCHEDULE_REPORT     set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),    interval END_TIME_INDEX Minute) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
    sql12_2 = "update SPPS_SCHEDULE_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ),     interval END_TIME_INDEX Second) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)



    sql12_3 = "update SPPS_SCHEDULE_REPORT sr1  inner join  SPPS_SCHEDULE_REPORT sr2 on  sr1.PRODUCTION_ORDER_ID=sr2.PRODUCTION_ORDER_ID and \
        sr1.LOCATION_ID=sr2.LOCATION_ID and sr1.scenario_ID=sr2.scenario_ID and sr1.client_id=sr2.client_id inner join xlps_machine_solver_ready msr \
        on msr.machine_id=sr2.machine_id and msr.client_id=sr2.client_id and msr.scenario_ID=sr2.scenario_ID  inner join xlps_operation_solver_ready osr \
        on msr.operation_id=osr.operation_id and msr.client_id=osr.client_id and msr.scenario_ID=osr.scenario_ID \
       set sr2.END_TIME = sr1.BEGIN_TIME,sr2.END_TIME_INDEX = sr1.BEGIN_TIME_INDEX where sr2.sequence_id=sr1.sequence_id-1 and sr2.routing_id=sr1.routing_id and osr.operation_type=3 and \
       sr1.client_id= " + str(ip_data.Client_ID) + ' and sr1.Scenario_ID= ' + str(ip_data.Scenario_ID)


    cnx1 = mysql.connector.connect(user='root', password='saddlepoint',  host=mp.Host_String,database=mp.DB_String)
    cursor1 = cnx1.cursor()
    active_connection = 1

    cursor1.execute(
        'delete from spps_schedule_report where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID= ' + str(ip_data.Scenario_ID))
    cursor1.execute(
        'delete from spps_schedule_new_report where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID= ' + str(
           ip_data.Scenario_ID))
    cursor1.execute(
        'delete from spps_schedule_qty_report where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID= ' + str(
           ip_data.Scenario_ID))
    cursor1.execute(
        'delete from spps_po_resource_qty_report where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID= ' + str(
           ip_data.Scenario_ID))
    cursor1.execute(
        'delete from spps_setup_report where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID= ' + str(ip_data.Scenario_ID))
    cursor1.execute(
        'Delete from SPPS_MATERIAL_STATUS where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID=' + str(ip_data.Scenario_ID))

    print ' ----------------------------- spps_machine_daily_capacity_report --------------------------------'

    params2 = [(ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, int(m[0]),
                datetime.datetime.strptime(str(ip_data.PlanningStartDate), "%Y-%m-%d") + datetime.timedelta(days=i),
                int(Processing_Time2[m[0], i]), int(Utilization2[m[0], i]))
               for m in ip_data.all_machines if m[0] in ps.non_overlapping_machines for i in range(0, int(Duration)) if
               int(Utilization2[m[0], i]) > 0]

    sql2 = "insert into spps_machine_daily_capacity_report(CLIENT_ID, Scenario_ID, LOCATION_ID, MACHINE_INDEX, DATE_INDEX, PROCESSING_TIME, \
                    UTILIZATION) values(%s,%s,%s,%s,%s,%s,%s)"

    sql141 = "update spps_machine_daily_capacity_report ru inner join  XLPS_MACHINE_SOLVER_READY msr on ru.Machine_Index=msr.Machine_Index and ru.Client_ID=msr.Client_ID \
    and ru.scenario_id=msr.scenario_id  set ru.Machine_ID=msr.Machine_ID where ru.CLIENT_ID= " + str(
        ip_data.Client_ID) + ' and ru.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql141_1 = "delete from spps_machine_daily_capacity_report  where Machine_id='Dummy' and CLIENT_ID= " + str(
        ip_data.Client_ID) + ' and Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql_post1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','657',DATE_INDEX from spps_machine_daily_capacity_report \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post2 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','656',UTILIZATION from spps_machine_daily_capacity_report \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    print ' ----------------------------- SPPS_RESOURCE_UTILIZATION --------------------------------'

    params3 = [(ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, i[0], ps.Up_Time[i[0]], ps.Down_Time[i[0]], ps.Idle_Time[i[0]],
                ps.Processing_Time1[i[0]], ps.Utilization[i[0]]) for i in ip_data.all_machines if i[0] in ps.non_overlapping_machines]
    sql3 = "insert into SPPS_RESOURCE_UTILIZATION(CLIENT_ID, Scenario_ID, LOCATION_ID, MACHINE_INDEX, TOTAL_AVAILABLE_TIME, SHUTDOWN_TIME, IDLE_TIME, PROCESSING_TIME, \
                    UTILIZATION) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    sql14 = "update SPPS_RESOURCE_UTILIZATION ru inner join  XLPS_MACHINE_SOLVER_READY msr on ru.Machine_Index=msr.Machine_Index and ru.Client_ID=msr.Client_ID \
    and ru.scenario_id=msr.scenario_id  set ru.Machine_ID=msr.Machine_ID where ru.CLIENT_ID= " + str(
        ip_data.Client_ID) + ' and ru.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql14_1 = "delete from SPPS_RESOURCE_UTILIZATION  where Machine_id='Dummy' and CLIENT_ID= " + str(
        ip_data.Client_ID) + ' and Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql_post3 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID,LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','631',TOTAL_AVAILABLE_TIME   from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post4 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID,  LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','629',SHUTDOWN_TIME from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post5 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','630',IDLE_TIME   from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post6 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','628',PROCESSING_TIME from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post7 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','632',UTILIZATION from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    print ' ----------------------------- SPPS_MATERIAL_REQUIREMENT --------------------------------'

    params4 = [(ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, int(p), Total_RM_Requirement[p], int(ps.maxrmavailable[p]), Total_RM_Requirement[p]) \
               for p in ip_data.Products if ip_data.Product_Type_ID[p] == 1 and p in ip_data.BOH_Total_Comb]

    sql4 = "insert into SPPS_MATERIAL_REQUIREMENT( CLIENT_ID, Scenario_ID, LOCATION_ID,PRODUCT_INDEX, REQUIREMENT,AVAILABILITY,USED) values(%s,%s,%s,%s,%s,%s,%s)"

    sql5 = 'UPDATE  SPPS_MATERIAL_REQUIREMENT sc inner join XLPS_PRODUCT_MASTER msr on msr.PRODUCT_INDEX =sc.PRODUCT_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.PRODUCT_ID=msr.PRODUCT_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql_post8 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID,  LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL',PRODUCT_ID,'NULL','NULL','NULL','NULL','NULL','640',REQUIREMENT from SPPS_MATERIAL_REQUIREMENT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post9 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID,  LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL',PRODUCT_ID,'NULL','NULL','NULL','NULL','NULL','641',AVAILABILITY from SPPS_MATERIAL_REQUIREMENT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post10 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID,  LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL',PRODUCT_ID,'NULL','NULL','NULL','NULL','NULL','642',USED from SPPS_MATERIAL_REQUIREMENT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    print ' ----------------------------- SPPS_SHORTAGE_REPORT --------------------------------'

    params15 = [(ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, i, 1, ps.getOrderDueDate(i), 0, 0, 0, ps.getOrderPriority(i)) for i in
                ps.current_pos if Del_Status[i] == 0]

    sql15 = "insert into SPPS_SHORTAGE_REPORT( Client_ID,Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX,PRODUCTION_ORDER_ID, ORDER_DUE_DATE_INDEX, \
                DELIVERY_DATE, Status,DELAY, ORDER_PRIORITY) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    params16 = [(ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, i, 1, ps.getOrderDueDate(i), ps.all_ends_var[i], 1,
                 ps.all_ends_var[i] - ps.getOrderDueDate(i), ps.getOrderPriority(i)) for i in ps.current_pos if Del_Status[i] == 1]

    ##    print "params15: ", params15
    ##    print "params16: ", params16
    sql16 = "insert into SPPS_SHORTAGE_REPORT( Client_ID,Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX,PRODUCTION_ORDER_ID, ORDER_DUE_DATE_INDEX, \
                DELIVERY_DATE, Status, DELAY,ORDER_PRIORITY) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    sql17 = "update SPPS_SHORTAGE_REPORT ss inner join  XLPS_PO_SOLVER_READY po on po.Production_Order_INDEX=ss.Production_Order_INDEX and ss.Client_ID=po.Client_ID \
    and po.scenario_id=ss.scenario_id  set ss.Production_Order_ID=po.Production_Order_ID where po.CLIENT_ID= " + str(
        ip_data.Client_ID) + ' and ss.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql25 = "update SPPS_SHORTAGE_REPORT ss inner join  XLPS_PO_SOLVER_READY po on po.Production_Order_INDEX=ss.Production_Order_INDEX and ss.Client_ID=po.Client_ID \
    and po.scenario_id=ss.scenario_id  set ss.ORDER_DUE_DATE=po.ORDER_DUE_DATE where po.CLIENT_ID= " + str(
        ip_data.Client_ID) + ' and ss.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql18 = "update SPPS_SHORTAGE_REPORT set DELIVERY_STATUS=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 \
    and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ), interval DELIVERY_DATE Hour) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql18_1 = "update SPPS_SHORTAGE_REPORT set DELIVERY_STATUS=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 \
    and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ), interval DELIVERY_DATE Minute) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql18_2 = "update SPPS_SHORTAGE_REPORT set DELIVERY_STATUS=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 \
    and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(
       ip_data.Scenario_ID) + " ), interval DELIVERY_DATE Second) where CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql25_1 = "update SPPS_SHORTAGE_REPORT set NON_DELIVERY= CAST(DATE_FORMAT(DELIVERY_STATUS,'%m/%d/%Y %T')  AS CHAR) where status=1 and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)
    sql25_2 = "update SPPS_SHORTAGE_REPORT set NON_DELIVERY= 'Non-Delivery' where status=0 and CLIENT_ID= " + str(
        ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql_post11 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID,  'NULL','NULL',LOCATION_ID,'NULL','NULL',PRODUCTION_ORDER_ID,'NULL','634',NON_DELIVERY from SPPS_SHORTAGE_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post11_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL','NULL',PRODUCTION_ORDER_ID,'NULL','635',Delay from SPPS_SHORTAGE_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    print ' ----------------------------- SPPS_PO_RESOURCE_QTY_REPORT --------------------------------'

    params191 = []
    material_track = []

    for (i1, j1, ar1, s, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
        if i1 in ps.current_pos and int(alt1) <> -1 and ps.active_var[(i1, j1, ar1, s, alt1)] == 1:
            for (i, j, ar, alt, ip, op) in ip_data.PO_BOM_COMB:
                if int(i1) == int(i) and int(j1) == int(j) and int(ar) == int(ar1) and int(alt1) == int(alt):
                    for m in ip_data.all_machines:
                        if int(ip_data.Machine_ID3[i1, j1, ar1, s, alt1]) == int(m[0]) and int(op) == int(ip_data.Product_Index1[i1, j1, ar1, s, alt1]):
                            if ip_data.Op_Type[j1] == 1:
                                params191.append((ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, int(i), int(j), int(m[0]), s, int(ip), int(op), \
                                                  ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                                  ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                                  ps.Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))], \
                                                  ip_data.OP_Setup_Time[i1, j1, ar1, s, alt1],
                                     int(ip_data.Processing_Time1[i1, j1, ar1, s, alt1] * ps.Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))]),
                                                  ip_data.Teardown_Time1[i1, j1, ar1, s, alt1]))
                                material_track.append((int(i), int(m[0]), int(ip), ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))]))
                            if ip_data.Op_Type[j1] == 2:
                                if m[0] in ip_data.Task_Split_Allowed_Machines:
                                    params191.append((ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, int(i), int(j), int(m[0]), s, int(ip), int(op), \
                                                  ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                                  ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                                  ps.Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))], \
                                                  ip_data.OP_Setup_Time1[i1, j1, ar1, s, alt1],
                                                  ps.fixed_end_time[(i1, j1, ar1, s, alt1)]-ps.fixed_start_time[(i1, j1, ar1, s, alt1)] -ip_data.OP_Setup_Time1[i1, j1, ar1, s, alt1],
                                                  ip_data.Teardown_Time1[i1, j1, ar1, s, alt1]))
                                else:
                                    params191.append((ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, int(i), int(j), int(m[0]), s, int(ip), int(op), \
                                                  ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                                  ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                                  ps.Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))], \
                                                  ip_data.OP_Setup_Time1[i1, j1, ar1, s, alt1],ip_data.Processing_Time1[i1, j1, ar1, s, alt1],
                                                  ip_data.Teardown_Time1[i1, j1, ar1, s, alt1]))

                                material_track.append((int(i), int(m[0]), int(ip), ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))]))
                            if ip_data.Op_Type[j1] == 3:
                                params191.append(
                                    (ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, int(i), int(j), int(m[0]), s, int(ip), int(op), \
                                     ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     ps.Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))], \
                                     0, ps.fixed_end_time[(i, j, ar, s, alt)] - ps.fixed_start_time[(i, j, ar, s, alt)], 0))
                                material_track.append((int(i), int(m[0]), int(ip), ps.Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))]))

    params19 = params191
    print "params19: ", params19

    sql19 = "insert into SPPS_PO_RESOURCE_QTY_REPORT(CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX, OPERATION_INDEX, MACHINE_INDEX,SEQUENCE_ID, INPUT_PRODUCT_INDEX, \
            OUTPUT_PRODUCT_INDEX, INPUT_PRODUCT_QUANTITY, INV_PRODUCT_QUANTITY, OUTPUT_PRODUCT_QUANTITY, SETUP_TIME, PROCESSING_TIME,TEARDOWN_TIME) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    sql20 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_PRODUCT_MASTER msr on msr.PRODUCT_INDEX =sc.INPUT_PRODUCT_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.INPUT_PRODUCT=msr.PRODUCT_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql21 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_PRODUCT_MASTER msr on msr.PRODUCT_INDEX =sc.OUTPUT_PRODUCT_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.OUTPUT_PRODUCT=msr.PRODUCT_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql22 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql23 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.PRODUCTION_ORDER_ID=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql24 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_OPERATION_SOLVER_READY msr on msr.OPERATION_INDEX =sc.OPERATION_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.OPERATION_ID=msr.OPERATION_ID where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)

    print " ----------------- Material_Profile -------------------------- "

    currenttime = datetime.datetime.now()
    print "Step51:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Material_Scheduled = {}
    Material_Scheduled_Comb = []
    Material_Scheduled_Comb1 = []
    Material_Profile_Comb2 = []
    Material_Status = []
    Material_Status1 = []
    # for p in Products:
    #     if Product_Type_ID[int(p)] == 1:
    #         for (m, seq, b, e) in valid_tasks_sorted_new:
    #             for (po, mac, ip, ip_qty) in material_track:
    #                 if p == ip and m == mac and po == PO[(m, seq)] and ip_qty > 0:
    #                     Material_Scheduled_Comb.append((p, b, -ip_qty))

    for p in ip_data.Products:
        if ip_data.Product_Type_ID[int(p)] == 1:
            for (po, mac, ip, ip_qty) in material_track:
                if p == ip and ip_qty > 0:
                    for (m, seq, b, e) in ps.valid_tasks_sorted_new:
                        if m == mac and po == ps.PO[(m, seq)]:
                            Material_Scheduled_Comb.append((p, b, -ip_qty))

    currenttime = datetime.datetime.now()
    print "Step52:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Material_Scheduled_Comb = sorted(Material_Scheduled_Comb)
    Material_Scheduled_Comb1 = [(int(key[0]), int(key[1]), int(sum(k for (i, j, k) in group))) for key, group in groupby(Material_Scheduled_Comb, key=lambda x: (x[0], x[1]))]
    Material_Profile_Comb1 = sorted(ip_data.Material_Profile_Comb)
    Material_Profile_Comb2 = [(int(key[0]), int(key[1]), int(sum(k for (i, j, k) in group))) for key, group in
                              groupby(Material_Profile_Comb1, key=lambda x: (x[0], x[1]))]
    Material_Status = Material_Profile_Comb2 + Material_Scheduled_Comb1
    Material_Status = sorted(Material_Status)
    Material_Status1 = [(int(key[0]), int(key[1]), int(sum(k for (i, j, k) in group))) for key, group in
                        groupby(Material_Status, key=lambda x: (x[0], x[1]))]

    currenttime = datetime.datetime.now()
    print "Step53:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    to_plot_x = {}
    to_plot_y = {}
    Material_Status2 = []
    products = set([Material_Status1[i][0] for i in range(len(Material_Status1))])
    for p in products:
        to_plot_x[p] = [Material_Status1[i][1] for i in range(len(Material_Status1)) if Material_Status1[i][0] == p]
        to_plot_y[p] = [Material_Status1[i][2] for i in range(len(Material_Status1)) if Material_Status1[i][0] == p]
        to_plot_y[p] = np.cumsum(to_plot_y[p])
        Material_Status2 = Material_Status2 + list((np.column_stack(([Material_Status1[i][0] for i in
                                                                      range(len(Material_Status1)) if
                                                                      Material_Status1[i][0] == p], to_plot_x[p],
                                                                     to_plot_y[p])).tolist()))

    ##    for p in products:
    ##        plt.step(to_plot_x[p],to_plot_y[p])
    ##        plt.hold()
    ##    plt.show()

    ##    print "Material_Profile_Comb2: ", Material_Profile_Comb2
    ##    print "Material_Scheduled_Comb1: ", Material_Scheduled_Comb1
    ##    print "Material_Status2: ", Material_Status2


    currenttime = datetime.datetime.now()
    print "Step54:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    MS = []

    for i in range(len(Material_Status2)):
        MS.append((int(Material_Status2[i][0]), int(Material_Status2[i][1]), int(Material_Status2[i][2]), 0, 0))
    for j in range(len(Material_Profile_Comb2)):
        MS.append((int(Material_Profile_Comb2[j][0]), int(Material_Profile_Comb2[j][1]), 0,
                   int(Material_Profile_Comb2[j][2]), 0))
    for k in range(len(Material_Scheduled_Comb1)):
        MS.append((int(Material_Scheduled_Comb1[k][0]), int(Material_Scheduled_Comb1[k][1]), 0, 0,
                   int(Material_Scheduled_Comb1[k][2])))

    MS1 = sorted(set(MS))

    currenttime = datetime.datetime.now()
    print "Step55:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    MS2 = []

    for key, group in groupby(MS1, lambda k: (k[0], k[1])):
        group = list(group)
        MS2.append(
            (key[0], key[1], sum(x[2] for x in group), sum(x[3] for x in group), sum(x[4] for x in group) * (-1)))
    ##    print "Cum MS2: ", MS2

    MS2 = sorted(MS2, key=lambda x: (x[0], x[1]))

    currenttime = datetime.datetime.now()
    print "Step56:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    params41 = [(ip_data.Client_ID, ip_data.Scenario_ID, ip_data.PlantID, int(MS2[i][0]), int(MS2[i][1]), int(MS2[i][2]), int(MS2[i][3]),
                 int(MS2[i][4])) for i in range(len(MS2))]

    ##    print "params41 : ", params41
    sql41 = "insert into SPPS_MATERIAL_STATUS(CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCT_INDEX, TIME_INDEX, CUM_QTY, Received_Qty, Consumed_Qty) \
                values(%s,%s,%s,%s,%s,%s,%s,%s)"

    sql42 = "update SPPS_MATERIAL_STATUS sm inner join xlps_product_master pm on pm.product_index=sm.product_index and sm.scenario_id=pm.scenario_id \
    and sm.client_id=pm.client_id set sm.product_id = pm.product_id where sm.client_id= " + str(
        ip_data.Client_ID) + " and sm.Scenario_ID= " + str(ip_data.Scenario_ID)

    sql43_1 = "update SPPS_MATERIAL_STATUS set TIME_DATE=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 \
    and parameter_id=601 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID) + " ), interval TIME_INDEX Hour) where \
    CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql43_2 = "update SPPS_MATERIAL_STATUS set TIME_DATE=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 \
    and parameter_id=601 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID) + " ), interval TIME_INDEX Minute) where \
    CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql43_3 = "update SPPS_MATERIAL_STATUS set TIME_DATE=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 \
    and parameter_id=601 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID) + " ), interval TIME_INDEX Second) where \
    CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

    sql_post41 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(ip_data.PlantID) + "','NULL','NULL','NULL','NULL','688',TIME_DATE from SPPS_MATERIAL_STATUS \
    where time_index < " + str(ip_data.PlanningHorizon) + " and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(
       ip_data.Scenario_ID)

    sql_post42 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(ip_data.PlantID) + "','NULL','NULL','NULL','NULL','689',Received_Qty from SPPS_MATERIAL_STATUS \
    where time_index < " + str(ip_data.PlanningHorizon) + " and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(
       ip_data.Scenario_ID)

    sql_post43 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(ip_data.PlantID) + "','NULL','NULL','NULL','NULL','690',Consumed_Qty from SPPS_MATERIAL_STATUS \
    where time_index < " + str(ip_data.PlanningHorizon) + " and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(
       ip_data.Scenario_ID)

    sql_post44 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(ip_data.PlantID) + "','NULL','NULL','NULL','NULL','691',CUM_QTY from SPPS_MATERIAL_STATUS \
    where time_index < " + str(ip_data.PlanningHorizon) + " and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(
       ip_data.Scenario_ID)

    print ' ----------------------------- SPPS_SCHEDULE_QTY_REPORT --------------------------------'

    sql26 = "insert into SPPS_SCHEDULE_QTY_REPORT (CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME, \
      INPUT_PRODUCT, INPUT_PRODUCT_QUANTITY,INV_PRODUCT_QUANTITY,OUTPUT_PRODUCT, OUTPUT_PRODUCT_QUANTITY,OPERATION_ID) \
    select distinct po.Client_ID, po.Scenario_ID, sr.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id, sr.SEQUENCE_ID, BEGIN_TIME, \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME SECOND), \
    INPUT_PRODUCT,INPUT_PRODUCT_Quantity,\
    INV_PRODUCT_Quantity,OUTPUT_PRODUCT,OUTPUT_PRODUCT_Quantity,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join spps_po_resource_qty_report po on sr.production_order_index=\
    po.production_order_index and sr.machine_index=po.machine_index and sr.SEQUENCE_ID=po.SEQUENCE_ID and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(ip_data.Client_ID) + " and sr.Scenario_ID=" + str(
       ip_data.Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql26_1 = "insert into SPPS_SCHEDULE_QTY_REPORT (CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME, TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME, \
      INPUT_PRODUCT, INPUT_PRODUCT_QUANTITY,INV_PRODUCT_QUANTITY,OUTPUT_PRODUCT, OUTPUT_PRODUCT_QUANTITY,OPERATION_ID) \
    select distinct po.Client_ID, po.Scenario_ID, sr.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id, sr.SEQUENCE_ID, BEGIN_TIME, \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME MINUTE), \
    INPUT_PRODUCT,INPUT_PRODUCT_Quantity,\
    INV_PRODUCT_Quantity,OUTPUT_PRODUCT,OUTPUT_PRODUCT_Quantity,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join spps_po_resource_qty_report po on sr.production_order_index=\
    po.production_order_index and sr.machine_index=po.machine_index and sr.SEQUENCE_ID=po.SEQUENCE_ID and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(ip_data.Client_ID) + " and sr.Scenario_ID=" + str(
       ip_data.Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql26_2 = "insert into SPPS_SCHEDULE_QTY_REPORT (CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME, TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME, \
      INPUT_PRODUCT, INPUT_PRODUCT_QUANTITY,INV_PRODUCT_QUANTITY,OUTPUT_PRODUCT, OUTPUT_PRODUCT_QUANTITY,OPERATION_ID) \
    select distinct po.Client_ID, po.Scenario_ID, sr.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id, sr.SEQUENCE_ID, BEGIN_TIME, DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME HOUR), \
    INPUT_PRODUCT,INPUT_PRODUCT_Quantity,\
    INV_PRODUCT_Quantity,OUTPUT_PRODUCT,OUTPUT_PRODUCT_Quantity,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join spps_po_resource_qty_report po on sr.production_order_index=\
    po.production_order_index and sr.machine_index=po.machine_index and sr.SEQUENCE_ID=po.SEQUENCE_ID and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(ip_data.Client_ID) + " and sr.Scenario_ID=" + str(
       ip_data.Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql26_3 = "update SPPS_SCHEDULE_QTY_REPORT sr1 inner join  SPPS_SCHEDULE_QTY_REPORT sr2 on  sr1.PRODUCTION_ORDER_ID=sr2.PRODUCTION_ORDER_ID \
    and sr1.LOCATION_ID=sr2.LOCATION_ID and sr1.scenario_ID=sr2.scenario_ID and sr1.client_id=sr2.client_id \
    inner join xlps_machine_solver_ready msr on msr.machine_id=sr2.machine_id and msr.client_id=sr2.client_id and msr.scenario_ID=sr2.scenario_ID  \
   inner join xlps_operation_solver_ready osr on msr.operation_id=osr.operation_id and msr.client_id=osr.client_id and msr.scenario_ID=osr.scenario_ID  \
    set sr2.PROCESSING_END_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_BEGIN_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_END_TIME = sr1.SETUP_BEGIN_TIME \
   where sr2.SEQUENCE_ID=sr1.SEQUENCE_ID-1 and osr.operation_type=3 and  sr1.scenario_ID= " + str(
       ip_data.Scenario_ID) + " and sr1.client_id= " + str(ip_data.Client_ID);


    sql26_4 = "update SPPS_SCHEDULE_QTY_REPORT sn inner join    \
        xlps_machine_solver_ready msr on sn.MACHINE_ID=msr.MACHINE_ID and sn.client_id=msr.client_id and sn.scenario_id=msr.scenario_id \
        set sn.machine_name = msr.MACHINE_NAME where sn.scenario_ID=" + str(ip_data.Scenario_ID) + "  and sn.client_id= " + str(ip_data.Client_ID);


    sql26_5 = "update SPPS_SCHEDULE_QTY_REPORT sn inner join xlps_po_solver_ready po on sn.PRODUCTION_ORDER_ID=po.PRODUCTION_ORDER_ID \
              and po.client_id=sn.client_id and sn.scenario_id=po.scenario_id inner join xlps_product_master pm on pm.PRODUCT_INDEX=po.PRODUCT_ID \
            and pm.client_id=sn.client_id and sn.scenario_id=pm.scenario_id set sn.product_name = pm.product_name \
            where sn.scenario_ID=" + str(ip_data.Scenario_ID) + "    and sn.client_id= " + str(ip_data.Client_ID);

    sql_post13 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','612',SETUP_BEGIN_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post14 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','613',SETUP_END_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post15 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','658',PROCESSING_BEGIN_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post16 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','659',PROCESSING_END_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post16_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','665',TEARDOWN_BEGIN_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post16_2 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','666',TEARDOWN_END_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post17 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','645',INPUT_PRODUCT from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post18 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','643',INPUT_PRODUCT_QUANTITY from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post18_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  sp.Client_ID, sp.Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','643',INPUT_PRODUCT_QUANTITY \
              from SPPS_PO_RESOURCE_QTY_REPORT sp inner join xlps_operation_solver_ready osr on osr.operation_id=sp.operation_id and \
              osr.client_id=sp.client_id and osr.scenario_id=sp.scenario_id where OPERATION_TYPE=3 and sp.CLIENT_ID= " + str(
        ip_data.Client_ID) + " and sp.Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post19 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','646',OUTPUT_PRODUCT from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post20 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','644',OUTPUT_PRODUCT_QUANTITY from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post20_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  sp.Client_ID, sp.Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','644',OUTPUT_PRODUCT_QUANTITY  \
              from SPPS_PO_RESOURCE_QTY_REPORT sp inner join xlps_operation_solver_ready osr on osr.operation_id=sp.operation_id and \
              osr.client_id=sp.client_id and osr.scenario_id=sp.scenario_id where OPERATION_TYPE=3 and sp.CLIENT_ID= " + str(
        ip_data.Client_ID) + " and sp.Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post21 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','664',INV_PRODUCT_QUANTITY from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post22 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','647',SEQUENCE_ID from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post22_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
        SP_DF_ID,DF_VALUE) \
        select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','693',MACHINE_NAME from SPPS_SCHEDULE_QTY_REPORT \
        where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post22_2 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
        SP_DF_ID,DF_VALUE) \
        select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','694',PRODUCT_NAME from SPPS_SCHEDULE_QTY_REPORT \
        where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post22_3 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
        SP_DF_ID,DF_VALUE) \
        select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','693',MACHINE_NAME from SPPS_SCHEDULE_NEW_REPORT \
        where  (location_from_ID, machine_id, production_order_id) not in (select distinct location_ID, machine_id, production_order_id from spps_schedule_qty_report) and  CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post22_4 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
        SP_DF_ID,DF_VALUE) \
        select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','694',PRODUCT_NAME from SPPS_SCHEDULE_NEW_REPORT \
        where (location_from_ID, machine_id, production_order_id) not in (select distinct location_ID, machine_id, production_order_id from spps_schedule_qty_report) and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post22_5 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
        SP_DF_ID,DF_VALUE) \
        select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','685',OPERATION_ID from SPPS_SCHEDULE_NEW_REPORT \
        where (location_from_ID, machine_id, production_order_id) not in (select distinct location_ID, machine_id, production_order_id from spps_schedule_qty_report) and  CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    print ' ----------------------------- SPPS_SCHEDULE_NEW_REPORT --------------------------------'

    sql27 = "insert into SPPS_SCHEDULE_NEW_REPORT (CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME,ROUTING_ID,OPERATION_ID) \
        select distinct po.Client_ID, po.Scenario_ID, SR.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id,sr.SEQUENCE_ID, BEGIN_TIME, \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND),\
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME SECOND),ROUTING_ID,OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join \
    spps_po_resource_qty_report po on sr.production_order_index= \
    po.production_order_index and sr.machine_index=po.machine_index and sr.operation_index= po.operation_index and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(ip_data.Client_ID) + " and sr.Scenario_ID=" + str(
       ip_data.Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql27_1 = 'insert into SPPS_SCHEDULE_NEW_REPORT (CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME,ROUTING_ID,OPERATION_ID) \
        select distinct po.Client_ID, po.Scenario_ID,SR.LOCATION_ID,po.PRODUCTION_ORDER_ID, po.machine_id,sr.SEQUENCE_ID, BEGIN_TIME, \
        DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE),\
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME MINUTE),ROUTING_ID,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join \
    spps_po_resource_qty_report po on sr.production_order_index= \
    po.production_order_index and sr.machine_index=po.machine_index  and sr.operation_index= po.operation_index and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!="Non-Delivery" and \
    sr.client_id=' + str(ip_data.Client_ID) + ' and sr.Scenario_ID=' + str(
       ip_data.Scenario_ID) + ' order by po.production_order_ID, po.Sequence_ID'

    sql27_2 = "insert into SPPS_SCHEDULE_NEW_REPORT (CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME,ROUTING_ID,OPERATION_ID) \
        select distinct po.Client_ID, po.Scenario_ID, SR.LOCATION_ID,po.PRODUCTION_ORDER_ID, po.machine_id,sr.SEQUENCE_ID, BEGIN_TIME, \
        DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR),\
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME HOUR),ROUTING_ID,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join \
    spps_po_resource_qty_report po on sr.production_order_index= \
    po.production_order_index and sr.machine_index=po.machine_index  and sr.operation_index= po.operation_index and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(ip_data.Client_ID) + " and sr.Scenario_ID=" + str(
       ip_data.Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql27_3 = "insert into spps_schedule_new_report(Client_ID, Scenario_ID,LOCATION_FROM_ID, PRODUCTION_ORDER_ID,mACHINE_ID,sequence_id,machine_group,routing_id, operation_id) \
      select  distinct po.Client_ID, po.Scenario_ID,'" + str(ip_data.PlantID) + "', po.PRODUCTION_ORDER_ID,msr.mACHINE_ID,rsr.sequence_id,999,rsr.routing_id,osr.OPERATION_ID from xlps_route_solver_ready_new rsr \
      inner join xlps_po_solver_ready po on po.client_id=rsr.client_id and po.production_order_index=rsr.production_order_index and po.scenario_ID=rsr.scenario_ID  \
      inner join xlps_machine_solver_ready msr on msr.MACHINE_Index=rsr.MACHINE_INDEX  and po.scenario_ID=msr.scenario_ID and po.client_id=msr.client_id \
    inner join xlps_holding_time ht on po.production_order_index=ht.production_order_index and rsr.alternate_index=ht.MACHINE_INDEX \
    and ht.operation_index=rsr.OPERATION_INDEX and po.scenario_ID=ht.scenario_ID and po.client_id=ht.client_id \
    inner join xlps_operation_solver_ready osr on osr.OPERATION_INDEX=ht.operation_index and po.scenario_ID=osr.scenario_ID and po.client_id=osr.client_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=po.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
     inner join spps_schedule_report ssr1 on ssr1.production_order_id=po.production_order_id and ssr1.client_id=po.client_id and ssr1.scenario_id=po.scenario_id \
    and ssr1.routing_id=rsr.routing_id where ssr.Non_Delivery!='Non-Delivery' and \
    po.client_ID=" + str(ip_data.Client_ID) + " and po.scenario_ID=" + str(ip_data.Scenario_ID);

    sql27_4 = "update spps_schedule_new_report sn \
    inner join spps_schedule_new_report sn1 on sn.PRODUCTION_ORDER_ID=sn1.PRODUCTION_ORDER_ID and sn.scenario_ID=sn1.scenario_ID and sn.client_id=sn1.client_id  \
    set sn.SETUP_BEGIN_TIME=sn1.TEARDOWN_END_TIME,sn.SETUP_END_TIME=sn1.TEARDOWN_END_TIME,sn.PROCESSING_BEGIN_TIME=sn1.TEARDOWN_END_TIME \
    where sn.SEQUENCE_ID=sn1.SEQUENCE_ID+1 and sn.machine_group=999 and sn.scenario_ID=" + str(
       ip_data.Scenario_ID) + " and sn.client_id=" + str(ip_data.Client_ID);

    # sql27_5 = "update spps_schedule_new_report sn \
    # inner join spps_schedule_new_report sn1 on sn.PRODUCTION_ORDER_ID=sn1.PRODUCTION_ORDER_ID and sn.scenario_ID=sn1.scenario_ID and sn.client_id=sn1.client_id  \
    # set sn.PROCESSING_END_TIME=sn1.SETUP_BEGIN_TIME,sn.TEARDOWN_BEGIN_TIME=sn1.SETUP_BEGIN_TIME,sn.TEARDOWN_END_TIME=sn1.SETUP_BEGIN_TIME \
    # where sn.SEQUENCE_ID=sn1.SEQUENCE_ID-1 and sn.machine_group=999 and sn.scenario_ID=" + str(
    #    ip_data.Scenario_ID) + " and sn.client_id=" + str(ip_data.Client_ID);
    #
    # sql27_6 = "update SPPS_SCHEDULE_NEW_REPORT sr1 inner join  SPPS_SCHEDULE_NEW_REPORT sr2 on  sr1.PRODUCTION_ORDER_ID=sr2.PRODUCTION_ORDER_ID \
    #  and sr1.LOCATION_FROM_ID=sr2.LOCATION_FROM_ID and sr1.scenario_ID=sr2.scenario_ID and sr1.client_id=sr2.client_id \
    #  inner join xlps_machine_solver_ready msr on msr.machine_id=sr2.machine_id and msr.client_id=sr2.client_id and msr.scenario_ID=sr2.scenario_ID  \
    # inner join xlps_operation_solver_ready osr on msr.operation_id=osr.operation_id and msr.client_id=osr.client_id and msr.scenario_ID=osr.scenario_ID  \
    #  set sr2.PROCESSING_END_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_BEGIN_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_END_TIME = sr1.SETUP_BEGIN_TIME \
    # where sr2.SEQUENCE_ID=sr1.SEQUENCE_ID-1 and osr.operation_type=3 and  sr1.scenario_ID= " + str(
    #     ip_data.Scenario_ID) + " and sr1.client_id= " + str(ip_data.Client_ID);

    # the above two queries is not working when the holding op. (Formulation-QC in Hetero) is the last operation of the blend lot
    # So, below query will update the processing time with max. holding time  given
    sql27_5 = "update spps_schedule_new_report sr inner join xlps_po_solver_ready po on po.production_order_id=sr.production_order_id and po.client_id=sr.client_id and  \
                 po.scenario_id=sr.scenario_id  inner join xlps_machine_solver_ready msr on msr.machine_id=sr.machine_id and msr.client_id=sr.client_id and msr.scenario_ID=sr.scenario_ID \
              inner join xlps_operation_solver_ready osr on msr.operation_id=osr.operation_id and msr.client_id=osr.client_id and msr.scenario_ID=osr.scenario_ID   \
            inner join xlps_holding_time ht on po.production_order_index=ht.production_order_index and  \
                ht.OPERATION_INDEX=osr.OPERATION_INDEX  and msr.client_id=ht.client_id and msr.scenario_ID=ht.scenario_ID   \
            set sr.PROCESSING_END_TIME= DATE_ADD(sr.PROCESSING_BEGIN_TIME, INTERVAL ht.MAX_HOLDING_TIME MINUTE), \
            sr.TEARDOWN_BEGIN_TIME= DATE_ADD(sr.PROCESSING_BEGIN_TIME, INTERVAL ht.MAX_HOLDING_TIME MINUTE),  \
            sr.TEARDOWN_END_TIME	=DATE_ADD(sr.PROCESSING_BEGIN_TIME, INTERVAL ht.MAX_HOLDING_TIME MINUTE) \
            where sr.scenario_ID=" + str( ip_data.Scenario_ID) + " and sr.client_id=" + str(ip_data.Client_ID);




    sql28 = 'UPDATE  SPPS_SCHEDULE_NEW_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_ID =sc.MACHINE_ID and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id inner join XLPS_OPERATION_SOLVER_READY osr on osr.OPERATION_ID =msr.OPERATION_ID and msr.CLIENT_ID=sc.CLIENT_ID \
           and osr.scenario_id=msr.scenario_id set sc.MACHINE_GROUP=osr.OPERATION_INDEX  where msr.CLIENT_ID = ' + str(
        ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)

    sql27_7 = "update SPPS_SCHEDULE_NEW_REPORT sn inner join spps_relation sr on sn.MACHINE_ID=sr.RESOURCE_ID and sn.client_id=sr.client_id \
        inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.CLIENT_ID and sn.scenario_id=df.scenario_id \
        set sn.machine_name = df.df_value where df.df_id=2061 and sn.scenario_ID=" + str(ip_data.Scenario_ID) + "  and sn.client_id= " + str(ip_data.Client_ID);


    sql27_8 = "update SPPS_SCHEDULE_NEW_REPORT sn inner join xlps_po_solver_ready po on sn.PRODUCTION_ORDER_ID=po.PRODUCTION_ORDER_ID \
                and po.client_id=sn.client_id and sn.scenario_id=po.scenario_id \
              inner join xlps_product_master pm on pm.PRODUCT_INDEX=po.PRODUCT_ID and pm.client_id=sn.client_id and sn.scenario_id=pm.scenario_id  \
                set sn.product_name = pm.PRODUCT_NAME where sn.scenario_ID=" + str(ip_data.Scenario_ID) + "    and sn.client_id= " + str(ip_data.Client_ID);

    ##    sql27_7 = "update spps_schedule_new_report sn inner join xlps_machine_solver_ready msr on sn.machine_id=msr.machine_id and sn.client_id=msr.client_id \
    ##    inner join xlps_route_solver_ready rsr on rsr.machine_index=msr.machine_index and rsr.routing_id=sn.routing_id and rsr.sequence_id=sn.sequence_id \
    ##    and rsr.client_id=sn.client_id and rsr.scenario_id=sn.scenario_id \
    ##    and sn.scenario_id=msr.scenario_id set sn.operation_id=msr.operation_id where sn.client_id= " + str(ip_data.Client_ID) + " and sn.scenario_id= " + str(ip_data.Scenario_ID);


    sql_post12 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','685',OPERATION_ID from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)



    sql_post12_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','686',ROUTING_ID from SPPS_SCHEDULE_NEW_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)


    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        params30 = [(ip_data.Client_ID, ip_data.Scenario_ID, int(m), int(po1), int(po2), int(end_time), int(start_time)) for
                    (m, po1, po2, end_time, start_time) in SDST_report \
                    if int(end_time) < int(ip_data.PlanningHorizon)]
        sql30 = "insert into SPPS_SETUP_REPORT(CLIENT_ID, Scenario_ID, MACHINE_INDEX, PRODUCTION_ORDER_INDEX_FROM,PRODUCTION_ORDER_INDEX_TO, \
                                      END_TIME_INDEX, BEGIN_TIME_INDEX) values(%s,%s,%s,%s,%s,%s,%s)"
        sql31 = 'UPDATE  SPPS_SETUP_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
            ip_data.Client_ID) + ' and sc.Scenario_ID= ' + str(ip_data.Scenario_ID)
        sql32 = 'UPDATE  SPPS_SETUP_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX_FROM and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id  set sc.PRODUCTION_ORDER_ID_FROM=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
            ip_data.Client_ID) + ' and sc.Scenario_ID = ' + str(ip_data.Scenario_ID)
        sql33 = 'UPDATE  SPPS_SETUP_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX_TO and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id  set sc.PRODUCTION_ORDER_ID_TO=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
            ip_data.Client_ID) + ' and sc.Scenario_ID = ' + str(ip_data.Scenario_ID)
        sql34 = "update SPPS_SETUP_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(
           ip_data.Scenario_ID) + " ), interval BEGIN_TIME_INDEX Hour) where CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
        sql34_1 = "update SPPS_SETUP_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(
           ip_data.Scenario_ID) + " ),   interval BEGIN_TIME_INDEX Minute) where CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
        sql34_2 = "update SPPS_SETUP_REPORT   set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(
           ip_data.Scenario_ID) + " ),    interval BEGIN_TIME_INDEX Second) where CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
        sql35 = "update SPPS_SETUP_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(
           ip_data.Scenario_ID) + " ),   interval END_TIME_INDEX hour) where CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
        sql35_1 = "update SPPS_SETUP_REPORT     set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(
           ip_data.Scenario_ID) + " ),    interval END_TIME_INDEX Minute) where CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)
        sql35_2 = "update SPPS_SETUP_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(
           ip_data.Scenario_ID) + " ),     interval END_TIME_INDEX Second) where CLIENT_ID= " + str(
            ip_data.Client_ID) + " and Scenario_ID=" + str(ip_data.Scenario_ID)

        sql_post23 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','681',PRODUCTION_ORDER_ID_FROM from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

        sql_post24 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','682',PRODUCTION_ORDER_ID_TO from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

        sql_post25 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','683',END_TIME from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

        sql_post26 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','684',BEGIN_TIME from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post99 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','665',TEARDOWN_BEGIN_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post100 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','666',TEARDOWN_END_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post101 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','612',SETUP_BEGIN_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post102 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','613',SETUP_END_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post103 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','658',PROCESSING_BEGIN_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post104 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','659',PROCESSING_END_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post105 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','647',SEQUENCE_ID from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(ip_data.Client_ID) + " and Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post106 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select distinct po.Client_ID, po.Scenario_ID, 'NULL','NULL',sr.LOCATION_FROM_ID,'NULL',sr.MACHINE_ID,sr.PRODUCTION_ORDER_ID,'NULL','645',pm.PRODUCT_ID from SPPS_SCHEDULE_NEW_REPORT sr \
              inner join xlps_machine_solver_ready msr on msr.machine_id=sr.machine_id and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
              inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_ID=sr.PRODUCTION_ORDER_ID and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
	      inner join xlps_route_solver_ready rsr on po.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and msr.machine_index=rsr.machine_index and rsr.client_id=sr.client_id and rsr.scenario_id=sr.scenario_id \
              inner join xlps_bom bm on bm.alternate_index=rsr.alternate_index and bm.operation_index=rsr.operation_index \
              and rsr.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.client_id=rsr.client_id and bm.scenario_id=rsr.scenario_id \
              inner join xlps_product_master pm on pm.product_index=bm.input_product_index and pm.client_id=bm.client_id and pm.scenario_id=bm.scenario_id \
              inner join xlps_po_products pop on pop.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pop.PRODUCT_ID=pm.PRODUCT_ID \
              and pop.client_id=bm.client_id and pop.scenario_id=bm.scenario_id \
              inner join spps_shortage_report ssr on ssr.production_order_id=po.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
              where ssr.Non_Delivery!='Non-Delivery' and \
              machine_group=999 and po.CLIENT_ID= " + str(ip_data.Client_ID) + " and po.Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post107 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select distinct po.Client_ID, po.Scenario_ID, 'NULL','NULL',sr.LOCATION_FROM_ID,'NULL',sr.MACHINE_ID,sr.PRODUCTION_ORDER_ID,'NULL','646',pm.PRODUCT_ID from SPPS_SCHEDULE_NEW_REPORT sr \
              inner join xlps_machine_solver_ready msr on msr.machine_id=sr.machine_id and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
              inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_ID=sr.PRODUCTION_ORDER_ID and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
	      inner join xlps_route_solver_ready rsr on po.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and msr.machine_index=rsr.machine_index and rsr.client_id=sr.client_id and rsr.scenario_id=sr.scenario_id \
              inner join xlps_bom bm on bm.alternate_index=rsr.alternate_index and bm.operation_index=rsr.operation_index \
              and rsr.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.client_id=rsr.client_id and bm.scenario_id=rsr.scenario_id \
              inner join xlps_product_master pm on pm.product_index=bm.output_product_index and pm.client_id=bm.client_id and pm.scenario_id=bm.scenario_id \
              inner join xlps_po_products pop on pop.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pop.PRODUCT_ID=pm.PRODUCT_ID \
              and pop.client_id=bm.client_id and pop.scenario_id=bm.scenario_id \
              inner join spps_shortage_report ssr on ssr.production_order_id=po.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
              where ssr.Non_Delivery!='Non-Delivery' and \
              machine_group=999 and po.CLIENT_ID= " + str(ip_data.Client_ID) + " and po.Scenario_ID= " + str(ip_data.Scenario_ID)

    sql_post51 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select po.Client_ID, po.Scenario_ID, 'NULL','NULL','" + str(ip_data.PlantID) + "','NULL','NULL',pos.PRODUCTION_ORDER_ID,'NULL','692',po.PRODUCT_ID from XLPS_PO_PRODUCTS po \
              inner join xlps_product_master pm on po.PRODUCT_ID=pm.PRODUCT_ID and po.client_id=pm.client_id and po.scenario_id=pm.scenario_id \
              inner join xlps_po_solver_ready pos on pos.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pos.client_id=po.client_id and pos.scenario_id=po.scenario_id \
              inner join spps_shortage_report ssr on ssr.production_order_id=pos.production_order_id and ssr.client_id=pos.client_id and ssr.scenario_id=pos.scenario_id \
              where ssr.Non_Delivery!='Non-Delivery' and \
              pm.PRODUCT_TYPE_ID=1 and po.CLIENT_ID= " + str(ip_data.Client_ID) + " and po.Scenario_ID= " + str(ip_data.Scenario_ID)

    sql200 = "insert into xlps_material_receipts_sfg(CLIENT_ID,SCENARIO_ID,BOH_Update_Flag,PRODUCT_ID,Arrival_time,Time_Index,Quantity,Cumul_Quantity) \
        select po.Client_ID, po.Scenario_ID,po.production_order_index,product_index, max(end_time), max(end_time_index),po.ORDER_QUANTITY,po.ORDER_QUANTITY from spps_schedule_report sr \
        inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=sr.PRODUCTION_ORDER_INDEX and po.SCENARIO_ID=sr.SCENARIO_ID \
        where po.CLIENT_ID= " + str(ip_data.Client_ID) + " and po.Scenario_ID= " + str(
       ip_data.Scenario_ID) + " group by po.Client_ID, po.Scenario_ID,po.production_order_index,product_index"

    sql201 = "insert into xlps_material_receipts_sfg(CLIENT_ID,SCENARIO_ID,BOH_Update_Flag,PRODUCT_ID,Arrival_time,Time_Index,Quantity,Cumul_Quantity) \
        select po.Client_ID, po.Scenario_ID,po.production_order_index,product_index, max(end_time), 0,0,0 from spps_schedule_report sr \
        inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=sr.PRODUCTION_ORDER_INDEX and po.SCENARIO_ID=sr.SCENARIO_ID \
        where po.CLIENT_ID= " + str(ip_data.Client_ID) + " and po.Scenario_ID= " + str(
       ip_data.Scenario_ID) + " group by po.Client_ID, po.Scenario_ID,po.production_order_index,product_index"

    sql202 = "insert into xlps_material_receipts_sfg(CLIENT_ID,SCENARIO_ID,BOH_Update_Flag,PRODUCT_ID,Arrival_time,Time_Index,Quantity,Cumul_Quantity) \
        select po.Client_ID, po.Scenario_ID,po.production_order_index,product_index, max(end_time), 43200,100000,100000 from spps_schedule_report sr \
        inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=sr.PRODUCTION_ORDER_INDEX and po.SCENARIO_ID=sr.SCENARIO_ID \
        where po.CLIENT_ID= " + str(ip_data.Client_ID) + " and po.Scenario_ID= " + str(
       ip_data.Scenario_ID) + " group by po.Client_ID, po.Scenario_ID,po.production_order_index,product_index"

    currenttime = datetime.datetime.now()
    # f1.write("Output data export started at %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
    # f1.write("\n")

    currenttime = datetime.datetime.now()
    print "Step56:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cursor1.execute(
        'Delete from XLPS_SCHEDULE_TIMES where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID=' + str(ip_data.Scenario_ID))
    #    cursor1.execute('Delete from XLPS_SFG_QTY where CLIENT_ID= '+ str(Client_ID)+ ' and Scenario_ID=' + str(ip_data.Scenario_ID))

    cursor1.executemany(sql05, params05)
    cursor1.executemany(sql0, params0)
    cursor1.executemany(sql0_0, params0_0)
    cursor1.execute(sql0_01)

    cursor1.executemany(sql1, params1)
    cursor1.execute(sql9)
    cursor1.execute(sql9_1)
    cursor1.execute(sql10)

    cursor1.executemany(sql2, params2)
    cursor1.execute(sql141)
    cursor1.execute(sql141_1)

    currenttime = datetime.datetime.now()
    print "Step57:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cursor1.executemany(sql3, params3)
    cursor1.execute(sql14)
    cursor1.execute(sql14_1)

    cursor1.executemany(sql4, params4)
    cursor1.execute(sql5)

    cursor1.executemany(sql15, params15)
    cursor1.executemany(sql16, params16)
    cursor1.execute(sql17)
    cursor1.execute(sql25)

    cursor1.executemany(sql19, params19)
    cursor1.execute(sql20)
    cursor1.execute(sql21)
    cursor1.execute(sql22)
    cursor1.execute(sql23)
    cursor1.execute(sql24)
    cursor1.executemany(sql41, params41)
    cursor1.execute(sql42)
    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        cursor1.executemany(sql30, params30)
        cursor1.execute(sql31)
        cursor1.execute(sql32)
        cursor1.execute(sql33)
    cnx1.commit()
    currenttime = datetime.datetime.now()
    print "Step58:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if ip_data.PlanningBucket == "Hours":
        cursor1.execute(sql11)
        cursor1.execute(sql12)
        cursor1.execute(sql18)
        cursor1.execute(sql25_1)
        cursor1.execute(sql25_2)
        cursor1.execute(sql26_2)
        cnx1.commit()
        cursor1.execute(sql27_2)
        cursor1.execute(sql0_02)
        cursor1.execute(sql0_05)
        cursor1.execute(sql43_1)
        if ip_data.Sequence_Dependent_Setup_Time == "Include":
            cursor1.execute(sql34)
            cursor1.execute(sql35)

    if ip_data.PlanningBucket == "Minutes":
        cursor1.execute(sql11_1)
        cursor1.execute(sql12_1)
        cursor1.execute(sql18_1)
        cursor1.execute(sql25_1)
        cursor1.execute(sql25_2)

        cursor1.execute(sql26_1)
        cnx1.commit()
        cursor1.execute(sql27_1)
        cursor1.execute(sql0_03)
        cursor1.execute(sql0_06)
        cursor1.execute(sql43_2)
        if ip_data.Sequence_Dependent_Setup_Time == "Include":
            cursor1.execute(sql34_1)
            cursor1.execute(sql35_1)

    if ip_data.PlanningBucket == "Seconds":
        cursor1.execute(sql11_2)
        cursor1.execute(sql12_2)
        cursor1.execute(sql18_2)
        cursor1.execute(sql25_1)
        cursor1.execute(sql25_2)
        cursor1.execute(sql26)
        cnx1.commit()
        cursor1.execute(sql27)
        cursor1.execute(sql0_04)
        cursor1.execute(sql0_07)
        cursor1.execute(sql43_3)
        if ip_data.Sequence_Dependent_Setup_Time == "Include":
            cursor1.execute(sql34_2)
            cursor1.execute(sql35_2)

    currenttime = datetime.datetime.now()
    print "Step59:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cnx1.commit()
    cursor1.execute(sql12_3)
    cursor1.execute(sql26_3)

    cursor1.execute(sql26_4)

    cursor1.execute(sql26_5)





    cursor1.execute(sql27_3)

    cursor1.execute(sql27_4)
    cursor1.execute(sql27_5)

    # cursor1.execute(sql27_6)
    cursor1.execute(sql27_7)

    cursor1.execute(sql27_8)


    cursor1.execute(sql_post1)
    cursor1.execute(sql_post2)

    cursor1.execute(sql_post3)
    cursor1.execute(sql_post4)

    cursor1.execute(sql_post5)
    cursor1.execute(sql_post6)

    cursor1.execute(sql_post7)
    cursor1.execute(sql_post8)
    cursor1.execute(sql_post9)

    cursor1.execute(sql_post10)
    cursor1.execute(sql_post11)
    cursor1.execute(sql_post11_1)
    cursor1.execute(sql_post13)

    cursor1.execute(sql_post14)
    cursor1.execute(sql_post15)
    cursor1.execute(sql_post16)
    cursor1.execute(sql_post16_1)
    cursor1.execute(sql_post16_2)

    cursor1.execute(sql_post17)
    cursor1.execute(sql_post18)
    cursor1.execute(sql_post18_1)
    cursor1.execute(sql_post19)

    cursor1.execute(sql_post20)
    cursor1.execute(sql_post20_1)
    cursor1.execute(sql_post21)
    cursor1.execute(sql_post22)

    cursor1.execute(sql_post22_1)
    cursor1.execute(sql_post22_2)
    cursor1.execute(sql_post12)
    cursor1.execute(sql_post12_1)

    cursor1.execute(sql_post41)
    cursor1.execute(sql_post42)
    cursor1.execute(sql_post43)
    cursor1.execute(sql_post44)

    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        cursor1.execute(sql_post23)
        cursor1.execute(sql_post24)
        cursor1.execute(sql_post25)
        cursor1.execute(sql_post26)
    cnx1.commit()
    currenttime = datetime.datetime.now()
    print "Step60:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cursor1.execute(sql_post99)

    cursor1.execute(sql_post100)

    cursor1.execute(sql_post101)

    cursor1.execute(sql_post102)

    cursor1.execute(sql_post103)

    cursor1.execute(sql_post104)

    cursor1.execute(sql_post105)

    cursor1.execute(sql_post106)

    cursor1.execute(sql_post107)

    cursor1.execute(sql_post51)
    cnx1.commit()
    cursor1.execute(sql28)
    cursor1.execute(sql_post22_3)
    cursor1.execute(sql_post22_4)
    cursor1.execute(sql_post22_5)

    currenttime = datetime.datetime.now()
    print "Step61:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    currenttime = datetime.datetime.now()
    # f1.write("Output data export completed at %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
    # f1.write("\n")
    # f1.close
    active_connection = -1
    cursor1.close()
    cnx1.commit()
    cnx1.close()



def High_Priority_Output_Update():
    cnx1 = mysql.connector.connect(user='root', password='saddlepoint',  host=mp.Host_String,database=mp.DB_String)
    cursor1 = cnx1.cursor()

    cursor1.execute('Delete from XLPS_SCHEDULE_TIMES where CLIENT_ID= ' + str(ip_data.Client_ID) + ' and Scenario_ID=' + str(ip_data.Scenario_ID))


    params0 = [(ip_data.Client_ID, ip_data.Scenario_ID, i, j, ar, s, alt, ps.fixed_start_time[(i, j, ar, s, alt)], \
                ps.fixed_end_time[(i, j, ar, s, alt)], ps.active_var[(i, j, ar, s, alt)]) \
               for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
               i in ps.current_pos and (i, j, ar, s, alt) in ps.fixed_decisions and ps.active_var[
                   (i, j, ar, s, alt)] == 1]


    sql0 = "insert into XLPS_SCHEDULE_TIMES(CLIENT_ID, Scenario_ID, PRODUCTION_ORDER_INDEX, OPERATION_INDEX,ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, \
               BEGIN_TIME_INDEX, END_TIME_INDEX,ACTIVE_STATUS) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    cursor1.executemany(sql0,params0)

    cursor1.close()
    cnx1.commit()
    cnx1.close()