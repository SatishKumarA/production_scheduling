from __future__ import division
print "importing pyomo"
from pyomo.environ import*
import sys
import mysql.connector
import os
import datetime
import time
PROCESS_ID = os.getpid()
solver_status = 0
try:
    JOB_ID = str(sys.argv[1])
except Exception as x:
    print x, "saddlepointv6_prod2"
    JOB_ID = 15224

def solve_supply_network_optimizer(JOB_ID):
    cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
    cursor = cnx.cursor()
    start_time = time.time()
    cursor.execute('update SPAPP_JOB set PID = '+str(PROCESS_ID)+' where JOb_ID ='  + str(JOB_ID))
    cnx.commit()
    cursor.execute('SET GLOBAL max_allowed_packet=1073741824')
    print 'Network Optimizer is invoked'
    Input_Data_Debugging_Flag = False
    Output_Data_Debugging_Flag = True
    Solver_Debugging_Flag = False

    cursor.execute('SELECT distinct Client_ID, Scenario_ID FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
    ClientID = cursor.fetchall()
    Client_ID  = ClientID[0][0]
    Scenario_ID  = ClientID[0][1]
    cursor.execute('SELECT Exception_log_link FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
    Info = cursor.fetchall()
    LogFile = Info[0][0]    
    print 'Client_ID:', Client_ID
    print 'JOB_ID:', JOB_ID
    print 'Scenario_ID:', Scenario_ID
    print 'LogFile:', LogFile

    f1 = open(LogFile,'a')
    f1.truncate
    currenttime = datetime.datetime.now()
    f1.write("-------- Network Optimizer Log ( JOB_ID - %s " %str(JOB_ID))
    f1.write(") -------- \n")
    f1.write("Step1: Data reading started at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.write("\n")  
##    cursor.execute('SELECT distinct COUNT(PERIOD_ID) FROM SPPS_PERIOD  where Client_ID ='  + str(Client_ID))
##    PlanningHorizon = cursor.fetchall()
    cursor.execute('DELETE from spps_solver_df_output where client_id='  + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))
    cursor.execute('DELETE from spps_solver_ts_output where client_id='  + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))
    cursor.execute('DELETE from xlps_production_order where client_id='  + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))

    cursor.execute('SELECT distinct PARAMETER_ID, PARAMETER_VALUE FROM SPAPP_PARAMETER  where \
                    MODULE_ID = 6 and Client_ID ='  + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
    Parameter_Settings = cursor.fetchall()

    for i in range(0,len(Parameter_Settings)):
        if Parameter_Settings[i][0]==621:
            Production_Capacity_param = Parameter_Settings[i][1]
            print "Production_Capacity: ", Production_Capacity_param
        if Parameter_Settings[i][0]==622:
            RM_Capacity = Parameter_Settings[i][1]
            print "RM_Capacity:", RM_Capacity
        elif Parameter_Settings[i][0]==620:
            Late_Delivery_Horizon = Parameter_Settings[i][1]
        elif Parameter_Settings[i][0]==618:
            PlanningHorizon = Parameter_Settings[i][1]
            print "PlanningHorizon: ", PlanningHorizon
        elif Parameter_Settings[i][0]==601:
            PlanningStartDate = Parameter_Settings[i][1]
            print "PlanningStartDate: ", PlanningStartDate
        elif Parameter_Settings[i][0]==623:
            Solver_Run_Time = Parameter_Settings[i][1]
            print "Solver_Run_Time: ", Solver_Run_Time
        elif Parameter_Settings[i][0]==619:
            Planning_Bucket = Parameter_Settings[i][1]
            print "Planning_Bucket: ", Planning_Bucket
    


    Planning_Level = Planning_Bucket
    #PlanningYear = 2016
    cursor.execute('SELECT distinct PERIOD_ID FROM spapp_period where period_value = "'+str(PlanningStartDate)+'" and period_bucket = "'+str(Planning_Level)+'" \
                    and Client_ID ='  + str(Client_ID))

    CurrentTimeBucket = cursor.fetchall()
    print CurrentTimeBucket

    CurrentTimeBucket0 = CurrentTimeBucket[0][0] - 1
    print CurrentTimeBucket[0][0]
    print CurrentTimeBucket0

    cursor.execute('SELECT distinct PERIOD_ID FROM spapp_period where period_id between ' + str(CurrentTimeBucket[0][0]) + ' and '+ str(CurrentTimeBucket[0][0])+ ' \
                      + ' + str(PlanningHorizon) + ' + ' + str(Late_Delivery_Horizon) + ' - 1 and  Client_ID = '+ str(Client_ID))
    Period_ID = cursor.fetchall()
    print Period_ID
    
    cursor.execute('SELECT distinct PERIOD_ID FROM spapp_period where period_id between ' + str(CurrentTimeBucket[0][0]) + ' and '+ str(CurrentTimeBucket[0][0])+ ' \
                      + ' + str(PlanningHorizon) + ' + ' + str(Late_Delivery_Horizon) + ' - 1 and  Client_ID = '  + str(Client_ID))
    Period_ID0 = cursor.fetchall()
    print Period_ID0
    
    EndTimeBucket = CurrentTimeBucket[0][0] + int(PlanningHorizon) + int(Late_Delivery_Horizon) - 1
    print EndTimeBucket

    cursor.execute('SELECT distinct sr.LOCATION_FROM_ID from spps_df_input df \
                   inner join spps_relation sr on sr.relation_id=df.relation_ID and sr.client_id=df.client_id  \
                   where df.DF_ID =674 and df.df_value in ("Planning","Both")  and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    PlantID = cursor.fetchall()
    PlantID =  PlantID[0][0]  
    print 'PlantID: ', PlantID
    
    cursor.execute('SELECT distinct OUTPUT_PRODUCT_ID from spps_df_input df inner join spps_relation sr on df.relation_id=sr.relation_id \
            and df.client_id=sr.client_id where df_id=2000 and sr.Client_ID ='  + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    IPProduct_ID = cursor.fetchall()    

    cursor.execute('SELECT distinct OUTPUT_PRODUCT_ID,convert(df_value, UNSIGNED INTEGER) from spps_df_input df inner join spps_relation sr on df.relation_id=sr.relation_id \
            and df.client_id=sr.client_id where df_id=673 and sr.Client_ID ='  + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    OPProduct_ID = cursor.fetchall()

    print "OPProduct_ID: ", OPProduct_ID

    cursor.execute('select distinct OUTPUT_PRODUCT_ID from spps_relation sr \
                join spps_df_input df on df.client_id = sr.CLIENT_ID and df.RELATION_ID = sr.RELATION_ID and df.DF_ID = 2003 and df.DF_VALUE = 3 \
                where df.CLIENT_ID ='  + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    Finished_Goods = cursor.fetchall()

    print 'Finished_Goods: ', Finished_Goods
    cursor.execute('SELECT distinct location_from_id from spps_df_input df inner join spps_relation sr on df.relation_id=sr.relation_id \
            and df.client_id=sr.client_id  where df_id=2020 and sr.Client_ID ='  + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    Location_ID = cursor.fetchall()
    print 'Location_ID: ', Location_ID
    cursor.execute('SELECT distinct sr.OPERATION_ID,sr.LOCATION_FROM_ID, 7,9999 from spps_df_input df \
                   inner join spps_relation sr on sr.relation_id=df.relation_ID and sr.client_id=df.client_id  \
                   where df.DF_ID =674 and df.df_value in ("Planning","Both")  and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    Resource_Master = cursor.fetchall()
    #
    # cursor.execute('SELECT distinct df1.df_value,sr.LOCATION_FROM_ID, 7,9999 from spps_df_input df  \
    #                inner join spps_relation sr on sr.relation_id=df.relation_ID and sr.client_id=df.client_id     \
    #                inner join spps_relation sr1 on sr.location_from_id=sr1.location_from_id and sr.client_id=sr1.client_id     \
    #                inner join spps_df_input df1 on df1.client_id=sr1.client_id and df.scenario_id=df1.scenario_id and df1.df_value=sr.operation_id  \
    #                where df.DF_ID =674 and df.df_value in ("Planning","Both") and   df1.DF_ID = 615 and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    #
    # Resource_Master = cursor.fetchall()
    
    print 'Resource_Master: ',Resource_Master
    cursor.execute('SELECT distinct sr.LOCATION_FROM_ID,sr.Output_Product_ID from spps_relation sr \
            inner join spps_df_input sdf on sr.relation_id=sdf.relation_id and sr.client_id=sdf.client_id \
            where sdf.DF_ID=677  and sr.client_id=' + str(Client_ID) + ' and sdf.Scenario_ID= ' + str(Scenario_ID))
    InitialInventory_Comb = cursor.fetchall()

    cursor.execute('SELECT distinct sr.LOCATION_FROM_ID,sr.Output_Product_ID,convert(sdf.df_value,UNSIGNED INTEGER) from spps_relation sr \
            inner join spps_df_input sdf on sr.relation_id=sdf.relation_id and sr.client_id=sdf.client_id \
            where sdf.DF_ID=677  and sr.client_id=' + str(Client_ID) + ' and sdf.Scenario_ID= ' + str(Scenario_ID) )
    InitialInventory = cursor.fetchall()

    print "InitialInventory: ", InitialInventory

    cursor.execute('SELECT distinct LOCATION_FROM_ID  from spps_df_input df1 inner join spps_relation sr1 on df1.relation_id= sr1.relation_id \
            and df1.client_id=sr1.client_id where df_id=2051 and sr1.Client_ID =' + str(Client_ID) + ' and df1.Scenario_ID= ' + str(Scenario_ID)) 
    Location_From_ID = cursor.fetchall()

    print 'Location_From_ID: ', Location_From_ID
    cursor.execute('SELECT distinct sp.Location_From_ID,df.DF_VALUE from spps_relation sp \
                    inner join spps_df_input df on sp.client_id=df.client_id and df.relation_id=sp.relation_ID where \
                    df.df_id=2023 and sp.Client_ID =' + str(Client_ID) + ' and df.Scenario_ID=' + str(Scenario_ID))
    Location_Type = cursor.fetchall()
    Location_Type.append(('Customer',4))

    cursor.execute('SELECT distinct LOCATION_TO_ID  from spps_df_input df1 inner join spps_relation sr1 on df1.relation_id= sr1.relation_id \
            and df1.client_id=sr1.client_id where df_id=2051 and sr1.Client_ID =' + str(Client_ID) + ' and df1.Scenario_ID= ' + str(Scenario_ID) + ' \
            union (SELECT "Customer" from spps_df_input limit 1)')
            
    Location_To_ID = cursor.fetchall()
##    for i in Location_To_ID:
##        Location_From_ID.append(i)
##    Location_To_ID.append('Customer')
    print 'Location_To_ID: ', Location_To_ID
    print 'Location_From_ID: ', Location_From_ID
##    cursor.execute('SELECT distinct LOCATION_TO_ID  from spps_df_input df1 inner join spps_relation sr1 on df1.relation_id= sr1.relation_id \
##            and df1.client_id=sr1.client_id where LOCATION_TO_ID not in  (select distinct LOCATION_FROM_ID from spps_df_input df  \
##            inner join spps_relation sr on df.relation_id= sr.relation_id and df.client_id=sr.client_id where df_id=2051 and \
##            sr.Client_ID ='  + str(Client_ID) + ' and df.Scenario_ID=' + str(Scenario_ID) + ') and  df_id=2051 and sr1.Client_ID \
##            =' + str(Client_ID) + ' and df1.Scenario_ID= ' + str(Scenario_ID))
##    EndNodes = cursor.fetchall()
##    EndNodes=[]
    cursor.execute('SELECT "Customer" from spps_df_input limit 1')
    EndNodes = cursor.fetchall()
##    EndNodes.append('Customer')
    print 'EndNodes: ', EndNodes
    cursor.execute('SELECT distinct LOCATION_FROM_ID  from spps_df_input df1 inner join spps_relation sr1 on df1.relation_id= sr1.relation_id \
            and df1.client_id=sr1.client_id where LOCATION_FROM_ID not in  (select distinct LOCATION_TO_ID from spps_df_input df  \
            inner join spps_relation sr on df.relation_id= sr.relation_id and df.client_id=sr.client_id where df_id=2051 and \
            sr.Client_ID ='  + str(Client_ID) + ' and df.Scenario_ID=' + str(Scenario_ID) + ') and  df_id=2051 and sr1.Client_ID \
            =' + str(Client_ID) + ' and df1.Scenario_ID= ' + str(Scenario_ID))
    InitialNodes = cursor.fetchall()
    print 'InitialNodes: ', InitialNodes
    cursor.execute('SELECT distinct sr.RESOURCE_ID, sr.OPERATION_ID, df1.DF_VALUE from spps_df_input df inner join  spps_relation sr on \
            sr.relation_id=df.relation_ID and sr.client_id=df.client_id \
            inner join spps_relation sr1 on sr1.OPERATION_ID = sr.OPERATION_ID and sr1.CLIENT_ID = sr.CLIENT_ID \
            inner join spps_df_input df1 on df1.RELATION_ID = sr1.RELATION_ID and df1.CLIENT_ID = sr1.CLIENT_ID and df1.SCENARIO_ID = df.SCENARIO_ID \
            where df.DF_ID =2061 and df1.df_id = 2077 and df.Client_ID ='  + str(Client_ID) + ' \
            and df.Scenario_ID= ' + str(Scenario_ID))
    Resource_ID = cursor.fetchall()

    # cursor.execute('SELECT distinct sr.RESOURCE_ID, df4.DF_VALUE, df1.DF_VALUE from spps_df_input df \
    # 	            inner join  spps_relation sr on sr.relation_id=df.relation_ID and sr.client_id=df.client_id \
    #                 inner join spps_df_input df4 on sr.relation_ID= df4.relation_ID and sr.client_id=df4.client_id and df.SCENARIO_ID=df4.scenario_id \
    # 	            inner join spps_relation sr1 on sr1.OPERATION_ID = df4.DF_VALUE and sr1.CLIENT_ID = sr.CLIENT_ID \
    # 	            inner join spps_df_input df1 on df1.RELATION_ID = sr1.RELATION_ID and df1.CLIENT_ID = sr1.CLIENT_ID and df1.SCENARIO_ID = df.SCENARIO_ID \
    #                 where df.DF_ID =2061 and df1.df_id = 2077 and df4.DF_ID = 615 and  df.Client_ID =' + str(Client_ID) + ' \
    #                 and df.Scenario_ID= ' + str(Scenario_ID))
    # Resource_ID = cursor.fetchall()

    print 'Resource_ID: ', Resource_ID

    cursor.execute('SELECT distinct sr.operation_id  from spps_df_input df inner join  spps_relation sr on \
            sr.relation_id=df.relation_ID and sr.client_id=df.client_id  where df.DF_ID =2061 and df.Client_ID ='  + str(Client_ID) + ' \
            and df.Scenario_ID= ' + str(Scenario_ID))
    EndMachines = cursor.fetchall()

    # cursor.execute('SELECT distinct df.df_value  from spps_df_input df inner join  spps_relation sr on \
    #         sr.relation_id=df.relation_ID and sr.client_id=df.client_id  where df.DF_ID =615 and df.Client_ID =' + str(Client_ID) + ' \
    #         and df.Scenario_ID= ' + str(Scenario_ID))
    # EndMachines = cursor.fetchall()

    print 'EndMachines: ', EndMachines
##    cursor.execute('select distinct r.OUTPUT_PRODUCT_ID, r.location_from_id,r.RESOURCE_ID, 1,0,0,0 from spps_df_input df1 \
##                inner join spps_relation r on r.relation_id=df1.relation_id where df1.DF_ID=602 and r.client_id=df1.client_id \
##                and  df1.Client_ID =' + str(Client_ID) + ' and df1.Scenario_ID= ' + str(Scenario_ID))

    cursor.execute('select distinct r.OUTPUT_PRODUCT_ID,r.LOCATION_FROM_ID,r.operation_id,1,0,0,0 from spps_df_input df \
                inner join spps_relation r on r.relation_id=df.relation_id  and r.client_id=df.client_id \
		inner join spps_df_input df2 ON df.CLIENT_ID=df2.CLIENT_ID and df.Scenario_ID=df2.Scenario_ID \
                inner join spps_relation sr on sr.OPERATION_ID = r.operation_id and sr.relation_id=df2.relation_id \
                inner join spps_df_input df3 ON df.CLIENT_ID=df3.CLIENT_ID and df.Scenario_ID=df3.Scenario_ID \
                where df.DF_ID = 602 and df2.df_id = 2060 and df.Client_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))

    MachineLevel = cursor.fetchall()

    print 'MachineLevel', MachineLevel

##    cursor.execute('SELECT distinct pd.LOCATION_ID, pd.Product_ID, pd.PERIOD_ID from XLSN_DEMAND pd where pd.Client_ID ='  + str(Client_ID))
##    LocationLevel_SDI_Comb = cursor.fetchall()

    cursor.execute('select distinct "Customer",r.output_product_id, period_id from spps_ts_input ts  \
                inner join spps_relation r  on r.relation_id=ts.relation_id  and ts.client_id=r.client_id \
                where sp_ts_id =601 and period_id between ' + str(CurrentTimeBucket[0][0])+ '   and ' + str(EndTimeBucket)+'   \
                and ts.Client_ID = ' + str(Client_ID)+'  and ts.Scenario_ID=' + str(Scenario_ID))         
    LocationLevel_SDI_Comb = cursor.fetchall()
    
##    cursor.execute('SELECT distinct pd.LOCATION_ID, pd.Product_ID, pd.PERIOD_ID, TS_VALUE from XLSN_DEMAND pd where pd.Client_ID ='  + str(Client_ID))
##    LocationLevel_SDI = cursor.fetchall()

    cursor.execute('select distinct "Customer",r.output_product_id, period_id,convert(sum(ts_value),DECIMAL(15,3)) from spps_ts_input ts  \
                inner join spps_relation r  on r.relation_id=ts.relation_id  and ts.client_id=r.client_id \
                where sp_ts_id =601 and period_id between ' + str(CurrentTimeBucket[0][0])+ '   and ' + str(EndTimeBucket)+'   \
                and ts.Client_ID = ' + str(Client_ID)+'  and ts.Scenario_ID=' + str(Scenario_ID) +' group by r.output_product_id, period_id')         
    LocationLevel_SDI = cursor.fetchall()
    
    print 'LocationLevel_SDI', LocationLevel_SDI
    def print_string(string,string1):
        # pass
        print  "\n",string,": ",string1,"\n"
    cursor.execute('select distinct "Customer", r.OUTPUT_PRODUCT_ID,period_id from spps_ts_input ts \
                inner join spps_relation r  on r.relation_id=ts.relation_id  and ts.client_id=r.client_id \
                where sp_ts_id =414 and period_id between ' + str(CurrentTimeBucket[0][0]) + ' and \
                '+ str(CurrentTimeBucket[0][0])+ ' + ' + str(PlanningHorizon) + ' + ' + str(Late_Delivery_Horizon) + ' - 1 and ts.Client_ID =' + str(Client_ID) + ' \
                and ts.Scenario_ID=' + str(Scenario_ID))
    LocationLevel_STI_Comb = cursor.fetchall()    
    print_string("LocationLevel_STI_Comb",LocationLevel_STI_Comb)
    cursor.execute('select distinct r.location_from_id, r.OUTPUT_PRODUCT_ID,period_id,convert(ts_value,UNSIGNED INTEGER) from spps_ts_input ts \
                inner join spps_relation r  on r.relation_id=ts.relation_id  and ts.client_id=r.client_id \
                where sp_ts_id =414 and period_id between ' + str(CurrentTimeBucket[0][0]) + ' and \
                '+ str(CurrentTimeBucket[0][0])+ ' + ' + str(PlanningHorizon) + ' + ' + str(Late_Delivery_Horizon) + ' - 1 and ts.Client_ID =' + str(Client_ID) + ' \
                and ts.Scenario_ID=' + str(Scenario_ID))
    LocationLevel_STI = cursor.fetchall()
    print_string("LocationLevel_STI",LocationLevel_STI)
##    cursor.execute('SELECT distinct sr.LOCATION_FROM_ID,sr.Output_Product_ID \
##        from spps_relation sr inner join spps_df_input sdf on sr.relation_id=sdf.relation_id and sr.client_id=sdf.client_id \
##        inner join spps_df_input sdf1 on sr.relation_id=sdf1.relation_id and sr.client_id=sdf1.client_id and sdf.scenario_id=sdf1.scenario_id \
##        where sdf.DF_ID=411 and sdf1.df_id=412 and sr.client_id=' + str(Client_ID) +' and sdf.Scenario_ID= ' + str(Scenario_ID) + ' \
##        union \
##        SELECT distinct plm.LOCATION_FROM_ID,plm.Output_Product_ID from spps_relation plm \
##        inner join  spps_df_input sdf1 on plm.relation_id=sdf1.relation_id and plm.client_id=sdf1.client_id  \
##        where sdf1.DF_ID=2050 \
##        and (plm.LOCATION_FROM_ID,plm.Output_Product_id) not in ( SELECT distinct sr.LOCATION_FROM_ID,sr.Output_Product_ID\
##        from spps_relation sr inner join spps_df_input sdf on sr.relation_id=sdf.relation_id and sr.client_id=sdf.client_id \
##        inner join spps_df_input sdf1 on sr.relation_id=sdf1.relation_id and sr.client_id=sdf1.client_id and sdf.scenario_id=sdf1.scenario_id \
##        where sdf.DF_ID=411 and sdf1.df_id=412 and sr.client_id=' + str(Client_ID) +' and sdf.Scenario_ID= ' + str(Scenario_ID) + ' \
##        ) and sdf1.Client_ID =' + str(Client_ID) + ' and sdf1.Scenario_ID= ' + str(Scenario_ID))
##
    cursor.execute('select LOCATION_FROM_ID, OUTPUT_PRODUCT_ID from spps_df_input df inner join spps_relation sr on sr.client_id=df.client_id \
                and sr.relation_id=df.relation_id where df_id in (676,2000) and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID) + ' \
                union \
                select LOCATION_FROM_ID, OUTPUT_PRODUCT_ID from spps_df_input df inner join spps_relation sr on sr.client_id=df.client_id \
                and sr.relation_id=df.relation_id where df_id in (676,2000) and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID) )
    
    Inventory_Storage_Comb = cursor.fetchall()
    print_string("Inventory_Storage_Comb",Inventory_Storage_Comb)
##    cursor.execute('SELECT distinct sr.LOCATION_FROM_ID,sr.OUTPUT_PRODUCT_ID,convert(sdf.DF_VALUE,UNSIGNED INTEGER),convert(sdf1.DF_VALUE,UNSIGNED INTEGER) \
##        from spps_relation sr inner join spps_df_input sdf on sr.relation_id=sdf.relation_id and sr.client_id=sdf.client_id \
##        inner join spps_df_input sdf1 on sr.relation_id=sdf1.relation_id and sr.client_id=sdf1.client_id and sdf.scenario_id=sdf1.scenario_id \
##        where sdf.DF_ID=411 and sdf1.df_id=412 and sr.client_id=' + str(Client_ID) +' and sdf.Scenario_ID= ' + str(Scenario_ID) + ' \
##        union \
##        SELECT distinct plm.LOCATION_FROM_ID,plm.OUTPUT_PRODUCT_ID,999999, convert(0,UNSIGNED INTEGER) from spps_relation plm \
##        inner join  spps_df_input sdf1 on plm.relation_id=sdf1.relation_id and plm.client_id=sdf1.client_id  \
##        where sdf1.DF_ID=2050 \
##        and (plm.LOCATION_FROM_ID,plm.OUTPUT_PRODUCT_ID) not in ( SELECT distinct sr.LOCATION_FROM_ID,sr.Output_Product_ID \
##        from spps_relation sr inner join spps_df_input sdf on sr.relation_id=sdf.relation_id and sr.client_id=sdf.client_id \
##        inner join spps_df_input sdf1 on sr.relation_id=sdf1.relation_id and sr.client_id=sdf1.client_id and sdf.scenario_id=sdf1.scenario_id \
##        where sdf.DF_ID=411 and sdf1.df_id=412 and sr.client_id=' + str(Client_ID) +' and sdf.Scenario_ID= ' + str(Scenario_ID) + ' \
##        ) and sdf1.Client_ID =' + str(Client_ID) + ' and sdf1.Scenario_ID= ' + str(Scenario_ID))
    Inventory_Storage = [] #cursor.fetchall()
    
    print_string("Inventory_Storage",Inventory_Storage)
    cursor.execute('SELECT distinct pd.product_id,pd.RESOURCE_ID,pd.LOCATION_ID, PERIOD_ID, CAPACITY from XLPS_CAPACITY pd where pd.Client_ID ='  + str(Client_ID))
    ResourceCapacity = cursor.fetchall()

##    print 'ResourceCapacity: ', ResourceCapacity
    
    cursor.execute('select distinct OUTPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,0,0 from xlps_modes m where m.Client_ID =' + str(Client_ID) )
    ODLocationLevel = cursor.fetchall()
    print 'ODLocationLevel: ', ODLocationLevel
    # cursor.execute('select distinct r.INPUT_PRODUCT_ID, r.OUTPUT_PRODUCT_ID,r.OPERATION_ID, df1.DF_VALUE,df2.DF_VALUE from spps_df_input df1 \
    #             inner join spps_relation sr2 on sr2.CLIENT_ID = df1.CLIENT_ID \
    #             inner join spps_df_input df3 on df3.RELATION_ID = sr2.RELATION_ID and df3.SCENARIO_ID = df1.SCENARIO_ID and df3.CLIENT_ID = df1.CLIENT_ID \
    #             inner join spps_df_input df2 ON df1.CLIENT_ID=df2.CLIENT_ID and df1.RELATION_ID=df2.RELATION_ID  and df1.Scenario_ID=df2.Scenario_ID \
    #             inner join spps_relation r on r.OPERATION_ID = sr2.operation_id and r.relation_id=df1.relation_id where df1.DF_ID=602 and df2.df_id = 621 and df3.df_id = 2060 and r.client_id=df1.client_id and \
    #             df1.Client_ID =' + str(Client_ID) + ' and df1.Scenario_ID= ' + str(Scenario_ID))
    # ConversionatMachine = cursor.fetchall()

    cursor.execute('select distinct r.INPUT_PRODUCT_ID, r.OUTPUT_PRODUCT_ID,r.OPERATION_ID, df1.DF_VALUE,df2.DF_VALUE from spps_df_input df1 \
                inner join spps_relation sr2 on sr2.CLIENT_ID = df1.CLIENT_ID \
                inner join spps_df_input df2 ON df1.CLIENT_ID=df2.CLIENT_ID and df1.RELATION_ID=df2.RELATION_ID  and df1.Scenario_ID=df2.Scenario_ID \
                inner join spps_relation r on r.OPERATION_ID = sr2.operation_id and r.relation_id=df1.relation_id where df1.DF_ID=602 and df2.df_id = 621 and r.client_id=df1.client_id and \
                df1.Client_ID =' + str(Client_ID) + ' and df1.Scenario_ID= ' + str(Scenario_ID))
    ConversionatMachine = cursor.fetchall()

    print 'ConversionatMachine: ', ConversionatMachine

    cursor.execute('select distinct  r.OUTPUT_PRODUCT_ID,r.location_from_id, (df.DF_VALUE ) from spps_df_input df \
                    inner join spps_relation r on r.relation_id=df.relation_id and r.client_id=df.client_id \
                    where df.DF_ID=676 and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))     
    ProcurementCost = cursor.fetchall()

    print 'ProcurementCost: ', ProcurementCost

    cursor.execute('select LOCATION_FROM_ID, OUTPUT_PRODUCT_ID from spps_df_input df inner join spps_relation sr on sr.client_id=df.client_id \
                and sr.relation_id=df.relation_id where df_id in (676,2000) and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID) + ' \
                union \
                select LOCATION_FROM_ID, OUTPUT_PRODUCT_ID from spps_df_input df inner join spps_relation sr on sr.client_id=df.client_id \
                and sr.relation_id=df.relation_id where df_id in (676,2000) and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID) )
    
    ValidOPComb = cursor.fetchall()
    
    print_string("ValidOPComb",ValidOPComb)
    cursor.execute('select OUTPUT_PRODUCT_ID,LOCATION_FROM_ID from spps_df_input df inner join spps_relation sr on sr.client_id=df.client_id \
                and sr.relation_id=df.relation_id where df_id in (676,2000) and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))     
    ValidIPComb = cursor.fetchall()
    
    print_string("ValidIPComb",ValidIPComb)
    # cursor.execute('select distinct r.OUTPUT_PRODUCT_ID,r.LOCATION_FROM_ID,r.OPERATION_ID from spps_df_input df inner join spps_relation r \
    #             on r.relation_id=df.relation_id  where DF_ID=602 and r.client_id=df.client_id and df.Client_ID =' + str(Client_ID) + ' \
    #              and df.Scenario_ID= ' + str(Scenario_ID))
    cursor.execute('select distinct r.OUTPUT_PRODUCT_ID,r.LOCATION_FROM_ID,sr.OPERATION_ID from spps_df_input df \
                    inner join spps_relation r on r.relation_id=df.relation_id  and r.client_id=df.client_id \
    		inner join spps_df_input df2 ON df.CLIENT_ID=df2.CLIENT_ID and df.Scenario_ID=df2.Scenario_ID \
                    inner join spps_relation sr on sr.OPERATION_ID = r.operation_id and sr.relation_id=df2.relation_id \
                    where df.DF_ID=602 and df2.df_id = 674 and df2.df_value in ("Planning","Both") and df.Client_ID =' + str(
        Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))

    ValidMComb_Uncon = cursor.fetchall()
    print_string("ValidMComb_Uncon",ValidMComb_Uncon)
    
    cursor.execute('select distinct r.OUTPUT_PRODUCT_ID,r.LOCATION_FROM_ID,sr.OPERATION_ID from spps_df_input df \
                inner join spps_relation r on r.relation_id=df.relation_id  and r.client_id=df.client_id \
		inner join spps_df_input df2 ON df.CLIENT_ID=df2.CLIENT_ID and df.Scenario_ID=df2.Scenario_ID \
                inner join spps_relation sr on sr.OPERATION_ID = r.operation_id and sr.relation_id=df2.relation_id \
                where df.DF_ID=602 and df2.df_id = 674 and df2.df_value in ("Planning","Both") and df.Client_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    ValidMComb_Con = cursor.fetchall()

    print "ValidMComb_Con: ", ValidMComb_Con
    
    cursor.execute('select product_id,location_id, operation_id, period_id, MIN_BATCH_SIZE, MAX_BATCH_SIZE, SETUP_TIME, PROCESSING_TIME, TEARDOWN_TIME, CAPACITY_KGS \
    from xlps_machine_batch_time where Client_ID =' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
    ValidMComb_Values = cursor.fetchall()
##    print "ValidMComb_Values",ValidMComb_Values

    cursor.execute('select distinct sr.operation_id from spps_relation sr inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id \
        where df.df_id=679 and df.df_value = "Yes" and df. Client_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID) + ' limit 1')
    Bottleneck_Operation = cursor.fetchall()
    print "Bottleneck_Operation: ", Bottleneck_Operation
    Bottleneck_Operation =  Bottleneck_Operation[0][0]
    # Bottleneck_Operation = ""
    print "Bottleneck_Operation: ", Bottleneck_Operation
    
    cursor.execute('select distinct location_id, operation_id, resource_id, MAX_BATCH_SIZE, MIN_BATCH_SIZE from xlps_machine_batch_time \
        where operation_id = "'+ str(Bottleneck_Operation) + '" and Client_ID =' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' \
        order by MAX_BATCH_SIZE desc')
    Lot_Bottleneck_Data = cursor.fetchall()
    print "Lot_Bottleneck_Data: ",Lot_Bottleneck_Data

    
    cursor.execute('select sr.Location_From_ID,sr.OUTPUT_PRODUCT_ID, sp.period_id, df1.df_value \
            from spps_relation sr inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id \
            inner join spps_df_input df1 on df1.relation_id=sr.relation_id and df1.client_id=sr.client_id and df.scenario_id=df1.scenario_id \
            inner join spapp_period sp on STR_TO_DATE(sp.period_value,"%m/%d/%Y")= STR_TO_DATE(df.df_value,"%m/%d/%Y %H:%i:%s") and sp.CLIENT_ID=sr.client_id \
            where df.df_id=638 and df1.df_id=639 and sr.Client_ID =' + str(Client_ID) + ' and df.Scenario_ID = ' + str(Scenario_ID))

    PlannedReceipts = cursor.fetchall()
    print_string("PlannedReceipts",PlannedReceipts)
    cursor.execute('select distinct r.INPUT_PRODUCT_ID,r.LOCATION_FROM_ID,sr.OPERATION_ID,df2.df_value from spps_df_input df inner join spps_relation r on \
                r.relation_id=df.relation_id  and r.client_id=df.client_id inner join spps_df_input df2 ON df.CLIENT_ID=df2.CLIENT_ID and \
                df.Scenario_ID=df2.Scenario_ID inner join spps_relation sr on sr.OPERATION_ID = r.operation_id and sr.relation_id=df2.relation_id \
                where df.DF_ID=602 and df2.df_id = 674 and df2.df_value in ("Planning","Both") and df.Client_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    ValidMIP = cursor.fetchall()
    
    print_string("ValidMIP",ValidMIP)

    
    cursor.execute('select distinct msd.MACHINE_INDEX, ms.SHUTDOWN_START_TIME_INDEX, ms.SHUTDOWN_END_TIME_INDEX From XLPS_MACHINE_SOLVER_READY \
                    msd inner join XLPS_OPERATION_SOLVER_READY osd on msd.OPERATION_ID=osd.OPERATION_ID and msd.CLIENT_ID=osd.CLIENT_ID \
                    and msd.Scenario_ID=osd.Scenario_ID inner join XLPS_Machine_Shutdown ms on  ms.Machine_ID=msd.Machine_ID and \
                    msd.CLIENT_ID=ms.CLIENT_ID and msd.Scenario_ID=ms.Scenario_ID where msd.CLIENT_ID = ' + str(Client_ID) +' and msd.Scenario_ID=' + str(Scenario_ID))
    Machine_Shutdown_Details = cursor.fetchall()
    print_string("Machine_Shutdown_Details",Machine_Shutdown_Details)

    cursor.close()
    cnx.commit()
    cnx.close()

    currenttime = datetime.datetime.now()
    f1.write("Step2: Data reading completed at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.write("\n")  
   
    currenttime = datetime.datetime.now()
    f1.write("Step3: Model building started at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.write("\n")  

    model = ConcreteModel()

    print' ----------------------------------- Model Data Declaration ----------------------'

    def T1(model):
        return (Period_ID[i][0] for i in range(0,len(Period_ID)))
    model.Period_ID = Set(initialize =T1)

    model.CurrentTimeBucket0 = Param(initialize = CurrentTimeBucket0)
    model.CurrentTimeBucket = Param(initialize = CurrentTimeBucket[0][0])

    def T12(model):
        return (Period_ID0[i][0] for i in range(0,len(Period_ID0)))
    model.Period_ID0 = Set(initialize =T12)
    model.Period_ID0.add(CurrentTimeBucket0)

    model.PlanningHorizon = Param(initialize=PlanningHorizon)
    model.EndTimeBucket = Param(initialize=EndTimeBucket)

    def T2(model):
        return (IPProduct_ID[i][0] for i in range(0,len(IPProduct_ID)))
    model.IPProduct_ID = Set(initialize =T2)

    def T3(model):
        return (OPProduct_ID[i][0] for i in range(0,len(OPProduct_ID)))
    model.OPProduct_ID = Set(initialize =T3)
    def SC_shelfLife(model,op):
        value = 999
        for i in range(0,len(OPProduct_ID)):
            if op==OPProduct_ID[i][0] and int(OPProduct_ID[i][1])>0:
                value = int(OPProduct_ID[i][1])
        return value
    try:
        model.Product_Shelf_Life = Param(model.OPProduct_ID,initialize = SC_shelfLife)
    except Exception as x:
        print x
        f1.write("\n")
        f1.write("Shelf Life value is not provided in Product Master. Please enter a default value of 999 in Shelf Life field")
        f1.write("\n")
        cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1',database='saddlepointv6_prod2', buffered=True)
        cursor = cnx.cursor()
        cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
        cursor.close()
        cnx.commit()
        cnx.close()


    def T31(model):
        return (Finished_Goods[i][0] for i in range(0,len(Finished_Goods)))
    model.FGs = Set(initialize =T31)
    
    def T3(model):
        return (OPProduct_ID[i][0] for i in range(0,int(len(OPProduct_ID)/2)))
    model.OPProduct_ID_temp = Set(initialize =T3)

    def T41(model):
        return (Location_ID[i][0] for i in range(0,len(Location_ID)))
    model.Location_ID = Set(initialize =T41)

    def T4(model):
        return (Location_From_ID[i][0] for i in range(0,len(Location_From_ID)))
    model.Location_From_ID = Set(initialize =T4)

    def T5(model):
        return (Location_To_ID[i][0] for i in range(0,len(Location_To_ID)))
    model.Location_To_ID = Set(initialize =T5)

    def T6(model):
        return (EndNodes[i][0] for i in range(0,len(EndNodes)))
    model.EndNodes = Set(initialize =T6)

    def T7(model):
        return (InitialNodes[i][0] for i in range(0,len(InitialNodes)))
    model.InitialNodes = Set(initialize =T7)

    def T8(model):
        return (Resource_ID[i][1] for i in range(0,len(Resource_ID)))
    model.Resource_ID = Set(initialize =T8)
    print "ask"
    def Res_type(model,r):
        print "orig: ", r
        for i in range(0,len(Resource_ID)):
            if r == Resource_ID[i][1]:
                if int(Resource_ID[i][2]) == 2:
                    return "Batch"
                elif int(Resource_ID[i][2]) == 1:
                    return "Discrete", r
                elif int(Resource_ID[i][2]) == 3:
                    return "Holding", r
                else:
                    print "New resource type", r
    model.Res_Type = Param(model.Resource_ID,initialize = Res_type)

    def Res_operation(model,r):
        for i in range(0,len(Resource_ID)):
            if r == Resource_ID[i][1]:
                return Resource_ID[i][1]
    model.Res_Operation = Param(model.Resource_ID,initialize = Res_operation)
    
    # for i in model.Resource_ID:
    #     print i, model.Res_Type[i]

    def T9(model):
        return (EndMachines[i][0] for i in range(0,len(EndMachines)))
    model.EndMachines = Set(initialize =T9)


    def LFT1(model):
        return ((Location_Type[i][0]) for i in range(0,len(Location_Type)))
    model.LFT2 = Set(initialize=LFT1, dimen=1)

    def RM1(model):
        return ((Resource_Master[i][0],Resource_Master[i][1]) for i in range(0,len(Resource_Master)))
    model.RM2 = Set(initialize=RM1, dimen=2)

    def LFT5(model,l):
        for i in range(0,len(Location_Type)):
            if l==Location_Type[i][0]:
                return (int(Location_Type[i][1]))
    model.LFT = Param(model.LFT2,initialize = LFT5)

    def ML1(model):
        return ((MachineLevel[i][0],MachineLevel[i][1],MachineLevel[i][2]) for i in range(0,len(MachineLevel)))
    model.ML2 = Set(initialize=ML1, dimen=3)

    def RR3(model,op,l,m):
        value = 9999999
        for i in range(0,len(MachineLevel)):
            if op==MachineLevel[i][0] and l==MachineLevel[i][1] and m==MachineLevel[i][2]:
                value = float(MachineLevel[i][3])
        return value
    model.RR = Param(model.ML2,initialize = RR3)

    def PC3(model,op,l,m):
        for i in range(0,len(MachineLevel)):
            if op==MachineLevel[i][0] and l==MachineLevel[i][1] and m==MachineLevel[i][2]:
                return float(MachineLevel[i][4])
    model.PC = Param(model.ML2,initialize = PC3)

    def RC1(model):
        return ((ResourceCapacity[i][0],ResourceCapacity[i][1],ResourceCapacity[i][2],ResourceCapacity[i][3]) for i in range(0,len(ResourceCapacity)))
    model.RC2 = Set(initialize=RC1, dimen=4)
    
    def RC1(model):
        return ((ResourceCapacity[i][1],ResourceCapacity[i][2],ResourceCapacity[i][3]) for i in range(0,len(ResourceCapacity)) \
                if (ResourceCapacity[i][1],ResourceCapacity[i][2],ResourceCapacity[i][3]) not in model.RC3)
    model.RC3 = Set(initialize=RC1, dimen=3)

    def RC5(model,p,m,l,t):
        for i in range(0,len(ResourceCapacity)):
            if p==ResourceCapacity[i][0] and m==ResourceCapacity[i][1] and l==ResourceCapacity[i][2] and t==ResourceCapacity[i][3]:
                return float(ResourceCapacity[i][4])
    model.Production_Capacity = Param(model.RC2,initialize = RC5)

##    def LL_SDI1(model):
##        return ((LocationLevel_SDI[i][0],LocationLevel_SDI[i][1],LocationLevel_SDI[i][2]) for i in range(0,len(LocationLevel_SDI)))
##    model.LL_SDI2 = Set(initialize=LL_SDI1, dimen=3)

##    def DemandComb(model):
##        return ((LocationLevel_SDI_Comb[i][0],LocationLevel_SDI_Comb[i][1],LocationLevel_SDI_Comb[i][2]) 
##                                    for i in range(0,len(LocationLevel_SDI_Comb)))
##    model.Demand_Comb = Set(initialize=DemandComb,dimen=3)

    
    def DemandComb(model):
        return ((LocationLevel_SDI_Comb[i][0],LocationLevel_SDI_Comb[i][1]) for i in range(0,len(LocationLevel_SDI_Comb)))
    model.Demand_Comb1 = Set(initialize=DemandComb,dimen=2)

    model.LL_SDI2 = model.Demand_Comb1  * model.Period_ID
    model.Demand_Comb = model.Demand_Comb1  * model.Period_ID
    def Demand3(model,l,op,t):
        for i in range(0,len(LocationLevel_SDI)):
            if l==LocationLevel_SDI[i][0] and op==LocationLevel_SDI[i][1] and t==LocationLevel_SDI[i][2]:
                return int(LocationLevel_SDI[i][3])
        
        return 0
    model.Demand = Param(model.LL_SDI2,initialize = Demand3)
    def TIComb(model):
        return ((LocationLevel_STI_Comb[i][0],LocationLevel_STI_Comb[i][1],LocationLevel_STI_Comb[i][2]) 
                            for i in range(0,len(LocationLevel_STI_Comb)))
    model.TI_Comb = Set(initialize=TIComb, dimen=3)

    def LL_STI1(model):
        return ((LocationLevel_STI[i][0],LocationLevel_STI[i][1],LocationLevel_STI[i][2]) for i in range(0,len(LocationLevel_STI)))        
    model.LL_STI2 = Set(initialize=LL_STI1, dimen=3)

    def TI3(model,l,op,t):
        for i in range(0,len(LocationLevel_STI)):
            if l==LocationLevel_STI[i][0] and op==LocationLevel_STI[i][1] and t==LocationLevel_STI[i][2]:
                return (LocationLevel_STI[i][3])
    model.TI = Param(model.LL_STI2,initialize = TI3)

    def ISComb(model):
        return ((Inventory_Storage_Comb[l][0],Inventory_Storage_Comb[l][1]) for l in range(0,len(Inventory_Storage_Comb)))
    model.IS_Comb = Set(initialize=ISComb,dimen=2)


    def IS1(model):
        return ((Inventory_Storage[i][0],Inventory_Storage[i][1]) for i in range(0,len(Inventory_Storage)))
    model.IS2 = Set(initialize=IS1, dimen=2)

    def SC3(model,l,op):
        for i in range(0,len(Inventory_Storage)):
            if l==Inventory_Storage[i][0] and op==Inventory_Storage[i][1]:
                return int(Inventory_Storage[i][3])
    model.SC = Param(model.IS2,initialize = SC3)

    def SCap3(model,l,op):
        for i in range(0,len(Inventory_Storage_Comb)):
            if l==Inventory_Storage[i][0] and op==Inventory_Storage[i][1]:
                return int(Inventory_Storage[i][2])
    model.SCap = Param(model.IS2,initialize = SCap3)

    def ODLL1(model):
        return ((ODLocationLevel[i][0],ODLocationLevel[i][1],ODLocationLevel[i][2]) for i in range(0,len(ODLocationLevel)))
    model.ODLL2 = Set(initialize=ODLL1, dimen=3)

    def TC3(model,op,Lf,Lt):
        for i in range(0,len(ODLocationLevel)):
            if op==ODLocationLevel[i][0] and Lf==ODLocationLevel[i][1] and Lt==ODLocationLevel[i][2]:
                return float(ODLocationLevel[i][3])
    model.TC = Param(model.ODLL2,initialize=TC3)

    def CAM1(model):
        return ((ConversionatMachine[i][0],ConversionatMachine[i][1],ConversionatMachine[i][2]) for i in range(0,len(ConversionatMachine)))
    model.CAM = Set(initialize=CAM1, dimen=3) 
    
    
    def AR3(model,ip,op,m):
        for i in range(0,len(ConversionatMachine)):
            if ip==ConversionatMachine[i][0] and op==ConversionatMachine[i][1] and m==ConversionatMachine[i][2]:
                return float(ConversionatMachine[i][3])
    model.AR = Param(model.CAM,initialize=AR3)

    def Yield3(model,ip,op,m):
        for i in range(0,len(ConversionatMachine)):
            if ip==ConversionatMachine[i][0] and op==ConversionatMachine[i][1] and m==ConversionatMachine[i][2]:
                return (ConversionatMachine[i][4])
    model.Yield = Param(model.CAM,initialize=Yield3)

    def PCC1(model):
        return ((ProcurementCost[i][0],ProcurementCost[i][1]) for i in range(0,len(ProcurementCost)))
    model.PCC2 = Set(initialize=PCC1, dimen=2)

    def PCC5(model,ip,l):
        for i in range(0,len(ProcurementCost)):
            if ip==ProcurementCost[i][0] and l==ProcurementCost[i][1]:
                return int(ProcurementCost[i][2])
    model.PCC = Param(model.PCC2,initialize=PCC5)
    
    def TC6(model,op,Lf,Lt):
        for i in range(0,len(ODLocationLevel)):
            if (ODLocationLevel[i][0],ODLocationLevel[i][1]) in model.PCC2:
                if op==ODLocationLevel[i][0] and Lf==ODLocationLevel[i][1] and Lt==ODLocationLevel[i][2]:
                    return int((ODLocationLevel[i][4]))+model.PCC[ODLocationLevel[i][0],ODLocationLevel[i][1]]
            else:
                if op==ODLocationLevel[i][0] and Lf==ODLocationLevel[i][1] and Lt==ODLocationLevel[i][2]:
                    return int((ODLocationLevel[i][4]))
    model.TT = Param(model.ODLL2,initialize=TC6)
    
    def II1(model):
        return ((InitialInventory[i][0],InitialInventory[i][1]) for i in range(0,len(InitialInventory)))
    model.II2 = Set(initialize=II1, dimen=2)

    def II4(model,l,op):
        for i in range(0,len(InitialInventory)):
            if l==InitialInventory[i][0] and op==InitialInventory[i][1]:
                return int(InitialInventory[i][2])
        return 0
    model.II = Param(model.IS_Comb,initialize=II4)

    def VOC1(model):
        return ((ValidOPComb[i][0],ValidOPComb[i][1]) for i in range(0,len(ValidOPComb)))
    model.ValidOPComb = Set(initialize=VOC1, dimen=2)
    
##    for i in model.Demand_Comb1:
##        model.ValidOPComb.add(i)
##    for i in model.ValidOPComb:
##        print i

    def VIC1(model):
        return ((ValidIPComb[i][0],ValidIPComb[i][1]) for i in range(0,len(ValidIPComb)))
    model.ValidIPComb = Set(initialize=VIC1, dimen=2)

    for (l,p) in model.Demand_Comb1:
        model.ValidOPComb.add((l,p))
        model.ValidIPComb.add((p,l))
##    for i in model.ValidIPComb:
##        print i

##    model.Demand_Comb = model.ValidOPComb * model.Period_ID
    
    def VMC1_Uncon(model):
        return ((ValidMComb_Uncon[i][0],ValidMComb_Uncon[i][1],ValidMComb_Uncon[i][2]) for i in range(0,len(ValidMComb_Uncon)))
    model.ValidMComb_Uncon = Set(initialize=VMC1_Uncon, dimen=3)

    def VMC1_Con(model):
        return ((ValidMComb_Con[i][0],ValidMComb_Con[i][1],ValidMComb_Con[i][2]) 
                            for i in range(0,len(ValidMComb_Con)))

    if Production_Capacity_param== "Constrained" :
            model.K5_1 = Set(initialize=VMC1_Con, dimen=3)
            model.K5 = model.K5_1 * model.Period_ID
    
    def VMC1_Values(model):
        return ((ValidMComb_Values[i][0],ValidMComb_Values[i][1],ValidMComb_Values[i][2]) 
                            for i in range(0,len(ValidMComb_Values)) if (ValidMComb_Values[i][0],ValidMComb_Values[i][1],ValidMComb_Values[i][2])
                not in model.ProdResValues)
    model.ProdResValues = Set(initialize=VMC1_Values, dimen=3)

    def PRV1(model,p,l,r):
        for i in range(0,len(ValidMComb_Values)):
            if p==ValidMComb_Values[i][0] and l==ValidMComb_Values[i][1] and r==ValidMComb_Values[i][2]:
                return (ValidMComb_Values[i][5])
    model.Max_Batch_Size = Param(model.ProdResValues,initialize=PRV1)

    def PRV2(model,p,l,r):
        for i in range(0,len(ValidMComb_Values)):
            if p==ValidMComb_Values[i][0] and l==ValidMComb_Values[i][1] and r==ValidMComb_Values[i][2]:
                return (ValidMComb_Values[i][4])
    model.Min_Batch_Size = Param(model.ProdResValues,initialize=PRV2)

    def VM1(model):
        return ((ValidMIP[i][0],ValidMIP[i][1],ValidMIP[i][2]) for i in range(0,len(ValidMIP)))
    model.ValidMIP = Set(initialize=VM1, dimen=3)


    def ReceiptsComb(model):
        return ((PlannedReceipts[i][0],PlannedReceipts[i][1],PlannedReceipts[i][2]) 
                                    for i in range(0,len(PlannedReceipts)))
    model.Receipts_Comb = Set(initialize=ReceiptsComb,dimen=3)

    def Planned_Receipts(model,l,p,t):
        value = 0
        for i in range(0,len(PlannedReceipts)):
            if l==PlannedReceipts[i][0] and p==PlannedReceipts[i][1] and t==PlannedReceipts[i][2]:
                value = float(PlannedReceipts[i][3])
        # print l,p,t,value
        return value
    model.PLANNEDRECEIPT = Param(model.Receipts_Comb,initialize = Planned_Receipts)

    def LotGenerationComb(model):
        return ((Lot_Bottleneck_Data[i][0],Lot_Bottleneck_Data[i][1],Lot_Bottleneck_Data[i][2]) 
                                    for i in range(0,len(Lot_Bottleneck_Data)))
    model.Lot_Comb = Set(initialize=LotGenerationComb,dimen=3,ordered=True)

    
    def LotGeneration(model,l,op,r):
        for i in range(0,len(Lot_Bottleneck_Data)):
            if l==Lot_Bottleneck_Data[i][0] and op==Lot_Bottleneck_Data[i][1] and r==Lot_Bottleneck_Data[i][2]:
                return (Lot_Bottleneck_Data[i][3])
    model.Lot_Max_Batch_Size = Param(model.Lot_Comb,initialize=LotGeneration)

    def LotGeneration_1(model,l,op,r):
        for i in range(0,len(Lot_Bottleneck_Data)):
            if l==Lot_Bottleneck_Data[i][0] and op==Lot_Bottleneck_Data[i][1] and r==Lot_Bottleneck_Data[i][2]:
                return (Lot_Bottleneck_Data[i][4])
    model.Lot_Min_Batch_Size = Param(model.Lot_Comb,initialize=LotGeneration_1)
    
##    print '-------------------- Sets & Parameters Initialization Start ------------------------'

    model.K = model.PCC2 * model.Period_ID
    model.K2 = model.ValidIPComb * model.Period_ID
    model.K3 = model.ValidMIP * model.Period_ID
    model.K4 = model.ValidOPComb * model.Period_ID
    model.K6 = model.ODLL2 * model.Period_ID
    model.K7 = model.ML2 * model.Period_ID
    model.K8 = model.CAM * model.Period_ID
    if Production_Capacity_param <> "Constrained" :
            model.K5 = model.ValidMComb_Uncon * model.Period_ID        


##    print ' --------------------------------   Decision Variables  ------------------------'
    
    model.ProdQtyatLoc = Var(model.K4, within=NonNegativeReals)

    model.IPQtyatLoc = Var(model.K2, within=NonNegativeReals)

    model.Dependent_Demand = Var(model.K2, within=NonNegativeReals)

    model.OpQtyatLoc = Var(model.K6, within=NonNegativeReals)

    model.NDQtyatLoc = Var(model.LL_SDI2, within=NonNegativeReals)

    model.LDQtyatLoc = Var(model.LL_SDI2, within=NonNegativeReals)

##    model.LDQtyatLoc_Disp = Var(model.LL_SDI2, within=NonNegativeReals)
    model.LDQtyatLoc_Disp = Var(model.LL_SDI2, within=NonNegativeReals,initialize = 0)

    model.On_Time_Delivery = Var(model.LL_SDI2, within=NonNegativeReals)

    model.InvQtyatLoc = Var(model.IS_Comb, model.Period_ID0, within=NonNegativeReals)
    model.Spoilage = Var(model.IS_Comb, model.Period_ID0, within=NonNegativeReals)

    model.Safety_Stock = Var(model.TI_Comb, within=NonNegativeReals)

    model.SS_Violation_Qty = Var(model.TI_Comb, within=NonNegativeReals)

    model.Procured_Qty = Var(model.K, within=NonNegativeReals)

    model.ProdQtyatMac = Var(model.K5, within=NonNegativeReals)

    model.ProducedLots = Var(model.K5, within=NonNegativeIntegers,initialize = 0)

    model.OpatMac=Var(model.K5, within=NonNegativeReals)

    model.IpatMac = Var(model.K3,within=NonNegativeReals)

    model.ResourceUtilization = Var(model.RC3, within=NonNegativeReals, initialize = 0 , bounds=(0,1))


    model.TotalCost1 = Var(model.OPProduct_ID,within=NonNegativeReals)
    model.TotalProductionCost1 = Var(model.OPProduct_ID,within=NonNegativeReals)
    model.TotalDistributionCost1 = Var(model.OPProduct_ID,within=NonNegativeReals)
    model.TotalInventoryCost1 = Var(model.OPProduct_ID,within=NonNegativeReals)
    model.TotalProcurementCost1 = Var(model.OPProduct_ID,within=NonNegativeReals)

    model.Distributed_Demand = Var(model.OPProduct_ID, model.Location_ID, model.Period_ID, within=NonNegativeReals)
    model.Distributed_Supply = Var(model.OPProduct_ID, model.Location_ID, model.Period_ID, within=NonNegativeReals)
    
    print ' ----------------------------Objective ----------------------------'
             
    def Total_Cost_rule(model):
        cost= (sum(model.NDQtyatLoc[k] * 9999 for k in model.LL_SDI2) + \
               sum(model.LDQtyatLoc[k] * 999 for k in model.LL_SDI2 if int(Late_Delivery_Horizon) > 0) +\
               sum(model.ProdQtyatMac[p1,l1,r1,t1]  * (float(model.PC[p2,l2,r2])+0.1) for (p1,l1,r1,t1) in model.K5 \
                    for (p2,l2,r2) in model.ML2 if p1==p2 and l1==l2 and r1==r2 )+ \
               sum(model.OpQtyatLoc[p,lf,lt,t] * ((model.TC[p,lf,lt])+0.1) for (p,lf,lt,t) in model.K6 ) + \
               sum(model.Procured_Qty[ip,l,t]*0.1 for (ip,l,t) in model.K) + \
               sum(model.InvQtyatLoc[l,p,t] * ((model.SC[l,p])+0.1)  for (l,p) in model.IS2 for t in model.Period_ID )+
               sum(model.Spoilage[l,p,t] * 9  for (l,p) in model.IS2 for t in model.Period_ID ))
        return cost
    model.Total_Cost = Objective(rule=Total_Cost_rule,sense=minimize)


    if int(Late_Delivery_Horizon) > 0:
        print ' ----------------------------------------------- Demand_Constraint_1 ------------------------------------------------'

        def Late_Delivery_Constraint_1(model,l,p,t):
            if (l,p,t) in model.LL_SDI2 and l in model.EndNodes and t == value(model.CurrentTimeBucket) and t <> value(model.EndTimeBucket):
                return (model.ProdQtyatLoc[l,p,t] + model.LDQtyatLoc[l,p,t] + model.NDQtyatLoc[l,p,t] == value(model.Demand[l,p,t]))
            else:                
                return Constraint.Skip                
        model.Constraint1_1 = Constraint(model.Demand_Comb,rule=Late_Delivery_Constraint_1)

        print ' ----------------------------------------------- Demand_Constraint_2 ------------------------------------------------'
        def Late_Delivery_Constraint_2(model,l,p,t):
            if (l,p,t) in model.LL_SDI2 and l in model.EndNodes and value(model.CurrentTimeBucket)< t < value(model.EndTimeBucket):
                return (model.ProdQtyatLoc[l,p,t] + model.LDQtyatLoc[l,p,t] + model.NDQtyatLoc[l,p,t] == value(model.Demand[l,p,t]) + model.LDQtyatLoc[l,p,t-1])
            else:                
                return Constraint.Skip                
        model.Constraint1_2 = Constraint(model.Demand_Comb,rule=Late_Delivery_Constraint_2)

        print ' ----------------------------------------------- Demand_Constraint_3 ------------------------------------------------'

        def Late_Delivery_Constraint_3(model,l,p,t):
            if (l,p,t) in model.LL_SDI2 and l in model.EndNodes and t == value(model.EndTimeBucket) and t <> value(model.CurrentTimeBucket):
                return (model.ProdQtyatLoc[l,p,t] + model.NDQtyatLoc[l,p,t] == value(model.Demand[l,p,t]) + model.LDQtyatLoc[l,p,t-1])
            else:                
                return Constraint.Skip                
        model.Constraint1_3 = Constraint(model.Demand_Comb,rule=Late_Delivery_Constraint_3)

        print ' ----------------------------------------------- Demand_Constraint_4 ------------------------------------------------'

        def Late_Delivery_Constraint_4(model,l,p,t):
            if (l,p,t) in model.LL_SDI2 and l in model.EndNodes and t == value(model.CurrentTimeBucket) and t == value(model.EndTimeBucket):
                return (model.ProdQtyatLoc[l,p,t] + model.NDQtyatLoc[l,p,t] == value(model.Demand[l,p,t]))
            else:                
                return Constraint.Skip                
        model.Constraint1_4 = Constraint(model.Demand_Comb,rule=Late_Delivery_Constraint_4)

        print ' ----------------------------------------------- Late_Delivery_Constraint 2------------------------------------------------'

        def Late_Delivery_Constraint2(model,l,p,t):
##            print l,p,t
            if t > value(model.CurrentTimeBucket)+ int(Late_Delivery_Horizon) + 1:
                return (model.NDQtyatLoc[l,p,t-int(Late_Delivery_Horizon)-1] >=
                        model.Demand[l,p,t-int(Late_Delivery_Horizon)-1] - sum(model.ProdQtyatLoc[l,p,t-t2] for t2 in range(1,int(Late_Delivery_Horizon)+2)))
            else:
                return Constraint.Skip               
        model.Constraint1_6 = Constraint(model.Demand_Comb,rule=Late_Delivery_Constraint2)

        print ' ----------------------------------------------- Late_Delivery_Constraint 3------------------------------------------------'

        def Late_Delivery_Constraint3(model,l,p,t):
            if t > value(model.CurrentTimeBucket) + int(Late_Delivery_Horizon) + 1:
                return (model.NDQtyatLoc[l,p,t-int(Late_Delivery_Horizon)-1] <= model.Demand[l,p,t-int(Late_Delivery_Horizon)-1])
            else:
                return Constraint.Skip               
        model.Constraint1_7 = Constraint(model.Demand_Comb,rule=Late_Delivery_Constraint3)

            
    if int(Late_Delivery_Horizon) == 0:
     
        print ' ------------------------------------ Demand_Constraint_5 -------------------------------------'

        def Late_Delivery_Constraint_5(model,l,p,t):
##            print 1,l,p,t, (l,p,t) in model.LL_SDI2,l in model.EndNodes
            if (l,p,t) in model.LL_SDI2:
##                print 2,l,p,t
                # return (model.ProdQtyatLoc[l,p,t] == (model.Demand[l,p,t]))
                return (model.ProdQtyatLoc[l,p,t] + model.NDQtyatLoc[l,p,t] == (model.Demand[l,p,t]))
            else:
                return Constraint.Skip                
        model.Constraint1_5 = Constraint(model.Demand_Comb,rule=Late_Delivery_Constraint_5)


    print ' ----------------------------------------------- On time delivery ------------------------------------------------'

##    def _On_Time_Delivery_Constraint(model,l,p,t):
##        if value(model.Demand[l,p,t]) <> 0:
##            if int(Late_Delivery_Horizon) <> 0 and t == (model.CurrentTimeBucket):
##                return (model.On_Time_Delivery[l,p,t] >= model.ProdQtyatLoc[l,p,t])
##            elif int(Late_Delivery_Horizon) <> 0 and t <> (model.CurrentTimeBucket):
##                return (model.On_Time_Delivery[l,p,t] >= model.ProdQtyatLoc[l,p,t] - model.LDQtyatLoc[l,p,t-1])
##            elif int(Late_Delivery_Horizon) == 0:
##                return (model.On_Time_Delivery[l,p,t] == model.ProdQtyatLoc[l,p,t])
##            else:
##                return Constraint.Skip
##        else:
##            return (model.On_Time_Delivery[l,p,t] == 0)
##    model.On_Time_Delivery_Constraint = Constraint(model.Demand_Comb,rule=_On_Time_Delivery_Constraint)
##
##    def _On_Time_Delivery_Constraint1(model,l,p,t):
##        if value(model.Demand[l,p,t]) <> 0:           
##            return (model.On_Time_Delivery[l,p,t] <= value(model.Demand[l,p,t]))
##        else:
##            return (model.On_Time_Delivery[l,p,t] == 0)
##    model.On_Time_Delivery_Constraint1 = Constraint(model.Demand_Comb,rule=_On_Time_Delivery_Constraint1)

    
    print ' ----------------------------------------------- LD_Display ------------------------------------------------'

    if int(Late_Delivery_Horizon) > 0:
        def LD_Display_Constraint(model,l,p,t):
            if t > value(model.CurrentTimeBucket) and int(value(model.LFT[l])) == 4:
                return (model.LDQtyatLoc_Disp[l,p,t] == model.LDQtyatLoc[l,p,t-1] + int(model.Demand[l,p,t]) - model.LDQtyatLoc[l,p,t] \
                      - model.NDQtyatLoc[l,p,t])
            else:                
                return model.LDQtyatLoc_Disp[l,p,t] == 0                
        model.LD_DisplayConstraint = Constraint(model.LL_SDI2,rule=LD_Display_Constraint)
        
    
    print ' ------------------------------------ Dependent_Demand ------------------------------------------'

    def Dependent_Demand1(model,p,l,t):
        # print p,l,t
        if int(value(model.LFT[l])) == 2:
            return (model.Dependent_Demand[p,l,t] == sum(model.IpatMac[p1,l1,r,t1] for (p1,l1,r,t1) in model.K3 \
                             if p1==p and l==l1 and t1==t))
        elif int(value(model.LFT[l])) == 4:
            return (model.IPQtyatLoc[p,l,t] == model.ProdQtyatLoc[l,p,t])
        else:
            return Constraint.Skip
    model.Constraint2 = Constraint(model.K2,rule=Dependent_Demand1)

    print ' ----------------------------------------------- Transportation_LT --------------------------------------'

    def Transportation_LT(model,p,l,t):
##        print 1,l
        if l not in model.InitialNodes:
##            print 2,l
            return (sum(model.OpQtyatLoc[p1,lf,lt,t1-int(model.TT[p1,lf,lt])] for (p1,lf,lt,t1) in model.K6 \
                                    if lt==l and p1==p and t1==t and t1>=value(model.TT[p1,lf,lt]) + value(model.CurrentTimeBucket)) == model.IPQtyatLoc[p,l,t])
        else:
            return Constraint.Skip        
    model.Constraint3 = Constraint(model.K2,rule=Transportation_LT)


    print ' --------------------------------- Inventory_Balance ------------------------------------------------'

    def Inventory_Balance(model,l,p,t):
        if l not in model.EndNodes and int(value(model.LFT[l])) == 2:
            if (l,p,t) in model.Receipts_Comb:
                return ((model.PLANNEDRECEIPT[l,p,t] + model.InvQtyatLoc[l,p,t-1] + model.IPQtyatLoc[p,l,t] + \
                    model.ProdQtyatLoc[l,p,t] == model.Dependent_Demand[p,l,t] + model.Spoilage[l,p,t-1]+ model.InvQtyatLoc[l,p,t] \
                        + sum(model.OpQtyatLoc[p2,lf1,lt1,t2] for (p2,lf1,lt1,t2) in model.K6 if lf1==l and p2==p and t2==t)))
            else:
                return ((model.InvQtyatLoc[l,p,t-1] + model.IPQtyatLoc[p,l,t] + \
                    model.ProdQtyatLoc[l,p,t] == model.Dependent_Demand[p,l,t] + model.Spoilage[l,p,t-1]+ model.InvQtyatLoc[l,p,t] \
                        + sum(model.OpQtyatLoc[p2,lf1,lt1,t2] for (p2,lf1,lt1,t2) in model.K6 if lf1==l and p2==p and t2==t)))
        elif l not in model.EndNodes and l not in model.InitialNodes:
            return (model.InvQtyatLoc[l,p,t-1] + model.IPQtyatLoc[p,l,t] == model.InvQtyatLoc[l,p,t] \
                        + sum(model.OpQtyatLoc[p2,lf1,lt1,t2] for (p2,lf1,lt1,t2) in model.K6 if lf1==l and p2==p and t2==t))
        elif l in model.InitialNodes and int(value(model.LFT[l])) == 1:
            return (model.Procured_Qty[p,l,t] == sum(model.OpQtyatLoc[p2,lf1,lt1,t2] for (p2,lf1,lt1,t2) in model.K6 if lf1==l and p2==p and t2==t))   
        else:
            return Constraint.Skip
    model.Constraint4 = Constraint(model.K4,rule=Inventory_Balance)

    if RM_Capacity == "Constrained":

        print ' --------------------------------- RM_Capacity Constraint  ------------------------------------------------'
         
        def RM_Capacity_Con(model,l,p,t):
            print l,p,t, int(value(model.LFT[l]))
            if l in model.InitialNodes and int(value(model.LFT[l])) == 1:
                print 11111111
                return (model.Procured_Qty[p,l,t] == 0)
            else:
                return Constraint.Skip
        model.Constraint4_1 = Constraint(model.K4,rule=RM_Capacity_Con)
    
#     print ' --------------------------------- Spoilage Qty ------------------------------------------------'
#     def make_spoilage_zero(model,l,p,t):
#
#             if (t < int(CurrentTimeBucket[0][0]) + model.Product_Shelf_Life[p]) or int(value(model.LFT[l])) == 1:
# ##                print 1,l,p,t
# ##                print (t ,int(CurrentTimeBucket[0][0]) + model.Product_Shelf_Life[p])
#                 return model.Spoilage[l,p,t] == 0
#             elif (l,p,t) in model.Receipts_Comb and int(value(model.LFT[l])) == 2:
# ##                print 2,l,p,t
# ##                print (t ,int(CurrentTimeBucket[0][0]) + model.Product_Shelf_Life[p])
#                 return (model.Spoilage[l,p,t] >= model.InvQtyatLoc[l,p,t] -
#                     sum(model.ProdQtyatLoc[l1,p1, t1-t2] + model.PLANNEDRECEIPT[l1,p1, t1-t2]+ model.IPQtyatLoc[p1,l1, t1-t2]
#                         for (l1,p1,t1) in model.K4 if p1==p and l1==l and t1==t for t2 in range(0,model.Product_Shelf_Life[p])))
#             elif int(value(model.LFT[l])) == 2:
# ##                print 3, l,p,t
# ##                print (t ,int(CurrentTimeBucket[0][0]) + model.Product_Shelf_Life[p])
#                 return (model.Spoilage[l,p,t] >= model.InvQtyatLoc[l,p,t] -
#                     sum(model.ProdQtyatLoc[l1,p1, t1-t2] + model.IPQtyatLoc[p1,l1, t1-t2]
#                         for (l1,p1,t1) in model.K4 if p1==p and l1==l and t1==t for t2 in range(0,model.Product_Shelf_Life[p])))
#             elif (l,p,t) in model.Receipts_Comb and int(value(model.LFT[l])) == 3:
# ##                print 4,l,p,t
# ##                print (t ,int(CurrentTimeBucket[0][0]) + model.Product_Shelf_Life[p])
#                 return (model.Spoilage[l,p,t] >= model.InvQtyatLoc[l,p,t] -
#                     sum( model.PLANNEDRECEIPT[l1,p1, t1-t2]+ model.IPQtyatLoc[p1,l1, t1-t2]
#                         for (l1,p1,t1) in model.K4 if p1==p and l1==l and t1==t for t2 in range(0,model.Product_Shelf_Life[p])))
#             elif int(value(model.LFT[l])) == 3:
# ##                print 5,l,p,t
# ##                print (t ,int(CurrentTimeBucket[0][0]) + model.Product_Shelf_Life[p])
#                 return (model.Spoilage[l,p,t] >= model.InvQtyatLoc[l,p,t] -
#                     sum(model.IPQtyatLoc[p1,l1, t1-t2]
#                         for (l1,p1,t1) in model.K4 if p1==p and l1==l and t1==t for t2 in range(0,model.Product_Shelf_Life[p])))
#             elif int(value(model.LFT[l])) == 4:
#                 return model.Spoilage[l,p,t] == 0
#     model.Shelf_life_con1 = Constraint(model.IS_Comb, model.Period_ID0,rule=make_spoilage_zero)


    
    print ' -------------------------- FinalProductionAtPlant -------------------------------------'

    def FinalProductionAtPlant(model,l,p,t):
        if (int(value(model.LFT[l])) == 2):
            return (model.ProdQtyatLoc[l,p,t] == sum(model.OpatMac[p1,l1,r,t1] for (p1,l1,r,t1) in model.K5 if p1==p \
                    and l1==l and t1==t and r in model.EndMachines))
        else:
            return Constraint.Skip
    model.Constraint5 = Constraint(model.K4,rule=FinalProductionAtPlant)


    print ' -------------------------------- Production_LT(need to include Location Type condition--------------------'

    def Production_LT(model,p,l,r,t):
        if (p,l,r,t) in model.K7 and int(value(model.LFT[l])) == 2:
            return (model.OpatMac[p,l,r,t] == model.ProdQtyatMac[p,l,r,t])
        else:
            return Constraint.Skip
    model.Constraint6 = Constraint(model.K5,rule=Production_LT)


    print ' -------------- AttachRate (need to include Location Type condition)----------------------'
      
##    def AttachRate(model,p,l,r,t):
####        print p,l,r,t
##        if int(value(model.LFT[l])) == 2:
##            return (model.IpatMac[p,l,r,t] == sum((model.ProdQtyatMac[p1,l1,r1,t1] * \
##            float(model.AR[ip,op,r2])) for (ip,op,r2,t2) in model.K8  \
##                    if ip==p and op==p1 and r2==r and r1==r and t1==t and l1==l and t2==t))
##        else:
##            return Constraint.Skip
##    model.Constraint7 = Constraint(model.K3,rule=AttachRate)

    def AttachRate(model,p,l,r,t):
        if int(value(model.LFT[l])) == 2:
            return (model.IpatMac[p,l,r,t] == sum(model.ProdQtyatMac[op,l,r2,t]* float(model.AR[ip,op,r2]) \
                                                          for (ip,op,r2,t2) in model.K8 if ip==p and r2==r and t2==t))
        else:
            return Constraint.Skip
    model.Constraint7 = Constraint(model.K3,rule=AttachRate)

    print ' --------------------------------- InitialInventory -------------------------------'

    def InitialInventory5(model,l,p):
        return (model.InvQtyatLoc[l,p,value(model.CurrentTimeBucket0)] == int(model.II[l,p]))
    model.Constraint10 = Constraint(model.IS_Comb,rule=InitialInventory5)

    print ' ------------------------------- ProductionCapacityConstraint --------------------------------'
    def ProductionCapacityConstraint(model,r,l,t):
        # print "r,l,t: ", r,l,t
        if Production_Capacity_param == "Constrained" and int(value(model.LFT[l])) == 2 and model.Res_Type[r] <> 3 and (r,l,t) in model.RC3:
            return (sum(model.ProdQtyatMac[p1,l1,r1,t1]/int(model.Production_Capacity[p1,model.Res_Operation[r],l,t]) for (p1,l1,r1,t1) in model.K5  \
                    if (p1,r1,l1,t1) in model.RC2 and l1==l and r1==r and t1==t and int(model.Production_Capacity[p1,model.Res_Operation[r],l,t]) > 0  ) \
                                   == model.ResourceUtilization[r,l,t])
        else:
            return Constraint.Skip
    model.Constraint11 = Constraint(model.RM2,model.Period_ID,rule=ProductionCapacityConstraint)

    def ProductionCapacityConstraint2(model,p,l,r,t):
        if Production_Capacity_param == "Constrained" and int(value(model.LFT[l])) == 2 and model.Res_Type[r]<>3:
            return (model.ProdQtyatMac[p,l,r,t] <= int(model.Production_Capacity[p,model.Res_Operation[r],l,t]))
        else:
            return Constraint.Skip
    model.Constraint11_1 = Constraint(model.K5,rule=ProductionCapacityConstraint2)
    
##    def ProductionCapacityConstraint(model,p,r,l,t):
####        print "checking combination", p,r,l,t,(p,r,l,t) in model.RC2,(p,l,r,t) in model.K5
##        if  (p,r,l,t) in model.RC2 and (p,l,r,t) in model.K5:
####            print "success", p,r,l,t
##            if Production_Capacity_param== "Constrained" and int(value(model.LFT[l])) == 2:
####                print "success111111111", p,r,l,t
##                return (sum(model.ProdQtyatMac[p1,l1,r1,t1]/int(model.Production_Capacity[p1,model.Res_Operation[r],l,t]) for (p1,l1,r1,t1) in model.K5  \
##                    for (p2,l2,r2) in model.ML2 if p1==p2 and l1==l and l2==l and r1==r and r2==r and t1==t) \
##                                   ==model.ResourceUtilization[r,l,t] )
##            else:
####                print "Undefined Resource?"
##                return Constraint.Skip
##        else:
##            return Constraint.Skip
##    model.Constraint11 = Constraint(model.OPProduct_ID,model.RM2,model.Period_ID,rule=ProductionCapacityConstraint)

    
    print "-"*20, "model.ProducedLots", "-"*20
    model.ProducedLots
    def prod_at_machine_indicator(model,p,l,r,t):

        if model.Res_Type[r] == "Batch" and (p,l,r) in model.ProdResValues:
            return (model.ProdQtyatMac[p,l,r,t] >= int(model.Max_Batch_Size[p,l,r])*(model.ProducedLots[p,l,r,t]-1) + int(model.Min_Batch_Size[p,l,r]))
        else:
            return Constraint.Skip
    model.prod_at_machine_indicator_con12 = Constraint(model.K5, rule = prod_at_machine_indicator)

    def prod_at_machine_indicator2(model,p,l,r,t):
        if model.Res_Type[r] == "Batch" and (p,l,r) in model.ProdResValues:
            return (model.ProdQtyatMac[p,l,r,t] <= int(model.Max_Batch_Size[p,l,r])*(model.ProducedLots[p,l,r,t]))
        else:
            return Constraint.Skip
    model.prod_at_machine_indicator_con1 = Constraint(model.K5, rule = prod_at_machine_indicator2)

    print ' ------------------------------ Resource Utilization -----------------------------------------'

##    if Production_Capacity_param== "Constrained":
##        def Resource_Utilization(model,r,l,t):
##            if l not in model.EndNodes and model.Production_Capacity[r,l,t]> 0:
##                return (model.ResourceUtilization[r,l,t] == (sum(model.ProdQtyatMac[p1,l1,r1,t1] * int(model.RR[p2,l2,r2]) \
##                    for (p1,l1,r1,t1) in model.K5 for (p2,l2,r2) in model.ML2 \
##            if p2==p1 and l1==l and l2==l and t1==t and r1==r and r2==r))/int(model.Production_Capacity[r,l,t]))
##            else:
##                return (model.ResourceUtilization[r,l,t] == 0)
##        model.Constraint13 = Constraint(model.RC2,rule=Resource_Utilization)

    currenttime = datetime.datetime.now()
    f1.write("Step4: Model building completed at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.write("\n") 
    f1.write("Step5: Model execution started at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.write("\n") 
    print '------------------------------Model Solve---------------------------------------------------'

    instance = model
    if Solver_Debugging_Flag == True:
        instance.pprint()
    opt=pyomo.opt.SolverFactory("cplex")
    solver_manager=SolverManagerFactory('serial')
    Solver_Run_Time=50
    opt.options['timelimit']=50
    opt.options['mipgap']=0.0000001
    instance.write('E:\Dropbox\Public\PPDS\NO_'+time.strftime("%Y%m%d-%H%M%S")+'.lp', io_options={'symbolic_solver_labels':True})
    results=solver_manager.solve(instance,opt=opt,tee=True) #,options="ratiogap=0",timelimit=float(Solver_Run_Time)
    instance.solutions.load_from(results)
    objective1 = model.Total_Cost()
    print "First run", objective1
    solver_status = str(results.Solution.Status)
    if Solver_Debugging_Flag == True:
        results.write()
        display(instance)
        print "The solver returned a status of: " +str(results.Solution.Status)
        print "Number of Constraints: ", results.problem.Number_of_constraints
        print "Number of  variables: ", results.problem.Number_of_variables

    currenttime = datetime.datetime.now()
    f1.write("Step6: Model execution completed at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.write("\n") 
    print '----------------------------------- Solver Output  -----------------------------------'
    print '---------------------------------ProdQtyatLoc -------------------------------------'
    ProdQtyatLoc_var=getattr(instance,"ProdQtyatLoc")
    ProdQtyatLoc_val={}
    for (l,p,t) in model.K4:
        ProdQtyatLoc_val[l,p,t] = ProdQtyatLoc_var[l,p,t].value

    print '---------------------------------On_Time_Delivery -------------------------------------'
    
    On_Time_Delivery_var=getattr(instance,"On_Time_Delivery")
    On_Time_Delivery_val={}
    for (l,p,t) in model.LL_SDI2:        
        if model.LFT[l]==4 and t == model.CurrentTimeBucket and (l,p,t) in model.K4:
            On_Time_Delivery_val[l,p,t] = max(0,ProdQtyatLoc_val[l,p,t])
        elif model.LFT[l]==4 and t > model.CurrentTimeBucket and (l,p,t) in model.K4:
            if int(Late_Delivery_Horizon) > 0:
                On_Time_Delivery_val[l,p,t] = max(0,ProdQtyatLoc_val[l,p,t]-model.LDQtyatLoc[l,p,t-1].value)
            else:
                On_Time_Delivery_val[l,p,t] = max(0,ProdQtyatLoc_val[l,p,t])
                    
        else:
            On_Time_Delivery_val[l,p,t]=round(On_Time_Delivery_var[l,p,t].value,10)
            
            
    print '---------------------------------NDQtyatLoc -------------------------------------'
    NDQtyatLoc_var=getattr(instance,"NDQtyatLoc")
    NDQtyatLoc_val={}
    for (l,p,t) in model.LL_SDI2:
        NDQtyatLoc_val[l,p,t] = round(NDQtyatLoc_var[l,p,t].value,5)

##    if int(Late_Delivery_Horizon) > 0:
    print '---------------------------------LDQtyatLoc -------------------------------------'
    LDQtyatLoc_var=getattr(instance,"LDQtyatLoc")
    LDQtyatLoc_val={}
    for (l,p,t) in model.LL_SDI2:
        LDQtyatLoc_val[l,p,t] = (LDQtyatLoc_var[l,p,t].value)
        
    print '---------------------------------LDQtyatLoc_Disp -------------------------------------'
    
    LDQtyatLoc_Disp_var=getattr(instance,"LDQtyatLoc_Disp")
    LDQtyatLoc_Disp_val={}
    for (l,p,t) in model.LL_SDI2:
        if model.LFT[l]==4:
            LDQtyatLoc_Disp_val[l,p,t] = LDQtyatLoc_Disp_var[l,p,t].value - On_Time_Delivery_val[l,p,t]
        else:
            LDQtyatLoc_Disp_val[l,p,t] = 0
        
##    print '---------------------------------OpQtyatLoc -------------------------------------'
    OpQtyatLoc_var=getattr(instance,"OpQtyatLoc")
    OpQtyatLoc_val={}
    for (p,lf,lt,t) in model.K6:
        OpQtyatLoc_val[p,lf,lt,t] = round(OpQtyatLoc_var[p,lf,lt,t].value,5)
        
    print '---------------------------------ProdQtyatMac -------------------------------------'
    ProdQtyatMac_var=getattr(instance,"ProdQtyatMac")
    ProdQtyatMac_val={}
    for (p,l,r,t) in model.K5:
        ProdQtyatMac_val[p,l,r,t] = round(ProdQtyatMac_var[p,l,r,t].value,5)
        if ProdQtyatMac_val[p,l,r,t]  > 0:
            print p,l,r,t, ProdQtyatMac_val[p,l,r,t]

    print '---------------------------------OpatMac -------------------------------------'
    OpatMac_var = getattr(instance, "OpatMac")
    OpatMac_val = {}
    for (p, l, r, t) in model.K5:
        OpatMac_val[p, l, r, t] = round(OpatMac_var[p, l, r, t].value, 5)
        if OpatMac_val[p, l, r, t] > 0:
            print p, l, r, t, OpatMac_val[p, l, r, t]

    print '---------------------------------IpatMac -------------------------------------'
    IpatMac_var = getattr(instance, "IpatMac")
    IpatMac_val = {}
    for (p, l, r, t) in model.K3:
        IpatMac_val[p, l, r, t] = round(IpatMac_var[p, l, r, t].value, 5)
        if IpatMac_val[p, l, r, t] > 0:
            print p, l, r, t, IpatMac_val[p, l, r, t]

    print '---------------------------------ProducedLots -------------------------------------'
    ProducedLots_var=getattr(instance,"ProducedLots")
    ProducedLots_val={}
    for (p,l,r,t) in model.K5:
        ProducedLots_val[p,l,r,t] = round(ProducedLots_var[p,l,r,t].value,5)



 
##    print '---------------------------------Procured_Qty -------------------------------------'
    Procured_Qty_var=getattr(instance,"Procured_Qty")
    Procured_Qty_val={}
    for (p,l,t) in model.K:
##        print p,l,t
        Procured_Qty_val[p,l,t] = round(Procured_Qty_var[p,l,t].value,5)
        if Procured_Qty_val[p, l, t] > 0:
            print p, l, t, ProdQtyatMac_val[p, l, t]

    print '---------------------------------InvQtyatLoc -------------------------------------'
    InvQtyatLoc_var=getattr(instance,"InvQtyatLoc")
    InvQtyatLoc_val={}
    for (l,p) in model.IS_Comb:
        for t in model.Period_ID0:
            InvQtyatLoc_val[l,p,t] = InvQtyatLoc_var[l,p,t].value

    print '---------------------------------IPQtyatLoc -------------------------------------'
##    IPQtyatLoc_var=getattr(instance,"IPQtyatLoc")
##    IPQtyatLoc_val={}
##    for (p,l,t) in model.K2:
##        print p,l,t,IPQtyatLoc_val[p,l,t]
##        IPQtyatLoc_val[p,l,t] = round(IPQtyatLoc_var[p,l,t].value,5)

    print '---------------------------------ResourceUtilization -------------------------------------'
    if Production_Capacity_param== "Constrained":
        ResourceUtilization_var=getattr(instance,"ResourceUtilization")
        ResourceUtilization_val={}
        for (r,l,t) in model.RC3:
            ResourceUtilization_val[r,l,t] = round(ResourceUtilization_var[r,l,t].value,5)
##            print (r,l,t), ResourceUtilization_val[r,l,t]


    if Output_Data_Debugging_Flag == True:
        
        print '--------------------------------- Solver Output -------------------------------------'

        print '---------------------------------ProdQtyatLoc -------------------------------------'
        for (l,p,t) in model.K4:
            if ProdQtyatLoc_val[l,p,t] > 0:
                print l,p,t, ProdQtyatLoc_val[l,p,t]

        print '---------------------------------On_Time_Delivery -------------------------------------'

        for (l,p,t) in model.Demand_Comb:
            if On_Time_Delivery_val[l,p,t]>0:
                print l,p,t,On_Time_Delivery_val[l,p,t]

            
        print '---------------------------------NDQtyatLoc -------------------------------------'
        for (l,p,t) in model.LL_SDI2:
            if NDQtyatLoc_val[l,p,t] >0:
                print l,p,t, NDQtyatLoc_val[l,p,t]

                

        if int(Late_Delivery_Horizon) > 0:
            print '---------------------------------LDQtyatLoc_Disp -------------------------------------'

            for (l,p,t) in model.LL_SDI2:
                if LDQtyatLoc_Disp_val[l,p,t] >0:
                    print l,p,t, LDQtyatLoc_Disp_val[l,p,t]

            print '---------------------------------LDQtyatLoc -------------------------------------'
            for (l,p,t) in model.LL_SDI2:
                if LDQtyatLoc_val[l,p,t] >0:
                    print l,p,t, LDQtyatLoc_val[l,p,t]
                
        print '---------------------------------OpQtyatLoc -------------------------------------'
        for (p,lf,lt,t) in model.K6:
            if OpQtyatLoc_val[p,lf,lt,t] > 0:
                print p,lf,lt,t, OpQtyatLoc_val[p,lf,lt,t]
            
        print '---------------------------------ProdQtyatMac -------------------------------------'
        for (p,l,r,t) in model.K5:
            if ProdQtyatMac_val[p,l,r,t] > 0:
                print p,l,r,t,ProdQtyatMac_val[p,l,r,t]

        print '---------------------------------ProducedLots -------------------------------------'
        for (p,l,r,t) in model.K5:
            if ProducedLots_val[p,l,r,t] > 0:
                print p,l,r,t,ProducedLots_val[p,l,r,t]
                
        print '---------------------------------Procured_Qty -------------------------------------'
        for (p,l,t) in model.K:
            if Procured_Qty_val[p,l,t] > 0:
                print p,l,t,Procured_Qty_val[p,l,t]

        print '---------------------------------InvQtyatLoc -------------------------------------'
        for (l,p) in model.IS_Comb:
            for t in model.Period_ID0:
                if InvQtyatLoc_val[l,p,t] > 0:
                    print l,p,t, InvQtyatLoc_val[l,p,t]

        print '---------------------------------IPQtyatLoc -------------------------------------'
##        for (p,l,t) in model.K2:
##            if IPQtyatLoc_val[p,l,t] > 0:
##                print p,l,t,IPQtyatLoc_val[p,l,t]

        print '---------------------------------ResourceUtilization -------------------------------------'
        if Production_Capacity_param == "Constrained":
            for (r,l,t) in model.RC3:
                if ResourceUtilization_val[r,l,t] > 0:
                    print r,l,t, ResourceUtilization_val[r,l,t]

                
    print '--------------------------------- Data Upload -------------------------------------'      

    
    params1 = [(Client_ID,Scenario_ID,p,'NULL',lf,lt,'NULL','NULL','NULL',t,602,OpQtyatLoc_val[p,lf,lt,t]) for (p,lf,lt,t) in model.K6 if OpQtyatLoc_val[p,lf,lt,t]>0]
    sql1 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID ,LOCATION_FROM_ID,LOCATION_TO_ID, Resource_ID, ORDER_ID, OPERATION_ID, PERIOD_ID , SP_TS_ID, TS_VALUE ) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s)"
    
    params3_IN = [(Client_ID,Scenario_ID,l,p,'NULL','NULL','NULL','NULL','NULL',t,607,model.IPQtyatLoc[p,l,t].value) for (p,l,t) in model.K2 if model.IPQtyatLoc[p,l,t].value>0]
    sql3_IN = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,LOCATION_FROM_ID, OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID,ORDER_ID, OPERATION_ID, PERIOD_ID, SP_TS_ID, TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"

    params2 = [(Client_ID,Scenario_ID,p,'NULL',l,'NULL',r,'NULL','NULL',t,604,ProdQtyatMac_val[p,l,r,t]) for (p,l,r,t) in model.K5 if ProdQtyatMac_val[p,l,r,t]>0]
    sql2 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_FROM_ID,LOCATION_TO_ID, OPERATION_ID, ORDER_ID, Resource_ID, PERIOD_ID, SP_TS_ID, TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"

    params3_3 = [(Client_ID,Scenario_ID,PlantID,p,'NULL','NULL','NULL','NULL','NULL',t,603,model.Demand[l,p,t]) for (l,p,t) in model.LL_SDI2 if model.Demand[l,p,t]>0]
    sql3_3 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,LOCATION_FROM_ID, OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID,ORDER_ID, OPERATION_ID, PERIOD_ID, SP_TS_ID, TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"

    params3 = [(Client_ID,Scenario_ID,PlantID,p,'NULL','NULL','NULL','NULL','NULL',t,613,NDQtyatLoc_val[l,p,t]) for (l,p,t) in model.LL_SDI2 if NDQtyatLoc_val[l,p,t]>0]
    sql3 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,LOCATION_FROM_ID, OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID,ORDER_ID, OPERATION_ID, PERIOD_ID, SP_TS_ID, TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"
    
    params3_1 = [(Client_ID,Scenario_ID,PlantID,p,'NULL','NULL','NULL','NULL','NULL',t,612,LDQtyatLoc_Disp_val[l,p,t]) for (l,p,t) in model.LL_SDI2 if LDQtyatLoc_Disp_val[l,p,t]>0]
    sql3_1 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,LOCATION_FROM_ID, OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,PERIOD_ID, SP_TS_ID, TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"    

    params3_2 = [(Client_ID,Scenario_ID,PlantID,p,'NULL','NULL','NULL','NULL','NULL',t,611,On_Time_Delivery_val[l,p,t]) for (l,p,t) in model.Demand_Comb if On_Time_Delivery_val[l,p,t]>0]
    sql3_2 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,LOCATION_FROM_ID, OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID,ORDER_ID, OPERATION_ID, PERIOD_ID, SP_TS_ID, TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"  
    
    params4 = [(Client_ID,Scenario_ID,l,p,'NULL','NULL','NULL','NULL','NULL',t,606,InvQtyatLoc_val[l,p,t]) for (l,p) in model.IS_Comb for t in model.Period_ID if InvQtyatLoc_val[l,p,t]>0]
    sql4 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,LOCATION_FROM_ID, OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID,ORDER_ID, OPERATION_ID, PERIOD_ID, SP_TS_ID,  TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"

    params4_1 = [(Client_ID,Scenario_ID,l,p,'NULL','NULL','NULL','NULL','NULL',t,605,model.Spoilage[l,p,t].value) for (l,p) in model.IS_Comb for t in model.Period_ID if InvQtyatLoc_val[l,p,t]>0]
    sql4_1 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,LOCATION_FROM_ID, OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID,ORDER_ID, OPERATION_ID, PERIOD_ID, SP_TS_ID,  TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"

    params5 = [(Client_ID,Scenario_ID,p,l,'NULL','NULL','NULL','NULL','NULL',t,626,Procured_Qty_val[p,l,t])  for (p,l,t) in model.K if Procured_Qty_val[p,l,t]>0]
    sql5 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,OUTPUT_PRODUCT_ID, LOCATION_FROM_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID, RESOURCE_ID,ORDER_ID, OPERATION_ID,PERIOD_ID, SP_TS_ID,  TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"

    if Production_Capacity_param == "Constrained":
        params6 = [(Client_ID,Scenario_ID,'NULL',l,'NULL','NULL','NULL','NULL',r,t,610,100*ResourceUtilization_val[r,l,t])  for (r,l,t) in model.RC3 if ResourceUtilization_val[r,l,t]>0]
        sql6 = "insert into spps_solver_ts_output (CLIENT_ID,Scenario_ID,Resource_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID,INPUT_PRODUCT_ID,LOCATION_TO_ID,ORDER_ID, OPERATION_ID, PERIOD_ID, SP_TS_ID,  TS_VALUE) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s)"

    currenttime = datetime.datetime.now()
    f1.write("Step7: Output data export started at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.write("\n")

    cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
    cursor = cnx.cursor()


    cursor.executemany(sql1, params1)

    cursor.executemany(sql2, params2)
    cursor.executemany(sql3, params3)
    
    cursor.executemany(sql3_IN, params3_IN)

    cursor.executemany(sql3_1, params3_1)
    cursor.executemany(sql3_2, params3_2)
    cursor.executemany(sql3_3, params3_3)
    cursor.executemany(sql4, params4)
    cursor.executemany(sql4_1, params4_1)

    cursor.executemany(sql5, params5)
    if  Production_Capacity_param == "Constrained":
        cursor.executemany(sql6, params6)


    print '--------------------------------- Data Upload Complete -------------------------------------'
    currenttime = datetime.datetime.now()
    f1.write("Step8: Output data export completed at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    f1.close
    cursor.close()
    cnx.commit()
    cnx.close()  
    return solver_status

if __name__ == '__main__':
    # try:
        print solve_supply_network_optimizer(JOB_ID)
    #     cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
    #     cursor = cnx.cursor()
    #     cursor.execute('update SPAPP_JOB set JOB_STATUS="COMPLETED",COMMENTS="No Comments", END_TIME=NOW(),INFO="Job completed" where JOB_ID = ' + str(JOB_ID))
    #     cursor.close()
    #     cnx.commit()
    #     cnx.close()
    # except Exception as x:
    #     print x
    #     cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
    #     cursor = cnx.cursor()
    #     cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
    #     cursor.close()
    #     cnx.commit()
    #     cnx.close()
