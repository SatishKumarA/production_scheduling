import mysql.connector
import time
import datetime
import csv
import pandas as pd
import sys
from collections import defaultdict

# from DB_Credentials import DB_String,Host_String

def print_time(message):
    pass
    currenttime = datetime.datetime.now()
    print message, "--- %s seconds" %  currenttime.strftime('%H:%M:%S')

global start_time
start_time = time.clock()

def preprocesslogic(JOB_ID):
    print_time("Step0 - Preprocessing Started ")
    cnx = mysql.connector.connect(user='root', password='saddlepoint', host='localhost', database='saddlepointv6_prod2')
    cursor = cnx.cursor()

    cursor.execute('SELECT distinct Client_ID, Scenario_ID,Exception_log_link FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
    Client_ID = cursor.fetchall()
    ClientID  = Client_ID[0][0]
    ScenarioID  = Client_ID[0][1]
    cursor.execute('SELECT distinct PARAMETER_ID, PARAMETER_VALUE FROM SPAPP_PARAMETER  where MODULE_ID = 6 and Client_ID =' + str(ClientID) + ' and Scenario_ID= ' + str(ScenarioID))
    Parameter_Settings = cursor.fetchall()

    for i in xrange(0, len(Parameter_Settings)):
        if Parameter_Settings[i][0] == 602:
            Planning_Bucket = str(Parameter_Settings[i][1])
        elif Parameter_Settings[i][0] == 603:
            Planning_Horizon = str(Parameter_Settings[i][1])
            #print PlanningHorizon
        elif Parameter_Settings[i][0] == 611:
            Late_Delivery_Horizon = str(Parameter_Settings[i][1])
        elif Parameter_Settings[i][0] == 601:
            Planning_Start_Date = str(Parameter_Settings[i][1])
        # print Planning_Start_Date

    cursor.execute('SELECT distinct PERIOD_ID FROM spapp_period where period_value = "' + str(Planning_Start_Date) + '" and period_bucket = "Days" and  Client_ID =' + str(ClientID))

    CurrentTimeBucket = cursor.fetchall()
    CurrentTimeBucket0 = CurrentTimeBucket[0][0] - 1

    cursor.execute('delete from spps_relation_temp where client_id =' + str(ClientID))
    cursor.execute('delete from spps_df_input_temp where CLIENT_ID = ' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));

    cursor.execute('Delete from XLPS_PRODUCT_MASTER where client_id= ' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    cursor.execute('Delete from XLPS_PO_PRODUCTS where client_id= ' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    cursor.execute('Delete from XLPS_BOM_TEMP where client_id= ' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    cursor.execute('Delete from XLPS_TREE_TABLE where client_id= ' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    cursor.execute('Delete from XLPS_BOM where client_id= ' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    cursor.execute('Delete from XLPS_SETUP_SOLVER_READY where CLIENT_ID= ' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    cnx.commit()

    print_time( "Step1 - XLPS_PRODUCT_MASTER ")
    if Planning_Bucket == "Hours":
        cursor.execute('INSERT INTO XLPS_PRODUCT_MASTER (CLIENT_ID, Scenario_ID,LOCATION_ID,PRODUCT_ID,PRODUCT_NAME,PRODUCT_TYPE_ID,SHELF_LIFE)  \
        select distinct df1.CLIENT_ID,df1.Scenario_ID,sr.location_from_id,df1.df_value,df4.df_value,df2.df_value,df3.df_value*24 from spps_df_input df1 \
        inner join spps_relation sr on sr.relation_id =df1.relation_id and sr.client_id=df1.client_id                                                                                                                    \
        inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id                                                    \
        and df1.scenario_id=df2.scenario_id	inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id     \
        and df1.scenario_id=df3.scenario_id inner join spps_df_input df4 on df1.relation_id=df4.relation_id and df1.client_id=df4.client_id       \
        and df1.scenario_id=df4.scenario_id	where df1.df_id=2000 and df2.df_id=2003 and df3.df_id=673 and df4.df_id=2001 and \
        df1.CLIENT_ID=' + str(ClientID) + ' and df1.SCENARIO_ID = ' + str(ScenarioID));

    elif Planning_Bucket == "Minutes":
        cursor.execute('INSERT INTO XLPS_PRODUCT_MASTER (CLIENT_ID, Scenario_ID,LOCATION_ID,PRODUCT_ID,PRODUCT_NAME,PRODUCT_TYPE_ID,SHELF_LIFE)  \
                select distinct df1.CLIENT_ID,df1.Scenario_ID,sr.location_from_id,df1.df_value,df4.df_value,df2.df_value,df3.df_value*24*60 from spps_df_input df1 \
                inner join spps_relation sr on sr.relation_id =df1.relation_id and sr.client_id=df1.client_id                                                                                                                    \
                inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id                                                    \
                and df1.scenario_id=df2.scenario_id	inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id     \
                and df1.scenario_id=df3.scenario_id inner join spps_df_input df4 on df1.relation_id=df4.relation_id and df1.client_id=df4.client_id       \
                and df1.scenario_id=df4.scenario_id	where df1.df_id=2000 and df2.df_id=2003 and df3.df_id=673 and df4.df_id=2001 and \
                df1.CLIENT_ID=' + str(ClientID) + ' and df1.SCENARIO_ID = ' + str(ScenarioID));
    elif Planning_Bucket == "Seconds":
        cursor.execute('INSERT INTO XLPS_PRODUCT_MASTER (CLIENT_ID, Scenario_ID,LOCATION_ID,PRODUCT_ID,PRODUCT_NAME,PRODUCT_TYPE_ID,SHELF_LIFE)  \
                select distinct df1.CLIENT_ID,df1.Scenario_ID,sr.location_from_id,df1.df_value,df4.df_value,df2.df_value,df3.df_value*24*60*60 from spps_df_input df1 \
                inner join spps_relation sr on sr.relation_id =df1.relation_id and sr.client_id=df1.client_id                                                                                                                    \
                inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id                                                    \
                and df1.scenario_id=df2.scenario_id	inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id     \
                and df1.scenario_id=df3.scenario_id inner join spps_df_input df4 on df1.relation_id=df4.relation_id and df1.client_id=df4.client_id       \
                and df1.scenario_id=df4.scenario_id	where df1.df_id=2000 and df2.df_id=2003 and df3.df_id=673 and df4.df_id=2001 and \
                df1.CLIENT_ID=' + str(ClientID) + ' and df1.SCENARIO_ID = ' + str(ScenarioID));

    # cursor.execute('SET @i:=-1;	UPDATE XLPS_PRODUCT_MASTER SET PRODUCT_INDEX = @i:=@i+1 where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' \
    #   order by PRODUCT_TYPE_ID',multi=True);

    cursor.execute('select count(*) from XLPS_PRODUCT_MASTER where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    pm_record_count = cursor.fetchall()

    for i in xrange(0,int(str(pm_record_count[0][0]))):
        cursor.execute('UPDATE XLPS_PRODUCT_MASTER SET PRODUCT_INDEX =' + str(i) + ' where ISNULL(PRODUCT_INDEX) and CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' \
                    order by PRODUCT_TYPE_ID limit 1');
    cnx.commit()

    print_time( "Step2 - XLPS_MACHINE_SOLVER_READY ")
    cursor.execute('Delete from XLPS_MACHINE_SOLVER_READY where client_id=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));

    cursor.execute('INSERT INTO XLPS_MACHINE_SOLVER_READY(CLIENT_ID, Scenario_ID, MACHINE_ID,MACHINE_NAME, OPERATION_ID,MAXIMUM_BATCH_SIZE,TASK_SPLIT_ALLOWED)    \
    select distinct sr1.CLIENT_ID,df1.Scenario_ID,sr1.resource_ID,df2.df_value,sr1.OPERATION_ID,1000,df3.DF_VALUE from spps_relation sr1 \
    inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id                                         \
    inner join spps_df_input df2 on sr1.relation_ID= df2.relation_ID and sr1.client_id=df2.client_id and df1.SCENARIO_ID=df2.scenario_id      \
    inner join spps_df_input df3 on sr1.relation_ID= df3.relation_ID and sr1.client_id=df3.client_id and df1.SCENARIO_ID=df3.scenario_id       \
    where df1.df_id = 2060 and df2.df_id = 2061 and df3.df_id = 696 and  df1.Scenario_ID=' + str(ScenarioID) + ' and df1.client_id=' + str(ClientID));

    cursor.execute('select count(*) from XLPS_MACHINE_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    msr_record_count = cursor.fetchall()

    for i in xrange(0,int(str(msr_record_count[0][0]))):
        cursor.execute('UPDATE XLPS_MACHINE_SOLVER_READY SET ALT_INDEX_KEY =' + str(i) + ' where ISNULL(ALT_INDEX_KEY) and CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' \
                    order by operation_id limit 1');
    cnx.commit()

    if Planning_Bucket == "Hours":
        cursor.execute('update XLPS_MACHINE_SOLVER_READY xl SET AVAILABLE_START_TIME_INDEX =0,  \
                AVAILABLE_END_TIME_INDEX = AVAILABLE_START_TIME_INDEX + ' + str(Planning_Horizon) + ' + ' + str(Late_Delivery_Horizon) + ' where client_id=' + str(ClientID) + '  and Scenario_ID='+ str(ScenarioID));
    elif Planning_Bucket == "Minutes":
        cursor.execute('update XLPS_MACHINE_SOLVER_READY xl SET AVAILABLE_START_TIME_INDEX =0, \
                AVAILABLE_END_TIME_INDEX = AVAILABLE_START_TIME_INDEX + ' + str(Planning_Horizon) + ' + ' + str(Late_Delivery_Horizon) + ' where client_id=' + str(ClientID) + '  and Scenario_ID=' + str(ScenarioID));
    elif Planning_Bucket == "Seconds":
        cursor.execute('update XLPS_MACHINE_SOLVER_READY xl SET AVAILABLE_START_TIME_INDEX =0, \
                AVAILABLE_END_TIME_INDEX = AVAILABLE_START_TIME_INDEX + ' + str(Planning_Horizon) + ' + ' + str(Late_Delivery_Horizon) + ' where client_id=' + str(ClientID) + '  and Scenario_ID=' + str(ScenarioID));

    print_time( "Step3 - XLPS_OPERATION_SOLVER_READY ")

    cursor.execute('Delete from XLPS_OPERATION_SOLVER_READY where client_id=' + str(ClientID) + '  and Scenario_ID='+ str(ScenarioID)) ;

    cursor.execute('INSERT INTO XLPS_OPERATION_SOLVER_READY (CLIENT_ID,Scenario_ID,  OPERATION_ID,NO_MACHINES, OPERATION_TYPE) \
    select distinct sr1.CLIENT_ID,df1.Scenario_ID,sr1.OPERATION_ID,df2.df_value,df1.df_value from spps_relation sr1                               \
    inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id                                             \
    inner join spps_df_input df2 on df1.relation_ID= df2.relation_ID and df1.CLIENT_ID=df2.client_id and df1.scenario_id=df2.SCENARIO_ID          \
    where df1.df_id = 2077 and df2.df_id = 616 and df1.CLIENT_ID=' + str(ClientID) + '  and df1.Scenario_ID= ' + str(ScenarioID)) ;

    cursor.execute('select count(*) from XLPS_OPERATION_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    osr_record_count = cursor.fetchall()
    for i in xrange(0,int(str(osr_record_count[0][0]))):
        cursor.execute('UPDATE XLPS_OPERATION_SOLVER_READY SET OPERATION_INDEX =' + str(i) + ' where ISNULL(OPERATION_INDEX) and CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' limit 1');
    cnx.commit()

    # cursor.execute('SET @i:=-1; UPDATE XLPS_OPERATION_SOLVER_READY SET OPERATION_INDEX = @i:=@i+1 where CLIENT_ID=' + str(ClientID) + '  and Scenario_ID='+ str(ScenarioID),multi=True);

    print_time( "Step4 - XLPS_PO_SOLVER_READY_TEMP ")
    cursor.execute('Delete from XLPS_PO_SOLVER_READY_TEMP where client_id=' + str(ClientID) + ' and Scenario_ID='+ str(ScenarioID));

    cursor.execute('INSERT INTO XLPS_PO_SOLVER_READY_TEMP(CLIENT_ID, Scenario_ID, PRODUCTION_ORDER_ID,PRODUCT_ID,ORDER_QUANTITY,PRIORITY,LEVEL) \
    select distinct sr1.CLIENT_ID,df1.Scenario_ID,sr1.ORDER_ID,pm.product_index,df1.df_value, df3.df_value,pm.PRODUCT_TYPE_ID from spps_relation sr1 \
    inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id      \
    inner join spps_df_input df2 on sr1.relation_ID= df2.relation_ID and sr1.client_id=df2.client_id  and df1.scenario_id=df2.SCENARIO_ID      \
    inner join spps_df_input df3 on sr1.relation_ID= df3.relation_ID and sr1.client_id=df3.client_id  and df1.scenario_id=df3.SCENARIO_ID   \
    inner join xlps_product_master pm on pm.product_id=df2.df_value and pm.client_id=df2.client_id and pm.scenario_id=df2.scenario_id  \
    inner join spps_relation sr2 on sr2.OUTPUT_PRODUCT_ID=pm.PRODUCT_ID and sr2.CLIENT_ID=pm.CLIENT_ID    \
    where df1.df_id = 605 and df2.df_id = 636 and df3.df_id = 637  and df1.df_value>0 and df1.CLIENT_ID=' + str(ClientID) + '  and df1.Scenario_ID='+ str(ScenarioID))

    if Planning_Bucket == "Hours":
        cursor.execute("update XLPS_PO_SOLVER_READY_TEMP xl \
            inner join spps_relation sr on sr.order_id=xl.production_order_id and xl.client_id=sr.client_id \
            inner join spps_df_input df on sr.relation_ID= df.relation_ID and sr.client_id=df.client_id \
            SET ORDER_DUE_DATE_INDEX = Datediff(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')," + str(Planning_Start_Date) + " * 24 + Hour(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')) + , \
            ORDER_DUE_DATE = STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')    \
            where df.df_id = 606 and df.CLIENT_ID=" + str(ClientID) + "  and df.Scenario_ID="+ str(ScenarioID));
    elif Planning_Bucket == "Minutes":
        cursor.execute("update XLPS_PO_SOLVER_READY_TEMP xl \
            inner join spps_relation sr on sr.order_id=xl.production_order_id and xl.client_id=sr.client_id \
            inner join spps_df_input df on sr.relation_ID= df.relation_ID and sr.client_id=df.client_id \
            SET ORDER_DUE_DATE_INDEX = Datediff(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s'),STR_TO_DATE('" + str(Planning_Start_Date) + "','%m/%d/%Y %H:%i:%s')) * 24*60 + Hour(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')) * 60 +  \
            Minute(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')), \
            ORDER_DUE_DATE = STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')    \
            where df.df_id = 606 and df.CLIENT_ID=" + str(ClientID) + "  and df.Scenario_ID=" + str(ScenarioID));
    elif Planning_Bucket == "Seconds":
        cursor.execute("update XLPS_PO_SOLVER_READY_TEMP xl \
            inner join spps_relation sr on sr.order_id=xl.production_order_id and xl.client_id=sr.client_id \
            inner join spps_df_input df on sr.relation_ID= df.relation_ID and sr.client_id=df.client_id \
            SET ORDER_DUE_DATE_INDEX = Datediff(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')," + str(Planning_Start_Date) + " * 24*60*60 + Hour(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s'))*60*60+  \
            Minute(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s'))*60 + Second(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')) , \
            ORDER_DUE_DATE = STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')    \
            where df.df_id = 606 and df.CLIENT_ID=" + str(ClientID) + "  and df.Scenario_ID=" + str(ScenarioID));

    # cursor.execute('SET @i:=-1;	UPDATE XLPS_PO_SOLVER_READY_TEMP SET PRODUCTION_ORDER_INDEX = @i:=@i+1 where CLIENT_ID=' + str(ClientID) + '  and Scenario_ID=' + str(ScenarioID) + ' \
    # order by Priority, ORDER_DUE_DATE_INDEX',multi=True);

    cursor.execute('select count(*) from XLPS_PO_SOLVER_READY_TEMP where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    po_temp_record_count = cursor.fetchall()
    for i in xrange(0,int(str(po_temp_record_count[0][0]))):
        cursor.execute('UPDATE XLPS_PO_SOLVER_READY_TEMP SET PRODUCTION_ORDER_INDEX =' + str(i) + ' where ISNULL(PRODUCTION_ORDER_INDEX) and \
        CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' limit 1');
    cnx.commit()

    print_time( "Step5 - XLPS_PO_SOLVER_READY ")
    cursor.execute('Delete from XLPS_PO_SOLVER_READY where client_id=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));

    cursor.execute('INSERT INTO XLPS_PO_SOLVER_READY   \
    ( CLIENT_ID, Scenario_ID, PRODUCTION_ORDER_ID,PRODUCT_ID,ORDER_QUANTITY,PRIORITY, ORDER_DUE_DATE,ORDER_DUE_DATE_INDEX,LEVEL)     \
    select distinct po.CLIENT_ID, po.Scenario_ID,po.PRODUCTION_ORDER_ID,po.product_id,po.ORDER_QUANTITY, po.priority,po.ORDER_DUE_DATE,po.ORDER_DUE_DATE_INDEX,LEVEL from \
    XLPS_PO_SOLVER_READY_TEMP po where po.ORDER_DUE_DATE_INDEX <= ' + str(Planning_Horizon) + ' and          \
    po.CLIENT_ID=' + str(ClientID) + ' and po.Scenario_ID=' + str(ScenarioID));

    # cursor.execute('SET @i:=-1;	UPDATE XLPS_PO_SOLVER_READY SET PRODUCTION_ORDER_INDEX = @i:=@i+1 where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID) + ' \
    # order by Priority, ORDER_DUE_DATE_INDEX',multi=True);

    cursor.execute('select count(*) from XLPS_PO_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    po_record_count = cursor.fetchall()
    for i in xrange(0,int(str(po_record_count[0][0]))):
        cursor.execute('UPDATE XLPS_PO_SOLVER_READY SET PRODUCTION_ORDER_INDEX =' + str(i) + ' where ISNULL(PRODUCTION_ORDER_INDEX) and CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' order by Priority, ORDER_DUE_DATE_INDEX limit 1');
    cnx.commit()
    # cursor.execute('Delete from XLPS_PO_SOLVER_READY where production_order_index>10 and CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));

    print_time( "Step6 - XLPS_MACHINE_SOLVER_READY - INDEX CREATION ")
    cursor.execute('SELECT count(*) FROM XLPS_MACHINE_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    count = cursor.fetchall()
    cursor.execute('SELECT MIN(ALT_INDEX_KEY) FROM XLPS_MACHINE_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    sTK = cursor.fetchall()
    cursor.execute('SELECT OPERATION_ID FROM XLPS_MACHINE_SOLVER_READY where ALT_INDEX_KEY=' + str(sTK[0][0]) + ' and CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    sPrevOpID = cursor.fetchall()
    i=0
    OpCounter= 0
    while i < count[0][0]:
        cursor.execute('SELECT OPERATION_ID FROM XLPS_MACHINE_SOLVER_READY where ALT_INDEX_KEY=' + str(sTK[0][0]+i) + ' and CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
        sNextOpID = cursor.fetchall()
        if sNextOpID==sPrevOpID:
            OpCounter=OpCounter+1
        else:
            OpCounter=1
        sPrevOpID=sNextOpID
        cursor.execute('UPDATE XLPS_MACHINE_SOLVER_READY SET ALTERNATE_INDEX=' + str(OpCounter-1) + ' where ALT_INDEX_KEY=' + str(sTK[0][0]+i) + ' and CLIENT_ID = '+ str(ClientID) + ' and Scenario_ID=' + str(ScenarioID))
        i=i+1

    # cursor.execute('SET @i:=-1;	UPDATE XLPS_MACHINE_SOLVER_READY SET MACHINE_INDEX = @i:=@i+1 where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));

    cursor.execute('select count(*) from XLPS_MACHINE_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    msr_record_count = cursor.fetchall()
    for i in xrange(0,int(str(msr_record_count[0][0]))):
        cursor.execute('UPDATE XLPS_MACHINE_SOLVER_READY SET MACHINE_INDEX =' + str(i) + ' where ISNULL(MACHINE_INDEX) and CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' limit 1');
    cnx.commit()

    # cursor.execute('SET @i:=-1;	UPDATE XLPS_MACHINE_SOLVER_READY SET ALT_INDEX_KEY = @i:=@i+1 where CLIENT_ID = ' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID) + ' order by operation_id');

    cursor.execute('select count(*) from XLPS_MACHINE_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID));
    msr_record_count = cursor.fetchall()
    for i in xrange(0,int(str(msr_record_count[0][0]))):
        cursor.execute('UPDATE XLPS_MACHINE_SOLVER_READY SET ALT_INDEX_KEY =' + str(i) + ' where ISNULL(ALT_INDEX_KEY) and CLIENT_ID=' + str(ClientID) + ' and SCENARIO_ID = ' + str(ScenarioID) + ' limit 1');
    cnx.commit()

     # to handle single resources performing multiple operations, all jobs ,all operations on that machine should be added to machine jobs set
    cursor.execute('update xlps_machine_solver_ready m1 join (select CLIENT_ID, Scenario_ID, machine_id, min(machine_index) as minindex from xlps_machine_solver_ready \
       where CLIENT_ID = ' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID) + ' group by machine_id) m2 on m1.machine_id = m2.machine_id and m1.CLIENT_ID=m2.CLIENT_ID and \
       m1.Scenario_ID=m2.Scenario_ID set m1.machine_index = minindex where m1.CLIENT_ID =' + str(ClientID) + ' and m1.Scenario_ID=' + str(ScenarioID));

    print_time(  "Step7 - XLPS_ROUTE_SOLVER_READY")

    cursor.execute(' Delete from XLPS_ROUTE_SOLVER_READY where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));

    #Data to handle production relations (product, operation, resource)
    cursor.execute('select distinct CLIENT_ID,RELATION_ID,OUTPUT_PRODUCT_ID,OPERATION_ID,RESOURCE_ID from spps_relation sr where RESOURCE_ID != "NULL" and sr.client_id=' + str(ClientID));
    spps_relation_temp = pd.DataFrame(cursor.fetchall(),columns=['CLIENT_ID','RELATION_ID','PRODUCT_ID','OPERATION_ID','RESOURCE_ID'])

    #Data to handle routing relations (product, operation)
    cursor.execute('select distinct CLIENT_ID,RELATION_ID,OUTPUT_PRODUCT_ID,OPERATION_ID from spps_relation sr where RESOURCE_ID = "NULL" and sr.client_id=' + str(ClientID));
    spps_relation_temp1 = pd.DataFrame(cursor.fetchall(),columns=['CLIENT_ID','RELATION_ID','PRODUCT_ID','OPERATION_ID'])

    # DF IDs  608,609,610,670,675 are of same dimension
    cursor.execute('select CLIENT_ID, RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,SCENARIO_ID from spps_df_input df  \
                    where df.CLIENT_ID = ' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID) + ' and DF_ID in (608,609,610,670,675)');
    spps_df_input_temp = pd.DataFrame(cursor.fetchall(),columns=['CLIENT_ID','RELATION_ID','DF_ID','DF_VALUE','INCREMENT_ID','SCENARIO_ID'])

    #Pivoting them so that records with same increment ID should be grouped for same dimension
    pvt0 = spps_df_input_temp.pivot_table(index=['CLIENT_ID', 'RELATION_ID', 'INCREMENT_ID','SCENARIO_ID'],  columns='DF_ID', values='DF_VALUE', aggfunc='sum')
    #Converting the pivot table to dataframe again to join with relation table
    df_new1 = pd.DataFrame(pvt0.to_records())
    # Joining with relation table to get the dimensions
    pvt0_merge = pd.merge(df_new1, spps_relation_temp, on=['RELATION_ID', 'CLIENT_ID'], how='inner')

    # DF IDs  620,661 are of same dimension
    cursor.execute('select CLIENT_ID, RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,SCENARIO_ID from spps_df_input df  \
                    where df.CLIENT_ID = ' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID) + ' and DF_ID in (620,661)');
    spps_df_input_temp1 = pd.DataFrame(cursor.fetchall(),columns=['CLIENT_ID','RELATION_ID','DF_ID','DF_VALUE','INCREMENT_ID','SCENARIO_ID'])

    #Pivoting them so that records with same increment ID should be grouped for same dimension
    pvt1 = spps_df_input_temp1.pivot_table(index=['CLIENT_ID', 'RELATION_ID', 'INCREMENT_ID','SCENARIO_ID'],  columns='DF_ID', values='DF_VALUE', aggfunc='sum')
    #Converting the pivot table to dataframe again to join with relation table
    df_new2 = pd.DataFrame(pvt1.to_records())
    # Joining with relation table to get the dimensions
    pvt1_merge = pd.merge(df_new2, spps_relation_temp1, on=['RELATION_ID', 'CLIENT_ID'], how='inner')

    pvt0_merge.to_csv(r'C:\Users\user\Downloads\pvt0.csv')
    pvt1_merge.to_csv(r'C:\Users\user\Downloads\pvt1.csv')

    # Join routing table and production details table to get basic routing data
    df0 = pd.merge(pvt0_merge, pvt1_merge, on=['OPERATION_ID', 'PRODUCT_ID', 'CLIENT_ID','SCENARIO_ID'], how='inner')
    df0.to_csv(r'C:\Users\user\Downloads\df0.csv')
    cursor.execute('select CLIENT_ID, MACHINE_ID,MACHINE_INDEX,OPERATION_ID,ALTERNATE_INDEX, SCENARIO_ID from xlps_machine_solver_ready where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    xlps_machine_solver_ready =  pd.DataFrame(cursor.fetchall(), columns=['CLIENT_ID', 'RESOURCE_ID','MACHINE_INDEX','OPERATION_ID','ALTERNATE_INDEX', 'SCENARIO_ID'])

    cursor.execute('select CLIENT_ID, OPERATION_ID,OPERATION_INDEX, SCENARIO_ID from xlps_operation_solver_ready where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    xlps_operation_solver_ready = pd.DataFrame(cursor.fetchall(),columns=['CLIENT_ID', 'OPERATION_ID','OPERATION_INDEX', 'SCENARIO_ID'])

    cursor.execute('select CLIENT_ID, product_index,product_id,product_type_id, SCENARIO_ID  from xlps_product_master where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    xlps_product_master = pd.DataFrame(cursor.fetchall(),columns=['CLIENT_ID','PRODUCT_INDEX', 'PRODUCT_ID','PRODUCT_TYPE_ID','SCENARIO_ID'])

    cursor.execute('select CLIENT_ID,production_order_index, product_id,SCENARIO_ID from xlps_po_solver_ready where CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    xlps_po_solver_ready = pd.DataFrame(cursor.fetchall(),columns=['CLIENT_ID','PRODUCTION_ORDER_INDEX', 'PRODUCT_INDEX','SCENARIO_ID'])
    xlps_po_solver_ready.PRODUCT_INDEX = pd.to_numeric(xlps_po_solver_ready.PRODUCT_INDEX, errors='coerce')

    # Join b/w spps_df_input_temp ,spps_relation_temp
    df1 = pd.merge(spps_df_input_temp, spps_relation_temp, on=['RELATION_ID', 'CLIENT_ID'], how='inner')
    # df1_1 = pd.merge(spps_df_input_temp1, spps_relation_temp, on=['RELATION_ID', 'CLIENT_ID'], how='inner')
    # Join b/w df1 ,xlps_product_master
    df1.to_csv(r'C:\Users\user\Downloads\df1.csv')
    df2 = pd.merge(df0, xlps_product_master, on=['PRODUCT_ID', 'CLIENT_ID', 'SCENARIO_ID'], how='inner')
    df2.to_csv(r'C:\Users\user\Downloads\df2.csv')
    # Join b/w df2 ,xlps_operation_solver_ready
    df3 = pd.merge(df2, xlps_operation_solver_ready, on=['OPERATION_ID', 'CLIENT_ID', 'SCENARIO_ID'], how='inner')
    df3.to_csv(r'C:\Users\user\Downloads\df3.csv')
    # Join b/w df3 ,xlps_machine_solver_ready
    df4 = pd.merge(df3, xlps_machine_solver_ready, on=['OPERATION_ID', 'RESOURCE_ID', 'CLIENT_ID', 'SCENARIO_ID'], how='inner')
    df4.to_csv(r'C:\Users\user\Downloads\df4.csv')
    # Join b/w df4 ,xlps_po_solver_ready
    df5 = pd.merge(df4, xlps_po_solver_ready, on=['PRODUCT_INDEX', 'CLIENT_ID', 'SCENARIO_ID'], how='inner')
    df5.to_csv(r'C:\Users\user\Downloads\df5.csv',columns=['CLIENT_ID','SCENARIO_ID','PRODUCTION_ORDER_INDEX','OPERATION_INDEX','661', '620', 'ALTERNATE_INDEX', 'MACHINE_INDEX', '608','609','610','670','675','PRODUCT_ID', 'PRODUCT_INDEX','PRODUCT_TYPE_ID'])


    df6 = df5[['CLIENT_ID','SCENARIO_ID','PRODUCTION_ORDER_INDEX','OPERATION_INDEX','661', '620', 'ALTERNATE_INDEX', 'MACHINE_INDEX', '608','609','610','670','675','PRODUCT_ID', 'PRODUCT_INDEX','PRODUCT_TYPE_ID']]
    df7= df6.values.tolist()

    sql = 'INSERT INTO XLPS_ROUTE_SOLVER_READY(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ROUTING_ID,SEQUENCE_ID,ALTERNATE_INDEX,MACHINE_INDEX, \
            OPERATION_SETUP_TIME,PROCESSING_TIME,TEARDOWN_TIME,MIN_BATCH_SIZE, MAX_BATCH_SIZE,OUTPUT_PRODUCT_ID,OUTPUT_PRODUCT_INDEX,LEVEL) \
            values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'

    cursor.executemany(sql, df7)

    print_time( "Step8 - XLPS_BOM_TEMP ")
    cursor.execute('Delete from xlps_bom_temp where client_id=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));

    cursor.execute('insert into xlps_bom_temp(CLIENT_ID ,Scenario_ID, INPUT_PRODUCT,OUTPUT_PRODUCT, ATTACH_RATE)  \
	select sr.client_id,df.Scenario_ID,sr.INPUT_PRODUCT_ID,sr.OUTPUT_PRODUCT_ID, df_value from spps_relation sr inner join  \
	spps_df_input df on df.client_id=sr.client_id and df.relation_id=sr.relation_id                                          \
	where df.df_id=602 and sr.client_id=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));

    cursor.execute('update xlps_bom_temp bm    \
	inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT \
	set bm.DESIGNATION="CHILD" where pm.product_type_id=1 and pm.client_id=' + str(ClientID) + ' and pm.Scenario_ID=' + str(ScenarioID));

    cursor.execute('update xlps_bom_temp bm   \
    inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT  \
	set bm.DESIGNATION="PARENT" where pm.product_type_id=2 or pm.product_type_id=3 and pm.client_id=' + str(ClientID) + ' and pm.Scenario_ID=' + str(ScenarioID));

    cursor.execute('update xlps_bom_temp bm \
	inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.OUTPUT_PRODUCT  \
	set bm.OUTPUT_PRODUCT_INDEX=pm.PRODUCT_INDEX where pm.client_id=' + str(ClientID) + ' and pm.Scenario_ID=' + str(ScenarioID));

    i = 0;
    j = 9999;
    while (i < po_record_count[0][0]):

        cursor.execute('SELECT distinct PRODUCT_INDEX,pm.PRODUCT_ID FROM XLPS_PO_SOLVER_READY sr        \
        inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.SCENARIO_ID=sr.SCENARIO_ID \
        where PRODUCTION_ORDER_INDEX=' + str(i) + ' and pm.CLIENT_ID=' + str(ClientID) + ' and pm.Scenario_ID=' + str(ScenarioID));
        sProductID_prodID = cursor.fetchall()

        sProductID = sProductID_prodID[0][0]
        prodID = sProductID_prodID[0][1]
        cursor.execute('INSERT INTO xlps_bom_temp(CLIENT_ID ,Scenario_ID,INPUT_PRODUCT,INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT,OUTPUT_PRODUCT_INDEX,DESIGNATION) \
        VALUES ('+str(ClientID)+',"'+str(ScenarioID)+'", "'+ str(prodID) +'", ' + str(j) + ', "'+ str(prodID) +'", ' + str(j) + ', "ROOT")');
        # cnx.commit();
        cursor.execute('INSERT INTO xlps_bom_temp(CLIENT_ID ,Scenario_ID,INPUT_PRODUCT,OUTPUT_PRODUCT,OUTPUT_PRODUCT_INDEX,DESIGNATION) \
        VALUES ('+str(ClientID)+','+str(ScenarioID)+', "'+ str(prodID) +'", "'+ str(prodID) +'", ' + str(j) + ', "PARENT")');
        # cnx.commit();
        j = j+1;
        i = i+1;

    cursor.execute('update xlps_bom_temp bm \
			inner join xlps_product_master pm on pm.client_id=bm.client_id  and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT \
			set bm.INPUT_PRODUCT_INDEX=pm.PRODUCT_INDEX  \
			where bm.designation<>"ROOT" and pm.client_id=' + str(ClientID) + ' and pm.Scenario_ID=' + str(ScenarioID));
    # cnx.commit();
    cursor.execute('delete from xlps_bom_temp where INPUT_PRODUCT_INDEX = OUTPUT_PRODUCT_INDEX \
             and designation="PARENT" and client_id=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID));
    # cnx.commit();
    cursor.execute('insert into xlps_tree_table(CLIENT_ID,Scenario_ID,FG,Components)                 \
	                SELECT distinct CLIENT_ID,Scenario_ID,INPUT_PRODUCT_INDEX, GetFamilyTree2(INPUT_PRODUCT_INDEX,' + str(ClientID) + ',' + str(ScenarioID) + ') FROM xlps_bom_temp \
	                where Designation = "PARENT"  and CLIENT_ID=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID) + ' order by ID desc');
    # cnx.commit();
    cursor.execute('SELECT distinct PRODUCTION_ORDER_INDEX, PRODUCT_INDEX FROM XLPS_PO_SOLVER_READY sr \
        inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.SCENARIO_ID=sr.SCENARIO_ID  \
        where pm.CLIENT_ID=' + str(ClientID) + ' and pm.Scenario_ID=' + str(ScenarioID));
    po_sprod = cursor.fetchall()
    # cnx.commit();
    cursor.execute('SELECT ID, Components, bo.input_product_index FROM xlps_bom_temp bo \
			inner join xlps_tree_table tr on tr.CLIENT_ID = bo.CLIENT_ID and tr.Scenario_ID = bo.Scenario_ID and tr.FG = bo.INPUT_PRODUCT_INDEX \
			inner join xlps_product_master pm on bo.INPUT_PRODUCT_INDEX=pm.product_index and pm.client_id=tr.client_id and pm.SCENARIO_ID=tr.SCENARIO_ID  \
			WHERE Designation = "PARENT" and bo.CLIENT_ID=' + str(ClientID) + ' and bo.Scenario_ID=' + str(ScenarioID)+ ' order by ID desc');
    id_comp_inputprod = cursor.fetchall()

    global id_input,comp
    comp={}
    id_input = []
    # id_input = [(id_comp_inputprod[i][0],id_comp_inputprod[i][2]) for i in range(0,len(id_comp_inputprod)) if (id_comp_inputprod[i][0],id_comp_inputprod[i][2]) not in id_input]
    for i in range(0, len(id_comp_inputprod)):
        if (id_comp_inputprod[i][0], id_comp_inputprod[i][2]) not in id_input:
            id_input.append((id_comp_inputprod[i][0],id_comp_inputprod[i][2]))
            comp[(id_comp_inputprod[i][0],id_comp_inputprod[i][2])] = id_comp_inputprod[i][1]

    # i=18
    # while(i < po_record_count[0][0]):
    #     if i==18:
    for (po,sprod) in po_sprod:
        # if po==18:
            for (id,inputprod) in id_input:
                 # print id,comp,inputprod
                 if inputprod==sprod:
                    sql2 = 'insert into xlps_po_products(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,PRODUCT_ID) \
                    select  ' + str(ClientID) + ', ' + str(ScenarioID)+','+str(po)+',INPUT_PRODUCT from xlps_bom_temp \
                    where ID = trim('+str(id)+') and client_id = ' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID)
                    # print sql2
                    cursor.execute(sql2)
                    # cnx.commit()
                    components = comp[(id,inputprod)].split(",")
                    for k in range(0, len(components)):
                        # print components[k]
                        sql3 = 'insert into xlps_po_products(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,PRODUCT_ID)  \
                        select '+ str(ClientID) + ', ' + str(ScenarioID)+','+str(po)+',INPUT_PRODUCT from xlps_bom_temp \
                        where ID = trim('+str(components[k])+') and client_id=' + str(ClientID) + ' and Scenario_ID=' + str(ScenarioID);
                        # print sql3
                        cursor.execute(sql3)
                        # cnx.commit()
    # i=i+1

    cursor.execute('update xlps_po_products bm \
	        inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.PRODUCT_ID=bm.PRODUCT_ID and pm.scenario_id=bm.scenario_id \
	        set bm.PRODUCT_INDEX=pm.PRODUCT_INDEX where pm.client_id=' + str(ClientID) + ' and pm.Scenario_ID=' + str(ScenarioID));

    print( "Step9 - XLPS_BOM ")

    cursor.execute('insert into xlps_bom (client_id, scenario_id, PRODUCTION_ORDER_INDEX, OPERATION_INDEX,ROUTING_ID, ALTERNATE_INDEX,  INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT_INDEX,ATTACH_RATE,YIELD, ALTERNATE_BOM_FLAG) \
		select distinct df1.client_id,df1.Scenario_ID,rsr.production_order_index,rsr.OPERATION_INDEX, rsr.ROUTING_ID,rsr.alternate_index,   \
        sp.PRODUCT_INDEX, sp1.product_index, df1.df_value, df2.df_value, CASE WHEN df3.df_value ="" THEN 0 ELSE df3.df_value END             \
		from spps_df_input df1 inner join spps_relation sr on sr.relation_id =df1.relation_id and sr.client_id=df1.client_id \
		inner join XLPS_OPERATION_SOLVER_READY xl on sr.OPERATION_ID=xl.OPERATION_ID and sr.CLIENT_ID=xl.CLIENT_ID and df1.scenario_id=xl.scenario_id \
		inner join XLPS_ROUTE_SOLVER_READY rsr on rsr.OPERATION_INDEX=xl.OPERATION_INDEX and rsr.CLIENT_ID=xl.CLIENT_ID and df1.scenario_id=rsr.scenario_id \
		inner join xlps_po_products sp                                                                                                                       \
        on sp.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and sr.INPUT_PRODUCT_ID =sp.PRODUCT_ID and sp.client_id=rsr.client_id and sp.scenario_id=rsr.scenario_id \
        inner join xlps_po_products sp1 on sp1.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX                                                                          \
		and sr.OUTPUT_PRODUCT_ID =sp1.product_id and sp1.client_id=rsr.client_id and sp1.scenario_id=rsr.scenario_id  		                                               \
		inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id and df1.scenario_id=df2.scenario_id                                 \
        inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id and df1.scenario_id=df3.scenario_id                                  \
		where  df1.df_id=602 and df2.df_id=621 and df3.df_id=687 and  df1.client_id=' + str(ClientID) + ' and df1.Scenario_ID=' + str(ScenarioID));

    # If the same operation is there in multiple stages (diff products..RN1 and RN2 in neuland) then bom of product 1 is coming as part of Product2
    # due to the tree structure (xlps_po_products, xlps_tree_table)
    cursor.execute('delete bm.* from xlps_bom bm      \
        inner join xlps_po_solver_ready po on po.production_order_index =bm.PRODUCTION_ORDER_INDEX and po.PRODUCT_ID!=bm.OUTPUT_PRODUCT_INDEX \
        and bm.client_id=po.client_id and bm.scenario_id=po.scenario_id where bm.CLIENT_ID=' + str(ClientID) + ' and bm.Scenario_ID=' + str(ScenarioID));


    print( "Step10 - XLPS_SETUP_SOLVER_READY" )

    cursor.execute('INSERT INTO XLPS_SETUP_SOLVER_READY(CLIENT_ID,Scenario_ID, MACHINE_ID, PRODUCT_FROM, PRODUCT_TO,SETUP_TIME)        \
        select sr.CLIENT_ID,df.Scenario_ID,sr.RESOURCE_ID,pm1.PRODUCT_INDEX,pm.PRODUCT_INDEX, df.df_value from spps_relation sr             \
        INNER JOIN spps_df_input df ON sr.CLIENT_ID=df.CLIENT_ID and df.relation_id=sr.relation_id                                           \
        inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT_ID and pm.client_id=sr.client_id and pm.SCENARIO_ID=df.SCENARIO_ID \
        inner join xlps_product_master pm1 on pm1.product_id=sr.INPUT_PRODUCT_ID and pm1.client_id=sr.client_id and pm1.SCENARIO_ID=df.SCENARIO_ID \
        where  df.df_id=622 and df.CLIENT_ID = ' + str(ClientID) + ' and df.Scenario_ID=' + str(ScenarioID));

    cursor.execute('update XLPS_SETUP_SOLVER_READY xl \
        INNER JOIN xlps_machine_solver_ready msr ON xl.MACHINE_ID=msr.MACHINE_ID and xl.CLIENT_ID=msr.CLIENT_ID  and xl.Scenario_ID=msr.Scenario_ID  \
        set xl.MACHINE_INDEX= msr.machine_index where xl.CLIENT_ID = ' + str(ClientID) + ' and xl.Scenario_ID=' + str(ScenarioID));




    cursor.close()
    cnx.commit()
    cnx.close()

    print_time( "STEP 11: Preprocessing Completed ")

    End_Time = time.clock() - start_time
    print 'Time Taken', End_Time
    # time.sleep(60)
    return "Preprocess Successful"


# if __name__ == '__main__':
#     preprocesslogic(JOB_ID)