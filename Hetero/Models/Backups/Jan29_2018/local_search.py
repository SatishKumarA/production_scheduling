

import os
import datetime
from datetime import date, timedelta
from time import time
import numpy
import math
from decimal import *

#import input_data as data
from input_data import *
import scheduler  as ps
import input_data  as ip_data
import output_data  as op_data


def LSOperator(PO_TASK_AR_SEQ_ALT_COMB,PO_TASK_AR_SEQ_ALT_COMB_New,active_var,obj_value,all_machine_ends,fixed_dec,fixed_start,fixed_end,makespan_value):

  print "inside LSOperator"
  all_machine_ends_orig=[]

  for (op,m,t) in all_machine_ends:
    all_machine_ends_orig.append((op,m,t))


  active_var_new={}
  fixed_dec_new={}
  fixed_dec_orig={}
  fixed_dec_new=fixed_dec
  fixed_dec_orig=fixed_dec

  fixed_start_new={}
  start_new={}
  fixed_start_orig={}
  fixed_start_new=fixed_start
  fixed_start_orig=fixed_start

  fixed_end_new={}
  end_new={}
  fixed_end_orig={}
  fixed_end_new=fixed_end
  fixed_end_orig=fixed_end
  Iteration_set_orig = {}
  Iteration_set_orig = PO_TASK_AR_SEQ_ALT_COMB_New
  loop = 1
  decision=1
  active_var_new = active_var
  loop_start_time =time()
  Solve_Time = ip_data.Solver_Time_Limit
  print "Iteration Set"
  Removable_Set = []
  iter_solve=0
  last_failed_seq =0
  next_iteration_item_check = 0
  while decision >= 0:
    loop = 1

    #prev_seq=1
    OP_Mac_Set=[]
    for (i,j,ar,s,alt) in PO_TASK_AR_SEQ_ALT_COMB_New:
       print (i,j,ar,s,alt)
       OP_Mac_Set.append((j,alt))
    OP_Mac_Set_Unique=set(OP_Mac_Set)

    Iteration_Set_Length = len(PO_TASK_AR_SEQ_ALT_COMB_New)
    print " iteration Set Length: ", len(PO_TASK_AR_SEQ_ALT_COMB_New)
    if Iteration_Set_Length==0:
      decision=1
      break

    for (i,j,ar,s,alt) in PO_TASK_AR_SEQ_ALT_COMB_New:
      Time_Fixed_Set=[]
      New_Set=[]
##      Current_Item = []

      if decision==1:
        all_machine_ends_new=[]
        for (op,m,t) in all_machine_ends:
          if op==j:
            all_machine_ends_new.append((op,m,t))
      else:
        all_machine_ends_new=[]
        for (op,m,t) in all_machine_ends_orig:
          if op==j:
            all_machine_ends_new.append((op,m,t))

      if time() - ip_data.Process_Start_Time > int(Solve_Time):
        print " ---------------------------- Updating Schedule to DB ----------------------------"
        op_data.Output_Update()
        return True
      print " ---------------  Loop: ", loop, " ------------------------"
      print "Decision made for PO ",i+1, " for operation ", s," on alternate machine ", alt
      print "Current Iteration :", i,j,ar,s,alt

      Current_Set = []
      #PO_Set=[]
      #PO_Set.append((i,j))
      for (j1,alt1) in OP_Mac_Set_Unique:
        for (i2,ar2,s2) in ip_data.PO_AR_SEQ_COMB:
          if i2==i and s2==s:
            active_var_new[(i2,j1,ar2,s2,alt1)] = 0
            New_Set.append((i2,j1,ar2,s2,alt1))

      PO_TASK_AR_SEQ_ALT_COMB_New1 = {}

      for (i3,j3,ar3,s3,alt3) in fixed_dec_new:
        if i3<>i:
          active_var_new[(i3,j3,ar3,s3,alt3)] = 1
          New_Set.append((i3,j3,ar3,s3,alt3))
          if (i3,j3,ar3,s3,alt3) not in Iteration_set_orig:
            start_new[(i3,j3,ar3,s3,alt3)] = fixed_start_new[(i3,j3,ar3,s3,alt3)]
            end_new[(i3,j3,ar3,s3,alt3)] = fixed_end_new[(i3,j3,ar3,s3,alt3)]
            Time_Fixed_Set.append((i3,j3,ar3,s3,alt3))
        else:
          if i3==i and s3<s:
            active_var_new[(i3,j3,ar3,s3,alt3)] = 1
            New_Set.append((i3,j3,ar3,s3,alt3))
            start_new[(i3,j3,ar3,s3,alt3)] = fixed_start_new[(i3,j3,ar3,s3,alt3)]
            end_new[(i3,j3,ar3,s3,alt3)] = fixed_end_new[(i3,j3,ar3,s3,alt3)]
            Time_Fixed_Set.append((i3,j3,ar3,s3,alt3))

      done=0

      for (op,m,t) in all_machine_ends_new:
        for (i4,j4,ar4,s4,alt4) in PO_TASK_AR_SEQ_ALT_COMB:
          if i4==i and j4==j and ar4==ar and s4==s and alt4<>alt and op==j4:
            if ip_data.Machine_ID[(i4,j4,ar4,s4,alt4)]==m:
              active_var_new[(i4,j4,ar4,s4,alt4)] = 1
              New_Set.append((i4,j4,ar4,s4,alt4))
              print "Machine_End Time :", (op,m,t)
              print "Assigned to machine: ",  i4,j4,ar4,s4,alt4
##              Current_Item.append((i4,j4,ar4,s4,alt4)) # to remove from total iteration set if decision=0
              done = 1
              break
        if done==1:
          done=0
          break


      decision, obj_value_new, active_var, PO_TASK_AR_SEQ_ALT_COMB,PO_TASK_AR_SEQ_ALT_COMB_New1,fixed_dec,all_machine_ends, fixed_start,fixed_end, makespan_value_new \
                    = ps.solve_production_scheduler2(active_var_new,start_new,end_new,obj_value,New_Set,
                                                  Time_Fixed_Set,s,0,makespan_value,loop,Iteration_Set_Length,iter_solve,last_failed_seq)

      print "After solve(Loop, Decision, Obj_Value, Makespan) : ", loop, decision, obj_value_new, makespan_value_new
      print "Solve_Time: ", ps.current_time() - ip_data.Process_Start_Time

      global Next_Iteration_Item
      if decision==1:
        next_iteration_item_check=next_iteration_item_check+1
        fixed_dec_new=fixed_dec
        fixed_dec_orig=fixed_dec
        fixed_start_new=fixed_start
        fixed_start_orig = fixed_start
        fixed_end_new=fixed_end
        fixed_end_orig = fixed_end
        all_machine_ends_new=all_machine_ends
        all_machine_ends_orig=all_machine_ends

        Next_Iteration_Item = []
        Next_Iteration_Item.append((i,j,ar,s,alt))
        iter_solve=0
        print "Next_Iteration_Item: "  , Next_Iteration_Item


      if decision==0:
        active_var_new[(i,j,ar,s,alt)] = 1
        New_Set.remove((i,j,ar,s,alt))
        fixed_dec_new = fixed_dec_orig
        fixed_start_new = fixed_start_orig
        fixed_end_new = fixed_end_orig
        last_failed_seq = s

      if decision==0:
        print "ask1"

        if next_iteration_item_check==0:
            if loop == Iteration_Set_Length:
                PO_TASK_AR_SEQ_ALT_COMB_New = ps.Total_Iteration_Set
                PO_TASK_AR_SEQ_ALT_COMB_New.remove((i,j,ar,s,alt))
                for (i,j,ar,s,alt) in Removable_Set:
                    if (i,j,ar,s,alt) in PO_TASK_AR_SEQ_ALT_COMB_New:
                        PO_TASK_AR_SEQ_ALT_COMB_New.remove((i,j,ar,s,alt))
                break
            else:
              print "Removed: ", i,j,ar,s,alt
              PO_TASK_AR_SEQ_ALT_COMB_New.remove((i,j,ar,s,alt))
              Removable_Set.append((i,j,ar,s,alt))
              print "Removable_Set:", Removable_Set
              #loop = loop + 1
              break
        else:
          print "ask3"
          PO_TASK_AR_SEQ_ALT_COMB_New = Next_Iteration_Item
          iter_solve=1
          break

      obj_value=obj_value_new
      makespan_value=makespan_value_new
      #prev_seq = s

      if loop == Iteration_Set_Length:
        if decision==1:
          PO_TASK_AR_SEQ_ALT_COMB_New = PO_TASK_AR_SEQ_ALT_COMB_New1
          break

      loop = loop + 1

  print " ---------------------------- Updating Schedule to DB ----------------------------"
  op_data.Output_Update()
  return True
