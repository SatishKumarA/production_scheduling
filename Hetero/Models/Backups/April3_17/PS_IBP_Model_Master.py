
import sys
import mysql.connector
import operator
from operator import itemgetter
from ortools.constraint_solver import pywrapcp
import os
import datetime
from datetime import date, timedelta
from time import time
import numpy as np
import math as math
from decimal import *
from itertools import groupby

print "Invoking PS Master file"
PROCESS_ID = os.getpid()
print "PROCESS_ID:  ", PROCESS_ID

try:
    JOB_ID = str(sys.argv[1])
except Exception as x:
    print x, "saddlepointv6_prod2"
    JOB_ID = 11218


cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
cursor = cnx.cursor()

cursor.execute('SELECT distinct Client_ID, Scenario_ID FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
ClientID = cursor.fetchall()
CONNECTION_ID = cnx.connection_id
print " DB CONNECTION_ID :", cnx.connection_id
cursor.execute('update SPAPP_JOB set PID = ' + str(PROCESS_ID) + ' where JOB_ID = '  + str(JOB_ID))
cursor.execute('update SPAPP_JOB set DB_CONNECTION_ID = ' + str(CONNECTION_ID) + ' where JOB_ID ='  + str(JOB_ID))
cursor.close()
cnx.commit()
cnx.close()
print ClientID
Client_ID  = ClientID[0][0]
if Client_ID == 3:
    print "hetero model"
    import PS_IBP_Model_Hetero as heteromodel
else:
    print "General model"
    import PS_IBP_Model_General as generalmodel
if __name__ == '__main__':
    try:
        # cursor.close()
        # cnx.commit()
        # cnx.close()
        print heteromodel.PriorityIterationSolve()
        cnx1 = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
        cursor1 = cnx1.cursor()
        cursor1.execute('update SPAPP_JOB set JOB_STATUS="PROCESSING",COMMENTS="No Comments", INFO="Job completed" where JOB_ID = ' + str(JOB_ID))
        cursor1.close()
        cnx1.commit()
        cnx1.close()        
    except Exception as x:
        print x
        cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
        cursor = cnx.cursor()
        cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
        cursor.close()
        cnx.commit()
        cnx.close()
