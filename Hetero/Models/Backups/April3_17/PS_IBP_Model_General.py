import sys
import mysql.connector
import operator
from operator import itemgetter
from ortools.constraint_solver import pywrapcp
import os
import datetime
from datetime import date, timedelta
from time import time
import numpy as np
import math as math
from decimal import *
from itertools import groupby
import pandas as pd

print 'Production Scheduler is Invoked'

currenttime = datetime.datetime.now()
print "Step0: %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

try:
    JOB_ID = str(sys.argv[1])
except Exception as x:
    print x, "saddlepointv6_prod2"
    JOB_ID = 11079

global Process_Start_Time
Process_Start_Time = time()
active_connection = -1
cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2')
cursor = cnx.cursor()

active_connection = 0
cursor.execute('SELECT distinct Client_ID, Scenario_ID,Exception_log_link FROM SPAPP_JOB where JOb_ID =' + str(JOB_ID))
ClientID = cursor.fetchall()
global Client_ID
global Scenario_ID
Client_ID = ClientID[0][0]
Scenario_ID = ClientID[0][1]
LogFile = ClientID[0][2]

PROCESS_ID = os.getpid()
print "PROCESS_ID:  ", PROCESS_ID
# cursor.execute('update xlps_po_solver_ready set Level=3')
# cursor.execute('update xlps_route_solver_ready set Level=3')

CONNECTION_ID = cnx.connection_id
print " DB CONNECTION_ID :", cnx.connection_id
cursor.execute('update SPAPP_JOB set PID = ' + str(PROCESS_ID) + ' where JOb_ID =' + str(JOB_ID))
cursor.execute('update SPAPP_JOB set DB_CONNECTION_ID = ' + str(CONNECTION_ID) + ', PID = ' + str(
    PROCESS_ID) + ' where JOB_ID =' + str(JOB_ID))

cursor.execute('update xlps_route_solver_ready set MAX_BATCH_SIZE=100000,MIN_BATCH_SIZE=0')
currenttime = datetime.datetime.now()
global f1
f1 = open(LogFile, 'a')
f1.truncate
f1.write("-------- Production Scheduler Log ( JOB_ID - %s " % str(JOB_ID))
f1.write(") -------- \n")
f1.write("Data reading started at %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
f1.write("\n")

currenttime = datetime.datetime.now()

cursor.execute(
    'select distinct Parameter_ID, Parameter_Value FROM spapp_parameter where module_id=6 and CLIENT_ID= ' + str(
        Client_ID) + ' and Scenario_ID=' + str(Scenario_ID));
Parameters = cursor.fetchall()
global RMConstraint
for i in range(0, len(Parameters)):
    if Parameters[i][0] == 608:
        global Solver_Time_Limit
        Solver_Time_Limit = Parameters[i][1]
    elif Parameters[i][0] == 614:
        global First_Solution_Time_Limit
        First_Solution_Time_Limit = Parameters[i][1]
    elif Parameters[i][0] == 615:
        global Iteration_Time_Limit
        Iteration_Time_Limit = Parameters[i][1]
    elif Parameters[i][0] == 605:
        global ObjectiveType
        ObjectiveType = Parameters[i][1]
    elif Parameters[i][0] == 610:
        RMConstraint = Parameters[i][1]
    elif Parameters[i][0] == 602:
        global PlanningBucket
        PlanningBucket = Parameters[i][1]
    elif Parameters[i][0] == 603:
        global PlanningHorizon
        PlanningHorizon = Parameters[i][1]
    elif Parameters[i][0] == 611:
        global LDHorizon
        LDHorizon = Parameters[i][1]
    elif Parameters[i][0] == 612:
        global SchedulingType
        SchedulingType = Parameters[i][1]
    elif Parameters[i][0] == 617:
        global Freeze_Time_Fence
        Freeze_Time_Fence = int(Parameters[i][1])
    elif Parameters[i][0] == 607:
        global Sequence_Dependent_Setup_Time
        Sequence_Dependent_Setup_Time = Parameters[i][1]

#print "RMConstraint: ", RMConstraint

##  print ' ----------------------------------- Planning Start Date -----------------------------------'
cursor.execute(
    'select distinct STR_TO_DATE(Parameter_Value,"%m/%d/%Y hh:mm:ss") FROM spapp_parameter where module_id=6 and parameter_id=601 and CLIENT_ID= ' + str(
        Client_ID) + ' and Scenario_ID=' + str(Scenario_ID));
global PlanningStartDate
PlanningStartDate = cursor.fetchall()
PlanningStartDate = PlanningStartDate[0][0]
print 'PlanningStartDate : ', PlanningStartDate
print 'Freeze_Time_Fence : ', Freeze_Time_Fence
global SDST_Split
global Setuptime_with_Holiday_Inbetween
global Produce_SFG_Lot_on_FG_Lot_production
Setuptime_with_Holiday_Inbetween = "Disable"
SDST_Split = "Allowed"
print 'SDST_Split with holiday in between : ', SDST_Split
Produce_SFG_Lot_on_FG_Lot_production="Enable"

##print ' ----------------------------------- PlantID -----------------------------------'
cursor.execute('select distinct location_id FROM xlps_product_master where CLIENT_ID= ' + str(
    Client_ID) + ' and Scenario_ID=' + str(Scenario_ID));
global PlantID
PlantID = cursor.fetchall()
PlantID = PlantID[0][0]
print 'PlantID : ', PlantID

cursor.execute('select distinct MACHINE_INDEX from XLPS_MACHINE_SOLVER_READY sr where sr.CLIENT_ID= ' + str(Client_ID) + ' \
    and sr.Scenario_ID= ' + str(Scenario_ID) + ' order by MACHINE_INDEX')
global all_machines
all_machines = cursor.fetchall()

cursor.execute('select distinct OPERATION_INDEX from XLPS_MACHINE_SOLVER_READY sr \
inner join xlps_operation_solver_ready osr on osr.operation_id=sr.operation_id where  sr.OPERATION_ID="Formulation-QC" and sr.CLIENT_ID= ' + str(Client_ID) + ' \
    and sr.Scenario_ID= ' + str(Scenario_ID) + ' order by MACHINE_INDEX')
global Form_QC_ID1
Form_QC_ID1 = cursor.fetchall()
print  "Form_QC_ID1; ", Form_QC_ID1
Form_QC_ID = []
for l in range(0, len(Form_QC_ID1)):
    Form_QC_ID.append(Form_QC_ID1[l][0])
print  "Form_QC_ID; ", Form_QC_ID

cursor.execute(
    'select distinct MACHINE_INDEX,AVAILABLE_START_TIME_INDEX, AVAILABLE_END_TIME_INDEX from XLPS_MACHINE_SOLVER_READY sr where sr.CLIENT_ID= ' + str(
        Client_ID) + ' \
    and sr.Scenario_ID= ' + str(Scenario_ID) + ' order by MACHINE_INDEX')
all_machines_times = cursor.fetchall()
global Machine_Avai_Start_Time
global Machine_Avai_End_Time
Machine_Avai_Start_Time = {}
Machine_Avai_End_Time = {}
for l in range(0, len(all_machines_times)):
    Machine_Avai_Start_Time[int(all_machines_times[l][0])] = int(all_machines_times[l][1])
    Machine_Avai_End_Time[int(all_machines_times[l][0])] = int(all_machines_times[l][2])

##  print ' ----------------------------------- Products -----------------------------------'
cursor.execute('select distinct product_index from xlps_product_master where CLIENT_ID= ' + str(
    Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
Products1 = cursor.fetchall()
global Products
Products = np.zeros([len(Products1)], dtype=int)
for l in range(0, len(Products1)):
    Products[int(Products1[l][0])] = Products1[l][0]

##  print ' ----------------------------------- ProductType -----------------------------------'
cursor.execute(
    'select distinct product_index,product_type_id,convert(shelf_life, UNSIGNED INTEGER) from xlps_product_master where CLIENT_ID= ' + str(
        Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
ProductType = cursor.fetchall()
global Product_Type_ID
Product_Type_ID = np.zeros([len(ProductType)], dtype=int)
global Product_Shelf_Life
Product_Shelf_Life = {}
for l in range(0, len(ProductType)):
    Product_Type_ID[ProductType[l][0]] = (ProductType[l][1])
    Product_Shelf_Life[ProductType[l][0]] = (ProductType[l][2])

##  print ' ----------------------------------- ProductionOrders -----------------------------------'

cursor.execute('select distinct PRODUCTION_ORDER_INDEX from xlps_po_solver_ready  where \
          client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX')

all_pos1 = cursor.fetchall()
global all_pos
all_pos = []
for l in range(0, len(all_pos1)):
    all_pos.append(int(all_pos1[l][0]))

##  print ' ----------------------------------- ProductionOrders -----------------------------------'

cursor.execute('select distinct PRODUCTION_ORDER_INDEX from xlps_po_solver_ready  where \
         priority="High" and client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
    Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX')

hp_pos1 = cursor.fetchall()
global hp_pos
hp_pos = []
for l in range(0, len(hp_pos1)):
    hp_pos.append(int(hp_pos1[l][0]))

print "hp_pos: ", hp_pos

print ' ----------------------------------- Operations_Machines -----------------------------------'

cursor.execute(
    'select distinct om.OPERATION_INDEX,om.NO_MACHINES,om.operation_type from XLPS_OPERATION_SOLVER_READY om where om.CLIENT_ID= ' + str(
        Client_ID) + ' \
        and Scenario_ID=' + str(Scenario_ID))
Operations_Machines = cursor.fetchall()
print "Operations_Machines: ", Operations_Machines
global Op_Type
Op_Type = {}
for l in range(0, len(Operations_Machines)):
    Op_Type[Operations_Machines[l][0]] = int(Operations_Machines[l][2])
print "Op_Type: ", Op_Type

##  print ' ----------------------------------- Operations_Machines_New -----------------------------------'

cursor.execute('select distinct om.OPERATION_INDEX,MACHINE_INDEX from XLPS_OPERATION_SOLVER_READY om inner join XLPS_MACHINE_SOLVER_READY ms on \
        om.operation_id=ms.operation_id and om.client_id=ms.client_id and om.scenario_id=ms.scenario_id where \
        om.CLIENT_ID= ' + str(Client_ID) + ' \
        and om.Scenario_ID=' + str(Scenario_ID))
global Operations_Machines_New
Operations_Machines_New = cursor.fetchall()

##  print ' ----------------------------------- Operations_Machines -----------------------------------'

cursor.execute('select PRODUCTION_ORDER_INDEX,operation_index,ROUTING_ID,count(alternate_index) from xlps_route_solver_ready  \
        where alternate_index != -1 and \
          client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
    Scenario_ID) + ' group by PRODUCTION_ORDER_INDEX,operation_index,ROUTING_ID ')
global op_op_ar
op_op_ar = cursor.fetchall()

currenttime = datetime.datetime.now()
print "Step0:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

##  print ' ----------------------------------- Route_Master-----------------------------------'
cursor.execute('select distinct sr.PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ROUTING_ID,SEQUENCE_ID,ALTERNATE_INDEX,MACHINE_INDEX,OPERATION_SETUP_TIME, \
               PROCESSING_TIME, TEARDOWN_TIME, ORDER_QUANTITY, MIN_BATCH_SIZE, MAX_BATCH_SIZE, sr.OUTPUT_PRODUCT_INDEX \
                 From XLPS_ROUTE_SOLVER_READY sr inner join XLPS_PO_SOLVER_READY po on po.PRODUCTION_ORDER_INDEX = sr.PRODUCTION_ORDER_INDEX \
                and sr.CLIENT_ID = po.CLIENT_ID and sr.Scenario_ID=po.Scenario_ID where sr.CLIENT_ID= ' + str(
    Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
               order by sr.PRODUCTION_ORDER_INDEX ');
global Sequence_Time
Sequence_Time = cursor.fetchall()
print Sequence_Time
global Machine_ID
global Order_Quantity
global Machine_ID
global OP_Setup_Time
global Processing_Time
global Teardown_Time
global Sequence_ID
global Min_Batch_Size
Machine_ID = {}
OP_Setup_Time = {}
Processing_Time = {}
Teardown_Time = {}
Sequence_ID = {}
Order_Quantity = {}
global Max_Batch_Size
Min_Batch_Size = {}
Max_Batch_Size = {}
global Product_Index
Product_Index = {}
for l in range(0, len(Sequence_Time)):
    Machine_ID[
        int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
            Sequence_Time[l][4])] = int(Sequence_Time[l][5])
    OP_Setup_Time[
        int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
            Sequence_Time[l][4])] = int(Sequence_Time[l][6])
    Processing_Time[
        int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
            Sequence_Time[l][4])] = Sequence_Time[l][7]
    Teardown_Time[
        int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
            Sequence_Time[l][4])] = int(Sequence_Time[l][8])
    Sequence_ID[int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2])] = int(Sequence_Time[l][3])
    Order_Quantity[int(Sequence_Time[l][0])] = int(Sequence_Time[l][9])
    Min_Batch_Size[int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][4])] = int(
        Sequence_Time[l][10])
    Max_Batch_Size[int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][4])] = int(
        Sequence_Time[l][11])
    Product_Index[
        int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
            Sequence_Time[l][4])] = int(Sequence_Time[l][12])

##  print ' ----------------------------------- Route_Master2-----------------------------------'
cursor.execute('select distinct sr.PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ALTERNATE_INDEX,MACHINE_INDEX \
                 From XLPS_ROUTE_SOLVER_READY sr inner join XLPS_PO_SOLVER_READY po on po.PRODUCTION_ORDER_INDEX = sr.PRODUCTION_ORDER_INDEX \
                and sr.CLIENT_ID = po.CLIENT_ID and sr.Scenario_ID=po.Scenario_ID where sr.CLIENT_ID= ' + str(
    Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
               order by sr.PRODUCTION_ORDER_INDEX');
Sequence_Time2 = cursor.fetchall()

cursor.execute('select distinct sr.PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ALTERNATE_INDEX \
                From XLPS_ROUTE_SOLVER_READY sr inner join XLPS_PO_SOLVER_READY po on po.PRODUCTION_ORDER_INDEX = sr.PRODUCTION_ORDER_INDEX \
                and sr.CLIENT_ID = po.CLIENT_ID and sr.Scenario_ID=po.Scenario_ID where sr.CLIENT_ID= ' + str(
    Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
               order by sr.PRODUCTION_ORDER_INDEX');
global PO_TASK_ALT_COMB
PO_TASK_ALT_COMB = cursor.fetchall()

currenttime = datetime.datetime.now()
print "Step1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')
global Machine_ID2
Machine_ID2 = {}
for l in range(0, len(Sequence_Time2)):
    Machine_ID2[int(Sequence_Time2[l][0]), int(Sequence_Time2[l][1]), int(Sequence_Time2[l][2])] = int(
        Sequence_Time2[l][3])

##  print ' ----------------------------------- PO_AR_COMB -----------------------------------'
cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, ROUTING_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
               where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
        and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,ROUTING_ID ');
global PO_AR_COMB
PO_AR_COMB = cursor.fetchall()

##  print ' ----------------------------------- PO_TASK_AR_SEQ_ALT_COMB-----------------------------------'
cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
               where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
        and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,SEQUENCE_ID');
global PO_TASK_AR_SEQ_ALT_COMB
PO_TASK_AR_SEQ_ALT_COMB = cursor.fetchall()

print ' ----------------------------------- PRECEDENCE_CONST-----------------------------------'
cursor.execute('select distinct rsr.PRODUCTION_ORDER_INDEX, rsr.OPERATION_INDEX,rsr1.OPERATION_INDEX, rsr.ROUTING_ID, \
    rsr.SEQUENCE_ID,rsr1.SEQUENCE_ID, rsr.ALTERNATE_INDEX,rsr1.ALTERNATE_INDEX From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
    inner join XLPS_ROUTE_SOLVER_READY rsr1 on rsr.PRODUCTION_ORDER_INDEX=rsr1.PRODUCTION_ORDER_INDEX and rsr.routing_id=rsr1.routing_id \
    and rsr1.sequence_id=rsr.sequence_id+1 and rsr.client_id=rsr1.client_id and rsr.scenario_id=rsr1.scenario_id  where rsr.CLIENT_ID= ' + str(
    Client_ID) + '   \
    and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by  rsr.PRODUCTION_ORDER_INDEX,rsr.SEQUENCE_ID');
global PRECEDENCE_CONST_COMB
PRECEDENCE_CONST_COMB = cursor.fetchall()
##print "PRECEDENCE_CONST_COMB: ", PRECEDENCE_CONST_COMB

print ' ----------------------------------- SHELF_LIFE_CONST-----------------------------------'
cursor.execute('select distinct rsr.PRODUCTION_ORDER_INDEX, rsr.OPERATION_INDEX,rsr1.OPERATION_INDEX, rsr.ROUTING_ID, \
    rsr.SEQUENCE_ID,rsr1.SEQUENCE_ID, rsr.ALTERNATE_INDEX,rsr1.ALTERNATE_INDEX, sl.OUTPUT_PRODUCT_INDEX From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
    inner join XLPS_ROUTE_SOLVER_READY rsr1 on rsr.PRODUCTION_ORDER_INDEX=rsr1.PRODUCTION_ORDER_INDEX and rsr.routing_id=rsr1.routing_id \
        and rsr.client_id=rsr1.client_id and rsr.scenario_id=rsr1.scenario_id  \
    inner join xlps_shelf_life2 sl on sl.production_order_index=rsr.production_order_index and sl.min_sequence=rsr.SEQUENCE_ID \
    and sl.max_sequence=rsr1.SEQUENCE_ID and rsr.CLIENT_ID=sl.client_id and rsr.SCENARIO_ID=sl.scenario_id \
    where rsr.CLIENT_ID=' + str(Client_ID) + ' and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by  rsr.SEQUENCE_ID');

SHELF_LIFE_COMB1 = cursor.fetchall()

global SHELF_LIFE_COMB2
global SHELF_LIFE_COMB
SHELF_LIFE_COMB2 = []
for l in range(0, len(SHELF_LIFE_COMB1)):
    SHELF_LIFE_COMB2.append((int(SHELF_LIFE_COMB1[l][0]), int(SHELF_LIFE_COMB1[l][1]), int(SHELF_LIFE_COMB1[l][2]),
                             int(SHELF_LIFE_COMB1[l][3]), int(SHELF_LIFE_COMB1[l][4]), int(SHELF_LIFE_COMB1[l][5]),
                             int(SHELF_LIFE_COMB1[l][6]), int(SHELF_LIFE_COMB1[l][7])))

SHELF_LIFE_COMB = set(SHELF_LIFE_COMB2)
global Shelf_Life_Prod_Index
Shelf_Life_Prod_Index = {}
for l in range(0, len(SHELF_LIFE_COMB1)):
    Shelf_Life_Prod_Index[int(SHELF_LIFE_COMB1[l][0]), int(SHELF_LIFE_COMB1[l][1]), int(SHELF_LIFE_COMB1[l][2]), int(
        SHELF_LIFE_COMB1[l][3]), int(SHELF_LIFE_COMB1[l][4]), int(SHELF_LIFE_COMB1[l][5]), int(
        SHELF_LIFE_COMB1[l][6]), int(SHELF_LIFE_COMB1[l][7])] = int(SHELF_LIFE_COMB1[l][8])

##print "SHELF_LIFE_COMB: ", SHELF_LIFE_COMB
currenttime = datetime.datetime.now()
print "Step2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

##  print ' ----------------------------------- PO_AR_SEQ_COMB-----------------------------------'
cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, ROUTING_ID, SEQUENCE_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
               where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
        and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX, SEQUENCE_ID');
global PO_AR_SEQ_COMB
PO_AR_SEQ_COMB = cursor.fetchall()

##  print ' ----------------------------------- PO_AR_SEQ_OUTP_COMB-----------------------------------'
cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, rsr.operation_index, rsr.ROUTING_ID, SEQUENCE_ID, bm.OUTPUT_PRODUCT_INDEX From XLPS_ROUTE_SOLVER_READY rsr \
inner join xlps_po_solver_ready po on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id  \
inner join xlps_bom bm on bm.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and bm.ROUTING_ID=rsr.ROUTING_ID \
and bm.OPERATION_INDEX=rsr.OPERATION_INDEX and rsr.ALTERNATE_INDEX=bm.ALTERNATE_INDEX and rsr.CLIENT_ID=bm.client_id and rsr.SCENARIO_ID=bm.SCENARIO_ID         \
               where rsr.CLIENT_ID= ' + str(Client_ID) + '   and rsr.Scenario_ID=' + str(
    Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX, SEQUENCE_ID');
global PO_TASK_AR_SEQ_OUTP_COMB
PO_TASK_AR_SEQ_OUTP_COMB = cursor.fetchall()

##  print ' ----------------------------------- PO_TASK_AR_SEQ_COMB-----------------------------------'
cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
        where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
        and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,SEQUENCE_ID');
global PO_TASK_AR_SEQ_COMB
PO_TASK_AR_SEQ_COMB = cursor.fetchall()

##  print ' ----------------------------------- PO_BOM_COMB -----------------------------------'

cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, ROUTING_ID,alternate_index, INPUT_PRODUCT_INDEX,bm.OUTPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
      where bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
    Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ');
global PO_BOM_COMB
PO_BOM_COMB = cursor.fetchall()

print ' ----------------------------------- Alternate_BOM -----------------------------------'

cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, ROUTING_ID, alternate_index, INPUT_PRODUCT_INDEX,bm.OUTPUT_PRODUCT_INDEX, \
            bm.alternate_bom_flag from xlps_bom bm inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
      where bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
    Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ');
global Alternate_BOM
Alternate_BOM = cursor.fetchall()
##Alternate_BOM={}

print "Alternate_BOM: ", Alternate_BOM
global Alt_BOM_Comb
global Alt_BOM_Flag
Alt_BOM_Comb = []
Alt_BOM_Flag = {}
for l in range(0, len(Alternate_BOM)):
    Alt_BOM_Flag[
        int(Alternate_BOM[l][0]), int(Alternate_BOM[l][1]), int(Alternate_BOM[l][2]), int(Alternate_BOM[l][3]), int(
            Alternate_BOM[l][4]), int(Alternate_BOM[l][5])] = int(Alternate_BOM[l][6])
    Alt_BOM_Comb.append((int(Alternate_BOM[l][0]), int(Alternate_BOM[l][1]), int(Alternate_BOM[l][2]),
                         int(Alternate_BOM[l][3]), int(Alternate_BOM[l][4]), int(Alternate_BOM[l][5])))

print ' ----------------------------------- Alt_BOM_Start_Value -----------------------------------'

cursor.execute(
    'select distinct production_order_index, ALTERNATE_BOM_FLAG from xlps_bom where ALTERNATE_BOM_FLAG mod 10 = 1 and client_id= ' + str(
        Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX ');
global Alt_BOM_Start_Value
Alt_BOM_Start_Value = cursor.fetchall()

print "Alt_BOM_Start_Value: ", Alt_BOM_Start_Value

print ' ----------------------------------- Alternate_BOM_COMB -----------------------------------'

cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, bm.OUTPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
      where  bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
    Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX');
global Alternate_BOM_COMB
Alternate_BOM_COMB = cursor.fetchall()
##Alternate_BOM_COMB={}

print ' ----------------------------------- Alternate_BOM_Products -----------------------------------'

cursor.execute('select distinct INPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
      where bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
    Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX');
global Alternate_BOM_Products1
Alternate_BOM_Products1 = cursor.fetchall()
##Alternate_BOM_Products1={}
global Alternate_BOM_Products
Alternate_BOM_Products = []
for l in range(0, len(Alternate_BOM_Products1)):
    Alternate_BOM_Products.append(int(Alternate_BOM_Products1[l][0]))

print "Alternate_BOM_Products: ", Alternate_BOM_Products

print ' ----------------------------------- PO_Alternate_BOM_Products -----------------------------------'
cursor.execute('  select distinct po.production_order_index, INPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id  \
      where bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ');

po_products_alt_bom_comb = cursor.fetchall()

currenttime = datetime.datetime.now()
print "Step3:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

if Sequence_Dependent_Setup_Time == "Include":

    cursor.execute('select production_order_index,product_id  from  xlps_po_solver_ready  where  CLIENT_ID= ' + str(
        Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))
    po_products_comb = cursor.fetchall()

    cursor.execute('select distinct MACHINE_INDEX, product_from, product_to, convert(SETUP_TIME, UNSIGNED INTEGER) \
  From XLPS_SETUP_SOLVER_READY  where  CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))
    setup_all_data = cursor.fetchall()

    currenttime = datetime.datetime.now()
    print "Step3_1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    SETUP_TIME_NEW0 = []
    SETUP_TIME_NEW = []
    SETUP_TIME_NEW2 = []
    SETUP_TIME_NEW3 = []
    for s in range(0, len(setup_all_data)):
        # if int(setup_all_data[s][0])==1:
        for p in range(0, len(po_products_comb)):
            if int(setup_all_data[s][1]) == int(po_products_comb[p][1]):
                for p1 in range(0, len(po_products_comb)):
                    if int(setup_all_data[s][2]) == int(po_products_comb[p1][1]) and int(po_products_comb[p][0]) != int(
                            po_products_comb[p1][0]):
                        SETUP_TIME_NEW0.append((setup_all_data[s][0], po_products_comb[p][0], po_products_comb[p1][0],
                                                setup_all_data[s][3]))
                        if int(po_products_comb[p][0]) < int(po_products_comb[p1][0]):
                            SETUP_TIME_NEW.append((
                                                  setup_all_data[s][0], po_products_comb[p][0], po_products_comb[p1][0],
                                                  setup_all_data[s][3]))
                            SETUP_TIME_NEW3.append((setup_all_data[s][0], po_products_comb[p][0],
                                                    po_products_comb[p1][0], setup_all_data[s][3]))
                        if int(po_products_comb[p][0]) > int(po_products_comb[p1][0]):
                            SETUP_TIME_NEW2.append((setup_all_data[s][0], po_products_comb[p][0],
                                                    po_products_comb[p1][0], setup_all_data[s][3]))

    currenttime = datetime.datetime.now()
    print "Step3_2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    # print ' ----------------------------------- SETUP_TIME_NEW0 -----------------------------------'
    # cursor.execute('select distinct MACHINE_INDEX, po.production_order_index,po1.production_order_index, convert(SETUP_TIME, UNSIGNED INTEGER) \
    # From XLPS_SETUP_SOLVER_READY st \
    # inner join xlps_po_products po on po.product_index=st.product_from and po.client_id=st.client_id and po.scenario_id=st.scenario_id \
    # inner join xlps_po_products po1 on po1.product_index=st.product_to and po1.client_id=st.client_id and po1.scenario_id=st.scenario_id  \
    # where  st.CLIENT_ID=3  and  st.CLIENT_ID= ' + str(Client_ID)+ ' and st.Scenario_ID=' + str(Scenario_ID))
    # SETUP_TIME_NEW0 = cursor.fetchall()

    global SETUP_TIME_NEW_COMB_ALL
    SETUP_TIME_NEW_COMB_ALL = []

    for l in range(0, len(SETUP_TIME_NEW0)):
        SETUP_TIME_NEW_COMB_ALL.append(
            (int(SETUP_TIME_NEW0[l][0]), int(SETUP_TIME_NEW0[l][1]), int(SETUP_TIME_NEW0[l][2])))

    ##  SETUP_TIME_NEW_COMB_ALL = set(SETUP_TIME_NEW_COMB_ALL)

    global SDST
    SDST = {}
    for l in range(0, len(SETUP_TIME_NEW0)):
        # print "SDST: ", int(SETUP_TIME_NEW0[l][0]),int(SETUP_TIME_NEW0[l][1]),int(SETUP_TIME_NEW0[l][2])
        SDST[int(SETUP_TIME_NEW0[l][0]), int(SETUP_TIME_NEW0[l][1]), int(SETUP_TIME_NEW0[l][2])] = int(
            SETUP_TIME_NEW0[l][3])

    currenttime = datetime.datetime.now()
    print "Step3_1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    print ' ----------------------------------- SETUP_TIME_NEW -----------------------------------'
    # cursor.execute('select distinct st.MACHINE_INDEX, po.production_order_index,po1.production_order_index, convert(SETUP_TIME, UNSIGNED INTEGER) \
    # From XLPS_SETUP_SOLVER_READY st  \
    # inner join xlps_po_solver_ready po on po.product_id=st.product_from and po.client_id=st.client_id and po.scenario_id=st.scenario_id \
    # inner join xlps_po_solver_ready po1 on po1.product_id=st.product_to and po1.client_id=st.client_id and po1.scenario_id=st.scenario_id \
    # where exists (select st1.MACHINE_INDEX,po2.production_order_index,po3.production_order_index \
    # From XLPS_SETUP_SOLVER_READY st1 \
    # inner join xlps_po_solver_ready po2 on po2.product_id=st1.product_from and po2.client_id=st1.client_id and po2.scenario_id=st1.scenario_id \
    # inner join xlps_po_solver_ready po3 on po3.product_id=st1.product_to and po3.client_id=st1.client_id and po3.scenario_id=st1.scenario_id  \
    # where  st1.CLIENT_ID= ' + str(Client_ID) + ' and st1.Scenario_ID=' + str(Scenario_ID) + ' and po2.production_order_index>po3.production_order_index \
    # and st.machine_index=st1.machine_index and po2.PRODUCTION_ORDER_INDEX=po1.production_order_index  and po3.production_order_index=po.production_order_index) \
    # and st.CLIENT_ID= 3 and st.CLIENT_ID= ' + str(Client_ID) + ' and st.Scenario_ID=' + str(Scenario_ID) + ' and po.production_order_index<po1.production_order_index')
    #
    # SETUP_TIME_NEW = cursor.fetchall()

    # for l in range(0,len(SETUP_TIME_NEW)):
    #  print SETUP_TIME_NEW[l][0], SETUP_TIME_NEW[l][1], SETUP_TIME_NEW[l][2], SETUP_TIME_NEW[l][3]
    global SETUP_TIME_NEW_COMB
    SETUP_TIME_NEW_COMB = []
    for l in range(0, len(SETUP_TIME_NEW)):
        SETUP_TIME_NEW_COMB.append((int(SETUP_TIME_NEW[l][0]), int(SETUP_TIME_NEW[l][1]), int(SETUP_TIME_NEW[l][2])))

    SETUP_TIME_NEW_COMB = set(SETUP_TIME_NEW_COMB)

    currenttime = datetime.datetime.now()
    print "Step3_2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    print ' ----------------------------------- SETUP_TIME_NEW2 -----------------------------------'
    # cursor.execute('select distinct st.MACHINE_INDEX, po.production_order_index,po1.production_order_index \
    # From XLPS_SETUP_SOLVER_READY st  \
    # inner join xlps_po_products po on po.product_index=st.product_from and po.client_id=st.client_id and po.scenario_id=st.scenario_id \
    # inner join xlps_po_products po1 on po1.product_index=st.product_to and po1.client_id=st.client_id and po1.scenario_id=st.scenario_id  \
    # where  st.CLIENT_ID=3  and st.CLIENT_ID= ' + str(Client_ID)+ ' and st.Scenario_ID=' + str(Scenario_ID) + ' and po.production_order_index>po1.production_order_index')
    #
    # SETUP_TIME_NEW2 = cursor.fetchall()
    global SETUP_TIME_NEW_COMB2
    SETUP_TIME_NEW_COMB2 = []

    for l in range(0, len(SETUP_TIME_NEW2)):
        SETUP_TIME_NEW_COMB2.append(
            (int(SETUP_TIME_NEW2[l][0]), int(SETUP_TIME_NEW2[l][1]), int(SETUP_TIME_NEW2[l][2])))

    SETUP_TIME_NEW_COMB2 = set(SETUP_TIME_NEW_COMB2)

    currenttime = datetime.datetime.now()
    print "Step3_3:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    print ' ----------------------------------- SETUP_TIME_NEW3 -----------------------------------'
    #
    # cursor.execute('select distinct st.MACHINE_INDEX, po.production_order_index,po1.production_order_index \
    # From XLPS_SETUP_SOLVER_READY st  \
    # inner join xlps_po_products po on po.product_index=st.product_from and po.client_id=st.client_id and po.scenario_id=st.scenario_id \
    # inner join xlps_po_products po1 on po1.product_index=st.product_to and po1.client_id=st.client_id and po1.scenario_id=st.scenario_id  \
    # where st.CLIENT_ID=3  and st.CLIENT_ID= ' + str(Client_ID)+ ' and st.Scenario_ID=' + str(Scenario_ID) + ' and po.production_order_index<po1.production_order_index')
    #
    # SETUP_TIME_NEW3 = cursor.fetchall()
    global SETUP_TIME_NEW_COMB3
    SETUP_TIME_NEW_COMB3 = []

    for l in range(0, len(SETUP_TIME_NEW3)):
        SETUP_TIME_NEW_COMB3.append(
            (int(SETUP_TIME_NEW3[l][0]), int(SETUP_TIME_NEW3[l][1]), int(SETUP_TIME_NEW3[l][2])))

    SETUP_TIME_NEW_COMB3 = set(SETUP_TIME_NEW_COMB3)

    currenttime = datetime.datetime.now()
    print "Step3_4:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    print ' ----------------------------------- SETUP_MACHINES-----------------------------------'

    cursor.execute('select distinct MACHINE_INDEX from XLPS_SETUP_SOLVER_READY st where st.CLIENT_ID= ' + str(
        Client_ID) + ' and st.Scenario_ID=' + str(Scenario_ID))
    global SETUP_MACHINES
    SETUP_MACHINES = cursor.fetchall()

    ##for l in range(0,len(SETUP_MACHINES)):
    ##  print SETUP_MACHINES[l][0]

print ' ----------------------------------- ORDER_DUE_DATE-----------------------------------'
cursor.execute('select distinct PRODUCTION_ORDER_INDEX, ORDER_DUE_DATE_INDEX, PRIORITY, PRODUCT_ID From XLPS_PO_SOLVER_READY \
where CLIENT_ID= ' + str(Client_ID) + ' \
    and Scenario_ID=' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX ');
global ORDER_DUE_DATE
ORDER_DUE_DATE = cursor.fetchall()

# print "ORDER_DUE_DATE: ", ORDER_DUE_DATE

print ' ----------------------------------- MACHINE_DETAILS-----------------------------------'
cursor.execute('select distinct MACHINE_INDEX, OPERATION_INDEX, ALTERNATE_INDEX, AVAILABLE_START_TIME_INDEX, AVAILABLE_END_TIME_INDEX From \
                XLPS_MACHINE_SOLVER_READY msd inner join XLPS_OPERATION_SOLVER_READY osd on msd.OPERATION_ID=osd.OPERATION_ID and \
                msd.CLIENT_ID=osd.CLIENT_ID and msd.Scenario_ID=osd.Scenario_ID where msd.CLIENT_ID= ' + str(
    Client_ID) + ' and msd.Scenario_ID=' + str(Scenario_ID))
Machine_Details = cursor.fetchall()
global Machine_Available_Start_Time
global Machine_Available_End_Time
Machine_Available_Start_Time = np.zeros([len(Machine_Details), len(Machine_Details)], dtype=int)
Machine_Available_End_Time = np.zeros([len(Machine_Details), len(Machine_Details)], dtype=int)
##Max_Batch_Size={}
for l in range(0, len(Machine_Details)):
    Machine_Available_Start_Time[int(Machine_Details[l][1]), int(Machine_Details[l][2])] = int(Machine_Details[l][3])
    Machine_Available_End_Time[int(Machine_Details[l][1]), int(Machine_Details[l][2])] = int(Machine_Details[l][4])
##  Max_Batch_Size[int(Machine_Details[l][1]),int(Machine_Details[l][2])] = int(Machine_Details[l][5])

##  print ' ----------------------------------- MACHINE_SHIFT_DETAILS-----------------------------------'
cursor.execute('select sr.MACHINE_INDEX, SHIFT_ID, START_TIME_INDEX, END_TIME_INDEX From xlps_machine_valid_ranges vr \
            inner join XLPS_MACHINE_SOLVER_READY sr on vr.machine_id=sr.machine_id and sr.CLIENT_ID=vr.CLIENT_ID and vr.scenario_id=sr.scenario_id \
            where sr.CLIENT_ID=' + str(Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
            order by  sr.MACHINE_INDEX, START_TIME_INDEX')
global Machine_Shift_Details
Machine_Shift_Details = cursor.fetchall()

##print "Machine_Shift_Details : ", Machine_Shift_Details
##  print ' ----------------------------------- MACHINE_SHUTDOWN_DETAILS -----------------------------------'
cursor.execute('select distinct msd.MACHINE_INDEX, ms.SHUTDOWN_START_TIME_INDEX, ms.SHUTDOWN_END_TIME_INDEX From XLPS_MACHINE_SOLVER_READY \
                msd inner join XLPS_OPERATION_SOLVER_READY osd on msd.OPERATION_ID=osd.OPERATION_ID and msd.CLIENT_ID=osd.CLIENT_ID \
                and msd.Scenario_ID=osd.Scenario_ID inner join XLPS_Machine_Shutdown ms on  ms.Machine_ID=msd.Machine_ID and \
                msd.CLIENT_ID=ms.CLIENT_ID and msd.Scenario_ID=ms.Scenario_ID where msd.CLIENT_ID = ' + str(Client_ID) + ' \
                and msd.Scenario_ID=' + str(Scenario_ID) + ' union \
               select distinct ms.MACHINE_INDEX, ms.HOLIDAY_START_TIME_INDEX, ms.HOLIDAY_END_TIME_INDEX From XLPS_MACHINE_HOLIDAY ms \
               where ms.CLIENT_ID = ' + str(Client_ID) + ' and ms.Scenario_ID=' + str(Scenario_ID))
global Machine_Shutdown_Details
Machine_Shutdown_Details = cursor.fetchall()

##  print ' ----------------------------------- BOM_DETAILS-----------------------------------'

cursor.execute('select distinct OUTPUT_PRODUCT_INDEX, INPUT_PRODUCT_INDEX, OPERATION_INDEX, attach_rate, yield from xlps_bom  where ALTERNATE_INDEX <>-1 and \
             CLIENT_ID= ' + str(Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
global BOM_DETAILS
BOM_DETAILS = cursor.fetchall()

currenttime = datetime.datetime.now()
print "Step4:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

##  print ' ----------------------------------- PO_AR_BOM_COMB -----------------------------------'

cursor.execute('select distinct po.production_order_index, OPERATION_INDEX,routing_id, alternate_index, INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
      where bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
    Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,routing_id ');
global PO_AR_BOM_COMB
PO_AR_BOM_COMB = cursor.fetchall()

currenttime = datetime.datetime.now()
print "Step4_1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

##  print ' ----------------------------------- MAT_BALANCE_CONST -----------------------------------'
cursor.execute('select distinct sr.production_order_index, bm.operation_index, bm1.operation_index, sr.routing_ID,sr.sequence_id,sr1.sequence_id, \
      bm1.input_product_index from xlps_bom bm inner join xlps_route_solver_ready sr on bm.production_order_index=sr.production_order_index \
      and sr.operation_index=bm.operation_index and sr.alternate_index=bm.alternate_index and bm.routing_id=sr.routing_id and bm.client_id=sr.client_id \
      and bm.scenario_id=sr.scenario_id inner join xlps_bom bm1 on bm.PRODUCTION_ORDER_INDEX=bm1.PRODUCTION_ORDER_INDEX and \
      bm1.INPUT_PRODUCT_INDEX=bm.OUTPUT_PRODUCT_INDEX and bm.ROUTING_ID=bm1.routing_id and bm.client_id=bm1.client_id and bm.scenario_id=bm1.scenario_id \
      inner join xlps_route_solver_ready sr1 on bm1.production_order_index=sr1.production_order_index and sr1.routing_id=bm1.routing_id \
      and sr1.operation_index=bm1.operation_index and sr1.alternate_index=bm1.alternate_index and bm1.client_id=sr1.client_id and bm1.scenario_id=sr1.scenario_id \
      and sr.sequence_id=sr1.sequence_id - 1 inner join xlps_po_solver_ready po on po.production_order_index=sr1.production_order_index and po.client_id=sr1.client_id \
      and po.scenario_id=sr1.scenario_id where sr.client_id=' + str(Client_ID) + ' and sr.scenario_id= ' + str(
    Scenario_ID) + ' and sr.alternate_index<>-1 and sr1.alternate_index<>-1 \
      order by sr.production_order_index')
global MAT_BALANCE_CONST
MAT_BALANCE_CONST = cursor.fetchall()

currenttime = datetime.datetime.now()
print "Step4_2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

##print ' ----------------------------------- PO_TASK_AR_SEQ_ALT_IP_COMB -----------------------------------'

cursor.execute('select distinct sr.production_order_index, sr.operation_index, sr.routing_id,sr.sequence_id, bm.alternate_index, \
bm.input_product_index from xlps_bom bm inner join xlps_route_solver_ready sr on bm.production_order_index=sr.production_order_index \
and sr.operation_index=bm.operation_index and sr.alternate_index=bm.alternate_index and bm.client_id=sr.client_id and bm.scenario_id=sr.scenario_id \
inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
where sr.client_id=' + str(Client_ID) + ' and sr.scenario_id= ' + str(
    Scenario_ID) + ' and sr.alternate_index<>-1 order by  sr.production_order_index,sr.sequence_id')
global PO_TASK_AR_SEQ_ALT_IP_COMB
PO_TASK_AR_SEQ_ALT_IP_COMB = cursor.fetchall()

cursor.execute('select distinct sr.production_order_index, sr.operation_index, sr.routing_ID,sr.sequence_id,bm.input_product_index, \
bm.output_product_index from xlps_bom bm inner join xlps_route_solver_ready sr on bm.production_order_index=sr.production_order_index \
and sr.operation_index=bm.operation_index and sr.alternate_index=bm.alternate_index and bm.client_id=sr.client_id and bm.scenario_id=sr.scenario_id \
where sr.client_id=' + str(Client_ID) + ' and sr.scenario_id= ' + str(
    Scenario_ID) + ' and sr.alternate_index<>-1 order by sr.production_order_index ,sr.sequence_id')
IP_OP_COMB = cursor.fetchall()

PO_TASK_AR_SEQ_OP_COMB1 = []
PO_TASK_AR_SEQ_IP_COMB1 = []

for l in range(0, len(IP_OP_COMB)):
    PO_TASK_AR_SEQ_OP_COMB1.append((int(IP_OP_COMB[l][0]), int(IP_OP_COMB[l][1]), int(IP_OP_COMB[l][2]),
                                    int(IP_OP_COMB[l][3]), int(IP_OP_COMB[l][5])))
    PO_TASK_AR_SEQ_IP_COMB1.append((int(IP_OP_COMB[l][0]), int(IP_OP_COMB[l][1]), int(IP_OP_COMB[l][2]),
                                    int(IP_OP_COMB[l][3]), int(IP_OP_COMB[l][4])))
global PO_TASK_AR_SEQ_OP_COMB
PO_TASK_AR_SEQ_OP_COMB = set(PO_TASK_AR_SEQ_OP_COMB1)
PO_TASK_AR_SEQ_IP_COMB = set(PO_TASK_AR_SEQ_IP_COMB1)

currenttime = datetime.datetime.now()
print "Step5:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

##  print ' ----------------------------------- PO_AR_BOM_IP_QTY -----------------------------------'

cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, routing_id,alternate_index, INPUT_PRODUCT_INDEX,1 from xlps_bom  bm \
    inner join xlps_po_products po on po.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.INPUT_PRODUCT_INDEX=po.product_index \
    and bm.client_id=po.client_id and bm.SCENARIO_ID=po.scenario_id \
    inner join xlps_po_solver_ready po1 \
        on po1.production_order_index=bm.production_order_index and po1.client_id=bm.client_id and po1.scenario_id=bm.scenario_id \
    where po.client_id=' + str(Client_ID) + '  and po.Scenario_ID= ' + str(
    Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ,routing_id ');
PO_AR_BOM_IP_QTY = cursor.fetchall()
global PO_AR_BOM_IP_COMB
PO_AR_BOM_IP_COMB = []
for l in range(0, len(PO_AR_BOM_IP_QTY)):
    PO_AR_BOM_IP_COMB.append([int(PO_AR_BOM_IP_QTY[l][0]), int(PO_AR_BOM_IP_QTY[l][1]), int(PO_AR_BOM_IP_QTY[l][2]),
                              int(PO_AR_BOM_IP_QTY[l][3]), int(PO_AR_BOM_IP_QTY[l][4])])

##  print ' ----------------------------------- PO_AR_BOM_OP_QTY -----------------------------------'

cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, routing_id,alternate_index, OUTPUT_PRODUCT_INDEX,1 from xlps_bom  bm \
    inner join xlps_po_products po on po.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.OUTPUT_PRODUCT_INDEX=po.product_index \
    and bm.client_id=po.client_id and bm.SCENARIO_ID=po.scenario_id \
    inner join xlps_po_solver_ready po1 \
        on po1.production_order_index=bm.production_order_index and po1.client_id=bm.client_id and po1.scenario_id=bm.scenario_id \
    where  po.client_id=' + str(Client_ID) + '  and po.Scenario_ID= ' + str(
    Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,routing_id ');
PO_AR_BOM_OP_QTY = cursor.fetchall()
global PO_AR_BOM_OP_COMB
PO_AR_BOM_OP_COMB = []
for l in range(0, len(PO_AR_BOM_OP_QTY)):
    PO_AR_BOM_OP_COMB.append([int(PO_AR_BOM_OP_QTY[l][0]), int(PO_AR_BOM_OP_QTY[l][1]), int(PO_AR_BOM_OP_QTY[l][2]),
                              int(PO_AR_BOM_OP_QTY[l][3]), int(PO_AR_BOM_OP_QTY[l][4])])

cursor.execute('select distinct production_order_index, OPERATION_INDEX, routing_id, OUTPUT_PRODUCT_INDEX,1 from xlps_op_output_count \
    where client_id=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' group by production_order_index, \
    OPERATION_INDEX, routing_id,OUTPUT_PRODUCT_INDEX ')
Multiple_Output_Domain = cursor.fetchall()
Output_Count = {}

Multiple_Output_Domain_COMB1 = []
Multiple_Output_Domain_COMB2 = []

for l in range(0, len(Multiple_Output_Domain)):
    Multiple_Output_Domain_COMB1.append(
        [int(Multiple_Output_Domain[l][0]), int(Multiple_Output_Domain[l][1]), int(Multiple_Output_Domain[l][2])])
    Multiple_Output_Domain_COMB2.append(
        [int(Multiple_Output_Domain[l][0]), int(Multiple_Output_Domain[l][1]), int(Multiple_Output_Domain[l][2]),
         int(Multiple_Output_Domain[l][3])])
    Output_Count[
        int(Multiple_Output_Domain[l][0]), int(Multiple_Output_Domain[l][1]), int(Multiple_Output_Domain[l][2]), int(
            Multiple_Output_Domain[l][3])] = int(Multiple_Output_Domain[l][4])

currenttime = datetime.datetime.now()
print "Step6:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

cursor.execute('select distinct operation_index From xlps_route_solver_ready where CLIENT_ID=' + str(Client_ID) + ' and \
      Scenario_ID=' + str(Scenario_ID) + ' order by operation_index')
global all_operations
all_operations = cursor.fetchall()

cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
               where rsr.CLIENT_ID=' + str(Client_ID) + ' and rsr.operation_index in (22,21,5,12) \
        and rsr.Scenario_ID= ' + str(Scenario_ID) + ' order by SEQUENCE_ID desc');

PO_TASK_AR_SEQ_ALT_together_COMB = cursor.fetchall()

##  print ' ----------------------------------- BOH -----------------------------------'
cursor.execute('select distinct product_id,time_index, convert(cumul_quantity, UNSIGNED INTEGER) from xlps_material_receipts \
            where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(Scenario_ID) + ' order by  product_id,time_index')
global BOH_T
BOH_T = cursor.fetchall()
global BOH_Comb
global BOH
BOH_Comb = []
BOH = {}
for l in range(0, len(BOH_T)):
    BOH_Comb.append([int(BOH_T[l][0]), int(BOH_T[l][1])])
    BOH[int(BOH_T[l][0]), int(BOH_T[l][1])] = int(BOH_T[l][2])

# ##  print ' ----------------------------------- BOH_SFG -----------------------------------'
# cursor.execute('select distinct product_id,time_index, convert(cumul_quantity, UNSIGNED INTEGER) from xlps_material_receipts_sfg \
#             where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(
#     Scenario_ID) + ' order by product_id,time_index')
# BOH_SFG_T = cursor.fetchall()
# global BOH_SFG
# global BOH_SFG_Comb
# BOH_SFG_Comb = []
# BOH_SFG = {}
# for l in range(0, len(BOH_SFG_T)):
#     BOH_SFG_Comb.append([int(BOH_SFG_T[l][0]), int(BOH_SFG_T[l][1])])
#     BOH_SFG[int(BOH_SFG_T[l][0]), int(BOH_SFG_T[l][1])] = int(BOH_SFG_T[l][2])
# print "BOH_SFG:", BOH_SFG

##  print ' ----------------------------------- Material_Profile -----------------------------------'
cursor.execute('select distinct product_id,time_index, convert(quantity, UNSIGNED INTEGER) from xlps_material_receipts \
            where quantity>=0 and client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(
    Scenario_ID) + ' order by  product_id,time_index')
global Material_Profile_Comb
Material_Profile_Comb = cursor.fetchall()

##global Material_Profile_Comb
##Material_Profile_Comb=[]
##Material_Profile={}
##for l in range(0,len(BOH_T2)):
##  Material_Profile_Comb.append([int(BOH_T2[l][0]),int(BOH_T2[l][1]),int(BOH_T2[l][2])])
##  Material_Profile[int(BOH_T2[l][0]),int(BOH_T2[l][1])] = int(BOH_T2[l][2])

##  print ' ----------------------------------- PO_RELATION -----------------------------------'
cursor.execute('select production_order_index_fg,production_order_index_sfg from xlps_po_relation \
where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(Scenario_ID))
global PO_Relation
PO_Relation = cursor.fetchall()

##  print ' ----------------------------------- FG_Lots -----------------------------------'
cursor.execute('select production_order_index,pm.product_index,order_quantity from xlps_po_solver_ready po \
    inner join xlps_product_master pm on po.PRODUCT_ID=pm.PRODUCT_INDEX and po.SCENARIO_ID=pm.SCENARIO_ID \
    where PRODUCT_TYPE_ID=3 and po.client_id=' + str(Client_ID) + ' and pm.client_id=' + str(
    Client_ID) + ' and po.Scenario_ID= ' + str(Scenario_ID) + ' order by order_due_date_index')
global FG_Lots
FG_Lots = cursor.fetchall()

FG_Order_Qty = {}
for i in range(0, len(FG_Lots)):
    FG_Order_Qty[FG_Lots[i][0]] = FG_Lots[i][2]

##  print ' ----------------------------------- SFG_Lots -----------------------------------'
cursor.execute('select production_order_index,pm.product_index,order_quantity from xlps_po_solver_ready po \
    inner join xlps_product_master pm on po.PRODUCT_ID=pm.PRODUCT_INDEX and po.SCENARIO_ID=pm.SCENARIO_ID \
    where PRODUCT_TYPE_ID=2 and po.client_id=' + str(Client_ID) + ' and pm.client_id=' + str(
    Client_ID) + ' and po.Scenario_ID= ' + str(Scenario_ID) + ' order by order_due_date_index')
global SFG_Lots
SFG_Lots = cursor.fetchall()

SFG_Order_Qty = {}
for i in range(0, len(SFG_Lots)):
    SFG_Order_Qty[SFG_Lots[i][0]] = SFG_Lots[i][2]

# cursor.execute('update xlps_sfg_fg_domain fg inner join xlps_product_master pm on pm.product_id=fg.input_product_id and  fg.client_id=pm.client_id\
#                 and fg.scenario_id=pm.scenario_id set fg.INPUT_PRODUCT_ID = pm.product_id  where pm.client_id = ' + str(Client_ID))
# cursor.execute('update xlps_sfg_fg_domain fg inner join xlps_product_master pm on pm.product_index=fg.output_product_index and  fg.client_id=pm.client_id\
#     and fg.scenario_id=pm.scenario_id set fg.OUTPUT_PRODUCT_ID = pm.product_id  where pm.client_id = ' + str(
#     Client_ID) + ' and pm.scenario_id = ' + str(Scenario_ID))

##  print ' ----------------------------------- SFG_FG_Domain -----------------------------------'
# cursor.execute('select pm.product_index,pm1.product_index, ip_qty,op_qty,attach_rate from xlps_sfg_fg_domain fg \
#                inner join xlps_product_master pm on pm.product_id= input_product_id and pm.client_id=fg.client_id and pm.scenario_id=fg.scenario_id \
# inner join xlps_product_master pm1 on pm1.product_id= output_product_id and pm1.client_id=fg.client_id and pm1.scenario_id=fg.scenario_id \
#     where fg.client_id=' + str(Client_ID) + ' and fg.Scenario_ID= ' + str(Scenario_ID))
global SFG_FG_Domain
global SFG_FG_Domain_Comb
# SFG_FG_Domain = cursor.fetchall()

FG_Qty = {}
SFG_Qty = {}
AR = {}
SFG_FG_Domain_Comb = []
# for i in range(0, len(SFG_FG_Domain)):
#     SFG_FG_Domain_Comb.append((SFG_FG_Domain[i][0], SFG_FG_Domain[i][1]))
#     FG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][3]
#     SFG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][2]
#     AR[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][4]

FG_Lots_sorted = sorted(FG_Lots, key=lambda x: (-x[2]))
SFG_Lots_sorted = sorted(SFG_Lots, key=lambda x: (-x[2]))
print  "FG_Lots_sorted ", FG_Lots_sorted
print  "SFG_Lots_sorted ", SFG_Lots_sorted

    # print "i, op", i, p
    # if i>=0 or i<=2:
cursor.execute(' select po.production_order_index,pm.product_index from xlps_production_order xl \
            inner join xlps_po_solver_ready po on po.production_order_id=xl.production_order_id  \
            and po.client_id=xl.client_id and po.scenario_id=xl.scenario_id  \
            inner join xlps_product_master pm on pm.product_id=xl.output_product and pm.scenario_id=xl.scenario_id \
            and pm.client_id=xl.client_id  where xl.level=2 ');
global SFG_PO_FG_Lot_Relation
SFG_PO_FG_Lot_Relation = cursor.fetchall()

##  print ' ----------------------------------- BOH_Total -----------------------------------'
cursor.execute('select distinct product_id, convert(sum(quantity), UNSIGNED INTEGER) from xlps_material_receipts \
           where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(Scenario_ID) + '  group by product_id ')
BOH_Total = cursor.fetchall()

global BOH_Total_Comb
global BOH_Total_Value
BOH_Total_Comb = []
BOH_Total_Value = {}
for l in range(0, len(BOH_Total)):
    BOH_Total_Comb.append(int(BOH_Total[l][0]))
    BOH_Total_Value[int(BOH_Total[l][0])] = int(BOH_Total[l][1])

global Initial_Schedule_Comb
Initial_Schedule_Comb = []
if int(Freeze_Time_Fence) > 0:
    print ' ----------------------------------- Initial Schedule -----------------------------------'
    if PlanningBucket == "Seconds":
        cursor.execute('select distinct PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX, ACTIVE_STATUS \
            From XLPS_SCHEDULE_TIMES where ACTIVE_STATUS = 1 and BEGIN_TIME_INDEX <=' + str(
            Freeze_Time_Fence) + ' *24*3600 and CLIENT_ID=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));
    elif PlanningBucket == "Minutes":
        cursor.execute('select distinct PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX, ACTIVE_STATUS \
            From XLPS_SCHEDULE_TIMES where ACTIVE_STATUS = 1 and BEGIN_TIME_INDEX <=' + str(
            Freeze_Time_Fence) + ' *24*60 and CLIENT_ID=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));
    elif PlanningBucket == "Hours":
        cursor.execute('select distinct PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX, ACTIVE_STATUS \
            From XLPS_SCHEDULE_TIMES where ACTIVE_STATUS = 1 and BEGIN_TIME_INDEX <=' + str(
            Freeze_Time_Fence) + ' *24 and CLIENT_ID=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));

    Initial_Schedule = cursor.fetchall()
    Task_Begin_Time = {}
    Task_End_Time = {}
    Task_Active_Status = {}

    for l in range(0, len(Initial_Schedule)):
        Initial_Schedule_Comb.append((int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]),
                                      int(Initial_Schedule[l][2]), int(Initial_Schedule[l][3]),
                                      int(Initial_Schedule[l][4])))
        Task_Begin_Time[int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]), int(Initial_Schedule[l][2]), int(
            Initial_Schedule[l][3]), int(Initial_Schedule[l][4])] = int(Initial_Schedule[l][5])
        Task_End_Time[int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]), int(Initial_Schedule[l][2]), int(
            Initial_Schedule[l][3]), int(Initial_Schedule[l][4])] = int(Initial_Schedule[l][6])
        Task_Active_Status[int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]), int(Initial_Schedule[l][2]), int(
            Initial_Schedule[l][3]), int(Initial_Schedule[l][4])] = int(Initial_Schedule[l][7])

##print ' ----------------------------------- HOLDING_TIME -----------------------------------'

cursor.execute('select distinct bm.production_order_index, bm.operation_index, bm.machine_index,min_holding_time,max_holding_time from xlps_holding_time bm\
      inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
where bm.client_id=' + str(Client_ID) + ' and bm.scenario_id= ' + str(Scenario_ID) + ' and bm.machine_index<>-1 order by bm.production_order_index, \
bm.operation_index,bm.machine_index')
Prod_Res_Holding_Time = cursor.fetchall()

Min_Holding_Time = {}
Max_Holding_Time = {}
Prod_Res_Holding_Time_Comb = []

for l in range(0, len(Prod_Res_Holding_Time)):
    Prod_Res_Holding_Time_Comb.append(
        [int(Prod_Res_Holding_Time[l][0]), int(Prod_Res_Holding_Time[l][1]), int(Prod_Res_Holding_Time[l][2])])
    Min_Holding_Time[
        int(Prod_Res_Holding_Time[l][0]), int(Prod_Res_Holding_Time[l][1]), int(Prod_Res_Holding_Time[l][2])] = int(
        Prod_Res_Holding_Time[l][3])
    Max_Holding_Time[
        int(Prod_Res_Holding_Time[l][0]), int(Prod_Res_Holding_Time[l][1]), int(Prod_Res_Holding_Time[l][2])] = int(
        Prod_Res_Holding_Time[l][4])

currenttime = datetime.datetime.now()
f1.write("Data reading completed at %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
f1.write("\n")

active_connection = -1
cursor.close()
cnx.commit()
cnx.close()

currenttime = datetime.datetime.now()
print "Step7:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')


# print PriorityIterationSolve()
#    return True


def PriorityIterationSolve():
    print "Inside Priority Iteration Solve"
    print "Calling high priority solve"
    global High_Priority_Comb
    global High_Priority_Begin_Time
    global High_Priority_End_Time
    global High_Priority_Active_Status
    High_Priority_Comb = []
    High_Priority_Begin_Time = {}
    High_Priority_End_Time = {}
    High_Priority_Active_Status = {}
    print "Len: ", len(hp_pos)
    if len(hp_pos) > 0:
        print "Calling high priority solve"
        FirstSolve(hp_pos, 0)

    print "Calling all priority solve"

    FirstSolve(all_pos, 1)

    return True


def FirstSolve(current_pos1, all_pos_iteration1):
    print "Inside First Solve"
    global current_pos
    global all_pos_iteration
    current_pos = []
    current_pos = current_pos1
    all_pos_iteration = all_pos_iteration1
    Solve_Time = Solver_Time_Limit
    ##  global active_var_new
    active_var_new = {}
    obj_value = 999999999999
    makespan_value = 999999999999
    first_solve = 1
    New_Set = []
    Current_Set = []
    fixed_dec = {}
    all_machine_ends = {}
    start_new = {}
    end_new = {}

    decision, obj_value_new, active_var, PO_TASK_AR_SEQ_ALT_COMB, PO_TASK_AR_SEQ_ALT_COMB_New, fixed_dec, all_machine_ends, fixed_start, fixed_end, makespan_value_new \
        = solve_production_scheduler2(active_var_new, start_new, end_new, obj_value, New_Set, Current_Set, 0,
                                      first_solve, makespan_value, 999, 999, 0, 0)
    currenttime = datetime.datetime.now()
    print "Step31_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    print "After First Solve (Decision, Objective Value, Makespan) : ", decision, obj_value_new, makespan_value_new

    Diff = int(time()) - int(Process_Start_Time)

    if decision == 1:
        if Diff > int(Solve_Time):
            currenttime = datetime.datetime.now()
            print "Step32:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            Output_Update()
        else:
            if all_pos_iteration == 1:
                ##        first_solve=0
                ##        return_val = LSOperator(PO_TASK_AR_SEQ_ALT_COMB,PO_TASK_AR_SEQ_ALT_COMB_New,active_var,obj_value_new,all_machine_ends,fixed_dec,fixed_start,fixed_end,makespan_value_new)
                ##        print return_val
                Output_Update()
                print "LS Disabled"
            else:
                print "High priority solve done"
                ##      first_solve=0
                ##      return_val = LSOperator(PO_TASK_AR_SEQ_ALT_COMB,PO_TASK_AR_SEQ_ALT_COMB_New,active_var,obj_value_new,all_machine_ends,fixed_dec,fixed_start,fixed_end,makespan_value_new)
                ##      print return_val
    else:
        f1.write("No Feasible Solution Found within time Limit")
        f1.write("\n")
        f1.close
        cnx2 = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1',
                                       database='saddlepointv6_prod2')
        cursor2 = cnx2.cursor()
        cursor2.execute(
            'update SPAPP_JOB set JOB_STATUS="COMPLETED", INFO="No feasible Solution found", END_TIME=NOW() where JOB_ID = ' + str(
                JOB_ID))
        cursor2.close()
        cnx2.commit()
        cnx2.close()

    print "Final Step"

    return True


def LSOperator(PO_TASK_AR_SEQ_ALT_COMB, PO_TASK_AR_SEQ_ALT_COMB_New, active_var, obj_value, all_machine_ends, fixed_dec,
               fixed_start, fixed_end, makespan_value):
    print "inside LSOperator"
    all_machine_ends_orig = []

    for (op, m, t) in all_machine_ends:
        all_machine_ends_orig.append((op, m, t))

    active_var_new = {}
    fixed_dec_new = {}
    fixed_dec_orig = {}
    fixed_dec_new = fixed_dec
    fixed_dec_orig = fixed_dec

    fixed_start_new = {}
    start_new = {}
    fixed_start_orig = {}
    fixed_start_new = fixed_start
    fixed_start_orig = fixed_start

    fixed_end_new = {}
    end_new = {}
    fixed_end_orig = {}
    fixed_end_new = fixed_end
    fixed_end_orig = fixed_end
    Iteration_set_orig = {}
    Iteration_set_orig = PO_TASK_AR_SEQ_ALT_COMB_New
    loop = 1
    decision = 1
    active_var_new = active_var
    loop_start_time = time()
    Solve_Time = Solver_Time_Limit
    print "Iteration Set"
    Removable_Set = []
    iter_solve = 0
    last_failed_seq = 0
    next_iteration_item_check = 0
    while decision >= 0:
        loop = 1

        # prev_seq=1
        OP_Mac_Set = []
        for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB_New:
            print (i, j, ar, s, alt)
            OP_Mac_Set.append((j, alt))
        OP_Mac_Set_Unique = set(OP_Mac_Set)

        Iteration_Set_Length = len(PO_TASK_AR_SEQ_ALT_COMB_New)
        print " iteration Set Length: ", len(PO_TASK_AR_SEQ_ALT_COMB_New)
        if Iteration_Set_Length == 0:
            decision = 1
            break

        for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB_New:
            Time_Fixed_Set = []
            New_Set = []
            ##      Current_Item = []

            if decision == 1:
                all_machine_ends_new = []
                for (op, m, t) in all_machine_ends:
                    if op == j:
                        all_machine_ends_new.append((op, m, t))
            else:
                all_machine_ends_new = []
                for (op, m, t) in all_machine_ends_orig:
                    if op == j:
                        all_machine_ends_new.append((op, m, t))

            if time() - Process_Start_Time > int(Solve_Time):
                print " ---------------------------- Updating Schedule to DB ----------------------------"
                Output_Update()
                return True
            print " ---------------  Loop: ", loop, " ------------------------"
            print "Decision made for PO ", i + 1, " for operation ", s, " on alternate machine ", alt
            print "Current Iteration :", i, j, ar, s, alt

            Current_Set = []
            # PO_Set=[]
            # PO_Set.append((i,j))
            for (j1, alt1) in OP_Mac_Set_Unique:
                for (i2, ar2, s2) in PO_AR_SEQ_COMB:
                    if i2 == i and s2 == s:
                        active_var_new[(i2, j1, ar2, s2, alt1)] = 0
                        New_Set.append((i2, j1, ar2, s2, alt1))

            PO_TASK_AR_SEQ_ALT_COMB_New1 = {}

            for (i3, j3, ar3, s3, alt3) in fixed_dec_new:
                if i3 <> i:
                    active_var_new[(i3, j3, ar3, s3, alt3)] = 1
                    New_Set.append((i3, j3, ar3, s3, alt3))
                    if (i3, j3, ar3, s3, alt3) not in Iteration_set_orig:
                        start_new[(i3, j3, ar3, s3, alt3)] = fixed_start_new[(i3, j3, ar3, s3, alt3)]
                        end_new[(i3, j3, ar3, s3, alt3)] = fixed_end_new[(i3, j3, ar3, s3, alt3)]
                        Time_Fixed_Set.append((i3, j3, ar3, s3, alt3))
                else:
                    if i3 == i and s3 < s:
                        active_var_new[(i3, j3, ar3, s3, alt3)] = 1
                        New_Set.append((i3, j3, ar3, s3, alt3))
                        start_new[(i3, j3, ar3, s3, alt3)] = fixed_start_new[(i3, j3, ar3, s3, alt3)]
                        end_new[(i3, j3, ar3, s3, alt3)] = fixed_end_new[(i3, j3, ar3, s3, alt3)]
                        Time_Fixed_Set.append((i3, j3, ar3, s3, alt3))

            done = 0

            for (op, m, t) in all_machine_ends_new:
                for (i4, j4, ar4, s4, alt4) in PO_TASK_AR_SEQ_ALT_COMB:
                    if i4 == i and j4 == j and ar4 == ar and s4 == s and alt4 <> alt and op == j4:
                        if Machine_ID[(i4, j4, ar4, s4, alt4)] == m:
                            active_var_new[(i4, j4, ar4, s4, alt4)] = 1
                            New_Set.append((i4, j4, ar4, s4, alt4))
                            print "Machine_End Time :", (op, m, t)
                            print "Assigned to machine: ", i4, j4, ar4, s4, alt4
                            ##              Current_Item.append((i4,j4,ar4,s4,alt4)) # to remove from total iteration set if decision=0
                            done = 1
                            break
                if done == 1:
                    done = 0
                    break

            decision, obj_value_new, active_var, PO_TASK_AR_SEQ_ALT_COMB, PO_TASK_AR_SEQ_ALT_COMB_New1, fixed_dec, all_machine_ends, fixed_start, fixed_end, makespan_value_new \
                = solve_production_scheduler2(active_var_new, start_new, end_new, obj_value, New_Set,
                                              Time_Fixed_Set, s, 0, makespan_value, loop, Iteration_Set_Length,
                                              iter_solve, last_failed_seq)

            print "After solve(Loop, Decision, Obj_Value, Makespan) : ", loop, decision, obj_value_new, makespan_value_new
            print "Solve_Time: ", current_time() - Process_Start_Time

            global Next_Iteration_Item
            if decision == 1:
                next_iteration_item_check = next_iteration_item_check + 1
                fixed_dec_new = fixed_dec
                fixed_dec_orig = fixed_dec
                fixed_start_new = fixed_start
                fixed_start_orig = fixed_start
                fixed_end_new = fixed_end
                fixed_end_orig = fixed_end
                all_machine_ends_new = all_machine_ends
                all_machine_ends_orig = all_machine_ends

                Next_Iteration_Item = []
                Next_Iteration_Item.append((i, j, ar, s, alt))
                iter_solve = 0
                print "Next_Iteration_Item: ", Next_Iteration_Item

            if decision == 0:
                active_var_new[(i, j, ar, s, alt)] = 1
                New_Set.remove((i, j, ar, s, alt))
                fixed_dec_new = fixed_dec_orig
                fixed_start_new = fixed_start_orig
                fixed_end_new = fixed_end_orig
                last_failed_seq = s

            if decision == 0:
                print "ask1"

                if next_iteration_item_check == 0:
                    if loop == Iteration_Set_Length:
                        PO_TASK_AR_SEQ_ALT_COMB_New = Total_Iteration_Set
                        PO_TASK_AR_SEQ_ALT_COMB_New.remove((i, j, ar, s, alt))
                        for (i, j, ar, s, alt) in Removable_Set:
                            if (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB_New:
                                PO_TASK_AR_SEQ_ALT_COMB_New.remove((i, j, ar, s, alt))
                        break
                    else:
                        print "Removed: ", i, j, ar, s, alt
                        PO_TASK_AR_SEQ_ALT_COMB_New.remove((i, j, ar, s, alt))
                        Removable_Set.append((i, j, ar, s, alt))
                        print "Removable_Set:", Removable_Set
                        # loop = loop + 1
                        break
                else:
                    print "ask3"
                    PO_TASK_AR_SEQ_ALT_COMB_New = Next_Iteration_Item
                    iter_solve = 1
                    break

            obj_value = obj_value_new
            makespan_value = makespan_value_new
            # prev_seq = s

            if loop == Iteration_Set_Length:
                if decision == 1:
                    PO_TASK_AR_SEQ_ALT_COMB_New = PO_TASK_AR_SEQ_ALT_COMB_New1
                    break

            loop = loop + 1

    print " ---------------------------- Updating Schedule to DB ----------------------------"
    Output_Update()
    return True


def current_time():
    return time()


def getProdOrderCount():
    return int(len(current_pos))


def getMachineSequence(PO, Task, AR):
    for i in range(0, len(Sequence_Time)):
        if (int(Sequence_Time[i][0]) == PO and int(Sequence_Time[i][1]) == Task and int(Sequence_Time[i][2]) == AR):
            return int(Sequence_Time[i][3])


def getOrderQuantity(PO):
    ##  print 1,PO
    for i in range(0, len(Sequence_Time)):
        if (int(Sequence_Time[i][0]) == int(PO)):
            ##      print 2,PO, int(Sequence_Time[i][9])
            return int(Sequence_Time[i][9])


def getnMachine(PO, Task, ar):
    for i in range(0, len(op_op_ar)):
        if (int(op_op_ar[i][0]) == PO and int(op_op_ar[i][1]) == Task and int(op_op_ar[i][2]) == ar):
            return int(op_op_ar[i][3])
    return 0


def getOrderDueDate(PO):
    for i in range(0, len(ORDER_DUE_DATE)):
        if (int(ORDER_DUE_DATE[i][0]) == PO):
            return int(ORDER_DUE_DATE[i][1])


def getOrderPriority(PO):
    for i in range(0, len(ORDER_DUE_DATE)):
        if (int(ORDER_DUE_DATE[i][0]) == PO):
            return ORDER_DUE_DATE[i][2]


def getProductID(PO):
    for i in range(0, len(ORDER_DUE_DATE)):
        if (int(ORDER_DUE_DATE[i][0]) == int(PO)):
            return int(ORDER_DUE_DATE[i][3])


def getMachineAvailableEndTime(Task, Alt):
    for i in range(0, len(Machine_Details)):
        if (int(Machine_Details[i][1]) == Task and int(Machine_Details[i][2]) == Alt):
            return int(Machine_Details[i][4])


def getAttachRate(op, ip, m):
    for i in range(0, len(BOM_DETAILS)):
        if (BOM_DETAILS[i][0] == op and BOM_DETAILS[i][1] == ip and BOM_DETAILS[i][2] == m):
            return (BOM_DETAILS[i][3])


def getYield(op, ip, m):
    for i in range(0, len(BOM_DETAILS)):
        if (BOM_DETAILS[i][0] == op and BOM_DETAILS[i][1] == ip and BOM_DETAILS[i][2] == m):
            return (BOM_DETAILS[i][4])


def find_invalid_ranges(machine_shutdown, machine_holidays):
    # print machine_shutdown, machine_holidays
    valid_range_array = np.zeros(int(PlanningHorizon))
    for i in machine_holidays:
        if i[1] < int(PlanningHorizon):
            valid_range_array[i[0] + 1: i[1] + 1] = 1
    for i in machine_shutdown:
        if i[1] < int(PlanningHorizon):
            valid_range_array[i[0] + 1: i[1] + 1] = 1
            ## Find where value chages. This point is start of invalid range, as index 0 is always 0.
    change_index = np.where(valid_range_array[:-1] != valid_range_array[1:])[0]
    # print change_index
    invalid_ranges = []
    if valid_range_array[0] == 0:
        index = 0
        while index < len(change_index):
            invalid_ranges.append((change_index[index], change_index[index + 1]))
            index = index + 2
    return invalid_ranges


def solve_production_scheduler2(active_var_new, start_new, end_new, obj_value, New_Set, Time_Fixed_Set, current_seq,
                                first_solve,
                                makespan_value, loop, Iteration_Set_Length, iter_solve, last_failed_seq):
    print "Before CP Solve Objective Value, Makespan : ", obj_value, makespan_value

    currenttime = datetime.datetime.now()

    solver = pywrapcp.Solver('Production Scheduling')

    PROD_ORDER_COUNT = int(len(current_pos))
    print "PROD_ORDER_COUNT: ", PROD_ORDER_COUNT

    TASK_MACHINE = {}
    for (i, j, ar, s) in PO_TASK_AR_SEQ_COMB:
        if i in current_pos:
            TASK_MACHINE[(i, j, ar, s)] = getnMachine(i, j, ar)

    print ' ----------------------------------------------- Machine Invalid Timings ------------------------------------------------'

    currenttime = datetime.datetime.now()
    print "Step7_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    invalid_end_time = []
    global Total_Shift_Timings
    Total_Shift_Timings = {}
    for m in all_machines:
        # Default Values per day
        if PlanningBucket == "Minutes":
            Total_Shift_Timings[m[0]] = 0
        elif PlanningBucket == "Hours":
            Total_Shift_Timings[m[0]] = 0
        elif PlanningBucket == "Seconds":
            Total_Shift_Timings[m[0]] = 0
        count = 0
        temp = 0
        x = 0
        y = 0
        for l in range(0, len(Machine_Shift_Details)):
            if m[0] == Machine_Shift_Details[l][0]:
                if count == 0:
                    x = 0
                    y = Machine_Shift_Details[l][2]
                    if int(x) != int(y):
                        invalid_end_time.append([m[0], x, y])
                    x = y
                    y = Machine_Shift_Details[l][3]
                    temp = y
                    count = count + 1
                else:
                    x = temp
                    y = Machine_Shift_Details[l][2]
                    if int(x) != int(y):
                        invalid_end_time.append([m[0], x, y])
                    x = y
                    y = Machine_Shift_Details[l][3]
                    temp = y

        if len(Machine_Shift_Details) > 0:
            for l in range(0, len(Machine_Shift_Details)):
                if m[0] == Machine_Shift_Details[l][0]:
                    if PlanningBucket == "Minutes" and Machine_Shift_Details[l][3] <= 1440:
                        Total_Shift_Timings[m[0]] = Total_Shift_Timings[m[0]] + Machine_Shift_Details[l][3] - \
                                                    Machine_Shift_Details[l][2]

                    elif PlanningBucket == "Hours" and Machine_Shift_Details[l][3] <= 24:
                        Total_Shift_Timings[m[0]] = Total_Shift_Timings[m[0]] + Machine_Shift_Details[l][3] - \
                                                    Machine_Shift_Details[l][2]

                    elif PlanningBucket == "Seconds" and Machine_Shift_Details[l][3] <= 86400:
                        Total_Shift_Timings[m[0]] = Total_Shift_Timings[m[0]] + Machine_Shift_Details[l][3] - \
                                                    Machine_Shift_Details[l][2]

        if PlanningBucket == "Minutes" and Total_Shift_Timings[m[0]] == 0:
            Total_Shift_Timings[m[0]] = 1440
        elif PlanningBucket == "Hours" and Total_Shift_Timings[m[0]] == 0:
            Total_Shift_Timings[m[0]] = 24
        elif PlanningBucket == "Seconds" and Total_Shift_Timings[m[0]] == 0:
            Total_Shift_Timings[m[0]] = 86400

    currenttime = datetime.datetime.now()
    print "Step7_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    global Invalid_Timings
    Invalid_Timings = []
    for l in xrange(0, len(invalid_end_time)):
        Invalid_Timings.append([invalid_end_time[l][0], invalid_end_time[l][1], invalid_end_time[l][2]])

    Shutdown_Timings = []
    for i in xrange(0, len(Machine_Shutdown_Details)):
        Shutdown_Timings.append([int(Machine_Shutdown_Details[i][0]), int(Machine_Shutdown_Details[i][1]),
                                 int(Machine_Shutdown_Details[i][2])])

    currenttime = datetime.datetime.now()
    print "Step7_3:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Invalid_Timings2 = []
    for m in all_machines:
        Machine_Invalid_Timings = []
        Machine_Shutdown_Timings = []
        for i in Invalid_Timings:
            if i[0] == m[0]:
                Machine_Invalid_Timings.append((i[1], i[2]))
        for j in Shutdown_Timings:
            if j[0] == m[0]:
                Machine_Shutdown_Timings.append((j[1], j[2]))
        # print "Machine_Invalid_Timings:", Machine_Invalid_Timings
        # print "Machine_Shutdown_Timings:", Machine_Shutdown_Timings
        invalid_ranges2 = find_invalid_ranges(Machine_Invalid_Timings, Machine_Shutdown_Timings)
        ##    print m[0],invalid_ranges2
        for k in invalid_ranges2:
            Invalid_Timings2.append([m[0], k[0], k[1] - k[0], 1])

            ##  print "Invalid_Timings2:", Invalid_Timings2

    currenttime = datetime.datetime.now()
    print "Step7_4:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------------------- Shutdown_Timings  --------------------------------------'

    for t in Invalid_Timings2:
        m1 = solver.FixedInterval(int(t[1]), int(t[2]), 'Invalid_%i_%i_%i' % (int(t[0]), int(t[1]), int(t[2])))
        t[3] = m1

    ##  print ' ----------------------------------------------- Defining Tasks ------------------------------------------------'
    all_tasks = {}
    global active_variables
    active_variables = {}
    alt_var = {}

    currenttime = datetime.datetime.now()
    print "Step8:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    ##  print "Initial_Schedule_Comb: ", Initial_Schedule_Comb
    for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
        if i in current_pos:
            if int(Freeze_Time_Fence) > 0:
                ##          print 100, (i,j,ar,s,alt) in Initial_Schedule_Comb
                if (i, j, ar, s, alt) in Initial_Schedule_Comb:
                    ##              print "Freeze:",(i,j,ar,s,alt)
                    all_tasks[(i, j, ar, s, alt)] = solver.IntervalVar(int(Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(Task_End_Time[(i, j, ar, s, alt)]),
                                                                       int(Task_End_Time[(i, j, ar, s, alt)]) - int(
                                                                           Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(Task_End_Time[(i, j, ar, s, alt)]) - int(
                                                                           Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(Task_End_Time[(i, j, ar, s, alt)]),
                                                                       False, 'all_tasks[(%i,%i,%i,%i,%i)]' % (
                                                                       i, j, ar, s, alt))

            if (i, j, ar, s, alt) not in Initial_Schedule_Comb:
                if (i, j, ar, s, alt) in High_Priority_Comb:
                    ##                print "High Priority:",(i,j,ar,s,alt)
                    all_tasks[(i, j, ar, s, alt)] = solver.IntervalVar(
                        int(High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]) - int(
                            High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]) - int(
                            High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]),
                        False, 'all_tasks[(%i,%i,%i,%i,%i)]' % (i, j, ar, s, alt))
                else:
                    ##                print "Normal Priority:",(i,j,ar,s,alt)
                    all_tasks[(i, j, ar, s, alt)] = solver.IntervalVar(int(Machine_Available_Start_Time[j, alt]),
                                                                       int(Machine_Available_End_Time[j, alt]),
                                                                       0, 100000000000,
                                                                       int(Machine_Available_Start_Time[j, alt]),
                                                                       int(Machine_Available_End_Time[j, alt]),
                                                                       True, 'all_tasks[(%i,%i,%i,%i,%i)]' % (
                                                                       i, j, ar, s, alt))

            active_variables[(i, j, ar, s, alt)] = solver.BoolVar()
            solver.Add(active_variables[(i, j, ar, s, alt)] == all_tasks[(i, j, ar, s, alt)].PerformedExpr().Var())

            if first_solve == 0 and (i, j, ar, s, alt) in New_Set:
                solver.Add(active_variables[(i, j, ar, s, alt)] == active_var_new[(i, j, ar, s, alt)])

            if first_solve == 0 and (i, j, ar, s, alt) in Time_Fixed_Set:
                solver.Add(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var() == start_new[(i, j, ar, s, alt)])
                solver.Add(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var() == end_new[(i, j, ar, s, alt)])

    currenttime = datetime.datetime.now()
    print "Step9:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, j, ar, s, outp) in PO_TASK_AR_SEQ_OUTP_COMB:
        if i in current_pos:  ## and outp not in Alternate_BOM_Products:
            alt_var[(i, j, ar, s, outp)] = solver.IntVar(0, 1000, 'alt_var[(%i,%i,%i,%s,%i)]' % (i, j, ar, s, outp))
            solver.Add(alt_var[(i, j, ar, s, outp)].MapTo([active_variables[(i1, j1, ar1, s1, alt)]
                                                           for (i1, j1, ar1, s1, alt) in PO_TASK_AR_SEQ_ALT_COMB
                                                           if i1 == i and ar1 == ar and s1 == s and j == j1
                                                           for (po, op, r, s3, outp1) in PO_TASK_AR_SEQ_OP_COMB
                                                           if po == i1 and j == op and ar1 == r and s == s3 and outp == outp1]))

            ##    ct0:alternative(alt_var[k], all(k2 in PO_TASK_AR_SEQ_OP_COMB, k1 in PO_TASK_AR_SEQ_ALT_COMB:
            ##      		k1.po==k.po && k.ar==k1.ar && k.seq==k1.seq
            ##      		&& k2.po==k.po && k.ar==k2.ar && k.seq==k2.seq && k2.op==k1.op && k.outp==k2.outp) all_tasks[k1]);
            ##    sr.production_order_index, sr.operation_index, sr.routing_ID,sr.sequence_id,bm.input_product_index, \
            ##bm.output_product_index
    currenttime = datetime.datetime.now()
    print "Step10:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    active_variables_ar = {}
    for (i, j, ar, s, outp) in PO_TASK_AR_SEQ_OUTP_COMB:
        if i in current_pos:
            active_variables_ar[(i, j, ar, s, outp)] = solver.BoolVar()
            solver.Add(active_variables_ar[(i, j, ar, s, outp)] == solver.Sum(
                [active_variables[(i1, j1, ar1, s1, alt)] for (i1, j1, ar1, s1, alt) in PO_TASK_AR_SEQ_ALT_COMB \
                 if i1 == i and ar1 == ar and s1 == s and j == j1 for (po, op, r, s3, outp1) in PO_TASK_AR_SEQ_OP_COMB \
                 if po == i1 and j == op and ar1 == r and s == s3 and outp == outp1]))
    getLastSequence = {}
    for (i, ar) in PO_AR_COMB:
        if i in current_pos:
            getLastSequence[i, ar] = max(s for (i1, j, ar1, s) in PO_TASK_AR_SEQ_COMB if i == i1 and ar1 == ar)

    for i in current_pos:
        solver.Add(solver.Sum(
            active_variables_ar[(i1, j, ar, s, outp)] for (i1, j, ar, s, outp) in PO_TASK_AR_SEQ_OUTP_COMB if
            i1 == i and getLastSequence[i1, ar] == s) == 1)

    for (i, j, ar, s, outp) in PO_TASK_AR_SEQ_OUTP_COMB:
        for (i1, j1, ar1, s1, outp1) in PO_TASK_AR_SEQ_OUTP_COMB:
            if i in current_pos and i == i1 and ar == ar1 and s != s1:
                solver.Add(active_variables_ar[(i, j, ar, s, outp)] == active_variables_ar[(i1, j1, ar1, s1, outp1)])

                ##forall(k in PO_AR_OP_OUTP_COMB, k1 in PO_AR_OP_OUTP_COMB:k.po==k1.po && k.ar==k1.ar && k.seq!=k1.seq)
                ##	ct11:active_variables_ar[k] == active_variables_ar[k1];

    currenttime = datetime.datetime.now()
    print "Step11:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------------------- Tasks and Sequence Creation ------------------------------------------------'

    currenttime = datetime.datetime.now()
    print "Step12:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    global non_overlapping_machines
    all_sequences = {}
    all_transitions = []
    OutputSeq_Mac_Job = {}
    Seq_ID = {}
    Route_ID = {}
    Oper_ID = {}
    Prod_ID = {}
    OutputSeq_ID = {}
    Mac_Sequence_PO = {}
    TSeq_Cnt = {}
    machines_jobs_sdst = {}
    machines_jobs_sdst_new = {}
    non_overlapping_machines_all = []
    Total_Actual_Jobs = {}
    Start_Invalid = {}
    End_Invalid = {}
    Duration_Invalid = {}

    if Sequence_Dependent_Setup_Time == "Include":
        for j in all_machines:
            ##      print "Machine: ", j[0]
            machines_jobs = []
            Seq_Cnt = 0
            Total_Actual_Jobs[j[0]] = 0
            noi = -1
            machines_jobs_sdst[j[0]] = []
            machines_jobs_sdst_new[j[0]] = []
            for (i, k, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
                ##        print i,k,ar,s,alt
                if i in current_pos and Machine_ID[i, k, ar, s, alt] == j[0]:
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i
                    ##          print i
                    OutputSeq_ID[(j[0], Seq_Cnt)] = 0
                    Seq_ID[(j[0], Seq_Cnt)] = s
                    Route_ID[(j[0], Seq_Cnt)] = ar
                    Oper_ID[(j[0], Seq_Cnt)] = k
                    Prod_ID[(j[0], Seq_Cnt)] = Product_Index[i, k, ar, s, alt]
                    machines_jobs.append(all_tasks[(i, k, ar, s, alt)])
                    machines_jobs_sdst[j[0]].append(all_tasks[(i, k, ar, s, alt)])
                    machines_jobs_sdst_new[j[0]].append(all_tasks[(i, k, ar, s, alt)])
                    Mac_Sequence_PO[(j[0], Seq_Cnt)] = i
                    Seq_Cnt = Seq_Cnt + 1
                    if Op_Type[k] != 3:
                        noi = j[0]
                        non_overlapping_machines_all.append((j[0]))
                        ##      print j[0], Seq_Cnt
            Total_Actual_Jobs[j[0]] = Seq_Cnt
            machines_jobs.extend(t[3] for t in Invalid_Timings2 if t[0] == j[0])
            machines_jobs_sdst_new[j[0]].extend(t[3] for t in Invalid_Timings2 if t[0] == j[0])
            ##      if j[0]==2:
            ##          print "machines_jobs_sdst_new: ", machines_jobs_sdst_new
            k = 1
            for t in Invalid_Timings2:
                if t[0] == j[0]:
                    ##          print j[0],Seq_Cnt, i , k
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i + k
                    OutputSeq_ID[(j[0], Seq_Cnt)] = 999
                    Start_Invalid[(j[0], Seq_Cnt)] = int(t[1])
                    End_Invalid[(j[0], Seq_Cnt)] = int(t[1]) + int(t[2])
                    Duration_Invalid[(j[0], Seq_Cnt)] = int(t[2])
                    ##          print "Duration_Invalid: ", j[0],Seq_Cnt, Duration_Invalid[(j[0],Seq_Cnt)]
                    Seq_Cnt = Seq_Cnt + 1
                    k = k + 1
            TSeq_Cnt[j[0]] = Seq_Cnt
            if noi == j[0]:
                ##        print "mac: ", j[0], noi
                disj = solver.DisjunctiveConstraint(machines_jobs, 'machine%i' % j[0])
                all_sequences[j[0]] = disj.SequenceVar()
                solver.Add(disj)
    else:
        for j in all_machines:
            machines_jobs = []
            Seq_Cnt = 0
            for (i, k, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
                if i in current_pos and Machine_ID[i, k, ar, s, alt] == j[0]:
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i
                    Seq_ID[(j[0], Seq_Cnt)] = s
                    Route_ID[(j[0], Seq_Cnt)] = ar
                    Oper_ID[(j[0], Seq_Cnt)] = k
                    Prod_ID[(j[0], Seq_Cnt)] = Product_Index[i, k, ar, s, alt]
                    machines_jobs.append(all_tasks[(i, k, ar, s, alt)])
                    Seq_Cnt = Seq_Cnt + 1
                if Op_Type[k] != 3:
                    noi = j[0]
                    non_overlapping_machines_all.append((j[0]))
            machines_jobs.extend(t[3] for t in Invalid_Timings2 if t[0] == j[0])
            k = 1
            for t in Invalid_Timings2:
                if t[0] == j[0]:
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i + k
                    OutputSeq_ID[(j[0], Seq_Cnt)] = 999
                    Seq_Cnt = Seq_Cnt + 1
                    k = k + 1
            machines_jobs.extend(t1[3] for t1 in Invalid_Timings2 if t1[0] == j[0])
            TSeq_Cnt[j[0]] = Seq_Cnt
            if noi == j[0]:
                disj = solver.DisjunctiveConstraint(machines_jobs, 'machine%i' % j[0])
                all_sequences[j[0]] = disj.SequenceVar()
                solver.Add(disj)

    print " ---------------- Renewable(Cumulative) Resource Constraint ----------------- "
    ##  S = [all_tasks[(i, j, ar, s, alt)] for (i,j,ar,s,alt) in PO_TASK_AR_SEQ_ALT_COMB if j==2]
    ##  C = 1
    ##  D = [1 for (i,j,ar,s,alt) in PO_TASK_AR_SEQ_ALT_COMB if j==2]
    ##  print "S: ", S
    ##  print "D: ", D
    ##  solver.Add(solver.Cumulative(S, D, C, "cumul"))



    non_overlapping_machines = set(non_overlapping_machines_all)

    currenttime = datetime.datetime.now()
    print "Step12_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  for seq in all_machines:
    ##    if seq[0] in non_overlapping_machines:
    ##        sequence = all_sequences[seq[0]];
    ##        sequence_count = TSeq_Cnt[seq[0]]
    ##        total_length = len(machines_jobs_sdst_new[seq[0]])
    ##        len_act_jobs = Total_Actual_Jobs[seq[0]]
    ##        len_invalid_times = total_length-len_act_jobs
    ##
    ####        print seq[0], sequence_count
    ##        for i in xrange(len_act_jobs):
    ##          for j in xrange(len_act_jobs):
    ##              if i<j and (seq[0],OutputSeq_Mac_Job[(seq[0],i)],OutputSeq_Mac_Job[(seq[0],j)]) in SETUP_TIME_NEW_COMB:
    ##                  t = sequence.Interval(i)
    ##                  t1 = sequence.Interval(j)
    ##                  for i2 in xrange(len_act_jobs,total_length):
    ##                      t2 = sequence.Interval(i2)
    ##                      if OutputSeq_ID[(seq[0],i2)] == 999 and OutputSeq_ID[(seq[0],i)] != 999 and OutputSeq_ID[(seq[0],j)] != 999:
    ####                          print i,OutputSeq_Mac_Job[(seq[0],i)], OutputSeq_Mac_Job[(seq[0],j)], SDST[(seq[0],OutputSeq_Mac_Job[(seq[0],i)],OutputSeq_Mac_Job[(seq[0],j)])], i2
    ##                          b100 = solver.IsLessOrEqualCstVar(t.EndExpr() - Start_Invalid[seq[0],i2],0)
    ##                          b200 = solver.IsGreaterOrEqualCstVar(t1.StartExpr() - End_Invalid[seq[0],i2],0)
    ##                          b101 = solver.IsGreaterOrEqualCstVar(t1.StartExpr()  - t.EndExpr() - \
    ##                                        SDST[(seq[0],OutputSeq_Mac_Job[(seq[0],i)],OutputSeq_Mac_Job[(seq[0],j)])] - Duration_Invalid[seq[0],i2], 0)
    ##
    ##                          solver.Add(b101>=b200*b100)
    ##
    ##                          b1000 = solver.IsLessOrEqualCstVar(t1.EndExpr() - Start_Invalid[seq[0],i2],0)
    ##                          b2000 = solver.IsGreaterOrEqualCstVar(t.StartExpr() - End_Invalid[seq[0],i2],0)
    ##                          b102 = solver.IsGreaterOrEqualCstVar(t.StartExpr()  - t1.EndExpr() - \
    ##                                        SDST[(seq[0],OutputSeq_Mac_Job[(seq[0],j)],OutputSeq_Mac_Job[(seq[0],i)])] - Duration_Invalid[seq[0],i2], 0)
    ##                          solver.Add(b102>=b2000*b1000)

    currenttime = datetime.datetime.now()
    print "Step12_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if Sequence_Dependent_Setup_Time == "Include":
        SDST_Var = {}
        SDST_Comb = []
        for m in SETUP_MACHINES:
            len_seq = len(machines_jobs_sdst[m[0]])

            for p in range(0, len_seq):
                for q in range(0, len_seq):
                    i = int(Mac_Sequence_PO[(m[0], p)])
                    i1 = int(Mac_Sequence_PO[(m[0], q)])
                    # if i < i1:
                    #     SDST_Comb.append((m[0], i, i1))
                    #     SDST_Var[(m[0], i, i1)] = solver.IntVar(0, 10000000000, 'SDST_Var[(%i,%i,%i)]' % (m[0], i, i1))
                    #     b1 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr(), 0)
                    #     b2 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][p].EndExpr(), 0)
                    #     solver.Add(solver.Sum((b1, b2)) == 1)
                    #     solver.Add(SDST_Var[(m[0], i, i1)] == b1 * ( machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr()) +  \
                    #                b2 * (machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][p].EndExpr()))

                    if (m[0], i, i1) in SETUP_TIME_NEW_COMB:
                        solver.Add(solver.Sum((solver.IsGreaterOrEqualCstVar(
                            machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr(),
                            SDST[(m[0], i1, i)]),
                                               solver.IsGreaterOrEqualCstVar(
                                                   machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][
                                                       p].EndExpr(), SDST[(m[0], i, i1)]))) == 1)

                    elif (m[0], i, i1) in SETUP_TIME_NEW_COMB2 or (m[0], i, i1) in SETUP_TIME_NEW_COMB3:
                        solver.Add(solver.Sum((solver.IsGreaterOrEqualCstVar(
                            machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr(), 0),
                                               solver.IsGreaterOrEqualCstVar(
                                                   machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][
                                                       p].EndExpr(), SDST[(m[0], i, i1)]))) == 1)

        if Setuptime_with_Holiday_Inbetween == "Enable":
            print " Setup b/w two jobs with a shutdown in between..."
        #
            combo1 = []
            combo2 = []
            combo3 = []
            currenttime = datetime.datetime.now()
            print "Step12_3:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        #
            for m in SETUP_MACHINES:
                total_length = len(machines_jobs_sdst_new[m[0]])
                len_act_jobs = Total_Actual_Jobs[m[0]]
                len_invalid_times = total_length - len_act_jobs
        #     ##      print m[0],total_length,len_act_jobs,len_invalid_times
                for p in xrange(len_act_jobs):
                    for q in xrange(len_act_jobs):
                        for r in xrange(len_act_jobs, total_length):
                            i = int(Mac_Sequence_PO[(m[0], p)])
                            i1 = int(Mac_Sequence_PO[(m[0], q)])
                            if (m[0], i, i1) in SETUP_TIME_NEW_COMB:
                                combo1.append((m[0], i, i1, p, q, r))
                            elif (m[0], i1, i) in SETUP_TIME_NEW_COMB:
                                combo2.append((m[0], i1, i, p, q, r))
                            elif (m[0], i, i1) in SETUP_TIME_NEW_COMB2 or (m[0], i, i1) in SETUP_TIME_NEW_COMB3:
                                combo3.append((m[0], i, i1, p, q, r))
        #
            print "combo1:", len(combo1)
            print "combo2:", len(combo2)
            print "combo3:", len(combo3)
        #
            currenttime = datetime.datetime.now()
            print "Step12_4:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        #
            if SDST_Split == "Allowed":
                print "SDST Split Allowed"
                for (m, i, i1, p, q, r) in combo1:
        #         ##            print "p,q: ", m, p, q, r
                    b100 = solver.IsLessOrEqualCstVar(machines_jobs_sdst_new[m][p].EndExpr() - Start_Invalid[(m, r)], 0)
                    b200 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst_new[m][q].StartExpr() - End_Invalid[(m, r)], 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() \
                        - SDST[(m, i, i1)] - Duration_Invalid[(m, r)], 0)
                    solver.Add(b101 >= b200 * b100)
        #
                currenttime = datetime.datetime.now()
                print "Step12_5:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        #
                for (m, i, i1, p, q, r) in combo2:
        #         ##            print "p,q: ", m, p, q, r
                    b100 = solver.IsLessOrEqualCstVar(machines_jobs_sdst_new[m][p].EndExpr() - Start_Invalid[(m, r)], 0)
                    b200 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst_new[m][q].StartExpr() - End_Invalid[(m, r)], 0)
                    b102 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() \
                        - SDST[(m, i1, i)] - Duration_Invalid[(m, r)], 0)
                    solver.Add(b102 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_6:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
                for (m, i, i1, p, q, r) in combo3:
                    b100 = solver.IsLessOrEqualCstVar(machines_jobs_sdst_new[m][p].EndExpr() - Start_Invalid[(m, r)], 0)
                    b200 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst_new[m][q].StartExpr() - End_Invalid[(m, r)], 0)
                    b103 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() \
                        - SDST[(m, i, i1)] - Duration_Invalid[(m, r)], 0)
                    solver.Add(b103 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_7:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            else:
                print "SDST Split Not Allowed"
                for (m, i, i1, p, q, r) in combo1:
                    b100 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b200 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr(), 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() - SDST[
                            (m, i, i1)], 0)
                    b201 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr() - SDST[
                            (m, i, i1)], 0)
                    solver.Add(b101 + b201 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_10:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
                for (m, i, i1, p, q, r) in combo2:
                    b100 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b200 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr(), 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() - SDST[
                            (m, i1, i)], 0)
                    b201 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr() - SDST[
                            (m, i1, i)], 0)
                    solver.Add(b101 + b201 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_11:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
                for (m, i, i1, p, q, r) in combo3:
                    b100 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b200 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr(), 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b201 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr() - SDST[
                            (m, i, i1)], 0)
                    solver.Add(b101 + b201 >= b200 * b100)

    currenttime = datetime.datetime.now()
    print "Step13:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    print ' ------------------------ Initial Schedule ---------------------------------------------'
    ##  if int(Freeze_Time_Fence)>0:
    ##    print "In freeze"
    ##    for (i,j,ar,s,alt) in Initial_Schedule_Comb:
    ##        if i in current_pos:
    ##            print i,j,Task_Active_Status[(i,j,ar,s,alt)], Task_Begin_Time[(i,j,ar,s,alt)], Task_End_Time[(i,j,ar,s,alt)]
    ##          solver.Add(active_variables[(i,j,ar,s,alt)] == Task_Active_Status[(i,j,ar,s,alt)])
    ##          solver.Add(all_tasks[(i,j,ar,s,alt)].SafeStartExpr(-1).Var() == Task_Begin_Time[(i,j,ar,s,alt)])
    ##          solver.Add(all_tasks[(i,j,ar,s,alt)].SafeEndExpr(-1).Var() == Task_End_Time[(i,j,ar,s,alt)])

    print ' ----------------------------------------------- Precedences inside a job.------------------------------------------------'

    for (i, j, j1, ar, s, s1, alt, alt1) in PRECEDENCE_CONST_COMB:
        if i in current_pos:
            t1 = all_tasks[(i, j, ar, s, alt)]
            t2 = all_tasks[(i, j1, ar, s1, alt1)]
            if j1 in Form_QC_ID:
                # print "askk", j1
                solver.Add(t2.StartsAfterEnd(t1))
            else:
                solver.Add(t2.SafeStartExpr(9999999).Var() >= t1.SafeEndExpr(-1).Var() - OP_Setup_Time[i, j1, ar, s1, alt1])
            if Op_Type[j] == 3:
                solver.Add(t2.StartsAtEnd(t1))
            if Op_Type[j1] == 3:
                solver.Add(t2.StartsAtEnd(t1))

    Shelf_Life_Gap = {}
    for (i, j, j1, ar, s, s1, alt, alt1) in SHELF_LIFE_COMB:
        if i in current_pos and Product_Shelf_Life[Shelf_Life_Prod_Index[(i, j, j1, ar, s, s1, alt, alt1)]] > 0:
            t1 = all_tasks[(i, j, ar, s, alt)]
            t2 = all_tasks[(i, j1, ar, s1, alt1)]
            ##        print i, Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)],Product_Shelf_Life[Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)]]
            ##        Shelf_Life_Gap[(i,j,j1,ar,s,s1,alt,alt1)] = solver.IntVar(0, Product_Shelf_Life[Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)]], \
            ##                                                                  'Shelf_Life_Gap[(%i,%i,%i,%i,%i,%i,%i,%i)]' % (i,j,j1,ar,s,s1,alt,alt1))
            ##        b1 = solver.IsLessOrEqualCstVar(Shelf_Life_Gap[(i,j,j1,ar,s,s1,alt,alt1)] -(t2.SafeStartExpr(-1).Var() - t1.SafeEndExpr(-1).Var()), 0)
            b1 = solver.IsLessOrEqualCstVar(
                (t2.SafeStartExpr(-1).Var() - t1.SafeEndExpr(-1).Var()) - Product_Shelf_Life[
                    Shelf_Life_Prod_Index[(i, j, j1, ar, s, s1, alt, alt1)]], 0)
            b2 = solver.IsEqualCstVar(active_variables[(i, j, ar, s, alt)], 1)
            b3 = solver.IsEqualCstVar(active_variables[(i, j1, ar, s1, alt1)], 1)
            solver.Add(b1 >= b2 * b3)



            ##  print ' ----------------------------------------------- BOM Explosion Constraints  ------------------------------------------------'
    currenttime = datetime.datetime.now()
    print "Step14:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    global Input_Product_Qty
    global Inv_Product_Qty
    global Output_Product_Qty

    Input_Product_Qty = {}
    Inv_Product_Qty = {}
    Output_Product_Qty = {}

    Total_Qty = {}
    for p in Products:
        Total_Qty[p] = 0
        for l in range(0, len(BOH_T)):
            if int(BOH_T[l][0]) == int(p):
                Total_Qty[p] = Total_Qty[p] + int(BOH_T[l][2])

    print ' ----------------------------------------------- Domain Calculation  ------------------------------------------------'

    global Dom_IP
    global PO_OP_SFG_Comb
    global Dom_OP
    Dom_IP = {}
    Dom_OP = {}
    PO_OP_SFG_Comb=[]
    print "current_pos count: ", len(current_pos)
    for (i1, ar1) in PO_AR_COMB:
        if i1 in current_pos:
            for s in range(getLastSequence[i1, ar1], 0, -1):
                for (i2, j2, ar2, s2) in PO_TASK_AR_SEQ_COMB:
                    # if i2 == 23 and i1==i2:
                    #     print "s2:",s2,i2,ar2
                    if i1 == i2 and s == s2 and ar1 == ar2:
                        for (i, j, ar, alt, ip, op) in PO_BOM_COMB:
                            # if i == 23 and i==i2:
                            #     print i,j,ar,alt,ip,op
                            if i == i1 and ar == ar1 and j == j2:
                                # if i1 == 23:
                                #     print "s: ", i,j,ar,ip,op
                                if Product_Type_ID[int(op)] == 3:
                                    Dom_IP[(i, j, ip)] = int(Order_Quantity[i] * getAttachRate(op, ip, j))
                                    PO_OP_SFG_Comb.append((i,j,ip))
                                    Dom_OP[(i, j, op)] = Order_Quantity[i]
                                    # if i1 == 23:
                                    #     print 1,s,j,ip,op, Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)]
                                    for (i3, j3, j4, ar3, s3, s4, p) in MAT_BALANCE_CONST:
                                        if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
                                            if Product_Type_ID[int(ip)] != 1:
                                                Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]
                                elif Product_Type_ID[int(op)] == 2 and getLastSequence[i1, ar1] == s:
                                    Dom_IP[(i, j, ip)] = int(Order_Quantity[i] * getAttachRate(op, ip, j))
                                    PO_OP_SFG_Comb.append((i, j, ip))
                                    Dom_OP[(i, j, op)] = Order_Quantity[i]
                                    # if i1 == 23:
                                    #     print 1,s,j,ip,op, Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)]
                                    for (i3, j3, j4, ar3, s3, s4, p) in MAT_BALANCE_CONST:
                                        if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
                                            if Product_Type_ID[int(ip)] != 1:
                                                Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]
                                elif Product_Type_ID[int(op)] == 2:
                                    # if i1 == 23:
                                    #     print 100, s,j,ar,ip,op
                                    if Product_Type_ID[int(ip)] == 1:
                                        Dom_IP[(i, j, ip)] = Dom_OP[(i, j, op)] * getAttachRate(op, ip, j)
                                        PO_OP_SFG_Comb.append((i, j, ip))
                                        if Dom_IP[(i, j, ip)] == 0:
                                            ##                            print i,j,ip,op,Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
                                            Dom_IP[(i, j, ip)] = 1
                                    else:
                                        Dom_IP[(i, j, ip)] = int(Dom_OP[(i, j, op)] * getAttachRate(op, ip, j))
                                        PO_OP_SFG_Comb.append((i, j, ip))
                                        if Dom_IP[(i, j, ip)] == 0:
                                            ##                            print i,j,ip,op,Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
                                            Dom_IP[(i, j, ip)] = 1
                                            ##                      print 2,s,j,ip,op, Dom_IP[(i,j,ip)] , Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
                                    for (i3, j3, j4, ar3, s3, s4, p) in MAT_BALANCE_CONST:
                                        if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
                                            if Product_Type_ID[int(ip)] != 1:
                                                Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]


    # for (i,j,ar,alt,ip,op) in PO_BOM_COMB:
    #    if i in current_pos:
    #        print "Domain i,j,ip,op :", i,j,ip,op, Dom_IP[(i,j,ip)],Dom_OP[(i,j,op)]

    SFG_FG_Domain = []
    for (i,j,ar,alt,ip,op) in PO_BOM_COMB:
           if Product_Type_ID[int(ip)]==2 and Product_Type_ID[int(op)]==3:
            SFG_FG_Domain.append((ip, op,Dom_IP[(i,j,ip)],Dom_OP[(i,j,op)],getAttachRate(op,ip,j)))
    SFG_FG_Domain = list(set(SFG_FG_Domain))
    for i in range(0, len(SFG_FG_Domain)):
        SFG_FG_Domain_Comb.append((SFG_FG_Domain[i][0], SFG_FG_Domain[i][1]))
        FG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][3]
        SFG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][2]
        AR[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][4]
    currenttime = datetime.datetime.now()
    print "Step14:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, j, ar, alt, ip) in PO_AR_BOM_IP_COMB:
        if i in current_pos:
            if Product_Type_ID[int(ip)] != 1:
                Input_Product_Qty[(i, j, ar, alt, int(ip))] = solver.IntVar([0, Dom_IP[(i, j, ip)]],
                                                                            'Input_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                                int(i), int(j), int(ar), int(alt), int(ip)))
            else:
                Input_Product_Qty[(i, j, ar, alt, int(ip))] = solver.IntVar(0, int(Dom_IP[(i, j, ip)]+1),
                                                                            'Input_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                            int(i), int(j), int(ar), int(alt), int(ip)))

    # horizon = 3000
    # b1 = {}
    # b2 = {}
    # for t in range(horizon):
    #     for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
    #         b1[(i, j, ar, s, alt,t)] = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var(), t)
    #         b2[t] = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), t)
    #
    # print "Reading Done"
    # for (i, j, ar, alt, ip) in PO_AR_BOM_IP_COMB:
    #   if i==0:
    #     for (po,po1) in  PO_Relation     :
    #         print "po,po1 :", po, po1
    #         if po==i and Sequence_ID[(i,j,ar)]==1:
    #             for t in range(horizon):
    #               solver.Add(
    #                   solver.Sum(b1[t1] * Dom_OP[(i1,j1,op)] for t1 in range(horizon)
    #                                                         for (i1, j1, ar1, alt1, op) in PO_AR_BOM_OP_COMB
    #                                                         if po1==i1 and Sequence_ID[(i1,j1,ar1)] == getLastSequence[i1, ar1] )
    #                           -  b2[t] * Dom_IP[(i,j,ip)] >= 0)



    currenttime = datetime.datetime.now()
    print "Step16:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, j, ar, alt, op) in PO_AR_BOM_OP_COMB:
        if i in current_pos:
            Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntVar([0, Dom_OP[(i, j, op)]],
                                                                    'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                    int(i), int(j), int(ar), int(alt), int(op)))
            ##    print '--------------------------------------------- Batch Resource Capacity ----------------------------------------------------------'

            ##        if Op_Type[j]==2 and Max_Batch_Size[(i,j,alt)]>0:
            ##            solver.Add(Output_Product_Qty[(i,j,ar,alt,op)] <= Max_Batch_Size[(i,j,alt)])

            ##    print '--------------------------------------------- Batch Resource Capacity ----------------------------------------------------------'

    for (i, j, ar, alt, op) in PO_AR_BOM_OP_COMB:
        if i in current_pos:
            if Op_Type[j] != 2:
                Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntVar([0, Dom_OP[(i, j, op)]],
                                                                        'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                        int(i), int(j), int(ar), int(alt), int(op)))
            else:
                if Dom_OP[(i, j, op)] > Max_Batch_Size[(i, j, alt)]:
                    ##                print "Dom > Batch: ",i,j,op,Dom_OP[(i,j,op)] , Max_Batch_Size[(i,j,alt)]
                    Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntConst(0,
                                                                              'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                              int(i), int(j), int(ar), int(alt),
                                                                              int(op)))
                else:
                    ##                print "Dom < Batch: ",i,j,op,Dom_OP[(i,j,op)] , Max_Batch_Size[(i,j,alt)]
                    Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntVar([0, Dom_OP[(i, j, op)]],
                                                                            'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                            int(i), int(j), int(ar), int(alt), int(op)))

    print '--------------------------------------------- Alternate BOM ----------------------------------------------------------'

    b91 = {}
    b92 = {}
    if len(Alternate_BOM) > 0:
        for (i, j, op) in Alternate_BOM_COMB:
            if i in current_pos:
                for (i1, j1, ar1, alt1, ip, op1) in Alt_BOM_Comb:
                    if i == i1 and j == j1 and op == op1:
                        b91[(i1, j1, ar1, alt1, ip, op1)] = solver.IntVar([0, 1], 'b91[(%i,%i,%i,%i,%i,%i)]' % (int(i1), int(j1), int(ar1), int(alt1), int(ip), int(op1)))
                        b92[(i1, j1, ar1, alt1, ip, op1)] = solver.IntVar([0, 1], 'b92[(%i,%i,%i,%i,%i,%i)]' % (int(i1), int(j1), int(ar1), int(alt1), int(ip), int(op1)))
                        for (i2, j2, ar2, s2) in PO_TASK_AR_SEQ_COMB:
                            if i2 == i and j2 == j and ar1 == ar2:
                                # print i,j,ar,ip,op1
                                solver.Add(b91[(i1, j1, ar1, alt1, ip, op1)] == solver.IsGreaterCstVar(Input_Product_Qty[(i1, j1, ar1, alt1, ip)], 0))
                                solver.Add(b92[(i1, j1, ar1, alt1, ip, op1)] == solver.IsEqualCstVar(active_variables[(i2, j2, ar2, s2, alt1)], 1))
                for (i10, st_val) in Alt_BOM_Start_Value:
                    if i10 == i:
                        solver.Add(solver.Sum(b91[(i1, j1, ar1, alt1, ip, op1)] * b92[(i1, j1, ar1, alt1, ip, op1)] for (i1, j1, ar1, alt1, ip, op1) in Alt_BOM_Comb \
                                              if 0 <= Alt_BOM_Flag[(i1, j1, ar1, alt1, ip, op1)] - st_val < 10 and i == i1 and j == j1 and op == op1) == 1)


    ##  print '--------------------------------------------- Max RM Available ----------------------------------------------------------'
    global maxrmavailable1
    global maxrmavailable
    maxrmavailable1 = {}
    maxrmavailable = {}
    for p in Products:
        if Product_Type_ID[p] == 1 and p in BOH_Total_Comb:
            maxrmavailable1[p] = max(BOH[p1, t] for (p1, t) in BOH_Comb if int(p1) == int(p) and int(t) < int(PlanningHorizon))
            ##      print 1,p,PlanningHorizon, maxrmavailable1[p]

    for p in Products:
        if Product_Type_ID[p] == 1 and p in BOH_Total_Comb:
            maxrmavailable[p] = max(BOH[p1, t] for (p1, t) in BOH_Comb if
                                    int(p1) == int(p) and int(t) < int(PlanningHorizon) and (
                                    int(BOH[p1, t]) < int(maxrmavailable1[p]) or [maxrmavailable1[p]]))
            ##      print p,maxrmavailable1[p], maxrmavailable[p]

            ##  print '--------------------------------------------- Interval Duration ----------------------------------------------------------'

    currenttime = datetime.datetime.now()
    print "Step19:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    Mult_Ops = []
    for (i, j, ar, s) in PO_TASK_AR_SEQ_COMB:
        if i in current_pos:
            for (i1, j1, ar1, alt, op) in PO_AR_BOM_OP_COMB:
                if i1 == i and j == j1 and ar == ar1:
                    ## to avoid output qty var and active var taking diferent values for alternate tasks
                    solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() <= active_variables[(i, j, ar, s, alt)] * 1000000000000)

                    if Op_Type[j] == 1:
                        ##          print "Discrete"
                        if (i, j) in Mult_Ops:
                            ##                print "2OP Domain: ", i,j, Two_Output_Dom_OP[(i,j)]
                            solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var()
                                       == active_variables[(i, j, ar, s, alt)] * (
                                       OP_Setup_Time[i, j, ar, s, alt] + Teardown_Time[i, j, ar, s, alt]
                                       + int(Two_Output_Dom_OP[(i, j)] * Processing_Time[
                                           i, j, ar, s, alt, int(Two_Output_Dom_OP[(i, j)])])))
                        elif int(Dom_OP[(i, j, op)]) == Order_Quantity[i]:
                            solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var()
                                       == active_variables[(i, j, ar, s, alt)] * (
                                       OP_Setup_Time[i, j, ar, s, alt] + Teardown_Time[i, j, ar, s, alt]
                                       + int(Dom_OP[(i, j, op)] * Processing_Time[i, j, ar, s, alt])))
                    elif Op_Type[j] == 2 and int(Dom_OP[(i, j, op)]) == int(Order_Quantity[i]):
                        # print int(Dom_OP[(i, j, op)]), int(Order_Quantity[i])
                        solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var()
                                   == active_variables[(i, j, ar, s, alt)] * (
                                       OP_Setup_Time[i, j, ar, s, alt] + int(Processing_Time[i, j, ar, s, alt]) +
                                       Teardown_Time[i, j, ar, s, alt]))
                    elif Op_Type[j] == 3 and int(Dom_OP[(i, j, op)]) == int(Order_Quantity[i]):
                        ##              print "Holding"
                        for (i2, j2, alt2) in Prod_Res_Holding_Time_Comb:
                            if i2 == i and j == j2 and alt == alt2:
                                ##                  print "Holding:"
                                ##                  print "y,i,j,op: ", i,j, Min_Holding_Time[i,j,alt], Max_Holding_Time[i,j,alt]
                                solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() >= active_variables[
                                    (i, j, ar, s, alt)] * Min_Holding_Time[i, j, alt])
                                solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() <= active_variables[
                                    (i, j, ar, s, alt)] * Max_Holding_Time[i, j, alt])

                    if s == getLastSequence[i, ar]:
                        solver.Add(
                            Output_Product_Qty[(i1, j1, ar, alt, int(op))] == active_variables[(i, j, ar, s, alt)] *
                            Order_Quantity[i])
                    else:
                        solver.Add(
                            Output_Product_Qty[(i1, j1, ar, alt, int(op))] >= active_variables[(i, j, ar, s, alt)] *
                            Dom_OP[(i, j, op)])

    print '--------------------------------------------- INVENTORY EQUATIONS ----------------------------------------------------------'

    InvQty = {}

    if RMConstraint == 'Constrained':
        print "------------------------------ RMConstraint ------------------------------------------------------------"
        b41 = {}
        b51 = {}
        for (i, j, ar, s, alt, ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
            if i in current_pos:
                for index, (p1, t1) in enumerate(BOH_Comb, start=0):
                    if int(p1) == int(ip) and index < len(BOH_Comb) - 1 and int(BOH_Comb[index + 1][0]) == int(p1) and t1 < BOH_Comb[index + 1][1]:
                        t2 = BOH_Comb[index + 1][1]
                        if Product_Type_ID[int(ip)] == 1:
                            b41[(i, j, ar, s, alt, ip, t1)] = solver.IntVar(0, 1, 'b41[(%i,%i,%i,%i,%i,%i,%i)]' % (int(i), int(j), int(ar), int(s), int(alt), int(ip), t1))
                            b51[(i, j, ar, s, alt, ip, t1)] = solver.IntVar(0, 1, 'b51[(%i,%i,%i,%i,%i,%i,%i)]' % (int(i), int(j), int(ar), int(s), int(alt), int(ip), t1))
                            solver.Add(b41[(i, j, ar, s, alt, ip, t1)] == solver.IsGreaterOrEqualCstVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), t1))
                            solver.Add(b51[(i, j, ar, s, alt, ip, t1)] == solver.IsLessCstVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), t2))

        m = []
        for index, (p1, t1) in enumerate(BOH_Comb, start=0):
            if p1 not in Alternate_BOM_Products:
                if index < len(BOH_Comb) - 1 and int(BOH_Comb[index + 1][0]) == int(p1) and t1 < BOH_Comb[index + 1][1]:
                    # print p1, t1, BOH[(p1,t1)]
                    t2 = BOH_Comb[index + 1][1]
                    if Product_Type_ID[int(p1)] == 1:
                        m.append(int(index))
                        solver.Add(solver.Sum(int(Dom_IP[(i, j, ip)]) * active_variables[(i, j, ar, s, alt)] * \
                                              solver.Sum(b41[(i, j, ar, s, alt, ip, BOH_Comb[t][1])] * b51[(i, j, ar, s, alt, ip, BOH_Comb[t][1])] \
                                                         for t in m if t < len(BOH_Comb) and int(BOH_Comb[t][0]) == int(p1)) \
                                              for (i3, j3, ar3, alt3, ip3) in PO_AR_BOM_IP_COMB if ip3 == p1 for (i, j, ar, s, alt, ip) in PO_TASK_AR_SEQ_ALT_IP_COMB \
                                              if i in current_pos and i3 == i and j == j3 and ar == ar3 and alt3 == alt and p1 == ip and alt3 != -1) <= BOH[(p1,t1)])
                        # print sum(Input_Product_Qty[(i, j, ar, alt, ip)] * active_variables[(i, j, ar, s, alt)] * sum(b41[(i, j, ar, s, alt, ip, BOH_Comb[t][1])] * b51[(i, j, ar, s, alt, ip, BOH_Comb[t][1])] for t in m if t < len(BOH_Comb) and int(BOH_Comb[t][0]) == int(p1)) for (i3, j3, ar3, alt3, ip3) in PO_AR_BOM_IP_COMB if ip3 == p1 for (i, j, ar, s, alt, ip) in PO_TASK_AR_SEQ_ALT_IP_COMB if i in current_pos and i3 == i and j == j3 and ar == ar3 and alt3 == alt and p1 == ip and alt3 != -1) <= int(BOH[(p1,t1)])

        m1 = []
        for index, (p1, t1) in enumerate(BOH_Comb, start=0):
            if p1 in Alternate_BOM_Products:
                ##          print p1,t1, BOH[(p1,t1)]
                if index < len(BOH_Comb) - 1 and int(BOH_Comb[index + 1][0]) == int(p1) and t1 < BOH_Comb[index + 1][1]:
                    t2 = BOH_Comb[index + 1][1]
                    if Product_Type_ID[int(p1)] == 1:
                        m1.append(int(index))
                        solver.Add(solver.Sum(Input_Product_Qty[(i,j,ar,alt,ip)] * solver.IsGreaterCstVar(Input_Product_Qty[(i, j, ar, alt, ip)], 0) * \
                                              solver.Sum(b41[(i, j, ar, s, alt, ip, BOH_Comb[t][1])] * b51[(i, j, ar, s, alt, ip, BOH_Comb[t][1])] \
                                                         for t in m1 if t < len(BOH_Comb) and int(BOH_Comb[t][0]) == int(p1)) \
                                              for (i3, j3, ar3, alt3, ip3) in PO_AR_BOM_IP_COMB if ip3 == p1 for (i, j, ar, s, alt, ip) in PO_TASK_AR_SEQ_ALT_IP_COMB \
                                              if i in current_pos and i3 == i and j == j3 and ar == ar3 and alt3 == alt and ip3 == ip) <= BOH[(p1, t1)])

    valid_po_comb = []
    fg_lots_with_no_sfg_lots=[] # Lots which doesn't have relevant SFG lots
    for (i, p, q) in FG_Lots:  # production_order_index,pm.product_index, order_quantity
        if i==90:
            print "ask"
        temp = 0 # to find FG lots which doesn't have enough SFG lots
        for (m, n) in SFG_FG_Domain_Comb:        # ip, op  or sfg,fg
            if n == p:
                FG_bal = 0
                for (i1, p1, q1) in SFG_Lots:  #production_order_index,pm.product_index,order_quantity
                    if m == p1:# and (i1,n) in SFG_PO_FG_Lot_Relation:
                        if FG_bal == 0:
                            fg_current = int(FG_Order_Qty[i] * AR[(p1, p)])
                            FG_bal = 9999
                        if (i, i1) in PO_Relation:
                            if fg_current >= SFG_Order_Qty[i1] and SFG_Order_Qty[i1] > 0:
                                fg_current = fg_current - SFG_Order_Qty[i1]
                                SFG_Order_Qty[i1] = 0
                                valid_po_comb.append((i, i1))
                                temp = 1
                            elif fg_current > 0 and fg_current < SFG_Order_Qty[i1]:
                                SFG_Order_Qty[i1] = SFG_Order_Qty[i1] - fg_current
                                fg_current = 0
                                valid_po_comb.append((i, i1))
                                temp = 2
        if temp == 0:
            fg_lots_with_no_sfg_lots.append(i)
    # print "valid_po_comb: ", valid_po_comb
    print "fg_lots_with_no_sfg_lots: ", fg_lots_with_no_sfg_lots

    global all_ends
    global all_starts
    all_starts = {}
    all_ends = {}

    for i in current_pos:
        all_ends[i] = solver.Max([all_tasks[(i1, j, ar, s, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB if i1 == i and alt1 != -1])
        all_starts[i] = solver.Min([all_tasks[(i1, j, ar, s, alt1)].SafeStartExpr(999999999).Var() for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB if i1 == i and alt1 != -1])



    print " ---- Relation between SFG and FG lots timings -------- "
    print "------ FG lot should start only after all the required SFG lots are finished ------------ "
    for i in all_pos:
        if i in current_pos and Product_Type_ID[int(getProductID(i))] == 3:
            # print "ask: ", i
            solver.Add(solver.Min([all_tasks[(i1, j, ar, s, alt1)].SafeStartExpr(999999999).Var() for (po, po1) in set(valid_po_comb) if i==po \
                                   for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
                                   if i1 == po and alt1 != -1]) >= \
                       solver.Max([all_tasks[(i1, j, ar, s, alt1)].SafeEndExpr(-1).Var() - OP_Setup_Time[i1, j, ar, s, alt1] * active_variables[(i1, j, ar, s, alt1)] \
                                   for (po, po1) in set(valid_po_comb) if i==po \
                                   for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
                                   if i1 == po1 and i1 in current_pos and alt1 != -1]))

    print " ------- Only if FG lot is produced, then SFG lot should be produced ----------- "
    if Produce_SFG_Lot_on_FG_Lot_production == "Enable":
        for (fg_po, sfg_po) in set(valid_po_comb):
            b81 = solver.IsGreaterOrEqualCstVar(solver.Min([all_tasks[(i, j, ar, s, alt)].SafeStartExpr(999999999).Var() \
                                                            for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB if i == fg_po and s==1 and alt != -1]), int(PlanningHorizon)+1)  #FG
            b82 = solver.IsGreaterOrEqualCstVar(solver.Min([all_tasks[(i, j, ar, s, alt)].SafeStartExpr(999999999).Var() \
                                                            for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB if i == sfg_po and s==1 and alt != -1]), int(PlanningHorizon)+1)    #SFG
            b83 = solver.IsLessOrEqualCstVar(all_ends[fg_po], int(PlanningHorizon))       #FG
            b84 = solver.IsLessOrEqualCstVar(all_ends[sfg_po], int(PlanningHorizon))     #SFG
            solver.Add(b81 == b82)
            solver.Add(b83 == b84)
    print " --------- For each lot, if one operation is pushed out of Planning Horizon, all operations will be pushed out ------ "
    for i in current_pos:
        b71 = solver.IsGreaterOrEqualCstVar(all_starts[i], int(PlanningHorizon) + 1)
        b73 = solver.IsGreaterOrEqualCstVar(all_ends[i], int(PlanningHorizon) + 1)
        solver.Add(b73 == b71)
    currenttime = datetime.datetime.now()
    print "Step20_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    MaxDueDate = max(getOrderDueDate(i) for i in current_pos)
    # for i in current_pos:
    #     print 100, i, getOrderQuantity(i)
    #totalOrderQty = sum(getOrderQuantity(i) for i in current_pos)

    MaxOrderDelay = int(LDHorizon)


    all_ends_task = {}
    all_starts_task = {}
    all_ends_operation = {}

    DV = {}
    ##  makespan_po={}


    makespan = solver.Max([all_tasks[(i1, j1, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j1, ar1, s1, alt1) in PO_TASK_AR_SEQ_ALT_COMB
         if i1 in current_pos])

    # for (po1,po2) in PO_Relation:
    #   print "po1,po2: ", po1,po2
    #  solver.Add(solver.Min([all_tasks[(i1, j, ar, s, alt1)].SafeStartExpr(999999999).Var() for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
    #               if i1 == po1 and alt1 <> -1]) >= \
    #           solver.Max([all_tasks[(i1, j, ar, s, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
    #             if i1 == po2 and alt1 <> -1]))



    currenttime = datetime.datetime.now()
    print "Step21:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, ar, s) in PO_AR_SEQ_COMB:
        if i in current_pos:
            all_ends_task[(i, ar, s)] = solver.Max([all_tasks[(i1, j, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar1, s1, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
                 if i1 == i and ar1 == ar and s1 == s and alt1 <> -1])

    currenttime = datetime.datetime.now()
    print "Step21:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, ar, s) in PO_AR_SEQ_COMB:
        if i in current_pos:
            all_starts_task[(i, ar, s)] = solver.Min([all_tasks[(i1, j, ar1, s1, alt1)].SafeStartExpr(99999999999).Var() for (i1, j, ar1, s1, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
                 if i1 == i and ar1 == ar and s1 == s and alt1 != -1])

    currenttime = datetime.datetime.now()
    print "Step22:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for o in all_operations:
        all_ends_operation[o] = solver.Max([all_tasks[(i1, j, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar1, s1, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
             if i1 in current_pos and int(j) == int(o[0])])
    currenttime = datetime.datetime.now()
    print "Step23:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Order_Delay = {}
    Order_Excess = {}
    M = 10000000000
    for i in current_pos:
        Order_Delay[i] = solver.IntVar(0, int(LDHorizon) + int(M))
        Order_Excess[i] = solver.IntVar(0, int(MaxDueDate) + int(M))
        solver.Add(all_ends[i] + Order_Excess[i] == Order_Delay[i] + getOrderDueDate(i))

    ##  print ' ----------------------------------------------- Objective(Make Span) ------------------------------------------------'


    obj_var = 0
    PO_Max = max(current_pos)
    print "current_pos: ", current_pos
    print "max: ", PO_Max
    if all_pos_iteration == 0:
        print "High Priority Iteration"
        obj_var = solver.Sum(Order_Delay[i] * (PO_Max + 1 - i) for i in current_pos)
    else:
        obj_var = solver.Sum(Order_Delay[i] * (PO_Max + 1 - i) for i in current_pos).Var() + \
                  solver.Sum(all_ends_task[(i, ar, s)] * (15 - s) * (PO_Max + 1 - i) for (i, ar, s) in PO_AR_SEQ_COMB if
                             i in current_pos).Var()

    objective = solver.Minimize(obj_var, 1)

    if first_solve == 0:
        if iter_solve == 1:
            solver.Add(solver.Max([all_tasks[(i1, j1, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j1, ar1, s1, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
                 if i1 in current_pos]) <= makespan_value)
        else:
            solver.Add(obj_var < obj_value)
            solver.Add(solver.Max(
                [all_tasks[(i1, j1, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j1, ar1, s1, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
                 if i1 in current_pos]) <= makespan_value)

            ##  print ' ----------------------------------------------- Search Phases ------------------------------------------------'
    vars_phase = solver.Phase([obj_var],
                              solver.CHOOSE_FIRST_UNBOUND,
                              solver.ASSIGN_MIN_VALUE)

    sequence_phase = solver.Phase([all_sequences[i[0]] for i in all_machines if i[0] in non_overlapping_machines],
                                  solver.SEQUENCE_DEFAULT)

    if SchedulingType == "Forward Scheduling":
        intervalPhase = solver.Phase([all_tasks[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB if i in current_pos],
            solver.INTERVAL_SET_TIMES_FORWARD)
    else:
        intervalPhase = solver.Phase([all_tasks[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB if i in current_pos],
            solver.INTERVAL_SET_TIMES_BACKWARD)

    active_phase = solver.Phase([active_variables[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB if i in current_pos],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    active_variables_ar_phase = solver.Phase([active_variables_ar[(i, j, ar, s, outp)] for (i, j, ar, s, outp) in PO_TASK_AR_SEQ_OUTP_COMB if
         i in current_pos],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    BOM_phase_IP = solver.Phase([Input_Product_Qty[(i, j, ar, alt, ip)] for (i, j, ar, alt, ip) in PO_AR_BOM_IP_COMB if i in current_pos],
        solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
        solver.ASSIGN_MAX_VALUE)

    BOM_phase_OP = solver.Phase([Output_Product_Qty[(i, j, ar, alt, op)] for (i, j, ar, alt, op) in PO_AR_BOM_OP_COMB if i in current_pos and Product_Type_ID[int(ip)] <> 3],
                                solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
                                solver.ASSIGN_MAX_VALUE)

    all_ends_phase = solver.Phase([all_ends_task[(i, ar, s)] for (i, ar, s) in PO_AR_SEQ_COMB if i in current_pos],
                                  solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
                                  solver.ASSIGN_MIN_VALUE)

    if Sequence_Dependent_Setup_Time == "Include":
        sdst_phase = solver.Phase([SDST_Var[(m, i, i1)] for (m, i, i1) in SDST_Comb if i in current_pos and i1 in current_pos],
        solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
        solver.ASSIGN_RANDOM_VALUE)

    b91_phase = solver.Phase([b91[(i1, j1, ar1, alt1, ip, op1)] for (i, j, op) in Alternate_BOM_COMB for (i1, j1, ar1, alt1, ip, op1) in Alt_BOM_Flag if i == i1 and j == j1 and op == op1 and i in current_pos],
        solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
        solver.ASSIGN_RANDOM_VALUE)

    ##  shelf_phase = solver.Phase([ Shelf_Life_Gap[(i,j,j1,ar,s,s1,alt,alt1)] for (i,j,j1,ar,s,s1,alt,alt1) in SHELF_LIFE_COMB \
    ##                               if i in current_pos and Product_Shelf_Life[Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)]]>0],
    ##                      solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
    ##                      solver.ASSIGN_RANDOM_VALUE)

    currenttime = datetime.datetime.now()
    print "Step23_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if Sequence_Dependent_Setup_Time == "Include":
        main_phase = solver.Compose([active_variables_ar_phase, active_phase, intervalPhase,  all_ends_phase, sequence_phase,vars_phase])
    # main_phase = solver.Compose([active_variables_ar_phase, active_phase, b91_phase, intervalPhase, BOM_phase_IP, sequence_phase, vars_phase])
    ##    main_phase = solver.Compose([active_variables_ar_phase,active_phase,intervalPhase,shelf_phase,sdst_phase,all_ends_phase, BOM_phase_IP,sequence_phase,vars_phase])
    else:
        main_phase = solver.Compose([active_variables_ar_phase, active_phase, intervalPhase, BOM_phase_IP, sequence_phase, vars_phase])

    ##  print ' ----------------------------------------------- Search Log ------------------------------------------------'
    search_log = solver.SearchLog(1000, obj_var)
    first_solution = solver.Assignment()

    for (i, j, ar, alt, ip) in PO_AR_BOM_IP_COMB:
        if i in current_pos:
            first_solution.Add(Input_Product_Qty[(i, j, ar, alt, ip)])
            ##    first_solution.Add(Inv_Product_Qty[(i,j,ar,alt,ip)])

    for (i, j, ar, alt, op) in PO_AR_BOM_OP_COMB:
        if i in current_pos:
            first_solution.Add(Output_Product_Qty[(i, j, ar, alt, op)])

    for i in current_pos:
        first_solution.Add(all_ends[i])
        first_solution.Add(all_starts[i])

    for i in all_machines:
        if i[0] in non_overlapping_machines:
            first_solution.Add(all_sequences[i[0]])

    first_solution.AddObjective(obj_var)

    for seq in all_machines:
        if seq[0] in non_overlapping_machines:
            ##      if x == seq[0]:
            ##        print "x:", x, seq[0]
            sequence = all_sequences[seq[0]];
            sequence_count = TSeq_Cnt[seq[0]]
            for i in range(sequence_count):
                t = sequence.Interval(i)
                first_solution.Add(t.SafeStartExpr(-1).Var())
                first_solution.Add(t.SafeEndExpr(-1).Var())
                first_solution.Add(t.DurationExpr().Var())

    for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
        if i in current_pos:
            first_solution.Add(active_variables[(i, j, ar, s, alt)])
            first_solution.Add(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var())
            first_solution.Add(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var())

    for (i, ar, s) in PO_AR_SEQ_COMB:
        if i in current_pos:
            first_solution.Add(all_ends_task[(i, ar, s)])
            first_solution.Add(all_starts_task[(i, ar, s)])
    if Sequence_Dependent_Setup_Time == "Include":
        for (m, i, i1) in SDST_Comb:
            if i in current_pos and i1 in current_pos:
                first_solution.Add(SDST_Var[(m, i, i1)])

            ##  for (i,j,ar,s,alt,ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
            ##    for index,(p1,t1) in enumerate(BOH_Comb,start=0):
            ##      if int(p1)==int(ip) and index < len(BOH_Comb)-1 and int(BOH_Comb[index+1][0])== int(p1) and t1<BOH_Comb[index+1][1]:
            ##        t2=BOH_Comb[index+1][1]
            ##        if Product_Type_ID[int(ip)]==1:
            ##          first_solution.Add(b51[(i,j,ar,s,alt,ip,t1)])

    currenttime = datetime.datetime.now()
    print "Step23_3:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    first_solution.Add(makespan)
    store_db = solver.StoreAssignment(first_solution)

    if first_solve == 1:
        solution_time_limit = solver.TimeLimit(int(First_Solution_Time_Limit) * 1000)
    else:
        solution_time_limit = solver.TimeLimit(int(Iteration_Time_Limit) * 1000)

    solution_db = solver.Compose([main_phase, store_db])
    collector = solver.LastSolutionCollector(first_solution)
    print "Invoking CP solve"
    currenttime = datetime.datetime.now()
    print "Step24:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    dec = solver.Solve(solution_db, [collector, objective, solution_time_limit])
    print "dec:", dec

    currenttime = datetime.datetime.now()
    print "Step25:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if dec == 1:
        print " -------------------- inside output calculation ------------------------ "
        ##    global sol1
        sol1 = collector.Solution(0)
        print "After CP solve Objective: ", collector.ObjectiveValue(0), sol1.Value(makespan)
        global active_var
        global fixed_decisions
        active_var = {}
        fixed_decisions = []
        global fixed_start_time
        global fixed_end_time
        fixed_start_time = {}
        fixed_end_time = {}
        for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
            if i in current_pos:
                active_var[(i, j, ar, s, alt)] = sol1.Value(active_variables[(i, j, ar, s, alt)])
                # print "Active: ", i,j,ar,s,alt, active_var[(i, j, ar, s, alt)]
                if active_var[(i, j, ar, s, alt)] == 1:
                    fixed_decisions.append((i, j, ar, s, alt))
                    fixed_start_time[(i, j, ar, s, alt)] = sol1.Value(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var())
                    fixed_end_time[(i, j, ar, s, alt)] = sol1.Value(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var())

        make_span = sol1.Value(makespan)

        global Input_Product_Qty_var
        ##    global Inv_Product_Qty_var
        global Output_Product_Qty_var
        Input_Product_Qty_var = {}
        ##    Inv_Product_Qty_var = {}
        Output_Product_Qty_var = {}

        ##  for (i,j,ar,s) in PO_TASK_AR_SEQ_COMB:
        ##    if i in current_pos:
        ##        for (i1,j1,ar1,alt,ip) in PO_AR_BOM_IP_COMB:
        ##            if i1==i and j==j1 and ar==ar1:
        ##                solver.Add(Input_Product_Qty[(i1,j1,ar,alt,int(ip))] >= active_variables[(i, j, ar, s, alt)] * Dom_IP[(i,j,ip)])


        for (i, j, ar, alt, ip) in PO_AR_BOM_IP_COMB:
            if i in current_pos:
                if (i,ip) in po_products_alt_bom_comb:
                # if ip in Alternate_BOM_Products:
                    Input_Product_Qty_var[(i, j, ar, alt, ip)] = sol1.Value(Input_Product_Qty[(i, j, ar, alt, ip)])
                    if Input_Product_Qty_var[(i, j, ar, alt, ip)]>0:
                        Input_Product_Qty_var[(i, j, ar, alt, ip)] =  Dom_IP[(i,j,ip)]
                        # print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[(i, j, ar, alt, ip)]
                    # if ip==173 or ip==218:
                    #     print "new: ", i, j, ar, alt, ip, Dom_IP[(i,j,ip)], sol1.Value(Input_Product_Qty[(i, j, ar, alt, ip)])
                else:
                    for (i1, j1, ar1, s) in PO_TASK_AR_SEQ_COMB:
                        if i1 == i and j == j1 and ar == ar1:
                            if sol1.Value(active_variables[(i, j, ar, s, alt)]) * Dom_IP[(i, j, ip)] < 0:
                                if active_var[(i, j, ar, s, alt)] == 1:
                                    Input_Product_Qty_var[(i, j, ar, alt, ip)] = Dom_IP[(i, j, ip)]
                                    # print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[ (i, j, ar, alt, ip)]
                                else:
                                    Input_Product_Qty_var[(i, j, ar, alt, ip)] = Dom_IP[(i, j, ip)]
                                    # print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[(i, j, ar, alt, ip)]
                            else:
                                Input_Product_Qty_var[(i, j, ar, alt, ip)] = sol1.Value(active_variables[(i, j, ar, s, alt)]) * Dom_IP[(i, j, ip)]
                                # print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[(i, j, ar, alt, ip)]
                                ##                    if sol1.Value(Input_Product_Qty[(i,j,ar,alt,ip)])>=0 and i==0:
                                ##                      print "Input_Product_Qty: ", i,j,ar,alt,ip,Input_Product_Qty_var[(i,j,ar,alt,ip)]

        for (i, j, ar, alt, op) in PO_AR_BOM_OP_COMB:
            if i in current_pos:
                Output_Product_Qty_var[(i, j, ar, alt, op)] = sol1.Value(Output_Product_Qty[(i, j, ar, alt, op)])
                ##      if sol1.Value(Output_Product_Qty[(i,j,ar,alt,op)])>0:
                ##        print "Output_Product_Qty: ",i,j,ar,alt,op, sol1.Value(Output_Product_Qty[(i,j,ar,alt,op)])

                ##    for (i,j,ar,s,alt,ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
                ##      for index,(p1,t1) in enumerate(BOH_Comb,start=0):
                ##        if int(p1)==int(ip) and index < len(BOH_Comb)-1 and int(BOH_Comb[index+1][0])== int(p1) and t1<BOH_Comb[index+1][1]:
                ##          t2=BOH_Comb[index+1][1]
                ##          if Product_Type_ID[int(ip)]==1:
                ##            print "b51: ",i,j,ar,s,alt,ip,t1, sol1.Value(b51[(i,j,ar,s,alt,ip,t1)])

        global all_ends_var
        all_ends_var = {}
        global all_starts_var
        all_starts_var = {}

        for i in current_pos:
            all_ends_var[i] = sol1.Value(all_ends[i])
            all_starts_var[i] = sol1.Value(all_starts[i])
            # print i, all_starts_var[i], all_ends_var[i]

        currenttime = datetime.datetime.now()
        print "Step26:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        global seq_size
        global Begin_Time
        global End_Time
        global SeqID
        global RouteID
        global OperID
        global ProdID
        global PO
        global Processing_Time1
        global Down_Time
        global Up_Time
        global Idle_Time
        global Utilization

        seq_size = {}
        Begin_Time = {}
        End_Time = {}
        SeqID = {}
        RouteID = {}
        OperID = {}
        ProdID = {}
        PO = {}
        Processing_Time1 = {}
        Down_Time = {}
        Up_Time = {}
        Idle_Time = {}
        Utilization = {}

        valid_tasks = []
        global valid_tasks_sorted
        valid_tasks_sorted = []
        global valid_comb
        valid_comb = []
        global valid_shutdown_comb
        valid_shutdown_comb = []

        print '--------------------------------- Times -------------------------------------'
        ##    print "non_overlapping_machines: ",non_overlapping_machines
        for i in all_machines:
            if i[0] in non_overlapping_machines:
                ##        print "machine: ", i[0]
                seq = all_sequences[i[0]];
                sequence = collector.ForwardSequence(0, seq)
                seq_size[i[0]] = len(sequence)

                temp1 = 0
                temp2 = 0
                for j in range(seq_size[i[0]]):
                    ##          print "seq, len: ", i[0], j, seq_size[i[0]]
                    t = seq.Interval(sequence[j])
                    PO[i[0], j] = int(OutputSeq_Mac_Job[(i[0], sequence[j])])
                    if int(OutputSeq_Mac_Job[(i[0], sequence[j])]) not in current_pos:
                        ##            print 100, i[0], j, t.StartExpr()
                        for st in Invalid_Timings2:
                            if st[0] == i[0] and str(t) == str(st[3]):
                                Begin_Time[i[0], j] = st[1]
                                End_Time[i[0], j] = st[1] + st[2]
                                valid_shutdown_comb.append((i[0], j))
                                ##                print 100,i[0],j, Begin_Time[i[0],j], End_Time[i[0],j]
                                continue
                    else:
                        Begin_Time[i[0], j] = int(collector.Value(0, t.SafeStartExpr(-1).Var()))
                        End_Time[i[0], j] = int(collector.Value(0, t.SafeEndExpr(-1).Var()))
                        SeqID[(i[0], j)] = Seq_ID[(i[0], sequence[j])]
                        RouteID[(i[0], j)] = Route_ID[(i[0], sequence[j])]
                        OperID[(i[0], j)] = Oper_ID[(i[0], sequence[j])]
                        ProdID[(i[0], j)] = Prod_ID[(i[0], sequence[j])]
                        temp2 = temp2 + End_Time[i[0], j] - Begin_Time[i[0], j]
                        ##            print 200,i[0],j,Begin_Time[i[0],j],End_Time[i[0],j]
                        valid_tasks.append((i[0], j, Begin_Time[i[0], j], End_Time[i[0], j]))
                        valid_comb.append((i[0], j))

                Processing_Time1[i[0]] = temp2
                Down_Time[i[0]] = temp1
                Up_Time[i[0]] = Machine_Avai_End_Time[i[0]] - Machine_Avai_Start_Time[i[0]]
                Idle_Time[i[0]] = Up_Time[i[0]] - Processing_Time1[i[0]] - Down_Time[i[0]]
                Utilization[i[0]] = float(Processing_Time1[i[0]] / float(Up_Time[i[0]]))

        valid_tasks_sorted = sorted(valid_tasks, key=lambda x: (x[0], -x[3]))

        ##    print "valid_shutdown_comb:", valid_shutdown_comb

        if all_pos_iteration == 0:
            for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
                if i in current_pos and active_var[(i, j, ar, s, alt)] == 1:
                    High_Priority_Comb.append((i, j, ar, s, alt))
                    High_Priority_Begin_Time[(i, j, ar, s, alt)] = int(fixed_start_time[(i, j, ar, s, alt)])
                    High_Priority_End_Time[(i, j, ar, s, alt)] = int(fixed_end_time[(i, j, ar, s, alt)])
                    High_Priority_Active_Status[(i, j, ar, s, alt)] = int(active_var[(i, j, ar, s, alt)])

            print "High_Priority_Comb: ", High_Priority_Comb
            print "High_Priority_Begin_Time: ", High_Priority_Begin_Time
            print "High_Priority_End_Time: ", High_Priority_End_Time
            print "High_Priority_Active_Status: ", High_Priority_Active_Status

        for i in all_machines:
            ##      print 1,i[0], non_overlapping_machines
            if i[0] in non_overlapping_machines:
                ##        print 2,i[0]
                seq = all_sequences[i[0]];
                sequence = collector.ForwardSequence(0, seq)
                seq_size1 = len(sequence)

                # print "Machine: ", i
                # for j in range(seq_size1):
                #     t = seq.Interval(sequence[j])
                    ##          print "xxxxxxxxx: ", i[0],j, OutputSeq_Mac_Job[(i[0],sequence[j])]
                #     if int(OutputSeq_Mac_Job[(i[0], sequence[j])]) in current_pos:
                #         print "Job ", OutputSeq_Mac_Job[(i[0], sequence[j])], "(", collector.Value(0, t.SafeStartExpr(
                #             -1).Var()), collector.Value(0, t.SafeEndExpr(-1).Var()), ")"

        currenttime = datetime.datetime.now()
        print "Step27:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        all_machine_ends1 = {}
        all_ends_machine1 = {}
        all_machine_ends_unsorted = []
        Avg_Operation_End_Time = {}
        k = {}
        PO_TASK_AR_SEQ_ALT_COMB_New_unsorted = []
        global Total_Iteration_Set
        Total_Iteration_Set = []
        # PO_Set=[]


        for (op, m) in Operations_Machines_New:
            all_ends_machine1[m] = next((e for (i, j, b, e) in valid_tasks_sorted if i == m), 0)
            all_machine_ends_unsorted.append((op, m, all_ends_machine1[m]))

        all_machine_ends1 = sorted(all_machine_ends_unsorted, key=operator.itemgetter(0, 2, 1))

        currenttime = datetime.datetime.now()
        print "Step28:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        print "iter_solve: ", iter_solve

        if first_solve == 1 or loop == Iteration_Set_Length or iter_solve == 1:
            print "Inside Avg End Times Calculation"
            for op in all_operations:
                for po in current_pos:
                    k[op[0], po] = 0
                    Avg_End_Time_Comb = []
                    for (i, j, alt) in PO_TASK_ALT_COMB:
                        if i == po and j == op[0]:
                            k[op[0], po] = k[op[0], po] + 1
                            Avg_End_Time_Comb.append((i, j, alt))

                            ##          k[op[0],po[0]] = sum(1 for (i,j,alt) in PO_TASK_ALT_COMB if i==po[0] and j==op[0])
                    if k[op[0], po] > 1:
                        Avg_Operation_End_Time[op[0], po] \
                            = sum(all_ends_machine1[m[0]] for (i, j, alt) in Avg_End_Time_Comb for m in all_machines if \
                                  i in current_pos and m[0] == Machine_ID2[(i, j, alt)]) / k[op[0], po]

            currenttime = datetime.datetime.now()
            print "Step29:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            count = 98
            MaxSeq = max(getLastSequence[i, ar] for (i, ar) in PO_AR_COMB if i in current_pos)
            print "MaxSeq : ", MaxSeq
            while count >= 1 and current_seq <= MaxSeq:
                count = 0
                ##        print "current_seq, Last_Failed_Seq", current_seq,last_failed_seq
                if iter_solve == 1 and current_seq < last_failed_seq and last_failed_seq <> 0:
                    current_seq = last_failed_seq + 1
                ##          print "next current_seq with last failed: ", current_seq
                if current_seq > last_failed_seq and last_failed_seq <> 0 and current_seq <> 1 and count == 0:
                    current_seq = current_seq + 1
                ##          print "next current_seq: ", current_seq
                if current_seq == last_failed_seq:
                    current_seq = current_seq + 1
                ##          print "incremented current_seq: ", current_seq
                for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
                    if i in current_pos and k[(j, i)] > 1 and sol1.Value(all_ends_task[(i, ar, s)]) > \
                            Avg_Operation_End_Time[(j, i)] and active_var[(i, j, ar, s, alt)] > 0:
                        Total_Iteration_Set.append((i, j, ar, s, alt))
                        if s == current_seq:
                            count = count + 1
                            PO_TASK_AR_SEQ_ALT_COMB_New_unsorted.append((i, j, ar, s, alt))
                print "count: ", count
                if count == 0:
                    current_seq = current_seq + 1
                    ##          print "current_seq increased if count=0: ", current_seq
                    if current_seq > MaxSeq:
                        count = 0
                    count = 99
                else:
                    print "element_count: ", count
                    count = 0
            print "next_seq: ", current_seq
            currenttime = datetime.datetime.now()
            print "Step30:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            ##      print "Total_Iteration_Set: ", Total_Iteration_Set
            if first_solve == 1:
                out = int(First_Solution_Time_Limit) + len(Total_Iteration_Set) * int(Iteration_Time_Limit)
                f1.write("\n")
                f1.write("Recommendation : ")
                f1.write("For best results Solver Run Time Limit (Sec) should be greater than :  %s" % out)
                f1.write(" seconds")
                f1.write("\n")
                f1.write("\n")

            PO_TASK_AR_SEQ_ALT_COMB_New = sorted(PO_TASK_AR_SEQ_ALT_COMB_New_unsorted, key=lambda x: (x[3], -x[0]))
            currenttime = datetime.datetime.now()
            print "Step31:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        else:
            PO_TASK_AR_SEQ_ALT_COMB_New = []
        currenttime = datetime.datetime.now()
        print "Step31_0:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        return (True, collector.ObjectiveValue(0), active_var, PO_TASK_AR_SEQ_ALT_COMB, PO_TASK_AR_SEQ_ALT_COMB_New,
                fixed_decisions, all_machine_ends1, fixed_start_time, fixed_end_time, make_span)
    else:
        if first_solve == 1:
            return (False, obj_value, [], PO_TASK_AR_SEQ_ALT_COMB, [], [], [], [], [], makespan_value)
        else:
            active_var_new2 = {}
            for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
                if i in current_pos:
                    active_var_new2[(i, j, ar, s, alt)] = active_var_new[(i, j, ar, s, alt)]

            return (False, obj_value, active_var_new2, PO_TASK_AR_SEQ_ALT_COMB, [], [], [], [], [], makespan_value)


def get_cum_qty(to_plot):
    to_plot_x = {}
    to_plot_y = {}
    levels_ = []
    products = set([to_plot[i][0] for i in range(len(to_plot))])
    print products
    for p in products:
        to_plot_x[p] = [to_plot[i][1] for i in range(len(to_plot)) if to_plot[i][0] == p]
        to_plot_y[p] = [to_plot[i][2] for i in range(len(to_plot)) if to_plot[i][0] == p]
        to_plot_y[p] = np.cumsum(to_plot_y[p])
        levels_ = levels_ + list((np.column_stack(
            ([to_plot[i][0] for i in range(len(to_plot)) if to_plot[i][0] == p], to_plot_x[p], to_plot_y[p])).tolist()))
    return levels_


def Output_Update():
    Del_Status = {}
    currenttime = datetime.datetime.now()
    print "Step33:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    for i in current_pos:
        # print i, all_starts_var[i],PlanningHorizon
        if int(all_starts_var[i]) >= int(PlanningHorizon) + 1:
            ##            print "yes"
            Del_Status[i] = 0
        else:
            Del_Status[i] = max(active_var[(i1, j, ar, s, alt)] for (i1, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB if
                                i == i1 and alt <> -1)

    Total_RM_Requirement = {}
    Total_RM_Requirement1 = {}
    ##    Total_RM_Requirement_Alt = {}
    currenttime = datetime.datetime.now()
    print "Step34:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##    for (i,j,ar,s,alt,ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
    ##        if i in current_pos and Input_Product_Qty_var[(i,j,ar,alt,ip)]>0:
    ##            if ip==0 or ip==4:
    ##                print i,alt,ip, Input_Product_Qty_var[(i,j,ar,alt,ip)], active_var[(i, j, ar, s, alt)]

    for p in Products:
        if Product_Type_ID[p] == 1:
            # Total_RM_Requirement[p] = sum(Input_Product_Qty_var[(i, j, ar, alt, ip)] * active_var[(i, j, ar, s, alt)] for (i, j, ar, s, alt, ip) in PO_TASK_AR_SEQ_ALT_IP_COMB \
            #     if i in current_pos and int(alt) <> -1 and int(ip) == int(p))
            Total_RM_Requirement[p] = sum(Dom_IP[(i, j, ip)] for (i, j, ip) in PO_OP_SFG_Comb if i in current_pos and int(ip) == int(p))
            ##        print p, Total_RM_Requirement[p]

    currenttime = datetime.datetime.now()
    print "Step40:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    if Sequence_Dependent_Setup_Time == "Include":
        global SDST_report
        SDST_report = []
        for i in SETUP_MACHINES:
            if i[0] in non_overlapping_machines:
                for j in xrange(seq_size[i[0]]):
                    j1 = j + 1
                    if j1 <= seq_size[i[0]] and (i[0], j) in valid_comb and (
                    i[0], j1) in valid_comb and j1 == j + 1 and (
                    int(i[0]), int(PO[i[0], j]), int(PO[i[0], j1])) in SETUP_TIME_NEW_COMB_ALL:
                        if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[
                            (i[0], int(PO[i[0], j]), int(PO[i[0], j1]))]:
                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j1]), End_Time[i[0], j],
                                                End_Time[i[0], j] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j1]))]))
                        else:
                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j1]), End_Time[i[0], j],
                                                Begin_Time[i[0], j1]))

        currenttime = datetime.datetime.now()
        print "Step41:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        if Setuptime_with_Holiday_Inbetween == "Enable":
        ##      SDST between two lots with a shutdown/holiday in between

            if SDST_Split == "Allowed":  # "Changeover Split allowed"
                for i in SETUP_MACHINES:
                    if i[0] in non_overlapping_machines:
                        for j in xrange(seq_size[i[0]]):
                            j1 = j + 1
                            j2 = j1 + 1
                            if j1 <= seq_size[i[0]] and j2 <= seq_size[i[0]] and (i[0], j) in valid_comb and (
                            i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (
                            int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                                        End_Time[i[0], j] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                elif Begin_Time[i[0], j2] - End_Time[i[0], j1] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                                        End_Time[i[0], j1] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                else:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                                        Begin_Time[i[0], j1]))
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                                        End_Time[i[0], j1] \
                                                        + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))] - (
                                                        Begin_Time[i[0], j1] - End_Time[i[0], j])))
                                ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                            j11 = j1 + 1
                            j2 = j11 + 1
                            if j1 <= seq_size[i[0]] and j11 <= seq_size[i[0]] and j2 <= seq_size[i[0]] and (
                            i[0], j) in valid_comb and (i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (
                            i[0], j11) in valid_shutdown_comb and (
                            int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                                        End_Time[i[0], j] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))

                                elif Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                                        End_Time[i[0], j1] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))

                                elif Begin_Time[i[0], j2] - End_Time[i[0], j11] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11],
                                                        End_Time[i[0], j11] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))

                                elif Begin_Time[i[0], j1] - End_Time[i[0], j] + Begin_Time[i[0], j11] - End_Time[
                                    i[0], j1] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                                        Begin_Time[i[0], j1]))
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                                        End_Time[i[0], j1] \
                                                        + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))] - (
                                                        Begin_Time[i[0], j1] - End_Time[i[0], j])))
                                else:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                                        Begin_Time[i[0], j1]))
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                                        Begin_Time[i[0], j11]))
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11],
                                                        End_Time[i[0], j11] \
                                                        + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]
                                                        - (Begin_Time[i[0], j1] - End_Time[i[0], j])
                                                        - (Begin_Time[i[0], j11] - End_Time[i[0], j1])))
            else:  # "Changeover Split not allowed"
                for i in SETUP_MACHINES:
                    if i[0] in non_overlapping_machines:
                        for j in xrange(seq_size[i[0]]):
                            j1 = j + 1
                            j2 = j1 + 1
                            if j1 <= seq_size[i[0]] and j2 <= seq_size[i[0]] and (i[0], j) in valid_comb and (
                            i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (
                            int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                                        End_Time[i[0], j] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                elif Begin_Time[i[0], j2] - End_Time[i[0], j1] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                                        End_Time[i[0], j1] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                            j11 = j1 + 1
                            j2 = j11 + 1
                            if j1 <= seq_size[i[0]] and j11 <= seq_size[i[0]] and j2 <= seq_size[i[0]] and (
                            i[0], j) in valid_comb and (i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (
                            i[0], j11) in valid_shutdown_comb and (
                            int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                                        End_Time[i[0], j] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                elif Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                                        End_Time[i[0], j1] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                elif Begin_Time[i[0], j2] - End_Time[i[0], j11] >= SDST[
                                    (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                    SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11],
                                                        End_Time[i[0], j11] + SDST[
                                                            (i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))

                                ##    if Sequence_Dependent_Setup_Time =="Include":
                                ##      global SDST_report
                                ##      SDST_report =[]
                                ##      for i in SETUP_MACHINES:
                                ##        if i[0] in non_overlapping_machines:
                                ##          for j in xrange(seq_size[i[0]]):
                                ##            for j1 in xrange(seq_size[i[0]]):
                                ##              if (i[0],j) in valid_comb and (i[0],j1) in valid_comb and j1==j+1 and (int(i[0]),int(PO[i[0],j]),int(PO[i[0],j1])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                  if Begin_Time[i[0],j1] - End_Time[i[0],j] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j1]))]:
                                ##                    SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j1]), End_Time[i[0],j],End_Time[i[0],j] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j1]))]))
                                ##                    break
                                ##                  else:
                                ##                    SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j1]), End_Time[i[0],j],Begin_Time[i[0],j1]))
                                ##                    break

                                ##      if SDST_Split == "Allowed":
                                ##          for i in SETUP_MACHINES:
                                ##            if i[0] in non_overlapping_machines:
                                ##              for j in xrange(seq_size[i[0]]):
                                ##                for j1 in xrange(seq_size[i[0]]):
                                ##                  for j2 in xrange(seq_size[i[0]]):
                                ##                    if j2==j1+1 and j1==j+1:
                                ##                      if (i[0],j) in valid_comb and (i[0],j2) in valid_comb and (i[0],j1) in valid_shutdown_comb and (int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                        if Begin_Time[i[0],j1] - End_Time[i[0],j] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j],End_Time[i[0],j] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))
                                ##                        elif Begin_Time[i[0],j2] - End_Time[i[0],j1] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j1],End_Time[i[0],j1] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))
                                ##                        else:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j],Begin_Time[i[0],j1]))
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j1],End_Time[i[0],j1] \
                                ##                                              + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]-(Begin_Time[i[0],j1]-End_Time[i[0],j])))

                                ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                                ##                    for j11 in xrange(seq_size[i[0]]):
                                ##                      if int(j1) == int(j) + 1 and int(j11) == int(j1) + 1 and int(j2) == int(j11) + 1:
                                ##                        if (i[0], j) in valid_comb and (i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (i[0], j11) in valid_shutdown_comb and (int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                          if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            #print 1, j,j1,j11,j2
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                ##                                            End_Time[i[0], j] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##
                                ##                          elif Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            #print 2, j, j1, j11, j2
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                ##                                            End_Time[i[0], j1] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##
                                ##                          elif Begin_Time[i[0], j2] - End_Time[i[0], j11] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            #print 3, j, j1, j11, j2
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11],
                                ##                                            End_Time[i[0], j11] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##
                                ##                          elif Begin_Time[i[0], j1] - End_Time[i[0], j] + Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j], Begin_Time[i[0], j1]))
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1], End_Time[i[0], j1] \
                                ##                             + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))] - (Begin_Time[i[0], j1] - End_Time[i[0], j])))
                                ##                          else:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j], Begin_Time[i[0], j1]))
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1], Begin_Time[i[0], j11]))
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11], End_Time[i[0], j11] \
                                ##                             + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]
                                ##                             - (Begin_Time[i[0], j1] - End_Time[i[0], j])
                                ##                             - (Begin_Time[i[0], j11] - End_Time[i[0], j1])))
                                ##      else:
                                ##           for i in SETUP_MACHINES:
                                ##            if i[0] in non_overlapping_machines:
                                ##              for j in xrange(seq_size[i[0]]):
                                ##                for j1 in xrange(seq_size[i[0]]):
                                ##                  for j2 in xrange(seq_size[i[0]]):
                                ##                    if j2==j1+1 and j1==j+1:
                                ##                      if (i[0],j) in valid_comb and (i[0],j2) in valid_comb and (i[0],j1) in valid_shutdown_comb and (int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                        if Begin_Time[i[0],j1] - End_Time[i[0],j] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j],End_Time[i[0],j] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))
                                ##                        elif Begin_Time[i[0],j2] - End_Time[i[0],j1] >= SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]:
                                ##                          SDST_report.append((int(i[0]),int(PO[i[0],j]),int(PO[i[0],j2]), End_Time[i[0],j1],End_Time[i[0],j1] + SDST[(i[0],int(PO[i[0],j]),int(PO[i[0],j2]))]))

                                ##    # "if there are two shutdowns between two orders with a setup in the gap between two shutdowns (incase there is only one working shift)"
                                ##                    for j11 in range(seq_size[i[0]]):
                                ##                      if int(j1) == int(j) + 1 and int(j11) == int(j1) + 1 and int(j2) == int(j11) + 1:
                                ##                        if (i[0], j) in valid_comb and (i[0], j2) in valid_comb and (i[0], j1) in valid_shutdown_comb and (i[0], j11) in valid_shutdown_comb and (int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2])) in SETUP_TIME_NEW_COMB_ALL:
                                ##                          if Begin_Time[i[0], j1] - End_Time[i[0], j] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j],
                                ##                                            End_Time[i[0], j] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##                          elif Begin_Time[i[0], j11] - End_Time[i[0], j1] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                             SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j1],
                                ##                                            End_Time[i[0], j1] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))
                                ##                          elif Begin_Time[i[0], j2] - End_Time[i[0], j11] >= SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]:
                                ##                            SDST_report.append((int(i[0]), int(PO[i[0], j]), int(PO[i[0], j2]), End_Time[i[0], j11],
                                ##                                            End_Time[i[0], j11] + SDST[(i[0], int(PO[i[0], j]), int(PO[i[0], j2]))]))

    currenttime = datetime.datetime.now()
    print "Step43:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Processing_Time2 = {}
    Utilization2 = {}
    for i in all_machines:
        if i[0] in non_overlapping_machines:
            k1 = 0
            k2 = 0
            Duration = 0
            if PlanningBucket == "Seconds":
                Duration = int(PlanningHorizon) / 86400
            elif PlanningBucket == "Minutes":
                Duration = int(PlanningHorizon) / 1440
            elif PlanningBucket == "Hours":
                Duration = int(PlanningHorizon) / 24

            for i1 in range(0, int(Duration)):
                if PlanningBucket == "Seconds":
                    k1 = i1 * 24 * 60 * 60
                    k2 = (i1 + 1) * 24 * 60 * 60
                elif PlanningBucket == "Minutes":
                    k1 = i1 * 24 * 60
                    k2 = (i1 + 1) * 24 * 60
                elif PlanningBucket == "Hours":
                    k1 = i1 * 24
                    k2 = (i1 + 1) * 24

                temp1 = 0
                temp2 = 0
                temp3 = 0
                temp4 = 0
                for j in range(seq_size[i[0]]):
                    if (int(i[0]), int(j)) in valid_comb:
                        if int(Begin_Time[i[0], j]) >= int(k1) and int(Begin_Time[i[0], j]) <= int(k2) and int(
                                End_Time[i[0], j]) <= int(k2):
                            temp1 = temp1 + End_Time[i[0], j] - Begin_Time[i[0], j]
                        elif Begin_Time[i[0], j] >= k1 and Begin_Time[i[0], j] <= k2 and End_Time[i[0], j] > k2:
                            temp2 = temp2 + k2 - Begin_Time[i[0], j]
                        elif Begin_Time[i[0], j] < k1 and End_Time[i[0], j] >= k1 and End_Time[i[0], j] <= k2:
                            temp3 = temp3 + End_Time[i[0], j] - k1
                        elif Begin_Time[i[0], j] < k1 and End_Time[i[0], j] > k2:
                            temp4 = temp4 + k2 - k1

                for (p, q, r, s, t) in SDST_report:
                    if p == i[0]:
                        if int(s) >= int(k1) and int(s) <= int(k2) and int(t) <= int(k2):
                            temp1 = temp1 + t - s
                        elif s >= k1 and s <= k2 and t > k2:
                            temp2 = temp2 + k2 - s
                        elif s < k1 and t >= k1 and t <= k2:
                            temp3 = temp3 + t - k1
                        elif s < k1 and t > k2:
                            temp4 = temp4 + k2 - k1

                Processing_Time2[i[0], i1] = temp1 + temp2 + temp3 + temp4
                Utilization2[i[0], i1] = float(Processing_Time2[i[0], i1]) * 100 / Total_Shift_Timings[i[0]]
                ##          print "PT: ", i[0], Processing_Time2[i[0],i1], Total_Shift_Timings[i[0]]


                ##    print " ----------------- Material_Profile -------------------------- "
                ##    Material_Status=[]
                ##    TH  = int(PlanningHorizon)
                ##    print Material_Profile
                ##    for i in range(0, TH):
                ##        for (p1,t1) in Material_Profile_Comb:
                ##           if t1 == i:
                ##               print p1, t1, Material_Profile[(p1,t1)]
                ##               Material_Status.append((p1,t1,Material_Profile[(p1,t1)]))
                ##
                ##    for (i,j,b,e) in valid_tasks_sorted:
                ##        if i == 0:
                ##            print "PO, Product, Begin, End: ", PO[(i,j)],getProductID(PO[(i,j)]), b, e
                ##
                ##    for (Client_ID, Scenario_ID, PlantID, i, j, m,s, ip, op, ip_qty1,ip_qty2, op_qty,setup, proc, td) in params19:
                ##        if i==0:
                ##            print "iq_qty: ", i, j, m, s, ip, op, ip_qty1, op_qty,
                ##
                ####    print Material_Status

    params05 = [(Client_ID, Scenario_ID, i, j, ip, op, Dom_IP[(i, j, ip)], Dom_OP[(i, j, op)]) for
                (i, j, ar, alt, ip, op) in PO_BOM_COMB if i in current_pos]

    sql05 = "insert into XLPS_SFG_QTY(CLIENT_ID, Scenario_ID, PRODUCTION_ORDER_INDEX, OPERATION_INDEX, INPUT_PRODUCT_INDEX, \
               OUTPUT_PRODUCT_INDEX,INPUT_QTY, OUTPUT_QTY) values(%s,%s,%s,%s,%s,%s,%s,%s)"

    params0 = [(Client_ID, Scenario_ID, i, j, ar, s, alt, fixed_start_time[(i, j, ar, s, alt)], \
                fixed_end_time[(i, j, ar, s, alt)], active_var[(i, j, ar, s, alt)]) \
               for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB if
               i in current_pos and (i, j, ar, s, alt) in fixed_decisions and active_var[(i, j, ar, s, alt)] == 1]

    sql0 = "insert into XLPS_SCHEDULE_TIMES(CLIENT_ID, Scenario_ID, PRODUCTION_ORDER_INDEX, OPERATION_INDEX,ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, \
               BEGIN_TIME_INDEX, END_TIME_INDEX,ACTIVE_STATUS) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    ##    if all_pos_iteration == 0:
    ##        for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
    ##            if i in current_pos and active_var[(i,j,ar,s,alt)]==1:
    ##              High_Priority_Comb.append((i,j,ar,s,alt))
    ##              High_Priority_Begin_Time[(i,j,ar,s,alt)] = int(fixed_start_time[(i,j,ar,s,alt)])
    ##              High_Priority_End_Time[(i,j,ar,s,alt)] = int(fixed_end_time[(i,j,ar,s,alt)])
    ##              High_Priority_Active_Status[(i,j,ar,s,alt)] = int(active_var[(i,j,ar,s,alt)])
    ##
    ##        print "High_Priority_Comb: ", High_Priority_Comb
    ##        print "High_Priority_Begin_Time: ", High_Priority_Begin_Time
    ##        print "High_Priority_End_Time: ", High_Priority_End_Time
    ##        print "High_Priority_Active_Status: ", High_Priority_Active_Status

    params1 = [
        (Client_ID, Scenario_ID, PlantID, PO[i, j], i, b, e, SeqID[i, j], RouteID[i, j], OperID[i, j], ProdID[i, j], 1)
        for (i, j, b, e) in valid_tasks_sorted \
        if int(PO[i, j]) in current_pos]

    ##    for (m,i,j) in Invalid_Timings:
    ##        print m,i,j

    params0_0 = [(Client_ID, Scenario_ID, m, i, j) for (m, i, j) in Invalid_Timings]
    sql0_0 = "insert into xlps_machine_invalid_ranges(CLIENT_ID, Scenario_ID, MACHINE_INDEX, START_TIME_INDEX, END_TIME_INDEX) values(%s,%s,%s,%s,%s)"
    sql0_01 = 'UPDATE  xlps_machine_invalid_ranges sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)
    sql0_02 = "update xlps_machine_invalid_ranges set Start_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ), interval START_TIME_INDEX Hour) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql0_03 = "update xlps_machine_invalid_ranges set Start_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),   interval START_TIME_INDEX Minute) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql0_04 = "update xlps_machine_invalid_ranges   set Start_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),    interval START_TIME_INDEX Second) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql0_05 = "update xlps_machine_invalid_ranges  set End_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),   interval END_TIME_INDEX hour) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql0_06 = "update xlps_machine_invalid_ranges     set End_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),    interval END_TIME_INDEX Minute) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql0_07 = "update xlps_machine_invalid_ranges  set End_Time=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),  interval END_TIME_INDEX Second) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql1 = "insert into SPPS_SCHEDULE_REPORT(CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX, MACHINE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX,SEQUENCE_ID,ROUTING_ID,OPERATION_INDEX,PRODUCT_INDEX,MODIFIED) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    sql9 = 'UPDATE  SPPS_SCHEDULE_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)
    sql9_1 = 'UPDATE  SPPS_SCHEDULE_REPORT sc inner join XLPS_OPERATION_SOLVER_READY msr on msr.OPERATION_INDEX =sc.OPERATION_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.OPERATION_ID=msr.OPERATION_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)
    sql10 = 'UPDATE  SPPS_SCHEDULE_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id  set sc.PRODUCTION_ORDER_ID=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID = ' + str(Scenario_ID)
    sql11 = "update SPPS_SCHEDULE_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ), interval BEGIN_TIME_INDEX Hour) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql11_1 = "update SPPS_SCHEDULE_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),   interval BEGIN_TIME_INDEX Minute) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql11_2 = "update SPPS_SCHEDULE_REPORT   set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),    interval BEGIN_TIME_INDEX Second) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql12 = "update SPPS_SCHEDULE_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),   interval END_TIME_INDEX hour) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql12_1 = "update SPPS_SCHEDULE_REPORT     set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),    interval END_TIME_INDEX Minute) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
    sql12_2 = "update SPPS_SCHEDULE_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ),     interval END_TIME_INDEX Second) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql12_3 = "update SPPS_SCHEDULE_REPORT sr1  inner join  SPPS_SCHEDULE_REPORT sr2 on  sr1.PRODUCTION_ORDER_ID=sr2.PRODUCTION_ORDER_ID and \
        sr1.LOCATION_ID=sr2.LOCATION_ID and sr1.scenario_ID=sr2.scenario_ID and sr1.client_id=sr2.client_id inner join xlps_machine_solver_ready msr \
        on msr.machine_id=sr2.machine_id and msr.client_id=sr2.client_id and msr.scenario_ID=sr2.scenario_ID  inner join xlps_operation_solver_ready osr \
        on msr.operation_id=osr.operation_id and msr.client_id=osr.client_id and msr.scenario_ID=osr.scenario_ID \
       set sr2.END_TIME = sr1.BEGIN_TIME,sr2.END_TIME_INDEX = sr1.BEGIN_TIME_INDEX where sr2.sequence_id=sr1.sequence_id-1 and sr2.routing_id=sr1.routing_id and osr.operation_type=3 and \
       sr1.client_id= " + str(Client_ID) + ' and sr1.Scenario_ID= ' + str(Scenario_ID)

    cnx1 = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1',
                                   database='saddlepointv6_prod2')
    cursor1 = cnx1.cursor()
    active_connection = 1

    cursor1.execute(
        'delete from spps_schedule_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
    cursor1.execute(
        'delete from spps_schedule_new_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
            Scenario_ID))
    cursor1.execute(
        'delete from spps_schedule_qty_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
            Scenario_ID))
    cursor1.execute(
        'delete from spps_po_resource_qty_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
            Scenario_ID))
    cursor1.execute(
        'delete from spps_setup_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
    cursor1.execute(
        'Delete from SPPS_MATERIAL_STATUS where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))

    print ' ----------------------------- spps_machine_daily_capacity_report --------------------------------'

    params2 = [(Client_ID, Scenario_ID, PlantID, int(m[0]),
                datetime.datetime.strptime(str(PlanningStartDate), "%Y-%m-%d") + datetime.timedelta(days=i),
                int(Processing_Time2[m[0], i]), int(Utilization2[m[0], i]))
               for m in all_machines if m[0] in non_overlapping_machines for i in range(0, int(Duration)) if
               int(Utilization2[m[0], i]) > 0]

    sql2 = "insert into spps_machine_daily_capacity_report(CLIENT_ID, Scenario_ID, LOCATION_ID, MACHINE_INDEX, DATE_INDEX, PROCESSING_TIME, \
                    UTILIZATION) values(%s,%s,%s,%s,%s,%s,%s)"

    sql141 = "update spps_machine_daily_capacity_report ru inner join  XLPS_MACHINE_SOLVER_READY msr on ru.Machine_Index=msr.Machine_Index and ru.Client_ID=msr.Client_ID \
    and ru.scenario_id=msr.scenario_id  set ru.Machine_ID=msr.Machine_ID where ru.CLIENT_ID= " + str(
        Client_ID) + ' and ru.Scenario_ID= ' + str(Scenario_ID)

    sql141_1 = "delete from spps_machine_daily_capacity_report  where Machine_id='Dummy' and CLIENT_ID= " + str(
        Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID)

    sql_post1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','657',DATE_INDEX from spps_machine_daily_capacity_report \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post2 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','656',UTILIZATION from spps_machine_daily_capacity_report \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    print ' ----------------------------- SPPS_RESOURCE_UTILIZATION --------------------------------'

    params3 = [(Client_ID, Scenario_ID, PlantID, i[0], Up_Time[i[0]], Down_Time[i[0]], Idle_Time[i[0]],
                Processing_Time1[i[0]], Utilization[i[0]]) for i in all_machines if i[0] in non_overlapping_machines]
    sql3 = "insert into SPPS_RESOURCE_UTILIZATION(CLIENT_ID, Scenario_ID, LOCATION_ID, MACHINE_INDEX, TOTAL_AVAILABLE_TIME, SHUTDOWN_TIME, IDLE_TIME, PROCESSING_TIME, \
                    UTILIZATION) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    sql14 = "update SPPS_RESOURCE_UTILIZATION ru inner join  XLPS_MACHINE_SOLVER_READY msr on ru.Machine_Index=msr.Machine_Index and ru.Client_ID=msr.Client_ID \
    and ru.scenario_id=msr.scenario_id  set ru.Machine_ID=msr.Machine_ID where ru.CLIENT_ID= " + str(
        Client_ID) + ' and ru.Scenario_ID= ' + str(Scenario_ID)

    sql14_1 = "delete from SPPS_RESOURCE_UTILIZATION  where Machine_id='Dummy' and CLIENT_ID= " + str(
        Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID)

    sql_post3 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID,LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','631',TOTAL_AVAILABLE_TIME   from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post4 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID,  LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','629',SHUTDOWN_TIME from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post5 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','630',IDLE_TIME   from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post6 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','628',PROCESSING_TIME from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post7 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, LOCATION_ID,'NULL','NULL','NULL',MACHINE_ID,'NULL','NULL','632',UTILIZATION from SPPS_RESOURCE_UTILIZATION \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    print ' ----------------------------- SPPS_MATERIAL_REQUIREMENT --------------------------------'

    params4 = [(Client_ID, Scenario_ID, PlantID, int(p), Total_RM_Requirement[p], int(maxrmavailable[p]), Total_RM_Requirement[p]) \
               for p in Products if Product_Type_ID[p] == 1 and p in BOH_Total_Comb]

    sql4 = "insert into SPPS_MATERIAL_REQUIREMENT( CLIENT_ID, Scenario_ID, LOCATION_ID,PRODUCT_INDEX, REQUIREMENT,AVAILABILITY,USED) values(%s,%s,%s,%s,%s,%s,%s)"

    sql5 = 'UPDATE  SPPS_MATERIAL_REQUIREMENT sc inner join XLPS_PRODUCT_MASTER msr on msr.PRODUCT_INDEX =sc.PRODUCT_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.PRODUCT_ID=msr.PRODUCT_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)

    sql_post8 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID,  LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL',PRODUCT_ID,'NULL','NULL','NULL','NULL','NULL','640',REQUIREMENT from SPPS_MATERIAL_REQUIREMENT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post9 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID,  LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL',PRODUCT_ID,'NULL','NULL','NULL','NULL','NULL','641',AVAILABILITY from SPPS_MATERIAL_REQUIREMENT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post10 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID,  LOCATION_FROM_ID,OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL',PRODUCT_ID,'NULL','NULL','NULL','NULL','NULL','642',USED from SPPS_MATERIAL_REQUIREMENT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    print ' ----------------------------- SPPS_SHORTAGE_REPORT --------------------------------'

    params15 = [(Client_ID, Scenario_ID, PlantID, i, 1, getOrderDueDate(i), 0, 0, 0, getOrderPriority(i)) for i in
                current_pos if Del_Status[i] == 0]

    sql15 = "insert into SPPS_SHORTAGE_REPORT( Client_ID,Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX,PRODUCTION_ORDER_ID, ORDER_DUE_DATE_INDEX, \
                DELIVERY_DATE, Status,DELAY, ORDER_PRIORITY) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    params16 = [(Client_ID, Scenario_ID, PlantID, i, 1, getOrderDueDate(i), all_ends_var[i], 1,
                 all_ends_var[i] - getOrderDueDate(i), getOrderPriority(i)) for i in current_pos if Del_Status[i] == 1]

    ##    print "params15: ", params15
    ##    print "params16: ", params16
    sql16 = "insert into SPPS_SHORTAGE_REPORT( Client_ID,Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX,PRODUCTION_ORDER_ID, ORDER_DUE_DATE_INDEX, \
                DELIVERY_DATE, Status, DELAY,ORDER_PRIORITY) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    sql17 = "update SPPS_SHORTAGE_REPORT ss inner join  XLPS_PO_SOLVER_READY po on po.Production_Order_INDEX=ss.Production_Order_INDEX and ss.Client_ID=po.Client_ID \
    and po.scenario_id=ss.scenario_id  set ss.Production_Order_ID=po.Production_Order_ID where po.CLIENT_ID= " + str(
        Client_ID) + ' and ss.Scenario_ID= ' + str(Scenario_ID)

    sql25 = "update SPPS_SHORTAGE_REPORT ss inner join  XLPS_PO_SOLVER_READY po on po.Production_Order_INDEX=ss.Production_Order_INDEX and ss.Client_ID=po.Client_ID \
    and po.scenario_id=ss.scenario_id  set ss.ORDER_DUE_DATE=po.ORDER_DUE_DATE where po.CLIENT_ID= " + str(
        Client_ID) + ' and ss.Scenario_ID= ' + str(Scenario_ID)

    sql18 = "update SPPS_SHORTAGE_REPORT set DELIVERY_STATUS=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 \
    and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ), interval DELIVERY_DATE Hour) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql18_1 = "update SPPS_SHORTAGE_REPORT set DELIVERY_STATUS=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 \
    and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ), interval DELIVERY_DATE Minute) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql18_2 = "update SPPS_SHORTAGE_REPORT set DELIVERY_STATUS=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601 \
    and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(
        Scenario_ID) + " ), interval DELIVERY_DATE Second) where CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql25_1 = "update SPPS_SHORTAGE_REPORT set NON_DELIVERY= CAST(DATE_FORMAT(DELIVERY_STATUS,'%m/%d/%Y %T')  AS CHAR) where status=1 and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID= " + str(Scenario_ID)
    sql25_2 = "update SPPS_SHORTAGE_REPORT set NON_DELIVERY= 'Non-Delivery' where status=0 and CLIENT_ID= " + str(
        Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql_post11 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID,  'NULL','NULL',LOCATION_ID,'NULL','NULL',PRODUCTION_ORDER_ID,'NULL','634',NON_DELIVERY from SPPS_SHORTAGE_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post11_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL','NULL',PRODUCTION_ORDER_ID,'NULL','635',Delay from SPPS_SHORTAGE_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    print ' ----------------------------- SPPS_PO_RESOURCE_QTY_REPORT --------------------------------'

    params191 = []
    material_track = []

    for (i1, j1, ar1, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB:
        # if i1==1:
        #     print "ask1"
        if i1 in current_pos and int(alt1) <> -1 and active_var[(i1, j1, ar1, s, alt1)] == 1:
            for (i, j, ar, alt, ip, op) in PO_BOM_COMB:
                # if i1 == 1:
                #     print "ask2"
                if int(i1) == int(i) and int(j1) == int(j) and int(ar) == int(ar1) and int(alt1) == int(alt):
                    for m in all_machines:
                        # if i1 == 1:
                        #     print "ask3" ,int(Machine_ID[i1, j1, ar1, s, alt1]) , int(m[0]) , int(op) , int(Product_Index[i1, j1, ar1, s, alt1])
                        if int(Machine_ID[i1, j1, ar1, s, alt1]) == int(m[0]) and int(op) == int(Product_Index[i1, j1, ar1, s, alt1]):
                            if Op_Type[j1] == 1:
                                # if i1 == 1:
                                #     print "ask4"
                                params191.append((Client_ID, Scenario_ID, PlantID, int(i), int(j), int(m[0]), s, int(ip), int(op), \
                                     Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))], \
                                     OP_Setup_Time[i1, j1, ar1, s, alt1],
                                     int(Processing_Time[i1, j1, ar1, s, alt1] * Output_Product_Qty_var[
                                         (int(i), int(j), int(ar), int(alt), int(op))]),
                                     Teardown_Time[i1, j1, ar1, s, alt1]))
                                material_track.append((int(i), int(m[0]), int(ip), Input_Product_Qty_var[
                                    (int(i), int(j), int(ar), int(alt), int(ip))]))
                            if Op_Type[j1] == 2:
                                # if i1 == 1:
                                #     print "ask5"
                                # print int(i), int(j), int(ar), int(ip), op, Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))]
                                params191.append((Client_ID, Scenario_ID, PlantID, int(i), int(j), int(m[0]), s, int(ip), int(op), \
                                     Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))], \
                                     OP_Setup_Time[i1, j1, ar1, s, alt1], Processing_Time[i1, j1, ar1, s, alt1],
                                     Teardown_Time[i1, j1, ar1, s, alt1]))
                                material_track.append((int(i), int(m[0]), int(ip), Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))]))
                            if Op_Type[j1] == 3:
                                # if i1 == 1:
                                #     print "ask6"
                                params191.append(
                                    (Client_ID, Scenario_ID, PlantID, int(i), int(j), int(m[0]), s, int(ip), int(op), \
                                     Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))], \
                                     Output_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(op))], \
                                     0, fixed_end_time[(i, j, ar, s, alt)] - fixed_start_time[(i, j, ar, s, alt)], 0))
                                material_track.append((int(i), int(m[0]), int(ip), Input_Product_Qty_var[(int(i), int(j), int(ar), int(alt), int(ip))]))

    params19 = params191

    sql19 = "insert into SPPS_PO_RESOURCE_QTY_REPORT(CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_INDEX, OPERATION_INDEX, MACHINE_INDEX,SEQUENCE_ID, INPUT_PRODUCT_INDEX, \
            OUTPUT_PRODUCT_INDEX, INPUT_PRODUCT_QUANTITY, INV_PRODUCT_QUANTITY, OUTPUT_PRODUCT_QUANTITY, SETUP_TIME, PROCESSING_TIME,TEARDOWN_TIME) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

    sql20 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_PRODUCT_MASTER msr on msr.PRODUCT_INDEX =sc.INPUT_PRODUCT_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.INPUT_PRODUCT=msr.PRODUCT_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)

    sql21 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_PRODUCT_MASTER msr on msr.PRODUCT_INDEX =sc.OUTPUT_PRODUCT_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.OUTPUT_PRODUCT=msr.PRODUCT_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)

    sql22 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)

    sql23 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.PRODUCTION_ORDER_ID=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)

    sql24 = 'UPDATE  SPPS_PO_RESOURCE_QTY_REPORT sc inner join XLPS_OPERATION_SOLVER_READY msr on msr.OPERATION_INDEX =sc.OPERATION_INDEX and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id set sc.OPERATION_ID=msr.OPERATION_ID where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)

    print " ----------------- Material_Profile -------------------------- "

    currenttime = datetime.datetime.now()
    print "Step51:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Material_Scheduled = {}
    Material_Scheduled_Comb = []
    Material_Scheduled_Comb1 = []
    Material_Profile_Comb2 = []
    Material_Status = []
    Material_Status1 = []
    for p in Products:
        if Product_Type_ID[int(p)] == 1:
            for (po, mac, ip, ip_qty) in material_track:
                if p == ip and ip_qty > 0:
                    for (m, seq, b, e) in valid_tasks_sorted:
                        if m == mac and po == PO[(m, seq)]:
                            Material_Scheduled_Comb.append((p, b, -ip_qty))

    currenttime = datetime.datetime.now()
    print "Step52:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Material_Scheduled_Comb = sorted(Material_Scheduled_Comb)
    Material_Scheduled_Comb1 = [(int(key[0]), int(key[1]), int(sum(k for (i, j, k) in group))) for key, group in
                                groupby(Material_Scheduled_Comb, key=lambda x: (x[0], x[1]))]
    Material_Profile_Comb1 = sorted(Material_Profile_Comb)
    Material_Profile_Comb2 = [(int(key[0]), int(key[1]), int(sum(k for (i, j, k) in group))) for key, group in
                              groupby(Material_Profile_Comb1, key=lambda x: (x[0], x[1]))]
    Material_Status = Material_Profile_Comb2 + Material_Scheduled_Comb1
    Material_Status = sorted(Material_Status)
    Material_Status1 = [(int(key[0]), int(key[1]), int(sum(k for (i, j, k) in group))) for key, group in
                        groupby(Material_Status, key=lambda x: (x[0], x[1]))]

    currenttime = datetime.datetime.now()
    print "Step53:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    to_plot_x = {}
    to_plot_y = {}
    Material_Status2 = []
    products = set([Material_Status1[i][0] for i in range(len(Material_Status1))])
    for p in products:
        to_plot_x[p] = [Material_Status1[i][1] for i in range(len(Material_Status1)) if Material_Status1[i][0] == p]
        to_plot_y[p] = [Material_Status1[i][2] for i in range(len(Material_Status1)) if Material_Status1[i][0] == p]
        to_plot_y[p] = np.cumsum(to_plot_y[p])
        Material_Status2 = Material_Status2 + list((np.column_stack(([Material_Status1[i][0] for i in
                                                                      range(len(Material_Status1)) if
                                                                      Material_Status1[i][0] == p], to_plot_x[p],
                                                                     to_plot_y[p])).tolist()))

    ##    for p in products:
    ##        plt.step(to_plot_x[p],to_plot_y[p])
    ##        plt.hold()
    ##    plt.show()

    ##    print "Material_Profile_Comb2: ", Material_Profile_Comb2
    ##    print "Material_Scheduled_Comb1: ", Material_Scheduled_Comb1
    ##    print "Material_Status2: ", Material_Status2


    currenttime = datetime.datetime.now()
    print "Step54:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    MS = []

    for i in range(len(Material_Status2)):
        MS.append((int(Material_Status2[i][0]), int(Material_Status2[i][1]), int(Material_Status2[i][2]), 0, 0))
    for j in range(len(Material_Profile_Comb2)):
        MS.append((int(Material_Profile_Comb2[j][0]), int(Material_Profile_Comb2[j][1]), 0,
                   int(Material_Profile_Comb2[j][2]), 0))
    for k in range(len(Material_Scheduled_Comb1)):
        MS.append((int(Material_Scheduled_Comb1[k][0]), int(Material_Scheduled_Comb1[k][1]), 0, 0,
                   int(Material_Scheduled_Comb1[k][2])))

    MS1 = sorted(set(MS))

    currenttime = datetime.datetime.now()
    print "Step55:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    MS2 = []

    for key, group in groupby(MS1, lambda k: (k[0], k[1])):
        group = list(group)
        MS2.append(
            (key[0], key[1], sum(x[2] for x in group), sum(x[3] for x in group), sum(x[4] for x in group) * (-1)))
    ##    print "Cum MS2: ", MS2

    MS2 = sorted(MS2, key=lambda x: (x[0], x[1]))

    currenttime = datetime.datetime.now()
    print "Step56:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    params41 = [(Client_ID, Scenario_ID, PlantID, int(MS2[i][0]), int(MS2[i][1]), int(MS2[i][2]), int(MS2[i][3]),
                 int(MS2[i][4])) for i in range(len(MS2))]

    ##    print "params41 : ", params41
    sql41 = "insert into SPPS_MATERIAL_STATUS(CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCT_INDEX, TIME_INDEX, CUM_QTY, Received_Qty, Consumed_Qty) \
                values(%s,%s,%s,%s,%s,%s,%s,%s)"

    sql42 = "update SPPS_MATERIAL_STATUS sm inner join xlps_product_master pm on pm.product_index=sm.product_index and sm.scenario_id=pm.scenario_id \
    and sm.client_id=pm.client_id set sm.product_id = pm.product_id where sm.client_id= " + str(
        Client_ID) + " and sm.Scenario_ID= " + str(Scenario_ID)

    sql43_1 = "update SPPS_MATERIAL_STATUS set TIME_DATE=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 \
    and parameter_id=601 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(Scenario_ID) + " ), interval TIME_INDEX Hour) where \
    CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql43_2 = "update SPPS_MATERIAL_STATUS set TIME_DATE=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 \
    and parameter_id=601 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(Scenario_ID) + " ), interval TIME_INDEX Minute) where \
    CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql43_3 = "update SPPS_MATERIAL_STATUS set TIME_DATE=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 \
    and parameter_id=601 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(Scenario_ID) + " ), interval TIME_INDEX Second) where \
    CLIENT_ID= " + str(Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

    sql_post41 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(PlantID) + "','NULL','NULL','NULL','NULL','688',TIME_DATE from SPPS_MATERIAL_STATUS \
    where time_index < " + str(PlanningHorizon) + " and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(
        Scenario_ID)

    sql_post42 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(PlantID) + "','NULL','NULL','NULL','NULL','689',Received_Qty from SPPS_MATERIAL_STATUS \
    where time_index < " + str(PlanningHorizon) + " and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(
        Scenario_ID)

    sql_post43 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(PlantID) + "','NULL','NULL','NULL','NULL','690',Consumed_Qty from SPPS_MATERIAL_STATUS \
    where time_index < " + str(PlanningHorizon) + " and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(
        Scenario_ID)

    sql_post44 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, \
    ORDER_ID, OPERATION_ID,  SP_DF_ID,DF_VALUE) \
    select Client_ID, Scenario_ID, PRODUCT_ID,'NULL','" + str(PlantID) + "','NULL','NULL','NULL','NULL','691',CUM_QTY from SPPS_MATERIAL_STATUS \
    where time_index < " + str(PlanningHorizon) + " and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(
        Scenario_ID)

    print ' ----------------------------- SPPS_SCHEDULE_QTY_REPORT --------------------------------'

    sql26 = "insert into SPPS_SCHEDULE_QTY_REPORT (CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME, \
      INPUT_PRODUCT, INPUT_PRODUCT_QUANTITY,INV_PRODUCT_QUANTITY,OUTPUT_PRODUCT, OUTPUT_PRODUCT_QUANTITY,OPERATION_ID) \
    select distinct po.Client_ID, po.Scenario_ID, sr.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id, sr.SEQUENCE_ID, BEGIN_TIME, \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME SECOND), \
    INPUT_PRODUCT,INPUT_PRODUCT_Quantity,\
    INV_PRODUCT_Quantity,OUTPUT_PRODUCT,OUTPUT_PRODUCT_Quantity,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join spps_po_resource_qty_report po on sr.production_order_index=\
    po.production_order_index and sr.machine_index=po.machine_index and sr.SEQUENCE_ID=po.SEQUENCE_ID and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(Client_ID) + " and sr.Scenario_ID=" + str(
        Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql26_1 = "insert into SPPS_SCHEDULE_QTY_REPORT (CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME, TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME, \
      INPUT_PRODUCT, INPUT_PRODUCT_QUANTITY,INV_PRODUCT_QUANTITY,OUTPUT_PRODUCT, OUTPUT_PRODUCT_QUANTITY,OPERATION_ID) \
    select distinct po.Client_ID, po.Scenario_ID, sr.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id, sr.SEQUENCE_ID, BEGIN_TIME, \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME MINUTE), \
    INPUT_PRODUCT,INPUT_PRODUCT_Quantity,\
    INV_PRODUCT_Quantity,OUTPUT_PRODUCT,OUTPUT_PRODUCT_Quantity,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join spps_po_resource_qty_report po on sr.production_order_index=\
    po.production_order_index and sr.machine_index=po.machine_index and sr.SEQUENCE_ID=po.SEQUENCE_ID and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(Client_ID) + " and sr.Scenario_ID=" + str(
        Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql26_2 = "insert into SPPS_SCHEDULE_QTY_REPORT (CLIENT_ID, Scenario_ID, LOCATION_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME, TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME, \
      INPUT_PRODUCT, INPUT_PRODUCT_QUANTITY,INV_PRODUCT_QUANTITY,OUTPUT_PRODUCT, OUTPUT_PRODUCT_QUANTITY,OPERATION_ID) \
    select distinct po.Client_ID, po.Scenario_ID, sr.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id, sr.SEQUENCE_ID, BEGIN_TIME, DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME HOUR), \
    INPUT_PRODUCT,INPUT_PRODUCT_Quantity,\
    INV_PRODUCT_Quantity,OUTPUT_PRODUCT,OUTPUT_PRODUCT_Quantity,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join spps_po_resource_qty_report po on sr.production_order_index=\
    po.production_order_index and sr.machine_index=po.machine_index and sr.SEQUENCE_ID=po.SEQUENCE_ID and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(Client_ID) + " and sr.Scenario_ID=" + str(
        Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql26_3 = "update SPPS_SCHEDULE_QTY_REPORT sr1 inner join  SPPS_SCHEDULE_QTY_REPORT sr2 on  sr1.PRODUCTION_ORDER_ID=sr2.PRODUCTION_ORDER_ID \
    and sr1.LOCATION_ID=sr2.LOCATION_ID and sr1.scenario_ID=sr2.scenario_ID and sr1.client_id=sr2.client_id \
    inner join xlps_machine_solver_ready msr on msr.machine_id=sr2.machine_id and msr.client_id=sr2.client_id and msr.scenario_ID=sr2.scenario_ID  \
   inner join xlps_operation_solver_ready osr on msr.operation_id=osr.operation_id and msr.client_id=osr.client_id and msr.scenario_ID=osr.scenario_ID  \
    set sr2.PROCESSING_END_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_BEGIN_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_END_TIME = sr1.SETUP_BEGIN_TIME \
   where sr2.SEQUENCE_ID=sr1.SEQUENCE_ID-1 and osr.operation_type=3 and  sr1.scenario_ID= " + str(
        Scenario_ID) + " and sr1.client_id= " + str(Client_ID);

    sql26_4 = "update SPPS_SCHEDULE_QTY_REPORT sn inner join    \
        xlps_machine_solver_ready msr on sn.MACHINE_ID=msr.MACHINE_ID and sn.client_id=msr.client_id and sn.scenario_id=msr.scenario_id \
        set sn.machine_name = msr.MACHINE_NAME where sn.scenario_ID=" + str(Scenario_ID) + "  and sn.client_id= " + str(Client_ID);
    sql26_5 = "update SPPS_SCHEDULE_QTY_REPORT sn inner join xlps_po_solver_ready po on sn.PRODUCTION_ORDER_ID=po.PRODUCTION_ORDER_ID \
              and po.client_id=sn.client_id and sn.scenario_id=po.scenario_id inner join xlps_product_master pm on pm.PRODUCT_INDEX=po.PRODUCT_ID \
            and pm.client_id=sn.client_id and sn.scenario_id=pm.scenario_id set sn.product_name = pm.product_name \
            where sn.scenario_ID=" + str(Scenario_ID) + "    and sn.client_id= " + str(Client_ID);

    sql_post13 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','612',SETUP_BEGIN_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post14 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','613',SETUP_END_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post15 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','658',PROCESSING_BEGIN_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post16 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','659',PROCESSING_END_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post16_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','665',TEARDOWN_BEGIN_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post16_2 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','666',TEARDOWN_END_TIME from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post17 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','645',INPUT_PRODUCT from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post18 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','643',INPUT_PRODUCT_QUANTITY from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post18_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  sp.Client_ID, sp.Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','643',INPUT_PRODUCT_QUANTITY \
              from SPPS_PO_RESOURCE_QTY_REPORT sp inner join xlps_operation_solver_ready osr on osr.operation_id=sp.operation_id and \
              osr.client_id=sp.client_id and osr.scenario_id=sp.scenario_id where OPERATION_TYPE=3 and sp.CLIENT_ID= " + str(
        Client_ID) + " and sp.Scenario_ID= " + str(Scenario_ID)

    sql_post19 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','646',OUTPUT_PRODUCT from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post20 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','644',OUTPUT_PRODUCT_QUANTITY from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post20_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  sp.Client_ID, sp.Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','644',OUTPUT_PRODUCT_QUANTITY  \
              from SPPS_PO_RESOURCE_QTY_REPORT sp inner join xlps_operation_solver_ready osr on osr.operation_id=sp.operation_id and \
              osr.client_id=sp.client_id and osr.scenario_id=sp.scenario_id where OPERATION_TYPE=3 and sp.CLIENT_ID= " + str(
        Client_ID) + " and sp.Scenario_ID= " + str(Scenario_ID)

    sql_post21 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','664',INV_PRODUCT_QUANTITY from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post22 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','647',SEQUENCE_ID from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)
    sql_post22_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
        SP_DF_ID,DF_VALUE) \
        select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','693',MACHINE_NAME from SPPS_SCHEDULE_QTY_REPORT \
        where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)
    sql_post22_2 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
        SP_DF_ID,DF_VALUE) \
        select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','694',PRODUCT_NAME from SPPS_SCHEDULE_QTY_REPORT \
        where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    print ' ----------------------------- SPPS_SCHEDULE_NEW_REPORT --------------------------------'

    sql27 = "insert into SPPS_SCHEDULE_NEW_REPORT (CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME,ROUTING_ID,OPERATION_ID) \
        select distinct po.Client_ID, po.Scenario_ID, SR.LOCATION_ID, po.PRODUCTION_ORDER_ID, po.machine_id,sr.SEQUENCE_ID, BEGIN_TIME, \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME SECOND), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND),\
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME SECOND), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME SECOND),ROUTING_ID,OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join \
    spps_po_resource_qty_report po on sr.production_order_index= \
    po.production_order_index and sr.machine_index=po.machine_index and sr.operation_index= po.operation_index and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(Client_ID) + " and sr.Scenario_ID=" + str(
        Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql27_1 = 'insert into SPPS_SCHEDULE_NEW_REPORT (CLIENT_ID, Scenario_ID, LOCATION_FROM_ID, PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME,ROUTING_ID,OPERATION_ID) \
        select distinct po.Client_ID, po.Scenario_ID,SR.LOCATION_ID,po.PRODUCTION_ORDER_ID, po.machine_id,sr.SEQUENCE_ID, BEGIN_TIME, \
        DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME MINUTE), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE),\
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME MINUTE), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME MINUTE),ROUTING_ID,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join \
    spps_po_resource_qty_report po on sr.production_order_index= \
    po.production_order_index and sr.machine_index=po.machine_index  and sr.operation_index= po.operation_index and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!="Non-Delivery" and \
    sr.client_id=' + str(Client_ID) + ' and sr.Scenario_ID=' + str(
        Scenario_ID) + ' order by po.production_order_ID, po.Sequence_ID'

    sql27_2 = "insert into SPPS_SCHEDULE_NEW_REPORT (CLIENT_ID, Scenario_ID, LOCATION_FROM_ID,PRODUCTION_ORDER_ID,MACHINE_ID,SEQUENCE_ID, SETUP_BEGIN_TIME, SETUP_END_TIME, \
        PROCESSING_BEGIN_TIME,PROCESSING_END_TIME,TEARDOWN_BEGIN_TIME,TEARDOWN_END_TIME,ROUTING_ID,OPERATION_ID) \
        select distinct po.Client_ID, po.Scenario_ID, SR.LOCATION_ID,po.PRODUCTION_ORDER_ID, po.machine_id,sr.SEQUENCE_ID, BEGIN_TIME, \
        DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME HOUR), DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR),\
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME HOUR), \
    DATE_ADD(BEGIN_TIME, INTERVAL SETUP_TIME+PROCESSING_TIME+TEARDOWN_TIME HOUR),ROUTING_ID,sr.OPERATION_ID from SPPS_SCHEDULE_REPORT sr inner join \
    spps_po_resource_qty_report po on sr.production_order_index= \
    po.production_order_index and sr.machine_index=po.machine_index  and sr.operation_index= po.operation_index and sr.client_id=po.client_id and sr.scenario_id=po.scenario_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=sr.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
    where ssr.Non_Delivery!='Non-Delivery' and \
    sr.client_id=" + str(Client_ID) + " and sr.Scenario_ID=" + str(
        Scenario_ID) + " order by po.production_order_ID, po.Sequence_ID"

    sql27_3 = "insert into spps_schedule_new_report(Client_ID, Scenario_ID,LOCATION_FROM_ID, PRODUCTION_ORDER_ID,mACHINE_ID,sequence_id,machine_group,routing_id, operation_id) \
      select  distinct po.Client_ID, po.Scenario_ID,'" + str(PlantID) + "', po.PRODUCTION_ORDER_ID,msr.mACHINE_ID,rsr.sequence_id,999,rsr.routing_id,osr.OPERATION_ID from xlps_route_solver_ready rsr \
      inner join xlps_po_solver_ready po on po.client_id=rsr.client_id and po.production_order_index=rsr.production_order_index and po.scenario_ID=rsr.scenario_ID  \
      inner join xlps_machine_solver_ready msr on msr.MACHINE_Index=rsr.MACHINE_INDEX  and po.scenario_ID=msr.scenario_ID and po.client_id=msr.client_id \
    inner join xlps_holding_time ht on po.production_order_index=ht.production_order_index and rsr.alternate_index=ht.MACHINE_INDEX \
    and ht.operation_index=rsr.OPERATION_INDEX and po.scenario_ID=ht.scenario_ID and po.client_id=ht.client_id \
    inner join xlps_operation_solver_ready osr on osr.OPERATION_INDEX=ht.operation_index and po.scenario_ID=osr.scenario_ID and po.client_id=osr.client_id \
    inner join spps_shortage_report ssr on ssr.production_order_id=po.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
     inner join spps_schedule_report ssr1 on ssr1.production_order_id=po.production_order_id and ssr1.client_id=po.client_id and ssr1.scenario_id=po.scenario_id \
    and ssr1.routing_id=rsr.routing_id and osr.operation_id=ssr1.operation_id where ssr.Non_Delivery!='Non-Delivery' and \
    po.client_ID=" + str(Client_ID) + " and po.scenario_ID=" + str(Scenario_ID);

    sql27_4 = "update spps_schedule_new_report sn \
    inner join spps_schedule_new_report sn1 on sn.PRODUCTION_ORDER_ID=sn1.PRODUCTION_ORDER_ID and sn.scenario_ID=sn1.scenario_ID and sn.client_id=sn1.client_id  \
    set sn.SETUP_BEGIN_TIME=sn1.TEARDOWN_END_TIME,sn.SETUP_END_TIME=sn1.TEARDOWN_END_TIME,sn.PROCESSING_BEGIN_TIME=sn1.TEARDOWN_END_TIME \
    where sn.SEQUENCE_ID=sn1.SEQUENCE_ID+1 and sn.machine_group=999 and sn.scenario_ID=" + str(
        Scenario_ID) + " and sn.client_id=" + str(Client_ID);

    sql27_5 = "update spps_schedule_new_report sn \
    inner join spps_schedule_new_report sn1 on sn.PRODUCTION_ORDER_ID=sn1.PRODUCTION_ORDER_ID and sn.scenario_ID=sn1.scenario_ID and sn.client_id=sn1.client_id  \
    set sn.PROCESSING_END_TIME=sn1.SETUP_BEGIN_TIME,sn.TEARDOWN_BEGIN_TIME=sn1.SETUP_BEGIN_TIME,sn.TEARDOWN_END_TIME=sn1.SETUP_BEGIN_TIME \
    where sn.SEQUENCE_ID=sn1.SEQUENCE_ID-1 and sn.machine_group=999 and sn.scenario_ID=" + str(
        Scenario_ID) + " and sn.client_id=" + str(Client_ID);

    sql28 = 'UPDATE  SPPS_SCHEDULE_NEW_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_ID =sc.MACHINE_ID and msr.CLIENT_ID=sc.CLIENT_ID \
           and msr.scenario_id=sc.scenario_id inner join XLPS_OPERATION_SOLVER_READY osr on osr.OPERATION_ID =msr.OPERATION_ID and msr.CLIENT_ID=sc.CLIENT_ID \
           and osr.scenario_id=msr.scenario_id set sc.MACHINE_GROUP=osr.OPERATION_INDEX  where msr.CLIENT_ID = ' + str(
        Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)

    sql27_6 = "update SPPS_SCHEDULE_NEW_REPORT sr1 inner join  SPPS_SCHEDULE_NEW_REPORT sr2 on  sr1.PRODUCTION_ORDER_ID=sr2.PRODUCTION_ORDER_ID \
    and sr1.LOCATION_FROM_ID=sr2.LOCATION_FROM_ID and sr1.scenario_ID=sr2.scenario_ID and sr1.client_id=sr2.client_id \
    inner join xlps_machine_solver_ready msr on msr.machine_id=sr2.machine_id and msr.client_id=sr2.client_id and msr.scenario_ID=sr2.scenario_ID  \
   inner join xlps_operation_solver_ready osr on msr.operation_id=osr.operation_id and msr.client_id=osr.client_id and msr.scenario_ID=osr.scenario_ID  \
    set sr2.PROCESSING_END_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_BEGIN_TIME = sr1.SETUP_BEGIN_TIME,sr2.TEARDOWN_END_TIME = sr1.SETUP_BEGIN_TIME \
   where sr2.SEQUENCE_ID=sr1.SEQUENCE_ID-1 and osr.operation_type=3 and  sr1.scenario_ID= " + str(Scenario_ID) + " and sr1.client_id= " + str(Client_ID);


    sql27_7 = "update SPPS_SCHEDULE_NEW_REPORT sn inner join spps_relation sr on sn.MACHINE_ID=sr.RESOURCE_ID and sn.client_id=sr.client_id \
        inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.CLIENT_ID and sn.scenario_id=df.scenario_id \
        set sn.machine_name = df.df_value where df.df_id=2061 and sn.scenario_ID=" + str(Scenario_ID) + "  and sn.client_id= " + str(Client_ID);
    sql27_8 = "update SPPS_SCHEDULE_NEW_REPORT sn inner join xlps_po_solver_ready po on sn.PRODUCTION_ORDER_ID=po.PRODUCTION_ORDER_ID \
                and po.client_id=sn.client_id and sn.scenario_id=po.scenario_id \
              inner join xlps_product_master pm on pm.PRODUCT_INDEX=po.PRODUCT_ID and pm.client_id=sn.client_id and sn.scenario_id=pm.scenario_id  \
                set sn.product_name = pm.PRODUCT_NAME where sn.scenario_ID=" + str(Scenario_ID) + "    and sn.client_id= " + str(Client_ID);

    ##    sql27_7 = "update spps_schedule_new_report sn inner join xlps_machine_solver_ready msr on sn.machine_id=msr.machine_id and sn.client_id=msr.client_id \
    ##    inner join xlps_route_solver_ready rsr on rsr.machine_index=msr.machine_index and rsr.routing_id=sn.routing_id and rsr.sequence_id=sn.sequence_id \
    ##    and rsr.client_id=sn.client_id and rsr.scenario_id=sn.scenario_id \
    ##    and sn.scenario_id=msr.scenario_id set sn.operation_id=msr.operation_id where sn.client_id= " + str(Client_ID) + " and sn.scenario_id= " + str(Scenario_ID);


    sql_post12 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','685',OPERATION_ID from SPPS_SCHEDULE_QTY_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post12_1 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','686',ROUTING_ID from SPPS_SCHEDULE_NEW_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    if Sequence_Dependent_Setup_Time == "Include":
        params30 = [(Client_ID, Scenario_ID, int(m), int(po1), int(po2), int(end_time), int(start_time)) for
                    (m, po1, po2, end_time, start_time) in SDST_report \
                    if int(end_time) < int(PlanningHorizon)]
        sql30 = "insert into SPPS_SETUP_REPORT(CLIENT_ID, Scenario_ID, MACHINE_INDEX, PRODUCTION_ORDER_INDEX_FROM,PRODUCTION_ORDER_INDEX_TO, \
                                      END_TIME_INDEX, BEGIN_TIME_INDEX) values(%s,%s,%s,%s,%s,%s,%s)"
        sql31 = 'UPDATE  SPPS_SETUP_REPORT sc inner join XLPS_MACHINE_SOLVER_READY msr on msr.MACHINE_INDEX =sc.MACHINE_INDEX and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id set sc.MACHINE_ID=msr.MACHINE_ID where msr.CLIENT_ID = ' + str(
            Client_ID) + ' and sc.Scenario_ID= ' + str(Scenario_ID)
        sql32 = 'UPDATE  SPPS_SETUP_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX_FROM and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id  set sc.PRODUCTION_ORDER_ID_FROM=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
            Client_ID) + ' and sc.Scenario_ID = ' + str(Scenario_ID)
        sql33 = 'UPDATE  SPPS_SETUP_REPORT sc inner join XLPS_PO_SOLVER_READY msr on msr.PRODUCTION_ORDER_INDEX =sc.PRODUCTION_ORDER_INDEX_TO and msr.CLIENT_ID=sc.CLIENT_ID and msr.scenario_id=sc.scenario_id  set sc.PRODUCTION_ORDER_ID_TO=msr.PRODUCTION_ORDER_ID where msr.CLIENT_ID = ' + str(
            Client_ID) + ' and sc.Scenario_ID = ' + str(Scenario_ID)
        sql34 = "update SPPS_SETUP_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(
            Scenario_ID) + " ), interval BEGIN_TIME_INDEX Hour) where CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
        sql34_1 = "update SPPS_SETUP_REPORT set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(
            Scenario_ID) + " ),   interval BEGIN_TIME_INDEX Minute) where CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
        sql34_2 = "update SPPS_SETUP_REPORT   set BEGIN_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(
            Scenario_ID) + " ),    interval BEGIN_TIME_INDEX Second) where CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
        sql35 = "update SPPS_SETUP_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(
            Scenario_ID) + " ),   interval END_TIME_INDEX hour) where CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
        sql35_1 = "update SPPS_SETUP_REPORT     set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(
            Scenario_ID) + " ),    interval END_TIME_INDEX Minute) where CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(Scenario_ID)
        sql35_2 = "update SPPS_SETUP_REPORT  set END_TIME=DATE_ADD((select STR_TO_DATE(Parameter_Value,'%m/%d/%Y') from spapp_parameter where MODULE_ID=6 and parameter_id=601       and CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(
            Scenario_ID) + " ),     interval END_TIME_INDEX Second) where CLIENT_ID= " + str(
            Client_ID) + " and Scenario_ID=" + str(Scenario_ID)

        sql_post23 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','681',PRODUCTION_ORDER_ID_FROM from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

        sql_post24 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','682',PRODUCTION_ORDER_ID_TO from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

        sql_post25 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','683',END_TIME from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

        sql_post26 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select  Client_ID, Scenario_ID, 'NULL','NULL','NULL','NULL',MACHINE_ID,PRODUCTION_ORDER_ID_FROM,'NULL','684',BEGIN_TIME from SPPS_SETUP_REPORT \
              where CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post99 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','665',TEARDOWN_BEGIN_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post100 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','666',TEARDOWN_END_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post101 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','612',SETUP_BEGIN_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post102 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','613',SETUP_END_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post103 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','658',PROCESSING_BEGIN_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post104 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','659',PROCESSING_END_TIME from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post105 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select Client_ID, Scenario_ID, 'NULL','NULL',LOCATION_FROM_ID,'NULL',MACHINE_ID,PRODUCTION_ORDER_ID,'NULL','647',SEQUENCE_ID from SPPS_SCHEDULE_NEW_REPORT \
              where machine_group=999 and CLIENT_ID= " + str(Client_ID) + " and Scenario_ID= " + str(Scenario_ID)

    sql_post106 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select distinct po.Client_ID, po.Scenario_ID, 'NULL','NULL',sr.LOCATION_FROM_ID,'NULL',sr.MACHINE_ID,sr.PRODUCTION_ORDER_ID,'NULL','645',pm.PRODUCT_ID from SPPS_SCHEDULE_NEW_REPORT sr \
              inner join xlps_machine_solver_ready msr on msr.machine_id=sr.machine_id and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
              inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_ID=sr.PRODUCTION_ORDER_ID and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
	      inner join xlps_route_solver_ready rsr on po.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and msr.machine_index=rsr.machine_index and rsr.client_id=sr.client_id and rsr.scenario_id=sr.scenario_id \
              inner join xlps_bom bm on bm.alternate_index=rsr.alternate_index and bm.operation_index=rsr.operation_index \
              and rsr.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.client_id=rsr.client_id and bm.scenario_id=rsr.scenario_id \
              inner join xlps_product_master pm on pm.product_index=bm.input_product_index and pm.client_id=bm.client_id and pm.scenario_id=bm.scenario_id \
              inner join xlps_po_products pop on pop.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pop.PRODUCT_ID=pm.PRODUCT_ID \
              and pop.client_id=bm.client_id and pop.scenario_id=bm.scenario_id \
              inner join spps_shortage_report ssr on ssr.production_order_id=po.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
              where ssr.Non_Delivery!='Non-Delivery' and \
              machine_group=999 and po.CLIENT_ID= " + str(Client_ID) + " and po.Scenario_ID= " + str(Scenario_ID)

    sql_post107 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID, LOCATION_TO_ID,RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select distinct po.Client_ID, po.Scenario_ID, 'NULL','NULL',sr.LOCATION_FROM_ID,'NULL',sr.MACHINE_ID,sr.PRODUCTION_ORDER_ID,'NULL','646',pm.PRODUCT_ID from SPPS_SCHEDULE_NEW_REPORT sr \
              inner join xlps_machine_solver_ready msr on msr.machine_id=sr.machine_id and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
              inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_ID=sr.PRODUCTION_ORDER_ID and msr.client_id=sr.client_id and msr.scenario_id=sr.scenario_id \
	      inner join xlps_route_solver_ready rsr on po.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and msr.machine_index=rsr.machine_index and rsr.client_id=sr.client_id and rsr.scenario_id=sr.scenario_id \
              inner join xlps_bom bm on bm.alternate_index=rsr.alternate_index and bm.operation_index=rsr.operation_index \
              and rsr.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.client_id=rsr.client_id and bm.scenario_id=rsr.scenario_id \
              inner join xlps_product_master pm on pm.product_index=bm.output_product_index and pm.client_id=bm.client_id and pm.scenario_id=bm.scenario_id \
              inner join xlps_po_products pop on pop.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pop.PRODUCT_ID=pm.PRODUCT_ID \
              and pop.client_id=bm.client_id and pop.scenario_id=bm.scenario_id \
              inner join spps_shortage_report ssr on ssr.production_order_id=po.production_order_id and ssr.client_id=po.client_id and ssr.scenario_id=po.scenario_id \
              where ssr.Non_Delivery!='Non-Delivery' and \
              machine_group=999 and po.CLIENT_ID= " + str(Client_ID) + " and po.Scenario_ID= " + str(Scenario_ID)

    sql_post51 = "insert into SPPS_SOLVER_DF_OUTPUT(CLIENT_ID, Scenario_ID, OUTPUT_PRODUCT_ID, INPUT_PRODUCT_ID, LOCATION_FROM_ID,LOCATION_TO_ID, RESOURCE_ID, ORDER_ID, OPERATION_ID,\
              SP_DF_ID,DF_VALUE) \
              select po.Client_ID, po.Scenario_ID, 'NULL','NULL','" + str(PlantID) + "','NULL','NULL',pos.PRODUCTION_ORDER_ID,'NULL','692',po.PRODUCT_ID from XLPS_PO_PRODUCTS po \
              inner join xlps_product_master pm on po.PRODUCT_ID=pm.PRODUCT_ID and po.client_id=pm.client_id and po.scenario_id=pm.scenario_id \
              inner join xlps_po_solver_ready pos on pos.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pos.client_id=po.client_id and pos.scenario_id=po.scenario_id \
              inner join spps_shortage_report ssr on ssr.production_order_id=pos.production_order_id and ssr.client_id=pos.client_id and ssr.scenario_id=pos.scenario_id \
              where ssr.Non_Delivery!='Non-Delivery' and \
              pm.PRODUCT_TYPE_ID=1 and po.CLIENT_ID= " + str(Client_ID) + " and po.Scenario_ID= " + str(Scenario_ID)

    sql200 = "insert into xlps_material_receipts_sfg(CLIENT_ID,SCENARIO_ID,BOH_Update_Flag,PRODUCT_ID,Arrival_time,Time_Index,Quantity,Cumul_Quantity) \
        select po.Client_ID, po.Scenario_ID,po.production_order_index,product_index, max(end_time), max(end_time_index),po.ORDER_QUANTITY,po.ORDER_QUANTITY from spps_schedule_report sr \
        inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=sr.PRODUCTION_ORDER_INDEX and po.SCENARIO_ID=sr.SCENARIO_ID \
        where po.CLIENT_ID= " + str(Client_ID) + " and po.Scenario_ID= " + str(
        Scenario_ID) + " group by po.Client_ID, po.Scenario_ID,po.production_order_index,product_index"

    sql201 = "insert into xlps_material_receipts_sfg(CLIENT_ID,SCENARIO_ID,BOH_Update_Flag,PRODUCT_ID,Arrival_time,Time_Index,Quantity,Cumul_Quantity) \
        select po.Client_ID, po.Scenario_ID,po.production_order_index,product_index, max(end_time), 0,0,0 from spps_schedule_report sr \
        inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=sr.PRODUCTION_ORDER_INDEX and po.SCENARIO_ID=sr.SCENARIO_ID \
        where po.CLIENT_ID= " + str(Client_ID) + " and po.Scenario_ID= " + str(
        Scenario_ID) + " group by po.Client_ID, po.Scenario_ID,po.production_order_index,product_index"

    sql202 = "insert into xlps_material_receipts_sfg(CLIENT_ID,SCENARIO_ID,BOH_Update_Flag,PRODUCT_ID,Arrival_time,Time_Index,Quantity,Cumul_Quantity) \
        select po.Client_ID, po.Scenario_ID,po.production_order_index,product_index, max(end_time), 43200,100000,100000 from spps_schedule_report sr \
        inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=sr.PRODUCTION_ORDER_INDEX and po.SCENARIO_ID=sr.SCENARIO_ID \
        where po.CLIENT_ID= " + str(Client_ID) + " and po.Scenario_ID= " + str(
        Scenario_ID) + " group by po.Client_ID, po.Scenario_ID,po.production_order_index,product_index"

    currenttime = datetime.datetime.now()
    f1.write("Output data export started at %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
    f1.write("\n")

    currenttime = datetime.datetime.now()
    print "Step56:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cursor1.execute(
        'Delete from XLPS_SCHEDULE_TIMES where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))
    ##    cursor1.execute('Delete from XLPS_SFG_QTY where CLIENT_ID= '+ str(Client_ID)+ ' and Scenario_ID=' + str(Scenario_ID))

    cursor1.executemany(sql05, params05)
    cursor1.executemany(sql0, params0)
    cursor1.executemany(sql0_0, params0_0)
    cursor1.execute(sql0_01)

    cursor1.executemany(sql1, params1)
    cursor1.execute(sql9)
    cursor1.execute(sql9_1)
    cursor1.execute(sql10)

    cursor1.executemany(sql2, params2)
    cursor1.execute(sql141)
    cursor1.execute(sql141_1)

    currenttime = datetime.datetime.now()
    print "Step57:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cursor1.executemany(sql3, params3)
    cursor1.execute(sql14)
    cursor1.execute(sql14_1)

    cursor1.executemany(sql4, params4)
    cursor1.execute(sql5)

    cursor1.executemany(sql15, params15)
    cursor1.executemany(sql16, params16)
    cursor1.execute(sql17)
    cursor1.execute(sql25)

    cursor1.executemany(sql19, params19)
    cursor1.execute(sql20)
    cursor1.execute(sql21)
    cursor1.execute(sql22)
    cursor1.execute(sql23)
    cursor1.execute(sql24)
    cursor1.executemany(sql41, params41)
    cursor1.execute(sql42)
    if Sequence_Dependent_Setup_Time == "Include":
        cursor1.executemany(sql30, params30)
        cursor1.execute(sql31)
        cursor1.execute(sql32)
        cursor1.execute(sql33)
    cnx1.commit()
    currenttime = datetime.datetime.now()
    print "Step58:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if PlanningBucket == "Hours":
        cursor1.execute(sql11)
        cursor1.execute(sql12)
        cursor1.execute(sql18)
        cursor1.execute(sql25_1)
        cursor1.execute(sql25_2)
        cursor1.execute(sql26_2)
        cnx1.commit()
        cursor1.execute(sql27_2)
        cursor1.execute(sql0_02)
        cursor1.execute(sql0_05)
        cursor1.execute(sql43_1)
        if Sequence_Dependent_Setup_Time == "Include":
            cursor1.execute(sql34)
            cursor1.execute(sql35)

    if PlanningBucket == "Minutes":
        cursor1.execute(sql11_1)
        cursor1.execute(sql12_1)
        cursor1.execute(sql18_1)
        cursor1.execute(sql25_1)
        cursor1.execute(sql25_2)
        cursor1.execute(sql26_1)
        cnx1.commit()
        cursor1.execute(sql27_1)
        cursor1.execute(sql0_03)
        cursor1.execute(sql0_06)
        cursor1.execute(sql43_2)
        if Sequence_Dependent_Setup_Time == "Include":
            cursor1.execute(sql34_1)
            cursor1.execute(sql35_1)

    if PlanningBucket == "Seconds":
        cursor1.execute(sql11_2)
        cursor1.execute(sql12_2)
        cursor1.execute(sql18_2)
        cursor1.execute(sql25_1)
        cursor1.execute(sql25_2)
        cursor1.execute(sql26)
        cnx1.commit()
        cursor1.execute(sql27)
        cursor1.execute(sql0_04)
        cursor1.execute(sql0_07)
        cursor1.execute(sql43_3)
        if Sequence_Dependent_Setup_Time == "Include":
            cursor1.execute(sql34_2)
            cursor1.execute(sql35_2)

    currenttime = datetime.datetime.now()
    print "Step59:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cnx1.commit()
    cursor1.execute(sql12_3)
    cursor1.execute(sql26_3)
    cursor1.execute(sql26_4)
    cursor1.execute(sql26_5)
    cursor1.execute(sql27_6)
    cursor1.execute(sql27_7)
    cursor1.execute(sql27_8)
    cursor1.execute(sql27_3)
    cursor1.execute(sql27_4)
    cursor1.execute(sql27_5)

    cursor1.execute(sql_post1)
    cursor1.execute(sql_post2)
    cursor1.execute(sql_post3)
    cursor1.execute(sql_post4)
    cursor1.execute(sql_post5)
    cursor1.execute(sql_post6)
    cursor1.execute(sql_post7)
    cursor1.execute(sql_post8)
    cursor1.execute(sql_post9)
    cursor1.execute(sql_post10)
    cursor1.execute(sql_post11)
    cursor1.execute(sql_post11_1)
    cursor1.execute(sql_post13)
    cursor1.execute(sql_post14)
    cursor1.execute(sql_post15)
    cursor1.execute(sql_post16)
    cursor1.execute(sql_post16_1)
    cursor1.execute(sql_post16_2)
    cursor1.execute(sql_post17)
    cursor1.execute(sql_post18)
    cursor1.execute(sql_post18_1)
    cursor1.execute(sql_post19)
    cursor1.execute(sql_post20)
    cursor1.execute(sql_post20_1)
    cursor1.execute(sql_post21)
    cursor1.execute(sql_post22)
    cursor1.execute(sql_post22_1)
    cursor1.execute(sql_post22_2)
    cursor1.execute(sql_post12)
    cursor1.execute(sql_post12_1)
    cursor1.execute(sql_post41)
    cursor1.execute(sql_post42)
    cursor1.execute(sql_post43)
    cursor1.execute(sql_post44)

    if Sequence_Dependent_Setup_Time == "Include":
        cursor1.execute(sql_post23)
        cursor1.execute(sql_post24)
        cursor1.execute(sql_post25)
        cursor1.execute(sql_post26)
    cnx1.commit()
    currenttime = datetime.datetime.now()
    print "Step60:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cursor1.execute(sql_post99)
    cursor1.execute(sql_post100)
    cursor1.execute(sql_post101)
    cursor1.execute(sql_post102)
    cursor1.execute(sql_post103)
    cursor1.execute(sql_post104)
    cursor1.execute(sql_post105)
    # cursor1.execute(sql_post106)
    # cursor1.execute(sql_post107)
    cursor1.execute(sql_post51)
    cursor1.execute(sql28)

    currenttime = datetime.datetime.now()
    print "Step61:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    currenttime = datetime.datetime.now()
    f1.write("Output data export completed at %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
    f1.write("\n")
    f1.close
    active_connection = -1
    cursor1.close()
    cnx1.commit()
    cnx1.close()


# if __name__ == '__main__':
    ##    try:
    # print main(0)
    # print PriorityIterationSolve()
####      print FirstSolve()
##    except Exception as x:
##      print x
##      if active_connection==1:
##        cursor1.execute('SELECT Exception_log_link FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
##        Info = cursor1.fetchall()
##        LogFile = Info[0][0]
##        f1 = open(LogFile,'a')
##        f1.write("Error Message : %s " %x)
##        f1.close
##        cursor1.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
##        cursor1.close()
##        cnx1.commit()
##        cnx1.close()
##      elif active_connection==0:
##        cursor.execute('SELECT Exception_log_link FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
##        Info = cursor.fetchall()
##        LogFile =Info[0][0]
##        f1 = open(LogFile,'a')
##        f1.write("Error Message : %s " %x)
##        f1.close
##        cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
##        cursor.close()
##        cnx.commit()
##        cnx.close()
##      else:
##        cnx2 = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2')
##        cursor2 = cnx2.cursor()
##        cursor2.execute('SELECT Exception_log_link FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
##        Info = cursor2.fetchall()
##        LogFile = Info[0][0]
##        f1 = open(LogFile,'a')
##        f1.write("Error Message : %s " %x)
##        f1.close
##        cursor2.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
##        cursor2.close()
##        cnx2.commit()
##        cnx2.close()


