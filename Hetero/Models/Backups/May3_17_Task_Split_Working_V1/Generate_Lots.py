from __future__ import division
print "importing pyomo"
from pyomo.environ import*
import sys
import mysql.connector
import os
import datetime
import time
PROCESS_ID = os.getpid()

    
try:
    JOB_ID = str(sys.argv[1])
except Exception as x:
    print x, "saddlepointv6_prod2"
    JOB_ID = 11765

def solve_supply_network_optimizer(JOB_ID):
    cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
    cursor = cnx.cursor()
    start_time = time.time()
    cursor.execute('update SPAPP_JOB set PID = '+str(PROCESS_ID)+' where JOb_ID ='  + str(JOB_ID))
    cnx.commit()

    print 'Lot Generation Logic is invoked'

    Input_Data_Debugging_Flag = False
    Output_Data_Debugging_Flag = False
    Solver_Debugging_Flag = False

    cursor.execute('SELECT distinct Client_ID, Scenario_ID FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
    ClientID = cursor.fetchall()
    global Client_ID
    global Scenario_ID
    Client_ID  = ClientID[0][0]
    Scenario_ID  = ClientID[0][1]
    cursor.execute('SELECT Exception_log_link FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
    Info = cursor.fetchall()
    LogFile = Info[0][0]    
    print 'Client_ID:', Client_ID
    print 'JOB_ID:', JOB_ID
    print 'Scenario_ID:', Scenario_ID
    print 'LogFile:', LogFile


    f1 = open(LogFile,'a')
    f1.truncate
    currenttime = datetime.datetime.now()
    f1.write("-------- Generate Lots Logic ( JOB_ID - %s " %str(JOB_ID))
    f1.write(") -------- \n")


    print ' ----------------------------------- Solver App Parameters -----------------------------------------------------------'

    cursor.execute('select distinct Solver_Parameter_ID, Solver_Parameter_Value FROM spapp_solver_parameter where module_id=6 and CLIENT_ID= ' + str(Client_ID));
    Solver_Parameters = cursor.fetchall()

    for i in range(0, len(Solver_Parameters)):
        if Solver_Parameters[i][0] == 608:
            global Lots_Created_From
            Lots_Created_From = Solver_Parameters[i][1]


    if Lots_Created_From == "Enable":
        Lot_Creation_From = "Production Plan"
        print "Lot_Creation_From: ", Lot_Creation_From
        cursor.execute('select OUTPUT_PRODUCT_ID,LOCATION_FROM_ID, Operation_ID, PERIOD_ID, TS_VALUE from spps_solver_ts_output where sp_ts_id=604 and \
                       Client_ID =' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
        PP_Data = cursor.fetchall()
        if len(PP_Data)==0:
            Lot_Creation_From = "Demand"
            f1.write("Lot Generation Setting: Generate Lots from loaded Demand as Production Plan is not available in the system")
            f1.write("\n")
        else:
            f1.write("Lot Generation Setting: Generate Lots from Production Plan")
            f1.write("\n")
    elif Lots_Created_From == "Disable":
        Lot_Creation_From = "Demand"
        print "Lot_Creation_From: ", Lot_Creation_From
        f1.write("Lot Generation Setting: Generate Lots from loaded Demand")
        f1.write("\n")




    Decimal_Handler = 1.0

    # f1.write("Step1: Data reading started at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    # f1.write("\n")

    print ' ----------------------------------- Solver Parameters -----------------------------------------------------------'
    cursor.execute('SELECT distinct PARAMETER_ID, PARAMETER_VALUE FROM SPAPP_PARAMETER  where \
                    MODULE_ID = 6 and Client_ID ='  + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
    Parameter_Settings = cursor.fetchall()

    for i in range(0,len(Parameter_Settings)):
        if Parameter_Settings[i][0]==621:
            Production_Capacity_param = Parameter_Settings[i][1]
            print "Production_Capacity: ", Production_Capacity_param
        if Parameter_Settings[i][0]==622:
            RM_Capacity = Parameter_Settings[i][1]
            print "RM_Capacity:", RM_Capacity
        elif Parameter_Settings[i][0]==620:
            Late_Delivery_Horizon = Parameter_Settings[i][1]
        elif Parameter_Settings[i][0]==618:
            PlanningHorizon = Parameter_Settings[i][1]
            print "PlanningHorizon: ", PlanningHorizon
        elif Parameter_Settings[i][0]==601:
            PlanningStartDate = Parameter_Settings[i][1]
            print "PlanningStartDate: ", PlanningStartDate
        elif Parameter_Settings[i][0]==623:
            Solver_Run_Time = Parameter_Settings[i][1]
            print "Solver_Run_Time: ", Solver_Run_Time
        elif Parameter_Settings[i][0]==619:
            Planning_Bucket = Parameter_Settings[i][1]
            print "Planning_Bucket: ", Planning_Bucket  

    Planning_Level = Planning_Bucket


    cursor.execute('SELECT distinct PERIOD_ID FROM spapp_period where period_value = "'+str(PlanningStartDate)+'" and period_bucket = "'+str(Planning_Level)+'" \
                    and Client_ID ='  + str(Client_ID))

    CurrentTimeBucket = cursor.fetchall()
##    print CurrentTimeBucket

    CurrentTimeBucket0 = CurrentTimeBucket[0][0] - 1
##    print CurrentTimeBucket[0][0]
##    print CurrentTimeBucket0

    cursor.execute('SELECT distinct PERIOD_ID FROM spapp_period where period_id between ' + str(CurrentTimeBucket[0][0]) + ' and '+ str(CurrentTimeBucket[0][0])+ ' \
                      + ' + str(PlanningHorizon) + ' + ' + str(Late_Delivery_Horizon) + ' - 1 and  Client_ID = '+ str(Client_ID))
    Period_ID = cursor.fetchall()
##    print Period_ID
    
    cursor.execute('SELECT distinct PERIOD_ID FROM spapp_period where period_id between ' + str(CurrentTimeBucket[0][0]) + ' and '+ str(CurrentTimeBucket[0][0])+ ' \
                      + ' + str(PlanningHorizon) + ' + ' + str(Late_Delivery_Horizon) + ' - 1 and  Client_ID = '  + str(Client_ID))
    Period_ID0 = cursor.fetchall()
##    print Period_ID0
    
    EndTimeBucket = CurrentTimeBucket[0][0] + int(PlanningHorizon) + int(Late_Delivery_Horizon) - 1
    print "EndTimeBucket: ", EndTimeBucket

    cursor.execute('SELECT distinct sr.LOCATION_FROM_ID from spps_df_input df \
                   inner join spps_relation sr on sr.relation_id=df.relation_ID and sr.client_id=df.client_id  \
                   where df.DF_ID =674 and df.df_value in ("Planning","Both")  and df.Client_ID = ' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    PlantID = cursor.fetchall()
    
    cursor.execute('select distinct OUTPUT_PRODUCT_ID from spps_relation sr \
                join spps_df_input df on df.client_id = sr.CLIENT_ID and df.RELATION_ID = sr.RELATION_ID and df.DF_ID = 2003 and df.DF_VALUE = 3 \
                where df.CLIENT_ID ='  + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    Finished_Goods = cursor.fetchall()

##    print 'Finished_Goods: ', Finished_Goods

    cursor.execute('select distinct OUTPUT_PRODUCT_ID from spps_relation sr \
                join spps_df_input df on df.client_id = sr.CLIENT_ID and df.RELATION_ID = sr.RELATION_ID and df.DF_ID = 2003 and df.DF_VALUE = 2 \
                where df.CLIENT_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))

    Semi_Finished_Goods = cursor.fetchall()

    cursor.execute('select distinct r.OUTPUT_PRODUCT_ID,r.LOCATION_FROM_ID,r.OPERATION_ID from spps_df_input df inner join spps_relation r \
                on r.relation_id=df.relation_id  where DF_ID=602 and r.client_id=df.client_id and df.Client_ID =' + str(Client_ID) + ' \
                 and df.Scenario_ID= ' + str(Scenario_ID))
    ValidMComb_Uncon = cursor.fetchall()
  
    cursor.execute('select distinct r.OUTPUT_PRODUCT_ID,r.LOCATION_FROM_ID,sr.OPERATION_ID from spps_df_input df \
                inner join spps_relation r on r.relation_id=df.relation_id  and r.client_id=df.client_id \
		inner join spps_df_input df2 ON df.CLIENT_ID=df2.CLIENT_ID and df.Scenario_ID=df2.Scenario_ID \
                inner join spps_relation sr on sr.OPERATION_ID = r.operation_id and sr.relation_id=df2.relation_id \
                where df.DF_ID=602 and df2.df_id = 674 and df2.df_value in ("Planning","Both") and df.Client_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    ValidMComb_Con = cursor.fetchall()
    
    cursor.execute('select product_id,location_id, operation_id, period_id, MIN_BATCH_SIZE, MAX_BATCH_SIZE, SETUP_TIME, PROCESSING_TIME, TEARDOWN_TIME, CAPACITY_KGS \
    from xlps_machine_batch_time where Client_ID =' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
    ValidMComb_Values = cursor.fetchall()


    cursor.execute('select distinct sr.operation_id from spps_relation sr \
    inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id \
    inner join spps_relation sr1 on sr1.OPERATION_ID=sr.OPERATION_ID and sr.CLIENT_ID=sr1.CLIENT_ID \
    inner join spps_df_input df1 on df1.RELATION_ID=sr1.RELATION_ID and df1.CLIENT_ID=sr1.CLIENT_ID and df1.SCENARIO_ID=df.SCENARIO_ID \
    where df.df_id=679 and df1.df_id=608 and df.df_value = "Yes" and df. Client_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID) + ' limit 1')

    Bottleneck_Operation_ID = cursor.fetchall()
    Bottleneck_Operation_ID =  Bottleneck_Operation_ID[0][0]
    print "Bottleneck_Operation_ID: ", Bottleneck_Operation_ID
    
    if Lot_Creation_From == "Demand":
        cursor.execute('SELECT distinct sr.location_from_id, pm.product_index,sr.OPERATION_ID,sr.Resource_ID,df1.increment_id,cast(df1.df_value as decimal(20,6))  \
        from spps_df_input df1 inner join spps_relation sr on sr.relation_id=df1.relation_ID and sr.client_id=df1.client_id \
        inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT_ID and pm.SCENARIO_ID=df1.SCENARIO_ID and pm.product_type_id=2 \
        inner join spps_relation sr1 inner join spps_df_input df on df.relation_id=sr1.relation_id and df.client_id=sr1.client_id \
         and sr1.OPERATION_ID=sr.OPERATION_ID \
        where df.df_id=679 and df.df_value = "Yes" and df1.df_id =675 \
        and df1.Client_ID = ' + str(Client_ID) + ' and df1.Scenario_ID = ' + str(Scenario_ID)+ ' order by sr1.OPERATION_ID, cast(df1.df_value as decimal(20,6))  desc')

        Lot_Bottleneck_Data1 = cursor.fetchall()                       
        
        cursor.execute('SELECT distinct sr.location_from_id, pm.product_index,sr.OPERATION_ID,sr.Resource_ID,df1.increment_id,cast(df1.df_value as decimal(20,6))  \
        from spps_df_input df1 inner join spps_relation sr on sr.relation_id=df1.relation_ID and sr.client_id=df1.client_id \
        inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT_ID and pm.SCENARIO_ID=df1.SCENARIO_ID  and pm.product_type_id=3 \
        inner join spps_relation sr1 inner join spps_df_input df on df.relation_id=sr1.relation_id and df.client_id=sr1.client_id \
         and sr1.OPERATION_ID=sr.OPERATION_ID \
        where df.df_id=679 and df.df_value = "Yes" and df1.df_id =675 \
        and df1.Client_ID = ' + str(Client_ID) + ' and df1.Scenario_ID = ' + str(Scenario_ID) + ' order by sr1.OPERATION_ID, cast(df1.df_value as decimal(20,6))  desc')

        Lot_Bottleneck_Data2 = cursor.fetchall()
        Lot_Bottleneck_Data= Lot_Bottleneck_Data1 + Lot_Bottleneck_Data2

        print "Lot_Bottleneck_Data: ", Lot_Bottleneck_Data

    else:
        # cursor.execute('select distinct location_id, operation_id, resource_id, product_id, MAX_BATCH_SIZE, MIN_BATCH_SIZE from xlps_machine_batch_time \
        # where operation_id = "'+ str(Bottleneck_Operation_ID) + '" and Client_ID =' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' \
        # order by MAX_BATCH_SIZE desc')
        cursor.execute('select distinct location_id, operation_id, resource_id, product_id, MAX_BATCH_SIZE, MIN_BATCH_SIZE from xlps_machine_batch_time \
        where Client_ID =' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' \
        order by MAX_BATCH_SIZE desc')
        Lot_Bottleneck_Data = cursor.fetchall()
        print "Lot_Bottleneck_Data: ",Lot_Bottleneck_Data
        
    cursor.execute('select OUTPUT_PRODUCT_ID,LOCATION_FROM_ID, Operation_ID, PERIOD_ID, TS_VALUE from spps_solver_ts_output where sp_ts_id=604 and \
                   Client_ID =' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))
    Production_Plan_Data = cursor.fetchall()

    print "Production_Plan_Data: ", Production_Plan_Data

    # Removed period limits from this query. ideally, period id should be b/w planning start date and endtimebucket.
    # to accept back orders and to avoid planning horizon change in PP for PS run, we removed both
    cursor.execute('select distinct "Customer", pm.product_index, period_id,convert(sum(ts_value),DECIMAL(20,6)) from spps_ts_input ts  \
                inner join spps_relation r  on r.relation_id=ts.relation_id  and ts.client_id=r.client_id \
                 inner join xlps_product_master pm on pm.product_id=r.OUTPUT_PRODUCT_ID and pm.SCENARIO_ID=ts.SCENARIO_ID \
                where sp_ts_id =601 and ts.Client_ID = ' + str(Client_ID)+'  and ts.Scenario_ID=' + str(Scenario_ID) +' group by r.output_product_id, period_id')
    LocationLevel_SDI = cursor.fetchall()
    print "LocationLevel_SDI: ", LocationLevel_SDI

##    Product_Demand={}
##    for l in range(0,len(LocationLevel_SDI)):
##      Product_Demand[LocationLevel_SDI[l][0]] = (LocationLevel_SDI[l][2])

      
##  print ' ----------------------------------- ProductType -----------------------------------'
    cursor.execute('select distinct product_index,product_type_id,convert(shelf_life, UNSIGNED INTEGER) from xlps_product_master where CLIENT_ID= ' + str(Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
    ProductType = cursor.fetchall()

    Product_Type_ID={}
    all_products=[]
    all_products_type2 = []
    for l in range(0,len(ProductType)):
      Product_Type_ID[ProductType[l][0]] = (ProductType[l][1])
      all_products.append(ProductType[l][0])
      if Product_Type_ID[ProductType[l][0]]==2:
        all_products_type2.append(ProductType[l][0])

##  print ' ----------------------------------- Route_Master-----------------------------------'
##    cursor.execute('select distinct PRODUCTION_ORDER_INDEX, ORDER_QUANTITY \
##                     From XLPS_PO_SOLVER_READY po  where CLIENT_ID= '+ str(Client_ID)+ ' and Scenario_ID=' + str(Scenario_ID) +' \
##                   order by PRODUCTION_ORDER_INDEX ');
##    Sequence_Time = cursor.fetchall()
##
##    Order_Quantity={}
##    for l in range(0,len(Sequence_Time)):
##      Order_Quantity[int(Sequence_Time[l][0])]  = int(Sequence_Time[l][1])

    print ' ----------------------------------- ORDER_DUE_DATE-----------------------------------'
    cursor.execute("select distinct PRODUCTION_ORDER_INDEX, PRODUCT_ID,SPLIT_STR(PRODUCTION_ORDER_ID,'-Lot',1),order_quantity From XLPS_PO_SOLVER_READY where CLIENT_ID= "+ str(Client_ID)+" \
    and Scenario_ID=" + str(Scenario_ID) + "  order by PRODUCTION_ORDER_INDEX ");
    ORDER_DUE_DATE = cursor.fetchall()

    # print "ORDER_DUE_DATE: ", ORDER_DUE_DATE

    Order_Quantity={}
    for l in range(0,len(ORDER_DUE_DATE)):
        Order_Quantity[int(ORDER_DUE_DATE[l][0])]  = int(ORDER_DUE_DATE[l][3]*int(Decimal_Handler))
        if Order_Quantity[int(ORDER_DUE_DATE[l][0])]>0:
            print Order_Quantity[int(ORDER_DUE_DATE[l][0])]

    print ' ----------------------------------- Product_Details-----------------------------------'
    cursor.execute("select distinct  PRODUCT_INDEX,PRODUCT_ID From XLPS_Product_master where CLIENT_ID= "+ str(Client_ID)+" \
    and Scenario_ID=" + str(Scenario_ID));
    Product_Details = cursor.fetchall()

        
        ##  print ' ----------------------------------- PO_AR_COMB -----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, ROUTING_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
                   where rsr.CLIENT_ID= '+ str(Client_ID)+' \
            and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,ROUTING_ID ');
    PO_AR_COMB = cursor.fetchall()

    print ' ----------------------------------- PO_BOM_COMB -----------------------------------'

    cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, ROUTING_ID,alternate_index, INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
        on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
      where bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(Scenario_ID)+ ' order by po.PRODUCTION_ORDER_INDEX,OPERATION_INDEX ');  
    PO_BOM_COMB = cursor.fetchall()
    # print "PO_BOM_COMB: ", PO_BOM_COMB
##  print ' ----------------------------------- PO_TASK_AR_SEQ_COMB-----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
        on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
        where rsr.CLIENT_ID= '+ str(Client_ID)+' \
        and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,SEQUENCE_ID');
    PO_TASK_AR_SEQ_COMB = cursor.fetchall()

##  print ' ----------------------------------- MAT_BALANCE_CONST -----------------------------------'
    
    cursor.execute('select distinct sr.production_order_index, bm.operation_index, bm1.operation_index, sr.routing_ID,sr.sequence_id,sr1.sequence_id, \
      bm1.input_product_index from xlps_bom bm inner join xlps_route_solver_ready sr on bm.production_order_index=sr.production_order_index \
      and sr.operation_index=bm.operation_index and sr.alternate_index=bm.alternate_index and bm.routing_id=sr.routing_id and bm.client_id=sr.client_id \
      and bm.scenario_id=sr.scenario_id inner join xlps_bom bm1 on bm.PRODUCTION_ORDER_INDEX=bm1.PRODUCTION_ORDER_INDEX and \
      bm1.INPUT_PRODUCT_INDEX=bm.OUTPUT_PRODUCT_INDEX and bm.ROUTING_ID=bm1.routing_id and bm.client_id=bm1.client_id and bm.scenario_id=bm1.scenario_id \
      inner join xlps_route_solver_ready sr1 on bm1.production_order_index=sr1.production_order_index and sr1.routing_id=bm1.routing_id \
      and sr1.operation_index=bm1.operation_index and sr1.alternate_index=bm1.alternate_index and bm1.client_id=sr1.client_id and bm1.scenario_id=sr1.scenario_id \
      and sr.sequence_id=sr1.sequence_id - 1 inner join xlps_po_solver_ready po on po.production_order_index=sr1.production_order_index and po.client_id=sr1.client_id \
      and po.scenario_id=sr1.scenario_id where sr.client_id=' + str(Client_ID) + ' and sr.scenario_id= ' + str(Scenario_ID) + ' and sr.alternate_index<>-1 and sr1.alternate_index<>-1 \
      order by sr.production_order_index,sr.sequence_id desc')
    MAT_BALANCE_CONST = cursor.fetchall()

##  print ' ----------------------------------- BOM_DETAILS-----------------------------------'

    cursor.execute('select distinct OUTPUT_PRODUCT_INDEX, INPUT_PRODUCT_INDEX, OPERATION_INDEX, attach_rate, yield from xlps_bom  where ALTERNATE_INDEX <>-1 and \
             CLIENT_ID= ' + str(Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
    BOM_DETAILS = cursor.fetchall()
##  print ' ----------------------------------- Bottleneck Operation -----------------------------------'

    cursor.execute('select distinct pm.PRODUCT_INDEX,osr.OPERATION_INDEX from spps_relation sr \
    inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id \
    inner join spps_relation sr1 on sr1.OPERATION_ID=sr.OPERATION_ID and sr.CLIENT_ID=sr1.CLIENT_ID \
    inner join spps_df_input df1 on df1.RELATION_ID=sr1.RELATION_ID and df1.CLIENT_ID=sr1.CLIENT_ID and df1.SCENARIO_ID=df.SCENARIO_ID \
    inner join xlps_operation_solver_ready osr on osr.operation_id=sr.operation_id and osr.client_id=sr.client_id and osr.scenario_id=df.SCENARIO_ID \
    inner join xlps_product_master pm on pm.product_id=sr1.output_product_id and pm.client_id=sr.client_id and pm.scenario_id=df.SCENARIO_ID \
    where df.df_id=679 and df1.df_id=608 and df.df_value = "Yes" and df.Client_ID =' + str(Client_ID) + ' and df.Scenario_ID= ' + str(Scenario_ID))
    Bottleneck_Operation = cursor.fetchall()
##    Bottleneck_Operation =  Bottleneck_Operation[0][0]
    # print "Bottleneck_Operation: ", Bottleneck_Operation

    getBO={}
    for l in range(0,len(Bottleneck_Operation)):
        getBO[int(Bottleneck_Operation[l][0])]  = int(Bottleneck_Operation[l][1])

    cursor.execute("select pm.product_index,pm1.product_index, DF_VALUE from spps_df_input df  \
    inner join spps_relation sr on sr.relation_id=df.relation_id and sr.client_id=df.client_id \
    inner join xlps_product_master pm on pm.PRODUCT_ID=sr.OUTPUT_PRODUCT_ID and pm.CLIENT_ID=sr.CLIENT_ID and pm.SCENARIO_ID=df.SCENARIO_ID \
    inner join xlps_product_master pm1 on pm1.PRODUCT_ID=sr.INPUT_PRODUCT_ID and pm1.CLIENT_ID=sr.CLIENT_ID and pm1.SCENARIO_ID=df.SCENARIO_ID \
    where df_id=695 and sr.CLIENT_ID=" + str(Client_ID) + " and df.scenario_id= " + str(Scenario_ID))

    SFG_Lot_Sizing = cursor.fetchall()
    # print "SFG_Lot_Sizing: ", SFG_Lot_Sizing

    Lot_Sizing_Comb=[]
    SFG_Lot_Size={}
    SFG_FG_Batch_Size_Relation=[]   # In hetero, batch size is specified between SFG & FG combination in Lot_Sizing sheet. This is to capture that relation.
    for l in range(0,len(SFG_Lot_Sizing)):
        SFG_Lot_Size[int(SFG_Lot_Sizing[l][0]),int(SFG_Lot_Sizing[l][1])]  = int(SFG_Lot_Sizing[l][2])
        Lot_Sizing_Comb.append((int(SFG_Lot_Sizing[l][0]), int(SFG_Lot_Sizing[l][1])))
        SFG_FG_Batch_Size_Relation.append(int(SFG_Lot_Sizing[l][1]))

    SFG_FG_Batch_Size_Relation=set(SFG_FG_Batch_Size_Relation)

    currenttime = datetime.datetime.now()
    #f1.write("Step3: Model building started at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    #f1.write("\n")  

    model = ConcreteModel()

    print' ----------------------------------- Model Data Declaration ----------------------'

    def T1(model):
        return (Period_ID[i][0] for i in range(0,len(Period_ID)))
    model.Period_ID = Set(initialize =T1)

    model.CurrentTimeBucket0 = Param(initialize = CurrentTimeBucket0)
    model.CurrentTimeBucket = Param(initialize = CurrentTimeBucket[0][0])

    def T12(model):
        return (Period_ID0[i][0] for i in range(0,len(Period_ID0)))
    model.Period_ID0 = Set(initialize =T12)
    model.Period_ID0.add(CurrentTimeBucket0)

    model.PlanningHorizon = Param(initialize=PlanningHorizon)
    model.EndTimeBucket = Param(initialize=EndTimeBucket)

    def getAttachRate(op,ip,m):
      for i in range(0,len(BOM_DETAILS)):
        if(BOM_DETAILS[i][0] == op and BOM_DETAILS[i][1] == ip and BOM_DETAILS[i][2] == m):
          return (BOM_DETAILS[i][3])

    def getProductID(p):
      for i in range(0,len(Product_Details)):
        if(int(Product_Details[i][0]) == int(p)):
          return Product_Details[i][1]         
        
    def T31(model):
        return (Finished_Goods[i][0] for i in range(0,len(Finished_Goods)))
    model.FGs = Set(initialize =T31)

    def T32(model):
        return (Semi_Finished_Goods[i][0] for i in range(0,len(Semi_Finished_Goods)))
    model.SFGs = Set(initialize =T32)

    def VMC1_Uncon(model):
        return ((ValidMComb_Uncon[i][0],ValidMComb_Uncon[i][1],ValidMComb_Uncon[i][2]) for i in range(0,len(ValidMComb_Uncon)))
    model.ValidMComb_Uncon = Set(initialize=VMC1_Uncon, dimen=3)

    def VMC1_Con(model):
        return ((ValidMComb_Con[i][0],ValidMComb_Con[i][1],ValidMComb_Con[i][2]) 
                            for i in range(0,len(ValidMComb_Con)))

    model.K5_1 = Set(initialize=VMC1_Con, dimen=3)
    model.K5 = model.K5_1 * model.Period_ID


    if Lot_Creation_From == "Demand":
        def LotGenerationComb(model):
            return ((Lot_Bottleneck_Data[i][0],Lot_Bottleneck_Data[i][1],Lot_Bottleneck_Data[i][2],Lot_Bottleneck_Data[i][3],Lot_Bottleneck_Data[i][4]) 
                                    for i in range(0,len(Lot_Bottleneck_Data)))
        model.Lot_Comb = Set(initialize=LotGenerationComb,dimen=5,ordered=True)

        def LotGenerationComb(model):
            return ((Lot_Bottleneck_Data[i][0],Lot_Bottleneck_Data[i][1]) for i in range(0,len(Lot_Bottleneck_Data)) if (Lot_Bottleneck_Data[i][0],Lot_Bottleneck_Data[i][1]) not in model.Lot_Comb2)
        model.Lot_Comb2 = Set(initialize=LotGenerationComb,dimen=2,ordered=True)
        
        def LotGeneration(model,l,op,r,t,inc):
            for i in range(0,len(Lot_Bottleneck_Data)):
                if l==Lot_Bottleneck_Data[i][0] and op==Lot_Bottleneck_Data[i][1] and r==Lot_Bottleneck_Data[i][2] and t==Lot_Bottleneck_Data[i][3] and inc==Lot_Bottleneck_Data[i][4]:
                    return int(round(Lot_Bottleneck_Data[i][5]*int(Decimal_Handler)))
        model.Lot_Batch_Size = Param(model.Lot_Comb,initialize=LotGeneration)

##        def LotGeneration_1(model,l,op,r,t):
##            for i in range(0,len(Lot_Bottleneck_Data)):
##                if l==Lot_Bottleneck_Data[i][0] and op==Lot_Bottleneck_Data[i][1] and r==Lot_Bottleneck_Data[i][2] and t==Lot_Bottleneck_Data[i][3]:
##                    return (Lot_Bottleneck_Data[i][5])
##        model.Lot_Max_Batch_Size = Param(model.Lot_Comb,initialize=LotGeneration_1)
    else:
        def LotGenerationComb(model):
            return ((Lot_Bottleneck_Data[i][0],Lot_Bottleneck_Data[i][1],Lot_Bottleneck_Data[i][2],Lot_Bottleneck_Data[i][3])
                                    for i in range(0,len(Lot_Bottleneck_Data)))
        model.Lot_Comb = Set(initialize=LotGenerationComb,dimen=4,ordered=True)
        
        def LotGeneration(model,l,op,r,p):
            for i in range(0,len(Lot_Bottleneck_Data)):
                if l==Lot_Bottleneck_Data[i][0] and op==Lot_Bottleneck_Data[i][1] and r==Lot_Bottleneck_Data[i][2] and p==Lot_Bottleneck_Data[i][3]:
                    return (Lot_Bottleneck_Data[i][4])
        model.Lot_Max_Batch_Size = Param(model.Lot_Comb,initialize=LotGeneration)

        def LotGeneration_1(model,l,op,r,p):
            for i in range(0,len(Lot_Bottleneck_Data)):
                if l==Lot_Bottleneck_Data[i][0] and op==Lot_Bottleneck_Data[i][1] and r==Lot_Bottleneck_Data[i][2] and p==Lot_Bottleneck_Data[i][3]:
                    return (Lot_Bottleneck_Data[i][5])
        model.Lot_Min_Batch_Size = Param(model.Lot_Comb,initialize=LotGeneration_1)

    
    def Production_Plan_1(model):
        return ((Production_Plan_Data[i][0],Production_Plan_Data[i][1],Production_Plan_Data[i][2] ,Production_Plan_Data[i][3])
                                  for i in range(0,len(Production_Plan_Data)))
    model.Production_Plan_Comb = Set(initialize=Production_Plan_1,dimen=4,ordered=True)

    def Production_Plan_Value_1(model,p,l,op,t):
        for i in range(0,len(Production_Plan_Data)):
            if p==Production_Plan_Data[i][0] and l==Production_Plan_Data[i][1] and op==Production_Plan_Data[i][2] and t==Production_Plan_Data[i][3]:
                return (Production_Plan_Data[i][4])
    model.Production_Plan_Value = Param(model.Production_Plan_Comb,initialize=Production_Plan_Value_1)

    def DemandComb(model):
        return ((LocationLevel_SDI[i][0],LocationLevel_SDI[i][1],LocationLevel_SDI[i][2]) for i in range(0,len(LocationLevel_SDI)))
    model.Demand_Comb = Set(initialize=DemandComb,dimen=3)

    def DemandComb(model):
        return ((LocationLevel_SDI[i][1]) for i in range(0,len(LocationLevel_SDI)))
    model.OPProduct_Comb = Set(initialize=DemandComb,dimen=1)

    def Demand3(model,l,p,t):
        for i in range(0,len(LocationLevel_SDI)):
            if l==LocationLevel_SDI[i][0] and p==LocationLevel_SDI[i][1] and t==LocationLevel_SDI[i][2]:
                return int(LocationLevel_SDI[i][3]*int(Decimal_Handler))
        return 0
    model.Demand = Param(model.Demand_Comb,initialize = Demand3)

    def DueDate3(model,p):
        for i in range(0,len(LocationLevel_SDI)):
            if p==LocationLevel_SDI[i][1]:
                return int(LocationLevel_SDI[i][2])
        return 0
    model.DueDate = Param(model.OPProduct_Comb,initialize = DueDate3)

    print '-------------------- Lot Creation Logic ------------------------'
    param = []
    present_orders = []
    cursor.execute('select relation_id,client_id,LOCATION_FROM_ID,ORDER_ID from spps_relation where order_id <> "NULL" and OUTPUT_PRODUCT_ID = "NULL" and \
        INPUT_PRODUCT_ID = "NULL"  and OPERATION_ID = "NULL" and RESOURCE_ID = "NULL" and LOCATION_FROM_ID <> "NULL"  and \
        LOCATION_TO_ID = "NULL"  and client_id = ' +str(Client_ID))
    present_orders = cursor.fetchall()
    print "print6"
    def T1(model):
        return ((present_orders[i][2],present_orders[i][3]) for i in range(0,len(present_orders)) if present_orders[i][2] not in model.present_orders)
    model.present_orders = Set(initialize =T1,dimen=2)
    new_orders = []

    getLastSequence = {}
    for (i,ar) in PO_AR_COMB:
        getLastSequence[i,ar] = max(s for (i1,j,ar1,s) in PO_TASK_AR_SEQ_COMB if i==i1 and ar1==ar)

    print ' ----------------------------------------------- Domain Calculation  ------------------------------------------------'


    global Dom_IP
    global Dom_OP
    Dom_IP={}
    Dom_OP={}
    for (i1,ar1) in PO_AR_COMB:
##      if i1 in current_pos:
        for s in range(getLastSequence[i1,ar1],0,-1):
          #print "s: ", i1, s
          for (i2,j2,ar2,s2) in PO_TASK_AR_SEQ_COMB:
            if i1==i2 and s==s2 and ar1==ar2:
              for (i,j,ar,alt,ip,op) in PO_BOM_COMB:
                if i==i1 and ar==ar1 and j==j2:
                  if Product_Type_ID[int(op)]==3:
                    Dom_IP[(i,j,ip)] = int(Order_Quantity[i] * getAttachRate(op,ip,j))
                    Dom_OP[(i,j,op)] = Order_Quantity[i]
                    for (i3,j3,j4,ar3,s3,s4,p) in MAT_BALANCE_CONST:
                      if i3==i1 and j4==j and ar3==ar1 and s4==s and s3==s-1 and ip==p:
                        if Product_Type_ID[int(ip)]!=1:
                          Dom_OP[(i3,j3,p)]= Dom_IP[(i,j,p)]
                  elif Product_Type_ID[int(op)]==2 and getLastSequence[i1,ar1]==s:   
                    Dom_IP[(i,j,ip)] = int(Order_Quantity[i] * getAttachRate(op,ip,j))
                    Dom_OP[(i,j,op)] = Order_Quantity[i]
                    for (i3,j3,j4,ar3,s3,s4,p) in MAT_BALANCE_CONST:
                      if i3==i1 and j4==j and ar3==ar1 and s4==s and s3==s-1 and ip==p:
                        if Product_Type_ID[int(ip)]!=1:
                          Dom_OP[(i3,j3,p)]= Dom_IP[(i,j,p)]                          
                  elif Product_Type_ID[int(op)]==2:
                      if Product_Type_ID[int(ip)]==1:
                        Dom_IP[(i,j,ip)] = Dom_OP[(i,j,op)] * getAttachRate(op,ip,j)
                      else:
                        Dom_IP[(i,j,ip)] = int(Dom_OP[(i,j,op)] * getAttachRate(op,ip,j))
                      for (i3,j3,j4,ar3,s3,s4,p) in MAT_BALANCE_CONST:
                          if i3==i1 and j4==j and ar3==ar1 and s4==s and s3==s-1 and ip==p:
                            if Product_Type_ID[int(ip)]!=1:    
                              Dom_OP[(i3,j3,p)]= Dom_IP[(i,j,p)]

    PO_SFG_COMB=[]
    PO_SFG_FG_COMB=[]
    SFG_FG_Domain=[]
    SFG_Demand = {}
    SFG_Comb=[]

    # if len(Lot_Sizing_Comb)!=0:      # If each SFG is associated with specific FG batch size as given in sheet Lot_Sizing, then SFG demand should not be aggregated
    for (i,j,ar,alt,ip,op) in PO_BOM_COMB:
        if ip in SFG_FG_Batch_Size_Relation:
           PO_SFG_COMB.append((i,j,ip,op))
           if Product_Type_ID[int(ip)]==2 and Product_Type_ID[int(op)]==3:
            PO_SFG_FG_COMB.append((i,j,ip,op,Dom_IP[(i,j,ip)],Dom_OP[(i,j,op)],getAttachRate(op,ip,j)))
            SFG_FG_Domain.append((ip, op,Dom_IP[(i,j,ip)],Dom_OP[(i,j,op)],getAttachRate(op,ip,j)))

    PO_SFG_COMB=list(set(PO_SFG_COMB))
    SFG_FG_Domain=list(set(SFG_FG_Domain))

    for p in all_products_type2:
        if p in SFG_FG_Batch_Size_Relation:
            sum1=0
            for (i,j,ip,op) in PO_SFG_COMB:
                sum1=0
                if int(ip)==(p) and (op,ip) in Lot_Sizing_Comb:
                    sum1 = sum1 + Dom_IP[(i,j,ip)]
                    # print i,j,ip,op, Dom_IP[(i,j,ip)]
                    SFG_Demand[(ip,op)] = sum1
                    SFG_Comb.append((ip,op))
                    # if SFG_Demand[(ip,op)]>0:
                    #     print "SFG Demand: ", ip,op, SFG_Demand[(ip,op)]
    # else:     # if the SFG demand can be aggregated for multiple FGs

    PO_SFG_FG_COMB1 = []
    SFG_Demand1 = {}
    SFG_FG_Domain1 = []
    PO_SFG_COMB1 = []
    SFG_Comb1 = []
    PO_SFG_COMB1_DueDate=[]

    for (i, j, ar, alt, ip, op) in PO_BOM_COMB:
        if ip not in SFG_FG_Batch_Size_Relation:

            # print "Due Date: ", model.DueDate[op]
            if Product_Type_ID[int(ip)] == 2 and Product_Type_ID[int(op)] == 3:
                PO_SFG_COMB1.append((i, j, ip))
                #print ip, op, model.DueDate[op]
                PO_SFG_COMB1_DueDate.append((ip, op, model.DueDate[op]))
                PO_SFG_FG_COMB1.append((i, j, ip, op, Dom_IP[(i, j, ip)], Dom_OP[(i, j, op)], getAttachRate(op, ip, j)))
                SFG_FG_Domain1.append((ip, op, Dom_IP[(i, j, ip)], Dom_OP[(i, j, op)], getAttachRate(op, ip, j)))
                # print "SFG_FG_Dom: ", i, ip, op, Dom_IP[(i, j, ip)], Dom_OP[(i, j, op)], getAttachRate(op, ip, j)
    dd={}
    PO_SFG_COMB1_DueDate =  list(set(PO_SFG_COMB1_DueDate))

    for ip in all_products_type2:
        min1=99999999999
        for (i, j, t) in PO_SFG_COMB1_DueDate:
            if int(ip) == int(i):
                min1 = min(t,min1)
        dd[ip]=min1
        # print "Due Date: " , ip, dd[ip]


    PO_SFG_COMB1=list(set(PO_SFG_COMB1))
    SFG_FG_Domain1=list(set(SFG_FG_Domain1))

    # Demand from multiple tablets/FGs will be aggregated if a blend/SFG is not mentioned in the Lot_Sizing sheet
    for p in all_products_type2:
        if p not in SFG_FG_Batch_Size_Relation:
           sum1=0
           if Product_Type_ID[int(p)]==2:
               for (i,j,ip) in PO_SFG_COMB1:
                   if int(ip)==(p):
                       sum1 = sum1 + Dom_IP[(i,j,ip)]
               SFG_Demand1[(p)] = sum1
               SFG_Comb1.append((p))

    # BO={}
    # for (i,j,ar,alt,ip,op) in PO_BOM_COMB:
    #     if j == Bottleneck_Operation:
    #         BO[getProductID(i)] = Dom_OP[(i,j,op)]


    print "Demand_Combinations: ", len(model.Demand_Comb)
    
    if Lot_Creation_From == "Demand":        
        for l,p,t in model.Demand_Comb:
          print l,p,t
          if model.Demand[l,p,t]>0:
            lot = 0
            print "l,p,t: ", l,p,t, model.Demand[l,p,t]
            to_allot = model.Demand[l,p,t]
            p11 = [int(model.Lot_Batch_Size[l1,p2,op1,r1,inc]) for l1,p2,op1,r1,inc in model.Lot_Comb if int(p)==int(p2)]
            p1 = f5(p11)
            print "p1: ", p1
            result = Hetero_Lots(to_allot,p1)
            print "result:' ", result
            for l1,p1 in model.Lot_Comb2:
                if int(p)==int(p1):
                    for j in range(0,len(result)):
                        param1 = (Client_ID,Scenario_ID,getProductID(p) + "-Lot" + str(lot),getProductID(p),l,int(t),0, result[j],getProductID(p),3)
                        print param1
                        param.append(param1)
                        if ((l1, (getProductID(p) + "-Lot" + str(lot)))) not in (model.present_orders) and ((l1,(getProductID(p) + "-Lot" + str(lot)))) not in (new_orders):
                            new_orders.append(("NULL","NULL",l1,"NULL",getProductID(p) + "-Lot" + str(lot),"NULL","NULL",Client_ID))
                        lot = lot + 1
        print 99999999
        # if len(Lot_Sizing_Comb) != 0:  # If each SFG is associated with specific FG batch size as given in sheet Lot_Sizing, then SFG demand should not be aggregated
        for (ip,op) in SFG_Comb:
            if ip in SFG_FG_Batch_Size_Relation and op in model.OPProduct_Comb:
              if SFG_Demand[ip,op]>0:
                lot1 = 0
                print "ip,op: ", ip,op, SFG_Demand[ip,op]
                to_allot = SFG_Demand[ip,op]
                for l1, p, op1, r1, inc in model.Lot_Comb:
                    if int(ip) == int(p):
                        print 100, int(model.Lot_Batch_Size[l1, p, op1, r1, inc]) , int(SFG_Lot_Size[(op, ip)])

                p1 = [int(model.Lot_Batch_Size[l1,p,op1,r1,inc]) for l1,p,op1,r1,inc in model.Lot_Comb if int(ip)==int(p) \
                                            and int(model.Lot_Batch_Size[l1,p,op1,r1,inc])==int(SFG_Lot_Size[(op,ip)])]
                print 200000  , to_allot,p1
                result = Hetero_Lots(to_allot,p1)
                print "result2:' ",p1, result
                for l1,p1 in model.Lot_Comb2:
                    if str(p)==str(p1):
                        for j in range(0,len(result)):
                            param1 = (Client_ID,Scenario_ID,getProductID(ip) + "_" + getProductID(op) + "-Lot" + str(lot1),getProductID(ip),l,model.DueDate[op],0, result[j],getProductID(op),2)
                            print param1
                            param.append(param1)
                            if ((l1, (getProductID(ip) + "_" + getProductID(op) + "-Lot" + str(lot1)))) not in (model.present_orders) and ((l1,(getProductID(ip) + "-Lot" + str(lot1)))) not in (new_orders):
                                new_orders.append(("NULL","NULL",l1,"NULL",getProductID(ip) + "_" + getProductID(op) + "-Lot" + str(lot1),"NULL","NULL",Client_ID))
                            lot1 = lot1 + 1
        # else:
        print 100000
        for (p) in SFG_Comb1:
            if p not in SFG_FG_Batch_Size_Relation:
                if SFG_Demand1[p] > 0:
                    lot1 = 0
                    print "p: ", p, SFG_Demand1[p]
                    to_allot = SFG_Demand1[p]
                    p1 = [int(model.Lot_Batch_Size[l1, p1, op1, r1, inc]) for l1, p1, op1, r1, inc in model.Lot_Comb if str(p) == str(p1)]
                    result = Hetero_Lots(to_allot, p1)
                    print "result:' ", p1, result
                    for l1, p1 in model.Lot_Comb2:
                        if str(p) == str(p1):
                            for j in range(0, len(result)):
                                param1 = (Client_ID, Scenario_ID, getProductID(p) + "-Lot" + str(lot1), getProductID(p), l,  dd[p], 0, result[j],getProductID(p),2)
                                print param1
                                param.append(param1)
                                if ((l1, (getProductID(p) + "-Lot" + str(lot1)))) not in (model.present_orders) and ((l1, (getProductID(p) + "-Lot" + str(lot1)))) not in (new_orders):
                                    new_orders.append(("NULL", "NULL", l1, "NULL", getProductID(p) + "-Lot" + str(lot1),"NULL", "NULL", Client_ID))
                                lot1 = lot1 + 1

    else:
        for p, l, op in model.ValidMComb_Uncon:
            lot = 0
            for t in model.Period_ID:
                if p in model.FGs and (p, l, op, t) in model.K5 and (p, l, op, t) in model.Production_Plan_Comb and model.Production_Plan_Value[p, l, op, t] > 0:
                    to_allot = model.Production_Plan_Value[p, l, op, t]
                    while to_allot > 0:
                        for l1, op1, r1,p1 in model.Lot_Comb:
                            if l1 == l and p==p1:
                                if to_allot >= model.Lot_Max_Batch_Size[l1, op1, r1,p]:
                                    param1 = (Client_ID, Scenario_ID, p + "-Lot" + str(lot), p, l,  t + 1, 404,int(model.Lot_Max_Batch_Size[l1, op1, r1,p]),op,2)
                                    param.append(param1)
                                    if ((l, (p + "-Lot" + str(lot)))) not in (model.present_orders) and ((l, (p + "-Lot" + str(lot)))) not in (new_orders):
                                        new_orders.append(("NULL", "NULL", l, "NULL", p + "-Lot" + str(lot), "NULL",
                                                           "NULL", Client_ID))
                                    else:
                                        print "already there"
                                    to_allot = to_allot - model.Lot_Max_Batch_Size[l1, op1, r1,p]
                                    lot = lot + 1
                                elif to_allot < model.Lot_Max_Batch_Size[l1, op1, r1,p] and to_allot != 0:
                                    if to_allot > model.Lot_Min_Batch_Size[l1, op1, r1,p]:
                                        param1 = (Client_ID, Scenario_ID, p + "-Lot" + str(lot), p, l, t + 1, 404,int(to_allot),op,2)
                                        param.append(param1)
                                        if ((l, (p + "-Lot" + str(lot)))) not in (model.present_orders) and (
                                        (l, (p + "-Lot" + str(lot)))) not in (new_orders):
                                            new_orders.append(("NULL", "NULL", l, "NULL", p + "-Lot" + str(lot), "NULL",
                                                               "NULL", Client_ID))
                                        to_allot = 0
                                        lot = lot + 1
                                    else:
                                        param1 = (Client_ID, Scenario_ID, p + "-Lot" + str(lot), p, l, t + 1, 404,int(model.Lot_Min_Batch_Size[l1, op1, r1,p]),op,2)
                                        param.append(param1)
                                        if ((l, (p + "-Lot" + str(lot)))) not in (model.present_orders) and ((l, (p + "-Lot" + str(lot)))) not in (new_orders):
                                            new_orders.append(("NULL", "NULL", l, "NULL", p + "-Lot" + str(lot), "NULL","NULL", Client_ID))
                                        to_allot = 0
                                        lot = lot + 1

                if p in model.SFGs and (p, l, op, t) in model.K5 and (p, l, op, t) in model.Production_Plan_Comb and \
                                model.Production_Plan_Value[p, l, op, t] > 0:
                    to_allot = model.Production_Plan_Value[p, l, op, t]
                    while to_allot > 0:
                        for l1, op1, r1,p1 in model.Lot_Comb:
                            # print l1, op1, r1,p1
                            if l1 == l and p==p1:
                                if to_allot >= model.Lot_Max_Batch_Size[l1, op1, r1,p]:
                                    param1 = (Client_ID, Scenario_ID, p + "-Lot" + str(lot), p, l, t + 1, 404,
                                              int(model.Lot_Max_Batch_Size[l1, op1, r1,p]), op, 2)
                                    param.append(param1)
                                    if ((l, (p + "-Lot" + str(lot)))) not in (model.present_orders) and (
                                    (l, (p + "-Lot" + str(lot)))) not in (new_orders):
                                        new_orders.append(("NULL", "NULL", l, "NULL", p + "-Lot" + str(lot), "NULL",
                                                           "NULL", Client_ID))
                                    else:
                                        print "already there"
                                    to_allot = to_allot - model.Lot_Max_Batch_Size[l1, op1, r1,p]
                                    lot = lot + 1
                                elif to_allot < model.Lot_Max_Batch_Size[l1, op1, r1,p] and to_allot != 0:
                                    if to_allot > model.Lot_Min_Batch_Size[l1, op1, r1,p]:
                                        param1 = (
                                        Client_ID, Scenario_ID, p + "-Lot" + str(lot), p, l, t + 1, 404, int(to_allot), op, 2)
                                        param.append(param1)
                                        if ((l, (p + "-Lot" + str(lot)))) not in (model.present_orders) and (
                                                (l, (p + "-Lot" + str(lot)))) not in (new_orders):
                                            new_orders.append(("NULL", "NULL", l, "NULL", p + "-Lot" + str(lot), "NULL",
                                                               "NULL", Client_ID))
                                        to_allot = 0
                                        lot = lot + 1
                                    else:
                                        param1 = (Client_ID, Scenario_ID, p + "-Lot" + str(lot), p, l, t + 1, 404,
                                                  int(model.Lot_Min_Batch_Size[l1, op1, r1,p]), op, 2)
                                        param.append(param1)
                                        if ((l, (p + "-Lot" + str(lot)))) not in (model.present_orders) and (
                                        (l, (p + "-Lot" + str(lot)))) not in (new_orders):
                                            new_orders.append(
                                                ("NULL", "NULL", l, "NULL", p + "-Lot" + str(lot), "NULL", "NULL", Client_ID))
                                        to_allot = 0
                                        lot = lot + 1

    if len(param)==0:
        f1.write("\n")
        f1.write("No Lots are generated. Please check the data and settings")
        f1.write("\n")
        cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR",COMMENTS="No Comments", END_TIME=NOW(),INFO="Job Failed" where JOB_ID = ' + str(JOB_ID))
        cursor.close()
        cnx.commit()
        cnx.close()
        exit()
    print "print7"
    cursor.execute('truncate table xlps_sfg_fg_domain')
    sfg_fg_param=[]
    for (ip,op,ipq,opq,ar) in SFG_FG_Domain:
        sfg_fg_param.append((Client_ID,Scenario_ID,ip,op,ipq,opq,ar))
    for (ip,op,ipq,opq,ar) in SFG_FG_Domain1:
        sfg_fg_param.append((Client_ID,Scenario_ID,ip,op,ipq,opq,ar))
    sql100 = "insert into xlps_sfg_fg_domain (CLIENT_ID,Scenario_ID,INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT_INDEX,IP_Qty, OP_Qty, ATTACH_RATE) values(%s,%s,%s,%s,%s,%s,%s)"
    cursor.executemany(sql100,sfg_fg_param)
    cursor.execute('update xlps_sfg_fg_domain fg inner join xlps_product_master pm on pm.product_index=fg.input_product_index and  fg.client_id=pm.client_id\
                    and fg.scenario_id=pm.scenario_id set fg.INPUT_PRODUCT_ID = pm.product_id  where pm.client_id = ' + str(Client_ID))
    cursor.execute('update xlps_sfg_fg_domain fg inner join xlps_product_master pm on pm.product_index=fg.output_product_index and  fg.client_id=pm.client_id\
        and fg.scenario_id=pm.scenario_id set fg.OUTPUT_PRODUCT_ID = pm.product_id  where pm.client_id = ' + str(Client_ID) + ' and pm.scenario_id = ' + str(Scenario_ID))

    cursor.execute('truncate table xlps_production_order')
    # print "param: ", param
    sq21 = "insert into xlps_production_order (CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_ID,OUTPUT_PRODUCT_ID,LOCATION_FROM_ID, PERIOD_ID, SP_TS_ID, TS_VALUE, output_product,level) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    cursor.executemany(sq21, param)
    print "print8"
    sq22 = "insert into spps_relation(`OUTPUT_PRODUCT_ID`,`INPUT_PRODUCT_ID`,`LOCATION_FROM_ID`,`LOCATION_TO_ID`,`ORDER_ID`,`OPERATION_ID`,`RESOURCE_ID`,`CLIENT_ID`)\
    values(%s,%s,%s,%s,%s,%s,%s,%s)"
    cursor.executemany(sq22, new_orders)
    print "print9"
    cnx.commit()
##    print "new_orders: ", new_orders
    cursor.execute('select relation_id,client_id,ORDER_ID from spps_relation where order_id <> "NULL" and LOCATION_FROM_ID <> "NULL" and \
                    OUTPUT_PRODUCT_ID = "NULL" and INPUT_PRODUCT_ID = "NULL"  and LOCATION_TO_ID = "NULL"  \
                    and OPERATION_ID = "NULL" and RESOURCE_ID = "NULL" and client_id = ' +str(Client_ID))
    all_orders = cursor.fetchall()
    def T1(model):
        return (all_orders[i][2] for i in range(0,len(all_orders)) if all_orders[i][2] not in model.all_orders )
    model.all_orders = Set(initialize =T1)

    def T2(model,o):
        for i in range(0,len(all_orders)):
            if o == all_orders[i][2]:
                #print all_orders[i][0]
                return all_orders[i][0]    
    model.Relation_IDs = Param(model.all_orders,initialize = T2)
    print "print10"
    for i in model.all_orders:
        #print "updating order i"
        #print 'update xlps_production_order set relation_id = '+str(model.Relation_IDs[i])+' \
                            #where PRODUCTION_ORDER_ID = "'+str(i)+'" and client_id = ' + str(Client_ID)
        cursor.execute('update xlps_production_order set relation_id = '+str(model.Relation_IDs[i])+' \
                            where PRODUCTION_ORDER_ID = "'+str(i)+'" and client_id = ' + str(Client_ID))
    cnx.commit()
##    print "INSERT INTO `spps_df_input`(`RELATION_ID`,`DF_ID`,`DF_VALUE`,`INCREMENT_ID`,`CLIENT_ID`,`SCENARIO_ID`)\
##    select RELATION_ID, 611, PRODUCTION_ORDER_ID,0, client_id, SCENARIO_ID from xlps_production_order where client_id = " + str(Client_ID) + " and scenario_id ="+str(Scenario_ID)

    cursor.execute('Delete from spps_df_input where df_id in (611,636,605,637,606) and client_id = ' + str(Client_ID) + ' and scenario_id = ' + str(Scenario_ID))
    cursor.execute('INSERT INTO spps_df_input(RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID) select RELATION_ID, 611, PRODUCTION_ORDER_ID,0, client_id, SCENARIO_ID from xlps_production_order where client_id = ' + str(Client_ID) + ' and scenario_id ='+str(Scenario_ID))
    cursor.execute('INSERT INTO spps_df_input(RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID) select RELATION_ID, 636, OUTPUT_PRODUCT_ID,0, client_id, SCENARIO_ID from xlps_production_order where client_id = ' + str(Client_ID) + ' and scenario_id ='+str(Scenario_ID))
    cursor.execute('INSERT INTO spps_df_input(RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID) select RELATION_ID, 605, TS_VALUE,0, client_id, SCENARIO_ID from xlps_production_order where client_id = ' + str(Client_ID) + ' and scenario_id ='+str(Scenario_ID))
    cursor.execute('INSERT INTO spps_df_input(RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID) select RELATION_ID, 637, "Normal",0, client_id, SCENARIO_ID from xlps_production_order where client_id = ' + str(Client_ID) + ' and scenario_id ='+str(Scenario_ID))

    cursor.execute('INSERT INTO spps_df_input(RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID) select RELATION_ID, 606, sp.period_value,0, pp.client_id, SCENARIO_ID from xlps_production_order pp \
                        join spapp_period sp on pp.client_id = sp.client_id and pp.period_id = sp.period_id and sp.period_bucket= "'+ str(Planning_Level)  + '" \
                        where pp.client_id = ' + str(Client_ID) + ' and pp.scenario_id ='+str(Scenario_ID))

    print '--------------------------------- Data Upload Complete -------------------------------------'
    currenttime = datetime.datetime.now()
    #f1.write("Step8: Output data export completed at %s " %currenttime.strftime('%Y-%m-%d %H:%M:%S') )
    #f1.close
    cursor.close()
    cnx.commit()
    cnx.close()  
    return 1

def Hetero_Lots(a,p1):
    c=[]
    while(a>0):
        for i in range(0,len(p1)):
            if a>0:
                b = a - p1[i]
                a = a - p1[i]
                if a>=0:                
                    c.append(p1[i])
                else:
                    a = a + p1[i]
            if b<0:
                d = a
                c1 = min ((p1[i]-d) for i in range(0,len(p1)) if  p1[i]-d>=0 )
                c.append(c1+d)
                a = -1
                b = 0
                break
    return c

def f5(seq, idfun=None): 
   # order preserving
   if idfun is None:
       def idfun(x): return x
   seen = {}
   result = []
   for item in seq:
       marker = idfun(item)
       # in old Python versions:
       # if seen.has_key(marker)
       # but in new ones:
       if marker in seen: continue
       seen[marker] = 1
       result.append(item)
   return result

if __name__ == '__main__':
    try:
        print solve_supply_network_optimizer(JOB_ID)
        cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
        cursor = cnx.cursor()
        cursor.execute('update SPAPP_JOB set JOB_STATUS="COMPLETED",COMMENTS="No Comments", END_TIME=NOW(),INFO="Job completed" where JOB_ID = ' + str(JOB_ID))
        cursor.close()
        cnx.commit()
        cnx.close()
    except Exception as x:
        print x
        cnx = mysql.connector.connect(user='root', password='saddlepoint', host='127.0.0.1', database='saddlepointv6_prod2',buffered=True)
        cursor = cnx.cursor()
        cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
        cursor.close()
        cnx.commit()
        cnx.close()

