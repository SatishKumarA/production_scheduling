


def Duration_Pre_Compute_Task_Split(Invalid_Timings2):
    original_task_duration = 1

    # start with list assuming no delays
    Duration1 = [original_task_duration] * 10

    # work backward, updating all durations based on each invalid timing
    for t in reversed(Invalid_Timings2):
        # unpack once (thanks Brian)
        stt, end = t
        delay = end - stt

        # any duration between stt/end -> invalid
        Duration1[stt:end] = [-1] * delay

        # any duration before stt -> delayed
        # (everything from 0 to stt is the same, so just use the first element)
        if stt <= Duration1[0] + delay:
            Duration1[:stt] = [Duration1[0] + delay] * stt

    return Duration1

Invalid_Timings2 = [(2,4), (6,7)]

Duration= Duration_Pre_Compute_Task_Split(Invalid_Timings2)
print Duration
