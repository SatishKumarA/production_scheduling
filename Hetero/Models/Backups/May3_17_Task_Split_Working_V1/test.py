import numpy as np
# task : start between 0 and 10, duration = 5
# break between 2 and 4, and 6 and 7.
#
# so duration is [8, 8, -1, -1, 6, 6, -1, 5, 5, 5]

break1 = [(2,4),(6,7), (8,9)]
original_task_duration = 5
Duration1=[]

for i in range(0, 10):
    break_sum = 0
    dur= original_task_duration
    k=1
    for j in range(0,len(break1)):
        if i < break1[j][1] and i >= break1[j][0]:
            Duration1.append(-1)
            k=0
            break
        else:
            if  break1[j][0] >= i - original_task_duration and i<break1[j][1]:
                break_sum =  break_sum + break1[j][1] - break1[j][0]
                dur=dur + break1[j][1] - break1[j][0]
    if k!=0:
        Duration1.append(break_sum + original_task_duration)
print Duration1





def find_invalid_range(Duration1):
    #print machine_shutdown, machine_holidays
    valid_range_array = np.zeros(10)

    for i in range(1,len(Duration1)):
        # if i<=10:
            valid_range_array[i] = Duration1[i]
    print valid_range_array
      ## Find where value chages. This point is start of invalid range, as index 0 is always 0.
    change_index = np.where(valid_range_array[:-1] != valid_range_array[1:])[0]
    print valid_range_array[:-1]

    print "change_index1: ", change_index

    invalid_ranges = []
    print valid_range_array[0]
    if valid_range_array[0] == 0:
          index = 1
          while index < len(change_index):
              invalid_ranges.append((change_index[index-1], change_index[index]))
              index = index + 1
    print "invalid_ranges: ", invalid_ranges
    return invalid_ranges

new1 = find_invalid_range(Duration1)



















# def Duration_Pre_Compute_Task_Split(Invalid_Timings2):
#     Duration1 = []
#
#     # for m in ip_data.Task_Split_Allowed_Machines:
#     for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
#         if ip_data.Machine_ID[(i, j, ar, s, alt)] in ip_data.Task_Split_Allowed_Machines and ip_data.Machine_ID[
#             (i, j, ar, s, alt)] == 10:
#             original_task_duration = int(ip_data.OP_Setup_Time[i, j, ar, s, alt]) + int(
#                 ip_data.Processing_Time[i, j, ar, s, alt]) + int(ip_data.Teardown_Time[i, j, ar, s, alt])
#             dur = original_task_duration
#             for h in xrange(0, 10000):  # int(ip_data.PlanningHorizon)):
#                 break_sum = 0
#                 k = 1
#                 Duration = [(i, j, ar, s, alt, h, -1) for t in Invalid_Timings2 if
#                             ip_data.Machine_ID[(i, j, ar, s, alt)] == t[0] \
#                             and t[0] <= 10000 and t[1] <= h < t[1] + t[2]]
#                 Duration1.append(Duration)
#                 for t in Invalid_Timings2:
#                     if ip_data.Machine_ID[(i, j, ar, s, alt)] == t[0] and t[0] <= 10000:
#                         if t[1] <= h < t[1] + t[2]:
#                             Duration1.append((i, j, ar, s, alt, h, -1))
#                             k = 0
#                             break
#                         else:
#                             if t[1] - dur <= h <= t[1] + dur and h < t[1] + t[2]:
#                                 break_sum += t[2]
#                                 dur = dur + t[2]
#                 if k != 0:
#                     Duration1.append((i, j, ar, s, alt, h, break_sum + original_task_duration))
#     return Duration1






def Duration_Pre_Compute_Task_Split(Invalid_Timings2):
    Duration1 = []
    Duration = []
    for (p, mac) in ip_data.PROD_MAC_Comb:
        if int(mac) in ip_data.Task_Split_Allowed_Machines and len(Invalid_Timings2) > 0 and mac == 10:
            original_task_duration = int(ip_data.PROD_MAC_Time[(p, mac)])
            # Duration = [(p, mac, h, -1) for h in xrange(0, int(ip_data.PlanningHorizon)) \
            #                                 for t in Invalid_Timings2 if int(mac)==t[0] and t[0]==10 and h in range(t[1], t[1] + t[2])]
            Duration = [(p, mac, h, -1) for t in Invalid_Timings2 if int(mac) == t[0] and t[0] == 10 \
                        for h in range(t[1], t[1] + t[2])]
            for h in xrange(0, int(ip_data.PlanningHorizon)):
                dur = original_task_duration
                break_sum = 0

                # if t[1] <= h < t[1] + t[2]:
                #     Duration1.append((p, mac, h, -1))
                #     k = 0
                #     break
                # else:
                # k=1

                # Duration1.append((p, mac, h, break_sum + original_task_duration)for h in xrange(0, int(ip_data.PlanningHorizon)) \
                #                             for t in Invalid_Timings2 if int(mac)==t[0] \
                #                             and h not in range(t[1],t[1]+t[2]) and t[1] - dur <= h <= t[1]+ dur and h < t[1] + t[2] )
                for t in Invalid_Timings2:
                    if int(mac) == t[0]:  # and t[1]<=int(ip_data.PlanningHorizon):  #and h not in (t[1],t[1]+t[2])
                        if h not in range(t[1], t[1] + t[2]) and t[1] - dur <= h <= t[1] + dur and h < t[1] + t[2]:
                            break_sum += t[2]
                            dur = dur + t[2]
                # if k != 0:
                Duration1.append((p, mac, h, break_sum + original_task_duration))
    return Duration1 + Duration