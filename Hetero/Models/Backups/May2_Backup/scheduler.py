
import operator
from operator import itemgetter
from ortools.constraint_solver import pywrapcp
# import os
import datetime
from datetime import date, timedelta
from time import time
import numpy as np
import mysql.connector
import math
from decimal import *


# from input_data import *
# from input_data import *
# from master import *

import input_data as ip_data
import master as mp
import output_data as op_data
# import scheduler as ps
import local_search as ls

def PriorityIterationSolve():
    print "Inside Priority Iteration Solve"
    print "Calling high priority solve"
    global High_Priority_Comb
    global High_Priority_Begin_Time
    global High_Priority_End_Time
    global High_Priority_Active_Status
    High_Priority_Comb = []
    High_Priority_Begin_Time = {}
    High_Priority_End_Time = {}
    High_Priority_Active_Status = {}
    print "Len: ", len(ip_data.hp_pos)

    if len(ip_data.hp_pos) > 0:
        print "Calling high priority solve"
        FirstSolve(ip_data.hp_pos, 0)

    print "Calling all priority solve"
    FirstSolve(ip_data.all_pos, 1)

    return True


def FirstSolve(current_pos1, all_pos_iteration1):
    print "Inside First Solve"
    global current_pos
    global all_pos_iteration
    current_pos = []
    current_pos = current_pos1
    all_pos_iteration = all_pos_iteration1
    Solve_Time = ip_data.Solver_Time_Limit
    ##  global active_var_new
    active_var_new = {}
    obj_value = 999999999999
    makespan_value = 999999999999
    first_solve = 1
    New_Set = []
    Current_Set = []
    fixed_dec = {}
    all_machine_ends = {}
    start_new = {}
    end_new = {}

    decision, obj_value_new, active_var, PO_TASK_AR_SEQ_ALT_COMB, PO_TASK_AR_SEQ_ALT_COMB_New, fixed_dec, all_machine_ends, fixed_start, fixed_end, makespan_value_new \
        = solve_production_scheduler2(active_var_new, start_new, end_new, obj_value, New_Set, Current_Set, 0,
                                      first_solve, makespan_value, 999, 999, 0, 0)
    currenttime = datetime.datetime.now()
    print "Step31_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    print "After First Solve (Decision, Objective Value, Makespan) : ", decision, obj_value_new, makespan_value_new

    Diff = int(time()) - int(ip_data.Process_Start_Time)

    if decision == 1:
        if Diff > int(Solve_Time):
            currenttime = datetime.datetime.now()
            print "Step32:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            if all_pos_iteration == 1:
                op_data.Output_Update()
            else:
                op_data.High_Priority_Output_Update()
        else:
            if all_pos_iteration == 1:
                if ip_data.Local_Search=="Enable":
                    print "Local Search Enabled"
                    first_solve=0
                    return_val = ls.LSOperator(PO_TASK_AR_SEQ_ALT_COMB,PO_TASK_AR_SEQ_ALT_COMB_New,active_var,obj_value_new,all_machine_ends,fixed_dec,fixed_start,fixed_end,makespan_value_new)
                    print return_val
                else:
                    op_data.Output_Update()
                    print "LS Disabled"
            else:
                print "High priority solve done"
                ##      first_solve=0
                ##      return_val = LSOperator(PO_TASK_AR_SEQ_ALT_COMB,PO_TASK_AR_SEQ_ALT_COMB_New,active_var,obj_value_new,all_machine_ends,fixed_dec,fixed_start,fixed_end,makespan_value_new)
                ##      print return_val
    else:
        f1 = open(ip_data.LogFile, 'a')
        f1.write("No Feasible Solution Found within time Limit")
        f1.write("\n")

        cnx2 = mysql.connector.connect(user='root', password='saddlepoint', host=mp.Host_String,database=mp.DB_String)
        cursor2 = cnx2.cursor()
        cursor2.execute('update SPAPP_JOB set JOB_STATUS="COMPLETED", INFO="No feasible Solution found", END_TIME=NOW() where JOB_ID = ' + str(mp.JOB_ID))
        cursor2.close()
        cnx2.commit()
        cnx2.close()

    print "Final Step"

    return True


def solve_production_scheduler2(active_var_new, start_new, end_new, obj_value, New_Set, Time_Fixed_Set, current_seq,
                                first_solve,
                                makespan_value, loop, Iteration_Set_Length, iter_solve, last_failed_seq):


    print "Before CP Solve Objective Value, Makespan : ", obj_value, makespan_value

    currenttime = datetime.datetime.now()

    # parameters = pywrapcp.Solver.DefaultSolverParameters()
    # parameters.trace_search = True
    # parameters.trace_propagation = True
    solver = pywrapcp.Solver('Production Scheduling')

    PROD_ORDER_COUNT = int(len(current_pos))
    print "PROD_ORDER_COUNT: ", PROD_ORDER_COUNT

    # global Dom_IP
    # global PO_OP_SFG_Comb
    # global Dom_OP
    Dom_IP = {}
    Dom_OP = {}
    Dom_IP, Dom_OP, PO_OP_SFG_Comb =  Domain_Calculation()

    # for (i,j,ar,alt,ip,op) in ip_dInput_Product_Qty
    SFG_FG_Domain = []

    for (i, j, ar, alt, ip, op) in ip_data.PO_BOM_COMB:
        if i in current_pos and ip_data.Product_Type_ID[int(ip)] == 2 and ip_data.Product_Type_ID[int(op)] == 3:
            # print ip, op
            # print Dom_IP[(i, j, ip)], Dom_OP[(i, j, op)]
            # print getAttachRate(op, ip, j)
            # print 100, op,ip,j
            SFG_FG_Domain.append((ip, op, Dom_IP[(i, j, ip)], Dom_OP[(i, j, op)], getAttachRate(op, ip, j)))
            # print "SFG_FG_Dom: ", i, ip, op,Dom_IP[(i,j,ip)],Dom_OP[(i,j,op)],getAttachRate(op,ip,j)
            # print 200
    SFG_FG_Domain = list(set(SFG_FG_Domain))
    # print "SFG_FG_Domain - ", SFG_FG_Domain

    for i in range(0, len(SFG_FG_Domain)):
        ip_data.SFG_FG_Domain_Comb.append((SFG_FG_Domain[i][0], SFG_FG_Domain[i][1]))
        ip_data.FG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = int(SFG_FG_Domain[i][3])
        ip_data.SFG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = int(SFG_FG_Domain[i][2])
        ip_data.AR[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][4]

    # In case of high priority iteration, SFG Qty data is assigned to FG lot and zeroed out.
    #  So for all pos iteration, we need to recalculate the assignment to create the valid po comb.
    for i in range(0, len(ip_data.SFG_Lots)):
        ip_data.SFG_Order_Qty[ip_data.SFG_Lots[i][0]] = int(ip_data.SFG_Lots[i][2])

    ip_data.SFG_FG_Domain_Comb = list(set(ip_data.SFG_FG_Domain_Comb))

    valid_po_comb = []
    valid_po_comb_truncated_sfg = []
    fg_of_sfglotscompleted = []
    sfglotscompleted = []
    fg_lots_with_no_sfg_lots=[]
    valid_po_comb, fg_lots_with_no_sfg_lots, fg_of_sfglotscompleted, valid_po_comb_truncated_sfg = valid_po_comb_calculation()
    for i in ip_data.Lots_Completed:
        if i in current_pos:
            current_pos.remove(i)
    TASK_MACHINE = {}
    for (i, j, ar, s) in ip_data.PO_TASK_AR_SEQ_COMB:
        if i in current_pos:
            TASK_MACHINE[(i, j, ar, s)] = getnMachine(i, j, ar)

    print ' ----------------------------------------------- Machine Invalid Timings ------------------------------------------------'

    currenttime = datetime.datetime.now()
    print "Step7_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    invalid_end_time = []
    global Total_Shift_Timings
    Total_Shift_Timings = {}
    for m in ip_data.all_machines:
        # Default Values per day
        if ip_data.PlanningBucket == "Minutes":
            Total_Shift_Timings[m[0]] = 0
        elif ip_data.PlanningBucket == "Hours":
            Total_Shift_Timings[m[0]] = 0
        elif ip_data.PlanningBucket == "Seconds":
            Total_Shift_Timings[m[0]] = 0
        count = 0
        temp = 0
        x = 0
        y = 0
        for l in range(0, len(ip_data.Machine_Shift_Details)):
            if m[0] == ip_data.Machine_Shift_Details[l][0]:
                if count == 0:
                    x = 0
                    y = ip_data.Machine_Shift_Details[l][2]
                    if int(x) != int(y):
                        invalid_end_time.append([m[0], x, y])
                    x = y
                    y = ip_data.Machine_Shift_Details[l][3]
                    temp = y
                    count = count + 1
                else:
                    x = temp
                    y = ip_data.Machine_Shift_Details[l][2]
                    if int(x) != int(y):
                        invalid_end_time.append([m[0], x, y])
                    x = y
                    y =ip_data.Machine_Shift_Details[l][3]
                    temp = y

        if len(ip_data.Machine_Shift_Details) > 0:
            for l in range(0, len(ip_data.Machine_Shift_Details)):
                if m[0] == ip_data.Machine_Shift_Details[l][0]:
                    if ip_data.PlanningBucket == "Minutes" and ip_data.Machine_Shift_Details[l][3] <= 1440:
                        Total_Shift_Timings[m[0]] = Total_Shift_Timings[m[0]] + ip_data.Machine_Shift_Details[l][3] - \
                                                    ip_data.Machine_Shift_Details[l][2]

                    elif ip_data.PlanningBucket == "Hours" and ip_data.Machine_Shift_Details[l][3] <= 24:
                        Total_Shift_Timings[m[0]] = Total_Shift_Timings[m[0]] + ip_data.Machine_Shift_Details[l][3] - \
                                                    ip_data.Machine_Shift_Details[l][2]

                    elif ip_data.PlanningBucket == "Seconds" and ip_data.Machine_Shift_Details[l][3] <= 86400:
                        Total_Shift_Timings[m[0]] = Total_Shift_Timings[m[0]] + ip_data.Machine_Shift_Details[l][3] - \
                                                    ip_data.Machine_Shift_Details[l][2]

        if ip_data.PlanningBucket == "Minutes" and Total_Shift_Timings[m[0]] == 0:
            Total_Shift_Timings[m[0]] = 1440
        elif ip_data.PlanningBucket == "Hours" and Total_Shift_Timings[m[0]] == 0:
            Total_Shift_Timings[m[0]] = 24
        elif ip_data.PlanningBucket == "Seconds" and Total_Shift_Timings[m[0]] == 0:
            Total_Shift_Timings[m[0]] = 86400

    currenttime = datetime.datetime.now()
    print "Step7_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    global Invalid_Timings
    Invalid_Timings = []
    for l in xrange(0, len(invalid_end_time)):
        Invalid_Timings.append([invalid_end_time[l][0], invalid_end_time[l][1], invalid_end_time[l][2]])

    Shutdown_Timings = []
    for i in xrange(0, len(ip_data.Machine_Shutdown_Details)):
        Shutdown_Timings.append([int(ip_data.Machine_Shutdown_Details[i][0]), int(ip_data.Machine_Shutdown_Details[i][1]),
                                 int(ip_data.Machine_Shutdown_Details[i][2])])

    currenttime = datetime.datetime.now()
    print "Step7_3:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Invalid_Timings2 = []
    for m in ip_data.all_machines:
        Machine_Invalid_Timings = []
        Machine_Shutdown_Timings = []
        for i in Invalid_Timings:
            if i[0] == m[0]:
                Machine_Invalid_Timings.append((i[1], i[2]))
        for j in Shutdown_Timings:
            if j[0] == m[0]:
                Machine_Shutdown_Timings.append((j[1], j[2]))
        # print "Machine_Invalid_Timings:", Machine_Invalid_Timings
        # print "Machine_Shutdown_Timings:", Machine_Shutdown_Timings
        invalid_ranges2 = find_invalid_ranges(Machine_Invalid_Timings, Machine_Shutdown_Timings)
        ##    print m[0],invalid_ranges2
        for k in invalid_ranges2:
            Invalid_Timings2.append([m[0], k[0], k[1] - k[0], 1])
    # Invalid_Timings2.append([9, 1000, 11960, 1])

    # print "Invalid_Timings2:", Invalid_Timings2

    currenttime = datetime.datetime.now()
    print "Step7_4:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------------------- Shutdown_Timings  --------------------------------------'

    for t in Invalid_Timings2:
        m1 = solver.FixedInterval(int(t[1]), int(t[2]), 'Invalid_%i_%i_%i' % (int(t[0]), int(t[1]), int(t[2])))
        t[3] = m1

    ##  print ' ----------------------------------------------- Defining Tasks ------------------------------------------------'
    global all_tasks
    all_tasks = {}
    global active_variables
    active_variables = {}
    alt_var = {}

    currenttime = datetime.datetime.now()
    print "Step8:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    ##  print "Initial_Schedule_Comb: ", Initial_Schedule_Comb
    for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
        if i in current_pos:
            if int(ip_data.Freeze_Time_Fence) > 0:
                ##          print 100, (i,j,ar,s,alt) in Initial_Schedule_Comb
                if (i, j, ar, s, alt) in ip_data.Initial_Schedule_Comb:
                    ##              print "Freeze:",(i,j,ar,s,alt)
                    all_tasks[(i, j, ar, s, alt)] = solver.IntervalVar(int(ip_data.Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(ip_data.Task_End_Time[(i, j, ar, s, alt)]),
                                                                       int(ip_data.Task_End_Time[(i, j, ar, s, alt)]) - int(
                                                                           ip_data.Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(ip_data.Task_End_Time[(i, j, ar, s, alt)]) - int(
                                                                           ip_data.Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(ip_data.Task_Begin_Time[(i, j, ar, s, alt)]),
                                                                       int(ip_data.Task_End_Time[(i, j, ar, s, alt)]),
                                                                       False, 'all_tasks[(%i,%i,%i,%i,%i)]' % (
                                                                           i, j, ar, s, alt))

            if (i, j, ar, s, alt) not in ip_data.Initial_Schedule_Comb:
                if (i, j, ar, s, alt) in High_Priority_Comb:
                    ##                print "High Priority:",(i,j,ar,s,alt)
                    all_tasks[(i, j, ar, s, alt)] = solver.IntervalVar(
                        int(High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]) - int(
                            High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]) - int(
                            High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_Begin_Time[(i, j, ar, s, alt)]),
                        int(High_Priority_End_Time[(i, j, ar, s, alt)]),
                        False, 'all_tasks[(%i,%i,%i,%i,%i)]' % (i, j, ar, s, alt))
                else:
                    ##                print "Normal Priority:",(i,j,ar,s,alt)
                    all_tasks[(i, j, ar, s, alt)] = solver.IntervalVar(int(ip_data.Machine_Available_Start_Time[j, alt]),
                                                                       int(ip_data.Machine_Available_End_Time[j, alt]),
                                                                       0, 100000000000,
                                                                       int(ip_data.Machine_Available_Start_Time[j, alt]),
                                                                       int(ip_data.Machine_Available_End_Time[j, alt]),
                                                                       True, 'all_tasks[(%i,%i,%i,%i,%i)]' % (
                                                                           i, j, ar, s, alt))

            active_variables[(i, j, ar, s, alt)] = solver.BoolVar()
            solver.Add(active_variables[(i, j, ar, s, alt)] == all_tasks[(i, j, ar, s, alt)].PerformedExpr().Var())

            if first_solve == 0 and (i, j, ar, s, alt) in New_Set:
                solver.Add(active_variables[(i, j, ar, s, alt)] == active_var_new[(i, j, ar, s, alt)])

            if first_solve == 0 and (i, j, ar, s, alt) in Time_Fixed_Set:
                solver.Add(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var() == start_new[(i, j, ar, s, alt)])
                solver.Add(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var() == end_new[(i, j, ar, s, alt)])

    currenttime = datetime.datetime.now()
    print "Step9:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, j, ar, s, outp) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB:
        if i in current_pos:  ## and outp not in Alternate_BOM_Products:
            alt_var[(i, j, ar, s, outp)] = solver.IntVar(0, 1000, 'alt_var[(%i,%i,%i,%s,%i)]' % (i, j, ar, s, outp))
            solver.Add(alt_var[(i, j, ar, s, outp)].MapTo([active_variables[(i1, j1, ar1, s1, alt)]
                                                           for (i1, j1, ar1, s1, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB
                                                           if i1 == i and ar1 == ar and s1 == s and j == j1
                                                           for (po, op, r, s3, outp1) in ip_data.PO_TASK_AR_SEQ_OP_COMB
                                                           if po == i1 and j == op and ar1 == r and s == s3 and outp == outp1]))

            ##    ct0:alternative(alt_var[k], all(k2 in PO_TASK_AR_SEQ_OP_COMB, k1 in PO_TASK_AR_SEQ_ALT_COMB:
            ##      		k1.po==k.po && k.ar==k1.ar && k.seq==k1.seq
            ##      		&& k2.po==k.po && k.ar==k2.ar && k.seq==k2.seq && k2.op==k1.op && k.outp==k2.outp) all_tasks[k1]);
            ##    sr.production_order_index, sr.operation_index, sr.routing_ID,sr.sequence_id,bm.input_product_index, \
            ##bm.output_product_index
    currenttime = datetime.datetime.now()
    print "Step10:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    active_variables_ar = {}
    for (i, j, ar, s, outp) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB:
        if i in current_pos:
            active_variables_ar[(i, j, ar, s, outp)] = solver.BoolVar()
            solver.Add(active_variables_ar[(i, j, ar, s, outp)] == solver.Sum([active_variables[(i1, j1, ar1, s1, alt)] \
                                                                               for (i1, j1, ar1, s1, alt) in
                                                                               ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                                                                               if
                                                                               i == i1 and ar == ar1 and s1 == s and j == j1 \
                                                                               for (po, op, r, s3, outp1) in
                                                                               ip_data.PO_TASK_AR_SEQ_OP_COMB \
                                                                               if
                                                                               po == i1 and j == op and ar1 == r and s == s3 and outp == outp1]))
    global getLastSequence
    getLastSequence = {}
    for (i, ar) in ip_data.PO_AR_COMB:
        if i in current_pos:
            getLastSequence[i, ar] = max(s for (i1, j, ar1, s) in ip_data.PO_TASK_AR_SEQ_COMB if i == i1 and ar1 == ar)

    for i in current_pos:
        solver.Add(solver.Sum(
            active_variables_ar[(i1, j, ar, s, outp)] for (i1, j, ar, s, outp) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB if
            i1 == i and getLastSequence[i1, ar] == s) == 1)

    for (i, j, ar, s, outp) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB:
        for (i1, j1, ar1, s1, outp1) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB:
            if i in current_pos and i == i1 and ar == ar1 and s != s1:
                solver.Add(active_variables_ar[(i, j, ar, s, outp)] == active_variables_ar[(i1, j1, ar1, s1, outp1)])

                ##forall(k in PO_AR_OP_OUTP_COMB, k1 in PO_AR_OP_OUTP_COMB:k.po==k1.po && k.ar==k1.ar && k.seq!=k1.seq)
                ##	ct11:active_variables_ar[k] == active_variables_ar[k1];

    currenttime = datetime.datetime.now()
    print "Step11:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------------------- Tasks and Sequence Creation ------------------------------------------------'

    currenttime = datetime.datetime.now()
    print "Step12:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    global non_overlapping_machines
    all_sequences = {}
    all_transitions = []
    OutputSeq_Mac_Job = {}
    Seq_ID = {}
    Route_ID = {}
    Oper_ID = {}
    Prod_ID = {}
    OutputSeq_ID = {}
    Mac_Sequence_PO = {}
    TSeq_Cnt = {}
    machines_jobs_sdst = {}
    machines_jobs_sdst_new = {}
    non_overlapping_machines_all = []
    Total_Actual_Jobs = {}
    Start_Invalid = {}
    End_Invalid = {}
    Duration_Invalid = {}

    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        for j in ip_data.all_machines:
            ##      print "Machine: ", j[0]
            machines_jobs = []
            Seq_Cnt = 0
            Total_Actual_Jobs[j[0]] = 0
            noi = -1
            machines_jobs_sdst[j[0]] = []
            machines_jobs_sdst_new[j[0]] = []
            for (i, k, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
                ##        print i,k,ar,s,alt
                if i in current_pos and ip_data.Machine_ID[i, k, ar, s, alt] == j[0]:
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i
                    ##          print i
                    OutputSeq_ID[(j[0], Seq_Cnt)] = 0
                    Seq_ID[(j[0], Seq_Cnt)] = s
                    Route_ID[(j[0], Seq_Cnt)] = ar
                    Oper_ID[(j[0], Seq_Cnt)] = k
                    Prod_ID[(j[0], Seq_Cnt)] = ip_data.Product_Index[i, k, ar, s, alt]
                    machines_jobs.append(all_tasks[(i, k, ar, s, alt)])
                    machines_jobs_sdst[j[0]].append(all_tasks[(i, k, ar, s, alt)])
                    machines_jobs_sdst_new[j[0]].append(all_tasks[(i, k, ar, s, alt)])
                    Mac_Sequence_PO[(j[0], Seq_Cnt)] = i
                    Seq_Cnt = Seq_Cnt + 1
                    if ip_data.Op_Type[k] != 3:
                        noi = j[0]
                        non_overlapping_machines_all.append((j[0]))
                        ##      print j[0], Seq_Cnt
            Total_Actual_Jobs[j[0]] = Seq_Cnt
            machines_jobs.extend(t[3] for t in Invalid_Timings2 if t[0] == j[0] and t[0] not in ip_data.Task_Split_Allowed_Machines)
            machines_jobs_sdst_new[j[0]].extend(t[3] for t in Invalid_Timings2 if t[0] == j[0] and t[0] not in ip_data.Task_Split_Allowed_Machines)
            ##      if j[0]==2:
            ##          print "machines_jobs_sdst_new: ", machines_jobs_sdst_new
            k = 9999
            for t in Invalid_Timings2:
                if t[0] == j[0] and t[0] not in ip_data.Task_Split_Allowed_Machines:
                    # print j[0],Seq_Cnt, i , k
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i + k
                    OutputSeq_ID[(j[0], Seq_Cnt)] = 999
                    Start_Invalid[(j[0], Seq_Cnt)] = int(t[1])
                    End_Invalid[(j[0], Seq_Cnt)] = int(t[1]) + int(t[2])
                    Duration_Invalid[(j[0], Seq_Cnt)] = int(t[2])
                    ##          print "Duration_Invalid: ", j[0],Seq_Cnt, Duration_Invalid[(j[0],Seq_Cnt)]
                    Seq_Cnt = Seq_Cnt + 1
                    k = k + 1
            TSeq_Cnt[j[0]] = Seq_Cnt
            if noi == j[0]:
                ##        print "mac: ", j[0], noi
                disj = solver.DisjunctiveConstraint(machines_jobs, 'machine%i' % j[0])
                all_sequences[j[0]] = disj.SequenceVar()
                solver.Add(disj)
    else:
        for j in ip_data.all_machines:
            machines_jobs = []
            Seq_Cnt = 0
            for (i, k, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
                if i in current_pos and ip_data.Machine_ID[i, k, ar, s, alt] == j[0]:
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i
                    Seq_ID[(j[0], Seq_Cnt)] = s
                    Route_ID[(j[0], Seq_Cnt)] = ar
                    Oper_ID[(j[0], Seq_Cnt)] = k
                    Prod_ID[(j[0], Seq_Cnt)] = ip_data.Product_Index[i, k, ar, s, alt]
                    machines_jobs.append(all_tasks[(i, k, ar, s, alt)])
                    Seq_Cnt = Seq_Cnt + 1
                if ip_data.Op_Type[k] != 3:
                    noi = j[0]
                    non_overlapping_machines_all.append((j[0]))
            machines_jobs.extend(t[3] for t in Invalid_Timings2 if t[0] == j[0] and t[0] not in ip_data.Task_Split_Allowed_Machines)
            k = 9999
            for t in Invalid_Timings2:
                if t[0] == j[0] and t[0] not in ip_data.Task_Split_Allowed_Machines:
                    OutputSeq_Mac_Job[(j[0], Seq_Cnt)] = i + k
                    OutputSeq_ID[(j[0], Seq_Cnt)] = 999
                    Seq_Cnt = Seq_Cnt + 1
                    k = k + 1
            machines_jobs.extend(t1[3] for t1 in Invalid_Timings2 if t1[0] == j[0])
            TSeq_Cnt[j[0]] = Seq_Cnt
            if noi == j[0]:
                disj = solver.DisjunctiveConstraint(machines_jobs, 'machine%i' % j[0])
                all_sequences[j[0]] = disj.SequenceVar()
                solver.Add(disj)

    # print " ---------------- Renewable(Cumulative) Resource Constraint ----------------- "
    ##  S = [all_tasks[(i, j, ar, s, alt)] for (i,j,ar,s,alt) in PO_TASK_AR_SEQ_ALT_COMB if j==2]
    ##  C = 1
    ##  D = [1 for (i,j,ar,s,alt) in PO_TASK_AR_SEQ_ALT_COMB if j==2]
    ##  print "S: ", S
    ##  print "D: ", D
    ##  solver.Add(solver.Cumulative(S, D, C, "cumul"))

    print " ---------------- Renewable(Cumulative) Resource Constraint ----------------- "
    for j1 in ip_data.Ops_Renewablw_res:
        S = [all_tasks[(i, j, ar, s, alt)] for (i,j,ar,s,alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if i in current_pos and  i in ip_data.POs_Renewablw_res and j1==j]
        C = 2
        D = [1 for (i,j,ar,s,alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if i in current_pos and  i in ip_data.POs_Renewablw_res and j==j1]
        # print "S: ", S
        # print "D: ", D
        solver.Add(solver.Cumulative(S, D, C, "cumul"))



    non_overlapping_machines = set(non_overlapping_machines_all)

    currenttime = datetime.datetime.now()
    print "Step12_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  for seq in all_machines:
    ##    if seq[0] in non_overlapping_machines:
    ##        sequence = all_sequences[seq[0]];
    ##        sequence_count = TSeq_Cnt[seq[0]]
    ##        total_length = len(machines_jobs_sdst_new[seq[0]])
    ##        len_act_jobs = Total_Actual_Jobs[seq[0]]
    ##        len_invalid_times = total_length-len_act_jobs
    ##
    ####        print seq[0], sequence_count
    ##        for i in xrange(len_act_jobs):
    ##          for j in xrange(len_act_jobs):
    ##              if i<j and (seq[0],OutputSeq_Mac_Job[(seq[0],i)],OutputSeq_Mac_Job[(seq[0],j)]) in SETUP_TIME_NEW_COMB:
    ##                  t = sequence.Interval(i)
    ##                  t1 = sequence.Interval(j)
    ##                  for i2 in xrange(len_act_jobs,total_length):
    ##                      t2 = sequence.Interval(i2)
    ##                      if OutputSeq_ID[(seq[0],i2)] == 999 and OutputSeq_ID[(seq[0],i)] != 999 and OutputSeq_ID[(seq[0],j)] != 999:
    ####                          print i,OutputSeq_Mac_Job[(seq[0],i)], OutputSeq_Mac_Job[(seq[0],j)], SDST[(seq[0],OutputSeq_Mac_Job[(seq[0],i)],OutputSeq_Mac_Job[(seq[0],j)])], i2
    ##                          b100 = solver.IsLessOrEqualCstVar(t.EndExpr() - Start_Invalid[seq[0],i2],0)
    ##                          b200 = solver.IsGreaterOrEqualCstVar(t1.StartExpr() - End_Invalid[seq[0],i2],0)
    ##                          b101 = solver.IsGreaterOrEqualCstVar(t1.StartExpr()  - t.EndExpr() - \
    ##                                        SDST[(seq[0],OutputSeq_Mac_Job[(seq[0],i)],OutputSeq_Mac_Job[(seq[0],j)])] - Duration_Invalid[seq[0],i2], 0)
    ##
    ##                          solver.Add(b101>=b200*b100)
    ##
    ##                          b1000 = solver.IsLessOrEqualCstVar(t1.EndExpr() - Start_Invalid[seq[0],i2],0)
    ##                          b2000 = solver.IsGreaterOrEqualCstVar(t.StartExpr() - End_Invalid[seq[0],i2],0)
    ##                          b102 = solver.IsGreaterOrEqualCstVar(t.StartExpr()  - t1.EndExpr() - \
    ##                                        SDST[(seq[0],OutputSeq_Mac_Job[(seq[0],j)],OutputSeq_Mac_Job[(seq[0],i)])] - Duration_Invalid[seq[0],i2], 0)
    ##                          solver.Add(b102>=b2000*b1000)

    currenttime = datetime.datetime.now()
    print "Step12_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        SDST_Var = {}
        SDST_Comb = []
        for m in ip_data.SETUP_MACHINES:
            len_seq = len(machines_jobs_sdst[m[0]])
            for p in range(0, len_seq):
                for q in range(0, len_seq):
                    i = int(Mac_Sequence_PO[(m[0], p)])
                    i1 = int(Mac_Sequence_PO[(m[0], q)])
                    # if i < i1:
                    #     SDST_Comb.append((m[0], i, i1))
                    #     SDST_Var[(m[0], i, i1)] = solver.IntVar(0, 10000000000, 'SDST_Var[(%i,%i,%i)]' % (m[0], i, i1))
                    #     b1 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr(), 0)
                    #     b2 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][p].EndExpr(), 0)
                    #     solver.Add(solver.Sum((b1, b2)) == 1)
                    #     solver.Add(SDST_Var[(m[0], i, i1)] == b1 * ( machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr()) +  \
                    #                b2 * (machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][p].EndExpr()))

                    if (m[0], i, i1) in ip_data.SETUP_TIME_NEW_COMB:
                        # if (m[0], i1, i) in SETUP_TIME_NEW_COMB_ALL:
                        # print   1, m[0], i, i1
                        solver.Add(solver.Sum((solver.IsGreaterOrEqualCstVar(
                            machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr(), \
                            ip_data.SDST[(m[0], i1, i)]), solver.IsGreaterOrEqualCstVar(
                            machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][p].EndExpr(),
                            ip_data.SDST[(m[0], i, i1)]))) == 1)
                        # else:
                        #     SDST[(m[0], i1, i)] = 0
                        #     solver.Add(solver.Sum((solver.IsGreaterOrEqualCstVar(machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr(), \
                        #         SDST[(m[0], i1, i)]), solver.IsGreaterOrEqualCstVar(machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][p].EndExpr(),SDST[(m[0], i, i1)]))) == 1)

                    elif (m[0], i, i1) in ip_data.SETUP_TIME_NEW_COMB2 or (m[0], i, i1) in ip_data.SETUP_TIME_NEW_COMB3:
                        # print 2, m[0], i, i1
                        solver.Add(solver.Sum((solver.IsGreaterOrEqualCstVar(
                            machines_jobs_sdst[m[0]][p].StartExpr() - machines_jobs_sdst[m[0]][q].EndExpr(), 0), \
                                               solver.IsGreaterOrEqualCstVar(
                                                   machines_jobs_sdst[m[0]][q].StartExpr() - machines_jobs_sdst[m[0]][
                                                       p].EndExpr(), ip_data.SDST[(m[0], i, i1)]))) == 1)

        currenttime = datetime.datetime.now()
        print "Step12_2_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        if ip_data.Setuptime_with_Holiday_Inbetween == "Enable":
            print " Setup b/w two jobs with a shutdown in between..."

            combo1 = []
            combo2 = []
            combo3 = []
            currenttime = datetime.datetime.now()
            print "Step12_3:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

            for m in ip_data.SETUP_MACHINES:
                total_length = len(machines_jobs_sdst_new[m[0]])
                len_act_jobs = Total_Actual_Jobs[m[0]]
                len_invalid_times = total_length - len_act_jobs
                ##      print m[0],total_length,len_act_jobs,len_invalid_times
                for p in xrange(len_act_jobs):
                    for q in xrange(len_act_jobs):
                        for r in xrange(len_act_jobs, total_length):
                            i = int(Mac_Sequence_PO[(m[0], p)])
                            i1 = int(Mac_Sequence_PO[(m[0], q)])
                            if (m[0], i, i1) in ip_data.SETUP_TIME_NEW_COMB:
                                combo1.append((m[0], i, i1, p, q, r))
                            elif (m[0], i1, i) in ip_data.SETUP_TIME_NEW_COMB:
                                combo2.append((m[0], i1, i, p, q, r))
                            elif (m[0], i, i1) in ip_data.SETUP_TIME_NEW_COMB2 or (m[0], i, i1) in ip_data.SETUP_TIME_NEW_COMB3:
                                combo3.append((m[0], i, i1, p, q, r))

            print "combo1:", len(combo1)
            print "combo2:", len(combo2)
            print "combo3:", len(combo3)

            currenttime = datetime.datetime.now()
            print "Step12_4:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

            if ip_data.SDST_Split == "Enable":
                print "SDST Split Allowed"
                for (m, i, i1, p, q, r) in combo1:
                    ##            print "p,q: ", m, p, q, r
                    b100 = solver.IsLessOrEqualCstVar(machines_jobs_sdst_new[m][p].EndExpr() - Start_Invalid[(m, r)], 0)
                    b200 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst_new[m][q].StartExpr() - End_Invalid[(m, r)], 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() \
                        - ip_data.SDST[(m, i, i1)] - Duration_Invalid[(m, r)], 0)
                    solver.Add(b101 >= b200 * b100)

                currenttime = datetime.datetime.now()
                print "Step12_5:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

                for (m, i, i1, p, q, r) in combo2:
                    ##            print "p,q: ", m, p, q, r
                    b100 = solver.IsLessOrEqualCstVar(machines_jobs_sdst_new[m][p].EndExpr() - Start_Invalid[(m, r)], 0)
                    b200 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst_new[m][q].StartExpr() - End_Invalid[(m, r)], 0)
                    b102 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() \
                        - ip_data.SDST[(m, i1, i)] - Duration_Invalid[(m, r)], 0)
                    solver.Add(b102 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_6:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
                for (m, i, i1, p, q, r) in combo3:
                    b100 = solver.IsLessOrEqualCstVar(machines_jobs_sdst_new[m][p].EndExpr() - Start_Invalid[(m, r)], 0)
                    b200 = solver.IsGreaterOrEqualCstVar(machines_jobs_sdst_new[m][q].StartExpr() - End_Invalid[(m, r)], 0)
                    b103 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() \
                        - ip_data.SDST[(m, i, i1)] - Duration_Invalid[(m, r)], 0)
                    solver.Add(b103 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_7:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            else:
                print "SDST Split Not Allowed"
                for (m, i, i1, p, q, r) in combo1:
                    b100 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b200 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr(), 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() - ip_data.SDST[
                            (m, i, i1)], 0)
                    b201 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr() - ip_data.SDST[
                            (m, i, i1)], 0)
                    solver.Add(b101 + b201 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_10:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
                for (m, i, i1, p, q, r) in combo2:
                    b100 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b200 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr(), 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr() - ip_data.SDST[
                            (m, i1, i)], 0)
                    b201 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr() - ip_data.SDST[
                            (m, i1, i)], 0)
                    solver.Add(b101 + b201 >= b200 * b100)
                currenttime = datetime.datetime.now()
                print "Step12_11:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
                for (m, i, i1, p, q, r) in combo3:
                    b100 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b200 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr(), 0)
                    b101 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][r].StartExpr() - machines_jobs_sdst_new[m][p].EndExpr(), 0)
                    b201 = solver.IsGreaterOrEqualCstVar(
                        machines_jobs_sdst_new[m][q].StartExpr() - machines_jobs_sdst_new[m][r].EndExpr() - ip_data.SDST[
                            (m, i, i1)], 0)
                    solver.Add(b101 + b201 >= b200 * b100)

    currenttime = datetime.datetime.now()
    print "Step13:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    print ' ------------------------ Initial Schedule ---------------------------------------------'
    ##  if int(Freeze_Time_Fence)>0:
    ##    print "In freeze"
    ##    for (i,j,ar,s,alt) in Initial_Schedule_Comb:
    ##        if i in current_pos:
    ##            print i,j,Task_Active_Status[(i,j,ar,s,alt)], Task_Begin_Time[(i,j,ar,s,alt)], Task_End_Time[(i,j,ar,s,alt)]
    ##          solver.Add(active_variables[(i,j,ar,s,alt)] == Task_Active_Status[(i,j,ar,s,alt)])
    ##          solver.Add(all_tasks[(i,j,ar,s,alt)].SafeStartExpr(-1).Var() == Task_Begin_Time[(i,j,ar,s,alt)])
    ##          solver.Add(all_tasks[(i,j,ar,s,alt)].SafeEndExpr(-1).Var() == Task_End_Time[(i,j,ar,s,alt)])

    print ' ----------------------------------------------- Precedences inside a job.------------------------------------------------'

    for (i, j, j1, ar, s, s1, alt, alt1) in ip_data.PRECEDENCE_CONST_COMB:
        if i in current_pos:
            t1 = all_tasks[(i, j, ar, s, alt)]
            t2 = all_tasks[(i, j1, ar, s1, alt1)]
            if j1 in ip_data.Form_QC_ID:
                solver.Add(t2.StartsAfterEnd(t1))
            else:
                solver.Add(t2.SafeStartExpr(9999999).Var() >= t1.SafeEndExpr(-1).Var() - ip_data.OP_Setup_Time[i, j1, ar, s1, alt1])
            if ip_data.Op_Type[j] == 3:
                solver.Add(t2.StartsAtEnd(t1))
            if ip_data.Op_Type[j1] == 3:
                solver.Add(t2.StartsAtEnd(t1))

    Next_Task_Gap = {}
    if ip_data.Subsequent_Op_with_allowed_gap == "Enable":
        for (po, ar, s) in ip_data.PO_AR_SEQ_COMB:
            if po in current_pos and s <= 1:
                Next_Task_Gap[(po, ar, s)] = solver.IntVar(0, 2 * 1440,
                                                           'Next_Task_Gap[(%i,%i,%i)]' % (int(po), int(ar), int(s)))
                solver.Add(abs(solver.Max(
                    [all_tasks[(i, j, ar1, s1, alt)].SafeEndExpr(-1).Var() for (i, j, j1, ar1, s1, s2, alt, alt1) in
                     ip_data.PRECEDENCE_CONST_COMB \
                     if i == po and ar == ar1 and alt != -1 and s == s1]) - \
                               solver.Max([all_tasks[(i, j1, ar1, s2, alt1)].SafeStartExpr(-1).Var() for
                                           (i, j, j1, ar1, s1, s2, alt, alt1) in ip_data.PRECEDENCE_CONST_COMB \
                                           if i == po and ar == ar1 and alt1 != -1 and s == s1])) == Next_Task_Gap[
                               (po, ar, s)])

    if ip_data.Parallel_Op_End_At_End == "Enable":
        for (i, j, j1, ar, s, alt, alt1) in ip_data.Parallel_Op_Gap:
            if i in current_pos:
                t1 = all_tasks[(i, j, ar, s, alt)]
                t2 = all_tasks[(i, j1, ar, s, alt1)]
                solver.Add(t2.EndsAtEnd(t1))

    if ip_data.Parallel_Op_Start_At_Start == "Enable":
        for (i, j, j1, ar, s, alt, alt1) in ip_data.Parallel_Op_Gap:
            if i in current_pos:
                t1 = all_tasks[(i, j, ar, s, alt)]
                t2 = all_tasks[(i, j1, ar, s, alt1)]
                solver.Add(t2.StartsAtStart(t1))

    Op_Gap = {}
    if ip_data.Parallel_Op_with_allowed_gap == "Enable":
        for (po, s) in ip_data.Parallel_Op_Gap_Comb:
            if po in current_pos:
                Op_Gap[(po, s)] = solver.IntVar(0, 2 * 1440, 'Op_Gap[(%i,%i)]' % (int(po), int(s)))
                solver.Add(abs(solver.Max(
                    [all_tasks[(i, j, ar, s1, alt)].SafeEndExpr(-1).Var() for (i, j, j1, ar, s1, alt, alt1) in
                     ip_data.Parallel_Op_Gap \
                     if i == po and alt != -1 and s == s1]) - \
                               solver.Max(
                                   [all_tasks[(i, j1, ar, s1, alt1)].SafeEndExpr(-1).Var() for (i, j, j1, ar, s1, alt, alt1)
                                    in ip_data.Parallel_Op_Gap \
                                    if i == po and alt1 != -1 and s == s1])) == Op_Gap[(po, s)])

    print " Gap logic not working, some flaw in the logic, need to check again"
    b61 = {}
    # for (i, j, j1, ar, s, alt, alt1) in Parallel_Op_Gap:
    #     t1 = all_tasks[(i, j, ar, s, alt)]
    #     t2 = all_tasks[(i, j1, ar, s, alt1)]
    #     b61[(i, j, j1, ar, s,  alt, alt1)] = solver.IntVar([0, 1], 'b61[(%i,%i,%i,%i,%i,%i,%i)]' % (int(i), int(j),int(j1),int(ar),int(s), int(alt), int(alt1)))
    #     b61[(i, j, j1, ar, s,  alt, alt1)] = solver.IsLessOrEqualCstVar(abs(t2.SafeEndExpr(0).Var() - t1.SafeEndExpr(0).Var()), 2 * 1440)
    #     b2 = solver.IsEqualCstVar(active_variables[(i, j, ar, s, alt)], 1)
    #     b3 = solver.IsEqualCstVar(active_variables[(i, j1, ar, s, alt1)], 1)
    #     solver.Add(b61[(i, j, j1, ar, s,  alt, alt1)] >= b2 * b3)         #active_variables[(i, j, ar, s, alt)] * active_variables[(i, j1, ar, s, alt1)])


    Shelf_Life_Gap = {}
    for (i, j, j1, ar, s, s1, alt, alt1) in ip_data.SHELF_LIFE_COMB:
        if i in current_pos and ip_data.Product_Shelf_Life[ip_data.Shelf_Life_Prod_Index[(i, j, j1, ar, s, s1, alt, alt1)]] > 0:
            t1 = all_tasks[(i, j, ar, s, alt)]
            t2 = all_tasks[(i, j1, ar, s1, alt1)]
            ##        print i, Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)],Product_Shelf_Life[Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)]]
            ##        Shelf_Life_Gap[(i,j,j1,ar,s,s1,alt,alt1)] = solver.IntVar(0, Product_Shelf_Life[Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)]], \
            ##                                                                  'Shelf_Life_Gap[(%i,%i,%i,%i,%i,%i,%i,%i)]' % (i,j,j1,ar,s,s1,alt,alt1))
            ##        b1 = solver.IsLessOrEqualCstVar(Shelf_Life_Gap[(i,j,j1,ar,s,s1,alt,alt1)] -(t2.SafeStartExpr(-1).Var() - t1.SafeEndExpr(-1).Var()), 0)
            b1 = solver.IsLessOrEqualCstVar(
                (t2.SafeStartExpr(-1).Var() - t1.SafeEndExpr(-1).Var()) - ip_data.Product_Shelf_Life[
                    ip_data.Shelf_Life_Prod_Index[(i, j, j1, ar, s, s1, alt, alt1)]], 0)
            b2 = solver.IsEqualCstVar(active_variables[(i, j, ar, s, alt)], 1)
            b3 = solver.IsEqualCstVar(active_variables[(i, j1, ar, s1, alt1)], 1)
            solver.Add(b1 >= b2 * b3)



            ##  print ' ----------------------------------------------- BOM Explosion Constraints  ------------------------------------------------'
    currenttime = datetime.datetime.now()
    print "Step14:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    global Input_Product_Qty
    global Inv_Product_Qty
    global Output_Product_Qty

    Input_Product_Qty = {}
    Inv_Product_Qty = {}
    Output_Product_Qty = {}

    Total_Qty = {}
    for p in ip_data.Products:
        Total_Qty[p] = 0
        for l in range(0, len(ip_data.BOH_T)):
            if int(ip_data.BOH_T[l][0]) == int(p):
                Total_Qty[p] = Total_Qty[p] + int(ip_data.BOH_T[l][2])

    print ' ----------------------------------------------- Domain Calculation  ------------------------------------------------'

    # global Dom_IP
    # global PO_OP_SFG_Comb
    # global Dom_OP
    # Dom_IP = {}
    # Dom_OP = {}
    # PO_OP_SFG_Comb = []
    # print "current_pos count: ", len(current_pos)
    # print "current_pos: ", current_pos
    # for (i1, ar1) in ip_data.PO_AR_COMB:
    #     if i1 in current_pos:
    #         for s in range(getLastSequence[i1, ar1], 0, -1):
    #             for (i2, j2, ar2, s2) in ip_data.PO_TASK_AR_SEQ_COMB:
    #                 # if i2 == 23 and i1==i2:
    #                 #     print "s2:",s2,i2,ar2
    #                 if i1 == i2 and s == s2 and ar1 == ar2:
    #                     for (i, j, ar, alt, ip, op) in ip_data.PO_BOM_COMB:
    #                         # if i == 23 and i==i2:
    #                         #     print i,j,ar,alt,ip,op
    #                         if i == i1 and ar == ar1 and j == j2:
    #                             # if i1 == 23:
    #                             #     print "s: ", i,j,ar,ip,op
    #                             if ip_data.Product_Type_ID[int(op)] == 3:
    #                                 Dom_IP[(i, j, ip)] = int(ip_data.Order_Quantity[i] * getAttachRate(op, ip, j))
    #                                 PO_OP_SFG_Comb.append((i, j, ip))
    #                                 Dom_OP[(i, j, op)] = int(ip_data.Order_Quantity[i])
    #                                 # if i1 == 23:
    #                                 #     print 1,s,j,ip,op, Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)]
    #                                 for (i3, j3, j4, ar3, s3, s4, p) in ip_data.MAT_BALANCE_CONST:
    #                                     if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
    #                                         if ip_data.Product_Type_ID[int(ip)] != 1:
    #                                             Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]
    #                             elif ip_data.Product_Type_ID[int(op)] == 2 and getLastSequence[i1, ar1] == s:
    #                                 Dom_IP[(i, j, ip)] = int(ip_data.Order_Quantity[i] * getAttachRate(op, ip, j))
    #                                 PO_OP_SFG_Comb.append((i, j, ip))
    #                                 Dom_OP[(i, j, op)] = int(ip_data.Order_Quantity[i])
    #                                 # if i1 == 23:
    #                                 #     print 1,s,j,ip,op, Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)]
    #                                 for (i3, j3, j4, ar3, s3, s4, p) in ip_data.MAT_BALANCE_CONST:
    #                                     if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
    #                                         if ip_data.Product_Type_ID[int(ip)] != 1:
    #                                             Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]
    #                             elif ip_data.Product_Type_ID[int(op)] == 2:
    #                                 # if i1 == 23:
    #                                 #     print 100, s,j,ar,ip,op
    #                                 if ip_data.Product_Type_ID[int(ip)] == 1:
    #                                     Dom_IP[(i, j, ip)] = Dom_OP[(i, j, op)] * getAttachRate(op, ip, j)
    #                                     PO_OP_SFG_Comb.append((i, j, ip))
    #                                     if Dom_IP[(i, j, ip)] == 0:
    #                                         ##                            print i,j,ip,op,Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
    #                                         Dom_IP[(i, j, ip)] = 1
    #                                 else:
    #                                     Dom_IP[(i, j, ip)] = int(Dom_OP[(i, j, op)] * getAttachRate(op, ip, j))
    #                                     PO_OP_SFG_Comb.append((i, j, ip))
    #                                     if Dom_IP[(i, j, ip)] == 0:
    #                                         ##                            print i,j,ip,op,Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
    #                                         Dom_IP[(i, j, ip)] = 1
    #                                         ##                      print 2,s,j,ip,op, Dom_IP[(i,j,ip)] , Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
    #                                 for (i3, j3, j4, ar3, s3, s4, p) in ip_data.MAT_BALANCE_CONST:
    #                                     if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
    #                                         if ip_data.Product_Type_ID[int(ip)] != 1:
    #                                             Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]

    # for (i,j,ar,alt,ip,op) in PO_BOM_COMB:
    #    if i in current_pos:
    #        print "Domain i,j,ip,op :", i,j,ip,op, Dom_IP[(i,j,ip)],Dom_OP[(i,j,op)]



    # SFG_FG_Domain = []
    #
    # for (i, j, ar, alt, ip, op) in ip_data.PO_BOM_COMB:
    #     if i in current_pos and ip_data.Product_Type_ID[int(ip)] == 2 and ip_data.Product_Type_ID[int(op)] == 3:
    #         SFG_FG_Domain.append((ip, op, Dom_IP[(i, j, ip)], Dom_OP[(i, j, op)], getAttachRate(op, ip, j)))
    #         # print "SFG_FG_Dom: ", i, ip, op,Dom_IP[(i,j,ip)],Dom_OP[(i,j,op)],getAttachRate(op,ip,j)
    # SFG_FG_Domain = list(set(SFG_FG_Domain))

    # print "SFG_FG_Domain: ", SFG_FG_Domain

    # for i in range(0, len(SFG_FG_Domain)):
    #     ip_data.SFG_FG_Domain_Comb.append((SFG_FG_Domain[i][0], SFG_FG_Domain[i][1]))
    #     ip_data.FG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = int(SFG_FG_Domain[i][3])
    #     ip_data.SFG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = int(SFG_FG_Domain[i][2])
    #     ip_data.AR[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][4]

    currenttime = datetime.datetime.now()
    print "Step14:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    Inv_Product_Qty_Flag = {}
    for (i, j, ar, alt, ip) in ip_data.PO_AR_BOM_IP_COMB:
        if i in current_pos:
            if ip_data.Product_Type_ID[int(ip)] != 1:
                Input_Product_Qty[(i, j, ar, alt, int(ip))] = solver.IntVar([0, int(Dom_IP[(i, j, ip)])],
                                                                            'Input_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                            int(i), int(j), int(ar), int(alt), int(ip)))
            else:
                Input_Product_Qty[(i, j, ar, alt, int(ip))] = solver.IntVar([0, int(Dom_IP[(i, j, ip)])],
                                                                            'Input_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                            int(i), int(j), int(ar), int(alt), int(ip)))
                Inv_Product_Qty_Flag[(i, j, ar, alt, int(ip))] = solver.BoolVar()

        # for (i1, j1, ar1, s1, alt1) in PO_TASK_AR_SEQ_ALT_COMB:
        #     if i==i1 and j==j1 and ar==ar1 and alt==alt1 and (i,ip) not in po_products_alt_bom_comb and Product_Type_ID[int(ip)] == 1:
        #         solver.Add(Input_Product_Qty[(i, j, ar, alt, int(ip))] ==  Inv_Product_Qty_Flag[(i, j, ar, alt, int(ip))]  * int(Dom_IP[(i,j,ip)]))
        for (i1, j1, ar1, s1, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
            if i in current_pos and i == i1 and j == j1 and ar == ar1 and alt == alt1 and ip not in ip_data.Alternate_BOM_Products and ip_data.Product_Type_ID[
                int(ip)] == 1:
                solver.Add(
                    Input_Product_Qty[(i, j, ar, alt, int(ip))] == Inv_Product_Qty_Flag[(i, j, ar, alt, int(ip))] * int(
                        Dom_IP[(i, j, ip)]))

    # horizon = 3000
    # b1 = {}
    # b2 = {}
    # for t in range(horizon):
    #     for (i, j, ar, s, alt) in PO_TASK_AR_SEQ_ALT_COMB:
    #         b1[(i, j, ar, s, alt,t)] = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var(), t)
    #         b2[t] = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), t)
    #
    # print "Reading Done"
    # for (i, j, ar, alt, ip) in PO_AR_BOM_IP_COMB:
    #   if i==0:
    #     for (po,po1) in  PO_Relation     :
    #         print "po,po1 :", po, po1
    #         if po==i and Sequence_ID[(i,j,ar)]==1:
    #             for t in range(horizon):
    #               solver.Add(
    #                   solver.Sum(b1[t1] * Dom_OP[(i1,j1,op)] for t1 in range(horizon)
    #                                                         for (i1, j1, ar1, alt1, op) in PO_AR_BOM_OP_COMB
    #                                                         if po1==i1 and Sequence_ID[(i1,j1,ar1)] == getLastSequence[i1, ar1] )
    #                           -  b2[t] * Dom_IP[(i,j,ip)] >= 0)



    currenttime = datetime.datetime.now()
    print "Step16:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, j, ar, alt, op) in ip_data.PO_AR_BOM_OP_COMB:
        if i in current_pos:
            Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntVar([0, int(Dom_OP[(i, j, op)])],
                                                                    'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                        int(i), int(j), int(ar), int(alt), int(op)))
            ##    print '--------------------------------------------- Batch Resource Capacity ----------------------------------------------------------'

            ##        if Op_Type[j]==2 and Max_Batch_Size[(i,j,alt)]>0:
            ##            solver.Add(Output_Product_Qty[(i,j,ar,alt,op)] <= Max_Batch_Size[(i,j,alt)])

            ##    print '--------------------------------------------- Batch Resource Capacity ----------------------------------------------------------'

    for (i, j, ar, alt, op) in ip_data.PO_AR_BOM_OP_COMB:
        if i in current_pos:
            if ip_data.Op_Type[j] != 2:
                Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntVar([0, int(Dom_OP[(i, j, op)])],
                                                                        'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                            int(i), int(j), int(ar), int(alt), int(op)))
            else:
                if Dom_OP[(i, j, op)] > ip_data.Max_Batch_Size[(i, j, alt)]:
                    ##                print "Dom > Batch: ",i,j,op,Dom_OP[(i,j,op)] , Max_Batch_Size[(i,j,alt)]
                    Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntConst(0,
                                                                              'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                                  int(i), int(j), int(ar), int(alt),
                                                                                  int(op)))
                else:
                    ##                print "Dom < Batch: ",i,j,op,Dom_OP[(i,j,op)] , Max_Batch_Size[(i,j,alt)]
                    Output_Product_Qty[(i, j, ar, alt, op)] = solver.IntVar([0, int(Dom_OP[(i, j, op)])],
                                                                            'Output_Product_Qty[(%i,%i,%i,%i,%i)]' % (
                                                                                int(i), int(j), int(ar), int(alt), int(op)))

    print '--------------------------------------------- Alternate BOM ----------------------------------------------------------'

    b91 = {}
    b92 = {}
    if len(ip_data.Alternate_BOM) > 0:
        for (i, j, op) in ip_data.Alternate_BOM_COMB:
            if i in current_pos:
                for (i1, j1, ar1, alt1, ip, op1) in ip_data.Alt_BOM_Comb:
                    if i == i1 and j == j1 and op == op1:
                        b91[(i1, j1, ar1, alt1, ip, op1)] = solver.IntVar([0, 1], 'b91[(%i,%i,%i,%i,%i,%i)]' % (
                        int(i1), int(j1), int(ar1), int(alt1), int(ip), int(op1)))
                        b92[(i1, j1, ar1, alt1, ip, op1)] = solver.IntVar([0, 1], 'b92[(%i,%i,%i,%i,%i,%i)]' % (
                        int(i1), int(j1), int(ar1), int(alt1), int(ip), int(op1)))
                        for (i2, j2, ar2, s2) in ip_data.PO_TASK_AR_SEQ_COMB:
                            if i2 == i and j2 == j and ar1 == ar2:
                                # print i,j,ar,ip,op1
                                solver.Add(b91[(i1, j1, ar1, alt1, ip, op1)] == solver.IsGreaterCstVar(
                                    Input_Product_Qty[(i1, j1, ar1, alt1, ip)], 0))
                                solver.Add(b92[(i1, j1, ar1, alt1, ip, op1)] == solver.IsEqualCstVar(
                                    active_variables[(i2, j2, ar2, s2, alt1)], 1))
                for (i10, st_val) in ip_data.Alt_BOM_Start_Value:
                    if i10 == i:
                        solver.Add(solver.Sum(b91[(i1, j1, ar1, alt1, ip, op1)] * b92[(i1, j1, ar1, alt1, ip, op1)] for
                                              (i1, j1, ar1, alt1, ip, op1) in ip_data.Alt_BOM_Comb \
                                              if 0 <= ip_data.Alt_BOM_Flag[(
                        i1, j1, ar1, alt1, ip, op1)] - st_val < 10 and i == i1 and j == j1 and op == op1) == 1)
                        # print solver.Sum(b91[(i1, j1, ar1, alt1, ip, op1)] * b92[(i1, j1, ar1, alt1, ip, op1)] for (i1, j1, ar1, alt1, ip, op1) in Alt_BOM_Comb if 0 <= Alt_BOM_Flag[(i1, j1, ar1, alt1, ip, op1)] - st_val < 10 and i == i1 and j == j1 and op == op1) == 1

    ##  print '--------------------------------------------- Max RM Available ----------------------------------------------------------'
    global maxrmavailable1
    global maxrmavailable
    maxrmavailable1 = {}
    maxrmavailable = {}
    for p in ip_data.Products:
        if ip_data.Product_Type_ID[p] == 1 and p in ip_data.BOH_Total_Comb:
            maxrmavailable1[p] = max(
                ip_data.BOH[p1, t] for (p1, t) in ip_data.BOH_Comb if int(p1) == int(p) and int(t) < int(ip_data.PlanningHorizon))
            ##      print 1,p,PlanningHorizon, maxrmavailable1[p]

    for p in ip_data.Products:
        if ip_data.Product_Type_ID[p] == 1 and p in ip_data.BOH_Total_Comb:
            maxrmavailable[p] = max(ip_data.BOH[p1, t] for (p1, t) in ip_data.BOH_Comb if
                                    int(p1) == int(p) and int(t) < int(ip_data.PlanningHorizon) and (
                                        int(ip_data.BOH[p1, t]) < int(maxrmavailable1[p]) or [maxrmavailable1[p]]))
            ##      print p,maxrmavailable1[p], maxrmavailable[p]



    if ip_data.Task_Split_Allowed == "Enable":
        print " ------------------------------------- Task Splitting Implementation ------------------------------------------------------ "
        Invalid_Timings3 = []
        Invalid_Timings4 = []
        Invalid_Timings3 = sorted(Invalid_Timings2, key=lambda x: (x[0], x[1], x[2]))
        for i in range(0,len(Invalid_Timings3)):
            Invalid_Timings4.append((Invalid_Timings3[i][0],Invalid_Timings3[i][1],Invalid_Timings3[i][2]))
        # print "Invalid_Timings3:", Invalid_Timings3
        # print "Invalid_Timings4:", Invalid_Timings4
        currenttime = datetime.datetime.now()
        print "Step19_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        Duration1 = Duration_Pre_Compute_Task_Split(Invalid_Timings4)
        currenttime = datetime.datetime.now()
        print "Step19_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        # print "Old len: ", len(Duration1)
        # print Duration1

        currenttime = datetime.datetime.now()
        print "Step20_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        same_dur_list={}
        PROD_MAC_DUR_COMB=[]
        for (p1, mac1, t, dur1) in Duration1:
            PROD_MAC_DUR_COMB.append((p1, mac1,dur1))
        PROD_MAC_DUR_COMB=list(set(PROD_MAC_DUR_COMB))

        for (p, mac, dur) in PROD_MAC_DUR_COMB:
            same_dur_list[(p,mac,dur)] = [(t) for (p1, mac1, t, dur1) in Duration1 if p1==p and mac==mac1 and dur==dur1]
            # print p,mac,dur, len(same_dur_list[(p,mac,dur)])

    b11={}
    if ip_data.Task_Split_Allowed == "Enable" and len(ip_data.Task_Split_Allowed_Machines) > 0 and len(Invalid_Timings4)>0:
        for (i, j, ar, s, alt, m) in ip_data.PO_TASK_AR_SEQ_ALT_TASK_SPLIT_COMB:
            if m in ip_data.Task_Split_Allowed_Machines:
                for (p, mac, dur) in same_dur_list:
                    # print int(p),getProductID(i) , int(mac),ip_data.Machine_ID[(i, j, ar, s, alt)]
                    if int(mac) == int(m) and int(p) == getProductID(i):
                        # print p,mac,t
                        # if dur != -1:
                            b11[(i, j, ar, s, alt)] = solver.IntVar([0, 1], 'b11[(%i,%i,%i,%i,%i)]' % (int(i), int(j), int(ar),int(s), int(alt)))
                            solver.Add(b11[(i, j, ar, s, alt)] == solver.IsMemberVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), same_dur_list[(p,mac,dur)]))
                            # b11 = solver.IsMemberVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), same_dur_list[(p,mac,dur)])
                            # b13 = solver.IsEqualCstVar(active_variables[(i, j, ar, s, alt)],1)
                            b12 = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var(), int(dur))
                            solver.Add(b12 >= b11[(i, j, ar, s, alt)] * active_variables[(i, j, ar, s, alt)])
                            # solver.Add(b12 >= b11  * b13)
                            # print b11



    print '--------------------------------------------- Interval Duration ----------------------------------------------------------'

    currenttime = datetime.datetime.now()
    print "Step19:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    Mult_Ops = []
    for (i, j, ar, s) in ip_data.PO_TASK_AR_SEQ_COMB:
        if i in current_pos:
            for (i1, j1, ar1, alt, op) in ip_data.PO_AR_BOM_OP_COMB:
                if i1 == i and j == j1 and ar == ar1 and (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:# and ip_data.Machine_ID[(i, j, ar, s, alt)] not in ip_data.Task_Split_Allowed_Machines:
                    ## to avoid output qty var and active var taking diferent values for alternate tasks
                    # print 1000000000000000000000000000000000000000000000000000,ip_data.Machine_ID[(i, j, ar, s, alt)]
                    solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() <= active_variables[(i, j, ar, s, alt)] * 1000000000000)
                    if ip_data.Op_Type[j] == 1:
                        ##          print "Discrete"
                        if (i, j) in Mult_Ops:
                            ##                print "2OP Domain: ", i,j, Two_Output_Dom_OP[(i,j)]
                            solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var()
                                       >= active_variables[(i, j, ar, s, alt)] * (ip_data.OP_Setup_Time[i, j, ar, s, alt] + ip_data.Teardown_Time[i, j, ar, s, alt]
                                           + int(ip_data.Two_Output_Dom_OP[(i, j)] * ip_data.Processing_Time[i, j, ar, s, alt, int(ip_data.Two_Output_Dom_OP[(i, j)])])))
                        elif int(Dom_OP[(i, j, op)]) == int(ip_data.Order_Quantity[i]):
                            if  ip_data.Machine_ID[(i, j, ar, s, alt)] in ip_data.Task_Split_Allowed_Machines:
                                solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var()
                                       >= active_variables[(i, j, ar, s, alt)] * (ip_data.OP_Setup_Time[i, j, ar, s, alt] + ip_data.Teardown_Time[i, j, ar, s, alt]
                                           + int(Dom_OP[(i, j, op)] * ip_data.Processing_Time[i, j, ar, s, alt])))
                            else:
                                solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var()
                                           == active_variables[(i, j, ar, s, alt)] * (ip_data.OP_Setup_Time[i, j, ar, s, alt] + ip_data.Teardown_Time[i, j, ar, s, alt]
                                           + int(Dom_OP[(i, j, op)] * ip_data.Processing_Time[i, j, ar, s, alt])))
                    elif ip_data.Op_Type[j] == 2 and int(Dom_OP[(i, j, op)]) == int(ip_data.Order_Quantity[i]):
                        if ip_data.Machine_ID[(i, j, ar, s, alt)] in ip_data.Task_Split_Allowed_Machines:
                            solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var()
                                   >= active_variables[(i, j, ar, s, alt)] * (ip_data.OP_Setup_Time[i, j, ar, s, alt] + int(ip_data.Processing_Time[i, j, ar, s, alt]) + ip_data.Teardown_Time[i, j, ar, s, alt]))
                        else:
                            solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() == active_variables[(i, j, ar, s, alt)] * (ip_data.OP_Setup_Time[i, j, ar, s, alt] +
                                            int(ip_data.Processing_Time[i, j, ar, s, alt]) + ip_data.Teardown_Time[i, j, ar, s, alt]))
                    elif ip_data.Op_Type[j] == 3 and int(Dom_OP[(i, j, op)]) == int(ip_data.Order_Quantity[i]):
                        for (i2, j2, alt2) in ip_data.Prod_Res_Holding_Time_Comb:
                            if i2 == i and j == j2 and alt == alt2:
                               # print "Hold:", i2, j2, alt2
                                solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() >= active_variables[(i, j, ar, s, alt)] * ip_data.Min_Holding_Time[i, j, alt])
                                solver.Add(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() <= active_variables[(i, j, ar, s, alt)] * ip_data.Max_Holding_Time[i, j, alt])

                    if s == getLastSequence[i, ar]:
                        solver.Add(Output_Product_Qty[(i1, j1, ar, alt, int(op))] == active_variables[(i, j, ar, s, alt)] * int(ip_data.Order_Quantity[i]))
                    else:
                        solver.Add(Output_Product_Qty[(i1, j1, ar, alt, int(op))] >= active_variables[(i, j, ar, s, alt)] * Dom_OP[(i, j, op)])

    currenttime = datetime.datetime.now()
    print "Step19_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    print '--------------------------------------------- INVENTORY EQUATIONS ----------------------------------------------------------'

    InvQty = {}

    if ip_data.RMConstraint == 'Constrained':
        print "------------------------------ RMConstraint ------------------------------------------------------------"
        b41 = {}
        b51 = {}
        for (i, j, ar, s, alt, ip) in ip_data.PO_TASK_AR_SEQ_ALT_IP_COMB:
            if i in current_pos:
                for index, (p1, t1) in enumerate(ip_data.BOH_Comb, start=0):
                    if int(p1) == int(ip) and index < len(ip_data.BOH_Comb) - 1 and int(ip_data.BOH_Comb[index + 1][0]) == int(p1) and t1 < \
                            ip_data.BOH_Comb[index + 1][1]:
                        t2 = ip_data.BOH_Comb[index + 1][1]
                        if ip_data.Product_Type_ID[int(ip)] == 1:
                            b41[(i, j, ar, s, alt, ip, t1)] = solver.IntVar(0, 1, 'b41[(%i,%i,%i,%i,%i,%i,%i)]' % (
                            int(i), int(j), int(ar), int(s), int(alt), int(ip), t1))
                            b51[(i, j, ar, s, alt, ip, t1)] = solver.IntVar(0, 1, 'b51[(%i,%i,%i,%i,%i,%i,%i)]' % (
                            int(i), int(j), int(ar), int(s), int(alt), int(ip), t1))
                            solver.Add(b41[(i, j, ar, s, alt, ip, t1)] == solver.IsGreaterOrEqualCstVar(
                                all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), t1))
                            solver.Add(b51[(i, j, ar, s, alt, ip, t1)] == solver.IsLessCstVar(
                                all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), t2))

        # for (i, j, ar, s, alt, ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
        #     if i in current_pos and Product_Type_ID[int(ip)] == 1:
        #         solver.Add(solver.Sum(b41[(i, j, ar, s, alt, ip, t1)]  \
        #                               for index, (p1, t1) in enumerate(BOH_Comb, start=0) if int(p1) == int(ip)  \
        #                               and index < len(BOH_Comb) - 1 and int(BOH_Comb[index + 1][0]) == int(p1) and t1 < BOH_Comb[index + 1][1]) <=1)




        m = []
        Total_RM_Requirement2 = []
        for index, (p1, t1) in enumerate(ip_data.BOH_Comb, start=0):
            # print p1,t1
            if p1 not in ip_data.Alternate_BOM_Products:
                if index < len(ip_data.BOH_Comb) - 1 and int(ip_data.BOH_Comb[index + 1][0]) == int(p1) and t1 < ip_data.BOH_Comb[index + 1][1]:
                    # print p1, t1, BOH[(p1,t1)]
                    t2 = ip_data.BOH_Comb[index + 1][1]
                    if ip_data.Product_Type_ID[int(p1)] == 1:
                        m.append(int(index))
                        solver.Add(solver.Sum(int(Dom_IP[(i, j, ip)]) * active_variables[(i, j, ar, s, alt)] * \
                                              solver.Sum(b41[(i, j, ar, s, alt, ip, ip_data.BOH_Comb[t][1])] * b51[
                                                  (i, j, ar, s, alt, ip, ip_data.BOH_Comb[t][1])] \
                                                         for t in m if t < len(ip_data.BOH_Comb) and int(ip_data.BOH_Comb[t][0]) == int(p1)) \
                                              for (i3, j3, ar3, alt3, ip3) in ip_data.PO_AR_BOM_IP_COMB if ip3 == p1 for
                                              (i, j, ar, s, alt, ip) in ip_data.PO_TASK_AR_SEQ_ALT_IP_COMB \
                                              if
                                              i in current_pos and i3 == i and j == j3 and ar == ar3 and alt3 == alt and p1 == ip and alt3 != -1) <=
                                   ip_data.BOH[(p1, t1)])

        m1 = []
        for index, (p1, t1) in enumerate(ip_data.BOH_Comb, start=0):
            if p1 in ip_data.Alternate_BOM_Products:
                # if p1==209:
                #     print "Alternate_BOM_Products for 209 : ", p1,t1, BOH[(p1, t1)]
                if index < len(ip_data.BOH_Comb) - 1 and int(ip_data.BOH_Comb[index + 1][0]) == int(p1) and t1 < ip_data.BOH_Comb[index + 1][1]:
                    t2 = ip_data.BOH_Comb[index + 1][1]
                    if ip_data.Product_Type_ID[int(p1)] == 1:
                        m1.append(int(index))
                        solver.Add(solver.Sum(Input_Product_Qty[(i, j, ar, alt, ip)] * solver.IsGreaterCstVar(
                            Input_Product_Qty[(i, j, ar, alt, ip)], 0) * \
                                              solver.Sum(b41[(i, j, ar, s, alt, ip, ip_data.BOH_Comb[t][1])] * b51[
                                                  (i, j, ar, s, alt, ip, ip_data.BOH_Comb[t][1])] \
                                                         for t in m1 if
                                                         t < len(ip_data.BOH_Comb) and int(ip_data.BOH_Comb[t][0]) == int(p1)) \
                                              for (i3, j3, ar3, alt3, ip3) in ip_data.PO_AR_BOM_IP_COMB if ip3 == p1 for
                                              (i, j, ar, s, alt, ip) in ip_data.PO_TASK_AR_SEQ_ALT_IP_COMB \
                                              if
                                              i in current_pos and i3 == i and j == j3 and ar == ar3 and alt3 == alt and ip3 == ip) <=
                                   ip_data.BOH[(p1, t1)])
                        # if p1 == 209:
                        #     print p1, t1, BOH[(p1, t1)]

    # In case of high priority iteration, SFG Qty data is assigned to FG lot and zeroed out.
    #  So for all pos iteration, we need to recalculate the assignment to create the valid po comb.
    for i in range(0, len(ip_data.SFG_Lots)):
        ip_data.SFG_Order_Qty[ip_data.SFG_Lots[i][0]] = int(ip_data.SFG_Lots[i][2])

    ip_data.SFG_FG_Domain_Comb = list(set(ip_data.SFG_FG_Domain_Comb))
    # valid_po_comb = []
    # valid_po_comb_truncated_sfg = []
    # fg_of_sfglotscompleted = []
    # sfglotscompleted = []
    # fg_lots_with_no_sfg_lots=[]

    # valid_po_comb,fg_lots_with_no_sfg_lots, fg_of_sfglotscompleted ,valid_po_comb_truncated_sfg = valid_po_comb_calculation()

    # fg_lots_with_no_sfg_lots = []  # Lots which doesn't have relevant SFG lots
    # for (i, p, q) in ip_data.FG_Lots:  # production_order_index,pm.product_index, order_quantity
    #     if i in current_pos:
    #         temp = 0  # to find FG lots which doesn't have enough SFG lots
    #         for (m, n) in ip_data.SFG_FG_Domain_Comb:  # ip, op  or sfg,fg
    #             if n == p:
    #                 FG_bal = 0
    #                 for (i1, p1, q1) in ip_data.SFG_Lots:  # production_order_index,pm.product_index,order_quantity
    #                     if i1 in current_pos and m == p1:  # and (i1,n) in SFG_PO_FG_Lot_Relation:
    #                         # print "i1, ip", i1, p1
    #                         if FG_bal == 0:
    #                             fg_current = round(ip_data.FG_Order_Qty[i] *ip_data.AR[(p1, p)])
    #                             FG_bal = 99999999
    #                             # print i, fg_current, SFG_Order_Qty
    #                         # print i,i1,p,p1, (i, i1) in PO_Relation , (p1,p) in SFG_FG_Domain_Comb
    #                         if (i, i1) in ip_data.PO_Relation:
    #                             if fg_current >= ip_data.SFG_Order_Qty[i1] and ip_data.SFG_Order_Qty[i1] > 0:
    #                                 fg_current = fg_current - ip_data.SFG_Order_Qty[i1]
    #                                 ip_data.SFG_Order_Qty[i1] = 0
    #                                 valid_po_comb.append((i, i1))
    #                                 temp = 1
    #                             elif fg_current > 0 and fg_current < ip_data.SFG_Order_Qty[i1]:
    #                                 ip_data.SFG_Order_Qty[i1] = ip_data.SFG_Order_Qty[i1] - fg_current
    #                                 fg_current = 0
    #                                 valid_po_comb.append((i, i1))
    #                                 temp = 2
    #         if temp == 0:
    #             fg_lots_with_no_sfg_lots.append(i)
    print "valid_po_comb length: ", len(valid_po_comb)
    print "valid_po_comb: ", valid_po_comb
    print "Truncated Valid PO Comb: ", valid_po_comb_truncated_sfg   #removed from valid po comb as the SFG lot is completed as per lot completion status, only FG needs to be planned
    print "fg_lots_with_no_sfg_lots: ", fg_lots_with_no_sfg_lots # if some fg lots didnt get sfg s with valid_po_comb logic, then it will be pushed to this, model will fail.
    print "FG lots for which of SFG Lots Completed: ", fg_of_sfglotscompleted  #fg FOR WHICH sfg LOTS ARE COMPLETED as per Completion Status
    # for (fg,sfg) in valid_po_comb_truncated_sfg:
    #     current_pos.remove(sfg)
        # print "Deleted sfg lot with lot number ", sfg

    if len(valid_po_comb)==0 and len(valid_po_comb_truncated_sfg)==0:
        f1 = open(ip_data.LogFile, 'a')
        f1.write(" ********* No valid Tablet and Blend combinations are available to schedule. Please check production lots data ************ ")

    if len(fg_lots_with_no_sfg_lots)>0:
        f1 = open(ip_data.LogFile, 'a')
        f1.write(" ********* Some of the Tablet Codes doesn't have Blend lots created. Please check production lots data ************ ")

    Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1 = []
    Phase_loop_PO_TASK_AR_SEQ_ALT_COMB = []
    Phase_pos_Seq1 = []
    Phase_pos = []

    for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
        if i in fg_of_sfglotscompleted:
            # print "SFG Lots already completed: ", i
            Phase_loop_PO_TASK_AR_SEQ_ALT_COMB.append((i, j, ar, s, alt))
            Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1.append((i, j, ar, s, alt))
            Phase_pos_Seq1.append(i)
            Phase_pos.append(i)


    for (po, po1) in valid_po_comb:
        for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
            if i == po and (i, j, ar, s, alt) not in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB:
                Phase_loop_PO_TASK_AR_SEQ_ALT_COMB.append((i, j, ar, s, alt))
                Phase_pos.append(i)
                if s == 1:
                    Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1.append((i, j, ar, s, alt))
                    Phase_pos_Seq1.append(i)
            if i == po1 and (i, j, ar, s, alt) not in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB:
                Phase_loop_PO_TASK_AR_SEQ_ALT_COMB.append((i, j, ar, s, alt))
                Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1.append((i, j, ar, s, alt))
                Phase_pos_Seq1.append(i)
                Phase_pos.append(i)


    print "Phase_loop_PO_TASK_AR_SEQ_ALT_COMB: ", Phase_loop_PO_TASK_AR_SEQ_ALT_COMB
    print "Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1: ", Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1
    print "current pos:",    current_pos
    if len(current_pos)==0:
        f1 = open(ip_data.LogFile, 'a')
        f1.write(" ********* No valid production lots found. Please check production lots data ************ ")
        f1.write("\n")
        cnx2 = mysql.connector.connect(user='root', password='saddlepoint',  host=mp.Host_String,database=mp.DB_String)
        cursor2 = cnx2.cursor()
        cursor2.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="No feasible Solution found", END_TIME=NOW() where JOB_ID = ' + str(mp.JOB_ID))
        cursor2.close()
        cnx2.commit()
        cnx2.close()
        exit()

    currenttime = datetime.datetime.now()
    print "Step20:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    global all_ends
    global all_starts
    all_starts = {}
    all_ends = {}

    for i in current_pos:
        all_ends[i] = solver.Max(
            [all_tasks[(i1, j, ar, s, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar, s, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
             i1 == i and alt1 != -1])
        all_starts[i] = solver.Min(
            [all_tasks[(i1, j, ar, s, alt1)].SafeStartExpr(999999999).Var() for (i1, j, ar, s, alt1) in
             ip_data.PO_TASK_AR_SEQ_ALT_COMB if i1 == i and alt1 != -1])

    print " ---- Relation between SFG and FG lots timings -------- "
    print "------ FG lot should start only after all the required SFG lots are finished ------------ "
    for i in ip_data.all_pos:
        if i in current_pos and ip_data.Product_Type_ID[int(getProductID(i))] == 3:
            solver.Add(solver.Min([all_tasks[(i1, j, ar, s, alt1)].SafeStartExpr(999999999).Var() + ip_data.OP_Setup_Time[
                i1, j, ar, s, alt1] * active_variables[(i1, j, ar, s, alt1)] for (po, po1) in set(valid_po_comb) if i == po \
                                   for (i1, j, ar, s, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                                   if i1 == po and alt1 != -1]) >= \
                       solver.Max([all_tasks[(i1, j, ar, s, alt1)].SafeEndExpr(-1).Var() \
                                   for (po, po1) in set(valid_po_comb) if i == po \
                                   for (i1, j, ar, s, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                                   if i1 == po1 and i1 in current_pos and alt1 != -1]))

    print " ------- Only if FG lot is produced, then SFG lot should be produced ----------- "
    if ip_data.Produce_SFG_Lot_on_FG_Lot_production == "Enable":
        for (fg_po, sfg_po) in set(valid_po_comb):
            if fg_po in current_pos and sfg_po in current_pos:
                b81 = solver.IsGreaterOrEqualCstVar(solver.Min([all_tasks[(i, j, ar, s, alt)].SafeStartExpr(999999999).Var() \
                                                                for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
                                                                i == fg_po and s == 1 and alt != -1]),
                                                    int(ip_data.PlanningHorizon) + 1)  # FG
                b82 = solver.IsGreaterOrEqualCstVar(solver.Min([all_tasks[(i, j, ar, s, alt)].SafeStartExpr(999999999).Var() \
                                                                for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
                                                                i == sfg_po and s == 1 and alt != -1]),
                                                    int(ip_data.PlanningHorizon) + 1)  # SFG
                b83 = solver.IsLessOrEqualCstVar(all_ends[fg_po], int(ip_data.PlanningHorizon))  # FG
                b84 = solver.IsLessOrEqualCstVar(all_ends[sfg_po], int(ip_data.PlanningHorizon))  # SFG
                solver.Add(b81 == b82)
                solver.Add(b83 == b84)

    print " --------- For each lot, if one operation is pushed out of Planning Horizon, all operations will be pushed out ------ "
    for i in current_pos:
        b71 = solver.IsGreaterOrEqualCstVar(all_starts[i], int(ip_data.PlanningHorizon) + 1)
        b73 = solver.IsGreaterOrEqualCstVar(all_ends[i], int(ip_data.PlanningHorizon) + 1)
        solver.Add(b73 == b71)

    currenttime = datetime.datetime.now()
    print "Step20_1:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

                            # print p, mac, b12 >= b11
                        # else:
                        #     solver.Add(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var() != t)

    # if ip_data.Task_Split_Allowed == "Enable" and len(ip_data.Task_Split_Allowed_Machines) > 0 and len(Invalid_Timings3)>0:
    #     for (i, j, ar, s, alt, m) in ip_data.PO_TASK_AR_SEQ_ALT_TASK_SPLIT_COMB:
    #         if m in ip_data.Task_Split_Allowed_Machines:
    #             for (p, mac, t, dur) in Duration1:
    #                 # print int(p),getProductID(i) , int(mac),ip_data.Machine_ID[(i, j, ar, s, alt)]
    #                 if int(mac) == int(m) and int(p) == getProductID(i):
    #                     # print p,mac,t
    #                     if dur != -1:
    #                         b11 = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(0).Var(), t)
    #                         b12 = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var(), int(dur))
    #                         solver.Add(b12 >= b11)
    #                     else:
    #                         solver.Add(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var() != t)



    # if ip_data.Task_Split_Allowed == "Enable" and len(ip_data.Task_Split_Allowed_Machines)>0:
    #         for (i, j, ar, s, alt, t, dur) in Duration1:
    #             if dur!=-1:
    #                 b11 = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(0).Var(), t)
    #                 b12 = solver.IsEqualCstVar(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var(), int(dur))
    #                 solver.Add(b12 >= b11)
    #                 # print b12>=b11
    #             else:
    #                 # b21 = solver.IsDifferentCstVar(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var(), t)
    #                 solver.Add(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var()!=t)
    #                 # b22 = solver.IsDifferentCstVar(all_tasks[(i, j, ar, s, alt)].DurationExpr().Var(), int(dur))
    #                 # solver.Add(b22 >= b21)
    #                 # solver.Add(b21==1)

    # all_tasks[(i, j, ar, s, alt)].DurationExpr().Var() ==   (all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var() == t) * int(dur)
    # Constraint * ct_i = solver.MakeEquality(solver.MakeElement(variables, variables[i]), dur);
    currenttime = datetime.datetime.now()
    print "Step20_3:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    MaxDueDate = max(getOrderDueDate(i) for i in current_pos)
    # for i in current_pos:
    #     print 100, i, getOrderQuantity(i)
    # totalOrderQty = sum(getOrderQuantity(i) for i in current_pos)

    MaxOrderDelay = int(ip_data.LDHorizon)

    all_ends_task = {}
    all_starts_task = {}
    all_ends_operation = {}

    DV = {}
    ##  makespan_po={}


    makespan = solver.Max(
        [all_tasks[(i1, j1, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j1, ar1, s1, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB
         if i1 in current_pos])

    # for (po1,po2) in PO_Relation:
    #   print "po1,po2: ", po1,po2
    #  solver.Add(solver.Min([all_tasks[(i1, j, ar, s, alt1)].SafeStartExpr(999999999).Var() for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
    #               if i1 == po1 and alt1 <> -1]) >= \
    #           solver.Max([all_tasks[(i1, j, ar, s, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar, s, alt1) in PO_TASK_AR_SEQ_ALT_COMB \
    #             if i1 == po2 and alt1 <> -1]))



    currenttime = datetime.datetime.now()
    print "Step21:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, ar, s) in ip_data.PO_AR_SEQ_COMB:
        if i in current_pos:
            all_ends_task[(i, ar, s)] = solver.Max(
                [all_tasks[(i1, j, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar1, s1, alt1) in
                 ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                 if i1 == i and ar1 == ar and s1 == s and alt1 <> -1])

    currenttime = datetime.datetime.now()
    print "Step21:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for (i, ar, s) in ip_data.PO_AR_SEQ_COMB:
        if i in current_pos:
            all_starts_task[(i, ar, s)] = solver.Min(
                [all_tasks[(i1, j, ar1, s1, alt1)].SafeStartExpr(99999999999).Var() for (i1, j, ar1, s1, alt1) in
                 ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                 if i1 == i and ar1 == ar and s1 == s and alt1 != -1])

    currenttime = datetime.datetime.now()
    print "Step22:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    for o in ip_data.all_operations:
        all_ends_operation[o] = solver.Max(
            [all_tasks[(i1, j, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j, ar1, s1, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB \
             if i1 in current_pos and int(j) == int(o[0])])
    currenttime = datetime.datetime.now()
    print "Step23:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    Order_Delay = {}
    Order_Excess = {}
    M = 10000000000
    for i in current_pos:
        Order_Delay[i] = solver.IntVar(0, int(ip_data.LDHorizon) + int(M))
        Order_Excess[i] = solver.IntVar(0, int(MaxDueDate) + int(M))
        solver.Add(all_ends[i] + Order_Excess[i] == Order_Delay[i] + getOrderDueDate(i))

    ##  print ' ----------------------------------------------- Objective(Make Span) ------------------------------------------------'


    obj_var = 0
    PO_Max = max(current_pos)
    print "current_pos: ", current_pos
    print "PO max Number: ", PO_Max
    if all_pos_iteration == 0:
        print "High Priority Iteration"
        obj_var = solver.Sum(Order_Delay[i] * (PO_Max + 1 - i) for i in current_pos)
    else:
        print "All Priority Iteration"
        obj_var = solver.Sum(Order_Delay[i] * (PO_Max + 1 - i) for i in current_pos).Var() + \
                  solver.Sum(all_ends_task[(i, ar, s)] for (i, ar, s) in ip_data.PO_AR_SEQ_COMB if i in current_pos).Var()
        # obj_var = solver.Sum(all_ends_task[(i, ar, s)] for (i, ar, s) in PO_AR_SEQ_COMB if i in current_pos).Var()

    objective = solver.Minimize(obj_var, 1)

    if first_solve == 0:
        if iter_solve == 1:
            solver.Add(solver.Max([all_tasks[(i1, j1, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j1, ar1, s1, alt1) in
                                   ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                                   if i1 in current_pos]) <= makespan_value)
        else:
            solver.Add(obj_var < obj_value)
            solver.Add(solver.Max([all_tasks[(i1, j1, ar1, s1, alt1)].SafeEndExpr(-1).Var() for (i1, j1, ar1, s1, alt1) in
                                   ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                                   if i1 in current_pos]) <= makespan_value)

            ##  print ' ----------------------------------------------- Search Phases ------------------------------------------------'
    vars_phase = solver.Phase([obj_var],
                              solver.CHOOSE_FIRST_UNBOUND,
                              solver.ASSIGN_MIN_VALUE)

    sequence_phase = solver.Phase([all_sequences[i[0]] for i in ip_data.all_machines if i[0] in non_overlapping_machines],
                                  solver.SEQUENCE_DEFAULT)

    if ip_data.SchedulingType == "Forward Scheduling":
        intervalPhase_default = solver.Phase(
            [all_tasks[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
             i in current_pos],
            solver.INTERVAL_SET_TIMES_FORWARD)
        intervalPhase = solver.Phase(
            [all_tasks[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1 if
             i in current_pos],
            solver.INTERVAL_SET_TIMES_FORWARD)
        intervalPhase2 = solver.Phase(
            [all_tasks[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB if
             i in current_pos and (i, j, ar, s, alt) not in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1],
            solver.INTERVAL_SET_TIMES_FORWARD)
    else:
        intervalPhase = solver.Phase(
            [all_tasks[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if i in current_pos],
            solver.INTERVAL_SET_TIMES_BACKWARD)

    active_phase = solver.Phase(
        [active_variables[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1 if
         i in current_pos],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    active_phase2 = solver.Phase(
        [active_variables[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB if
         i in current_pos
         and (i, j, ar, s, alt) not in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    active_phase_Seq1 = solver.Phase(
        [active_variables[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
         i in current_pos and s == 1],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_MIN_VALUE)

    active_phase_default = solver.Phase([active_variables[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if
         i in current_pos],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    active_variables_ar_phase_default = solver.Phase([active_variables_ar[(i, j, ar, s, outp)] for (i, j, ar, s, outp) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB if
          i in current_pos],
         solver.CHOOSE_RANDOM,
         solver.ASSIGN_RANDOM_VALUE)

    active_variables_ar_phase = solver.Phase(
        [active_variables_ar[(i, j, ar, s, outp)] for (i, j, ar, s, alt) in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1
         for (i1, j1, ar1, s1, outp) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB if
         i == i1 and j == j1 and ar == ar1 and s == s1 and i in current_pos],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    active_variables_ar_phase2 = solver.Phase(
        [active_variables_ar[(i, j, ar, s, outp)] for (i, j, ar, s, alt) in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB
         for (i1, j1, ar1, s1, outp) in ip_data.PO_TASK_AR_SEQ_OUTP_COMB if
         i == i1 and j == j1 and ar == ar1 and s == s1 and i in current_pos and (
         i, j, ar, s, alt) not in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    BOM_phase_IP2 = solver.Phase([Input_Product_Qty[(i, j, ar, alt, ip)] for (i, j, ar, alt, ip) in ip_data.PO_AR_BOM_IP_COMB if
                                  i in current_pos and ip_data.Product_Type_ID[int(ip)] == 1
                                  and ip not in ip_data.Alternate_BOM_Products],
                                 solver.CHOOSE_FIRST_UNBOUND,
                                 solver.ASSIGN_MAX_VALUE)

    BOM_phase_IP_Flag = solver.Phase(
        [Inv_Product_Qty_Flag[(i, j, ar, alt, ip)] for (i, j, ar, alt, ip) in ip_data.PO_AR_BOM_IP_COMB if
         i in current_pos and ip_data.Product_Type_ID[int(ip)] == 1 and ip not in ip_data.Alternate_BOM_Products],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_MAX_VALUE)

    BOM_phase_OP = solver.Phase([Output_Product_Qty[(i, j, ar, alt, op)] for (i, j, ar, alt, op) in ip_data.PO_AR_BOM_OP_COMB if
                                 i in current_pos and ip_data.Product_Type_ID[int(ip)] != 3],
                                solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
                                solver.ASSIGN_RANDOM_VALUE)

    all_ends_phase_Seq1 = solver.Phase(
        [all_ends_task[(i, ar, s)] for (i, ar, s) in ip_data.PO_AR_SEQ_COMB if i in current_pos and s == 1],
        solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
        solver.ASSIGN_RANDOM_VALUE)

    all_ends_phase = solver.Phase([all_ends_task[(i, ar, s)] for (i, ar, s) in ip_data.PO_AR_SEQ_COMB if i in current_pos],
                                  solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
                                  solver.ASSIGN_RANDOM_VALUE)

    all_starts_phase = solver.Phase([all_starts[i] for i in Phase_pos if i in current_pos],
                                    solver.CHOOSE_RANDOM,
                                    solver.ASSIGN_RANDOM_VALUE)

    Task_start_phase = solver.Phase([all_tasks[(i1, j, ar1, s1, alt1)].SafeStartExpr(-1).Var() for (i1, j, ar1, s1, alt1) in \
                                     Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1 if i1 in current_pos],
                                    solver.CHOOSE_RANDOM,
                                    solver.ASSIGN_MIN_VALUE)

    Task_start_phase2 = solver.Phase([all_tasks[(i1, j, ar1, s1, alt1)].SafeStartExpr(-1).Var() for (i1, j, ar1, s1, alt1) in ip_data.PO_TASK_AR_SEQ_ALT_COMB \
                                      if i1 in current_pos and (i1, j, ar1, s1, alt1) not in Phase_loop_PO_TASK_AR_SEQ_ALT_COMB_Seq1],
                                    solver.CHOOSE_RANDOM,
                                    solver.ASSIGN_MIN_VALUE)

    active_phase_Last = solver.Phase(
        [active_variables[(i, j, ar, s, alt)] for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB if i in current_pos and
         s == getLastSequence[(i, ar)] and alt != -1],
        solver.CHOOSE_RANDOM,
        solver.ASSIGN_RANDOM_VALUE)

    if ip_data.Subsequent_Op_with_allowed_gap == "Enable":
        PO_AR_SEQ_Phase = []
        for i in Phase_pos:
            for (po, ar, s) in ip_data.PO_AR_SEQ_COMB:
                if i == po and (po, ar, s) not in PO_AR_SEQ_Phase:
                    PO_AR_SEQ_Phase.append((po, ar, s))

        Next_Task_Gap_phase = solver.Phase(
            [Next_Task_Gap[(po, ar, s)] for (po, ar, s) in PO_AR_SEQ_Phase if po in current_pos and s <= 1],
            solver.CHOOSE_RANDOM,
            solver.ASSIGN_RANDOM_VALUE)

    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        sdst_phase = solver.Phase(
            [SDST_Var[(m, i, i1)] for (m, i, i1) in SDST_Comb if i in current_pos and i1 in current_pos],
            solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
            solver.ASSIGN_MIN_VALUE)

    b91_phase_loop = []
    for i2 in Phase_pos_Seq1:
        for (i, j, op) in ip_data.Alternate_BOM_COMB:
            if i2 == i:
                for (i1, j1, ar1, alt1, ip, op1) in ip_data.Alt_BOM_Flag:
                    if i == i1 and j == j1 and op == op1 and i in current_pos:
                        b91_phase_loop.append((i, j, ar1, alt1, ip, op))

    b91_phase2_loop = []
    for i2 in Phase_pos:
        for (i, j, op) in ip_data.Alternate_BOM_COMB:
            if i2 == i:
                for (i1, j1, ar1, alt1, ip, op1) in ip_data.Alt_BOM_Flag:
                    if i == i1 and j == j1 and op == op1 and i in current_pos:
                        b91_phase2_loop.append((i, j, ar1, alt1, ip, op))

    b91_phase = solver.Phase([b91[(i1, j1, ar1, alt1, ip, op1)] for (i1, j1, ar1, alt1, ip, op1) in b91_phase_loop],
                             solver.CHOOSE_FIRST_UNBOUND,
                             solver.ASSIGN_RANDOM_VALUE)

    b91_phase2 = solver.Phase([b91[(i1, j1, ar1, alt1, ip, op1)] for (i1, j1, ar1, alt1, ip, op1) in b91_phase2_loop],
                              solver.CHOOSE_FIRST_UNBOUND,
                              solver.ASSIGN_RANDOM_VALUE)

    # Parallel_Op_Gap_Phase = []
    # for po in Phase_pos:
    #     for (i, j, j1, ar, s, alt, alt1) in Parallel_Op_Gap:
    #         if po == i and (i, j, j1, ar, s, alt, alt1) not in Parallel_Op_Gap_Phase:
    #             Parallel_Op_Gap_Phase.append((i, j, j1, ar, s, alt, alt1))
    #
    # # Parallel_Op_Gap_Phase=list(set(Parallel_Op_Gap_Phase))
    # print "Parallel_Op_Gap_Phase: ", Parallel_Op_Gap_Phase

    if ip_data.Parallel_Op_with_allowed_gap == "Enable":
        Parallel_Op_Gap_Comb_Phase = []

        for po in Phase_pos:
            for (i, s) in ip_data.Parallel_Op_Gap_Comb:
                if po == i and (i, s) not in Parallel_Op_Gap_Comb_Phase:
                    Parallel_Op_Gap_Comb_Phase.append((i, s))

        print "Parallel_Op_Gap_Phase: ", Parallel_Op_Gap_Comb_Phase

        Op_Gap_phase = solver.Phase(
            [Op_Gap[po, s] for (po, s) in Parallel_Op_Gap_Comb_Phase if po in current_pos and po in Phase_pos_Seq1],
            solver.CHOOSE_RANDOM,
            solver.ASSIGN_RANDOM_VALUE)
        Op_Gap_phase2 = solver.Phase([Op_Gap[po, s] for (po, s) in Parallel_Op_Gap_Comb_Phase if
                                      po in current_pos and po in Phase_pos and po not in Phase_pos_Seq1],
                                     solver.CHOOSE_FIRST_UNBOUND,
                                     solver.ASSIGN_RANDOM_VALUE)

    # b61_phase = solver.Phase([b61[(i, j, j1, ar, s, alt, alt1)]for (i,j,j1,ar,s,alt,alt1) in Parallel_Op_Gap_Phase if i in current_pos],
    #                                 solver.CHOOSE_RANDOM,
    #                                 solver.ASSIGN_RANDOM_VALUE)


    ##  shelf_phase = solver.Phase([ Shelf_Life_Gap[(i,j,j1,ar,s,s1,alt,alt1)] for (i,j,j1,ar,s,s1,alt,alt1) in SHELF_LIFE_COMB \
    ##                               if i in current_pos and Product_Shelf_Life[Shelf_Life_Prod_Index[(i,j,j1,ar,s,s1,alt,alt1)]]>0],
    ##                      solver.CHOOSE_MIN_SIZE_LOWEST_MIN,
    ##                      solver.ASSIGN_RANDOM_VALUE)

    if ip_data.RMConstraint == "Constrained":
        b41_phase_loop = []
        for (i, j, ar, s, alt, ip) in ip_data.PO_TASK_AR_SEQ_ALT_IP_COMB:
            if i in current_pos:
                for (p1, t1) in ip_data.BOH_Comb:
                    if int(p1) == int(ip) and t1 == 0 and t1 < int(ip_data.PlanningHorizon) and ip_data.Product_Type_ID[int(ip)] == 1:
                        for i2 in Phase_pos_Seq1:
                            if i2 == i:
                                b41_phase_loop.append((i, j, ar, s, alt, ip, t1))

        b41_phase2_loop = []
        for (i, j, ar, s, alt, ip) in ip_data.PO_TASK_AR_SEQ_ALT_IP_COMB:
            if i in current_pos:
                for (p1, t1) in ip_data.BOH_Comb:
                    if int(p1) == int(ip) and t1 != 0 and t1 < int(ip_data.PlanningHorizon) and ip_data.Product_Type_ID[int(ip)] == 1:
                        for i2 in Phase_pos_Seq1:
                            if i2 == i:
                                b41_phase2_loop.append((i, j, ar, s, alt, ip, t1))

        b41_phase = solver.Phase([b41[(i, j, ar, s, alt, ip, t1)] for (i, j, ar, s, alt, ip, t1) in b41_phase_loop],
                                 solver.CHOOSE_RANDOM,
                                 solver.ASSIGN_MAX_VALUE)

        b41_phase2 = solver.Phase([b41[(i, j, ar, s, alt, ip, t1)] for (i, j, ar, s, alt, ip, t1) in b41_phase2_loop],
                                  solver.CHOOSE_RANDOM,
                                  solver.ASSIGN_MAX_VALUE)

    BOM_phase_IP_loop = []
    for (i, j, ar, alt, ip) in ip_data.PO_AR_BOM_IP_COMB:
        if i in current_pos and ip_data.Product_Type_ID[int(ip)] == 1:
            for i2 in Phase_pos:
                if i2 == i:
                    BOM_phase_IP_loop.append((i, j, ar, alt, ip))

    BOM_phase_IP = solver.Phase([Input_Product_Qty[(i, j, ar, alt, ip)] for (i, j, ar, alt, ip) in BOM_phase_IP_loop],
                                solver.CHOOSE_RANDOM,
                                solver.ASSIGN_MAX_VALUE)


    # b11_phase = solver.Phase([b11[(i, j, ar, s, alt)] for (i, j, ar, s, alt, m) in ip_data.PO_TASK_AR_SEQ_ALT_TASK_SPLIT_COMB \
    #         if i in Phase_pos_Seq1 and m in ip_data.Task_Split_Allowed_Machines for (p, mac, dur) in same_dur_list if int(mac) == int(m) and int(p) == getProductID(i)],
    #     solver.CHOOSE_RANDOM,
    #     solver.ASSIGN_RANDOM_VALUE)
    # b11_phase2 = solver.Phase(
    #     [b11[(i, j, ar, s, alt)] for (i, j, ar, s, alt, m) in ip_data.PO_TASK_AR_SEQ_ALT_TASK_SPLIT_COMB \
    #      if i in Phase_pos and i not in Phase_pos_Seq1 and m in ip_data.Task_Split_Allowed_Machines for (p, mac, dur) in same_dur_list if
    #      int(mac) == int(m) and int(p) == getProductID(i)],
    #     solver.CHOOSE_RANDOM,
    #     solver.ASSIGN_RANDOM_VALUE)

    currenttime = datetime.datetime.now()
    print "Step23_2:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if len(ip_data.Level2_available) > 0:

        if ip_data.Sequence_Dependent_Setup_Time == "Include":
            print "Inside Search Strategy where two level data is scheduled "
            if ip_data.RMConstraint == "Constrained":
                main_phase = solver.Compose(
                    [active_variables_ar_phase, b41_phase, b91_phase, b91_phase2, active_phase, intervalPhase, b41_phase2,
                     active_variables_ar_phase2, active_phase2, intervalPhase2,
                     all_ends_phase, sequence_phase, vars_phase])
            else:
                if ip_data.Task_Split_Allowed == "Enable":
                    print "Inside Search Strategy when Task Split is allowed"
                    main_phase = solver.Compose([active_variables_ar_phase, active_phase, b91_phase, Task_start_phase,  intervalPhase,
                                             active_variables_ar_phase2, active_phase2, b91_phase2, Task_start_phase2,  intervalPhase2,
                                             all_ends_phase,sequence_phase, vars_phase])
                else:
                    print "Inside Search Strategy when Task Split is not allowed"
                    main_phase = solver.Compose([active_variables_ar_phase, b91_phase, active_phase,  intervalPhase,
                                                active_variables_ar_phase2, b91_phase2, active_phase2,  intervalPhase2,
                                                all_ends_phase,sequence_phase, vars_phase])

    else:
    	print "Inside Search Strategy where only single level data is scheduled"
        main_phase = solver.Compose([active_variables_ar_phase_default, active_phase_default, intervalPhase_default, sequence_phase, vars_phase])

    ##  print ' ----------------------------------------------- Search Log ------------------------------------------------'
    search_log = solver.SearchLog(1000, obj_var)
    first_solution = solver.Assignment()

    for (i, j, ar, alt, ip) in ip_data.PO_AR_BOM_IP_COMB:
        if i in current_pos:
            first_solution.Add(Input_Product_Qty[(i, j, ar, alt, ip)])
            ##    first_solution.Add(Inv_Product_Qty[(i,j,ar,alt,ip)])

    for (i, j, ar, alt, op) in ip_data.PO_AR_BOM_OP_COMB:
        if i in current_pos:
            first_solution.Add(Output_Product_Qty[(i, j, ar, alt, op)])

    for i in current_pos:
        first_solution.Add(all_ends[i])
        first_solution.Add(all_starts[i])

    for i in ip_data.all_machines:
        if i[0] in non_overlapping_machines:
            first_solution.Add(all_sequences[i[0]])

    first_solution.AddObjective(obj_var)

    for seq in ip_data.all_machines:
        if seq[0] in non_overlapping_machines:
            # print "x:", seq[0]
            sequence = all_sequences[seq[0]];
            sequence_count = TSeq_Cnt[seq[0]]
            for i in range(sequence_count):
                t = sequence.Interval(i)
                first_solution.Add(t.SafeStartExpr(-1).Var())
                first_solution.Add(t.SafeEndExpr(-1).Var())
                first_solution.Add(t.DurationExpr().Var())

    for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
        if i in current_pos:
            first_solution.Add(active_variables[(i, j, ar, s, alt)])
            first_solution.Add(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var())
            first_solution.Add(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var())

    for (i, ar, s) in ip_data.PO_AR_SEQ_COMB:
        if i in current_pos:
            first_solution.Add(all_ends_task[(i, ar, s)])
            first_solution.Add(all_starts_task[(i, ar, s)])
    if ip_data.Sequence_Dependent_Setup_Time == "Include":
        for (m, i, i1) in SDST_Comb:
            if i in current_pos and i1 in current_pos:
                first_solution.Add(SDST_Var[(m, i, i1)])

                ##  for (i,j,ar,s,alt,ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
                ##    for index,(p1,t1) in enumerate(BOH_Comb,start=0):
                ##      if int(p1)==int(ip) and index < len(BOH_Comb)-1 and int(BOH_Comb[index+1][0])== int(p1) and t1<BOH_Comb[index+1][1]:
                ##        t2=BOH_Comb[index+1][1]
                ##        if Product_Type_ID[int(ip)]==1:
                ##          first_solution.Add(b51[(i,j,ar,s,alt,ip,t1)])

    currenttime = datetime.datetime.now()
    print "Step23_3:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    first_solution.Add(makespan)
    store_db = solver.StoreAssignment(first_solution)

    if first_solve == 1:
        if all_pos_iteration == 0:
            solution_time_limit = solver.TimeLimit(int(ip_data.High_Priority_Solution_Time_Limit) * 1000)
        else:
            solution_time_limit = solver.TimeLimit(int(ip_data.First_Solution_Time_Limit) * 1000)
    else:
        solution_time_limit = solver.TimeLimit(int(ip_data.Iteration_Time_Limit) * 1000)

    solution_db = solver.Compose([main_phase, store_db])
    collector = solver.LastSolutionCollector(first_solution)
    print "Invoking CP solve"
    currenttime = datetime.datetime.now()
    print "Step24:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    dec = solver.Solve(solution_db, [collector, objective, solution_time_limit])
    print "dec:", dec

    currenttime = datetime.datetime.now()
    print "Step25:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if dec == 1:
        print " -------------------- inside output calculation ------------------------ "
        ##    global sol1
        sol1 = collector.Solution(0)
        print "After CP solve Objective: ", collector.ObjectiveValue(0), sol1.Value(makespan)
        global active_var
        global fixed_decisions
        active_var = {}
        fixed_decisions = []
        global fixed_start_time
        global fixed_end_time
        fixed_start_time = {}
        fixed_end_time = {}
        for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
            if i in current_pos:
                active_var[(i, j, ar, s, alt)] = sol1.Value(active_variables[(i, j, ar, s, alt)])
                # if i==1:
                #     print "Active: ", i,j,ar,s,alt, active_var[(i, j, ar, s, alt)]
                if active_var[(i, j, ar, s, alt)] == 1:
                    fixed_decisions.append((i, j, ar, s, alt))
                    fixed_start_time[(i, j, ar, s, alt)] = sol1.Value(all_tasks[(i, j, ar, s, alt)].SafeStartExpr(-1).Var())
                    fixed_end_time[(i, j, ar, s, alt)] = sol1.Value(all_tasks[(i, j, ar, s, alt)].SafeEndExpr(-1).Var())

        make_span = sol1.Value(makespan)

        global Input_Product_Qty_var
        ##    global Inv_Product_Qty_var
        global Output_Product_Qty_var
        Input_Product_Qty_var = {}
        ##    Inv_Product_Qty_var = {}
        Output_Product_Qty_var = {}

        ##  for (i,j,ar,s) in PO_TASK_AR_SEQ_COMB:
        ##    if i in current_pos:
        ##        for (i1,j1,ar1,alt,ip) in PO_AR_BOM_IP_COMB:
        ##            if i1==i and j==j1 and ar==ar1:
        ##                solver.Add(Input_Product_Qty[(i1,j1,ar,alt,int(ip))] >= active_variables[(i, j, ar, s, alt)] * Dom_IP[(i,j,ip)])


        for (i, j, ar, alt, ip) in ip_data.PO_AR_BOM_IP_COMB:
            if i in current_pos:
                if (i, ip) in ip_data.po_products_alt_bom_comb:
                    # if ip in Alternate_BOM_Products:
                    #     print i, j, ar, alt, ip, sol1.Value(Input_Product_Qty[(i, j, ar, alt, ip)])
                    Input_Product_Qty_var[(i, j, ar, alt, ip)] = sol1.Value(Input_Product_Qty[(i, j, ar, alt, ip)])
                    if Input_Product_Qty_var[(i, j, ar, alt, ip)] > 0:
                        Input_Product_Qty_var[(i, j, ar, alt, ip)] = Dom_IP[(i, j, ip)]
                        ##print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[(i, j, ar, alt, ip)]
                        # if ip==173 or ip==218:
                        #     print "new: ", i, j, ar, alt, ip, Dom_IP[(i,j,ip)], sol1.Value(Input_Product_Qty[(i, j, ar, alt, ip)])
                else:
                    for (i1, j1, ar1, s) in ip_data.PO_TASK_AR_SEQ_COMB:
                        if i1 == i and j == j1 and ar == ar1 and (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
                            if sol1.Value(active_variables[(i, j, ar, s, alt)]) * Dom_IP[(i, j, ip)] < 0:
                                if active_var[(i, j, ar, s, alt)] == 1:
                                    Input_Product_Qty_var[(i, j, ar, alt, ip)] = Dom_IP[(i, j, ip)]
                                    # if i==1:
                                    #     print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[ (i, j, ar, alt, ip)]
                                else:
                                    Input_Product_Qty_var[(i, j, ar, alt, ip)] = Dom_IP[(i, j, ip)]
                                    # if i==1:
                                    #     print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[(i, j, ar, alt, ip)]
                            else:
                                Input_Product_Qty_var[(i, j, ar, alt, ip)] = sol1.Value(
                                    active_variables[(i, j, ar, s, alt)]) * Dom_IP[(i, j, ip)]
                                # if i==1:
                                #     print "Input_Product_Qty: ", i, j, ar, alt, ip, Input_Product_Qty_var[(i, j, ar, alt, ip)]
                                ##                    if sol1.Value(Input_Product_Qty[(i,j,ar,alt,ip)])>=0 and i==0:
                                ##                      print "Input_Product_Qty: ", i,j,ar,alt,ip,Input_Product_Qty_var[(i,j,ar,alt,ip)]

        for (i, j, ar, alt, op) in ip_data.PO_AR_BOM_OP_COMB:
            if i in current_pos:
                Output_Product_Qty_var[(i, j, ar, alt, op)] = sol1.Value(Output_Product_Qty[(i, j, ar, alt, op)])
                ##      if sol1.Value(Output_Product_Qty[(i,j,ar,alt,op)])>0:
                ##        print "Output_Product_Qty: ",i,j,ar,alt,op, sol1.Value(Output_Product_Qty[(i,j,ar,alt,op)])

                ##    for (i,j,ar,s,alt,ip) in PO_TASK_AR_SEQ_ALT_IP_COMB:
                ##      for index,(p1,t1) in enumerate(BOH_Comb,start=0):
                ##        if int(p1)==int(ip) and index < len(BOH_Comb)-1 and int(BOH_Comb[index+1][0])== int(p1) and t1<BOH_Comb[index+1][1]:
                ##          t2=BOH_Comb[index+1][1]
                ##          if Product_Type_ID[int(ip)]==1:
                ##            print "b51: ",i,j,ar,s,alt,ip,t1, sol1.Value(b51[(i,j,ar,s,alt,ip,t1)])

        global all_ends_var
        all_ends_var = {}
        global all_starts_var
        all_starts_var = {}

        for i in current_pos:
            all_ends_var[i] = sol1.Value(all_ends[i])
            all_starts_var[i] = sol1.Value(all_starts[i])
            # print i, all_starts_var[i], all_ends_var[i]

        currenttime = datetime.datetime.now()
        print "Step26:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        global seq_size
        global Begin_Time
        global End_Time
        global SeqID
        global RouteID
        global OperID
        global ProdID
        global PO
        global Processing_Time1
        global Down_Time
        global Up_Time
        global Idle_Time
        global Utilization

        seq_size = {}
        Begin_Time = {}
        End_Time = {}
        SeqID = {}
        RouteID = {}
        OperID = {}
        ProdID = {}
        PO = {}
        Processing_Time1 = {}
        Down_Time = {}
        Up_Time = {}
        Idle_Time = {}
        Utilization = {}

        valid_tasks = []
        global valid_tasks_sorted
        valid_tasks_sorted = []
        global valid_comb
        valid_comb = []
        global valid_shutdown_comb
        valid_shutdown_comb = []

        print '--------------------------------- Times -------------------------------------'
        ##    print "non_overlapping_machines: ",non_overlapping_machines
        for i in ip_data.all_machines:
            if i[0] in non_overlapping_machines:
                # print "machine: ", i[0]
                seq = all_sequences[i[0]];
                sequence = collector.ForwardSequence(0, seq)
                seq_size[i[0]] = len(sequence)

                temp1 = 0
                temp2 = 0
                for j in range(seq_size[i[0]]):
                    # print "seq, len: ", i[0], j, seq_size[i[0]]
                    t = seq.Interval(sequence[j])
                    # print int(OutputSeq_Mac_Job[(i[0], sequence[j])]),int(OutputSeq_Mac_Job[(i[0], sequence[j])]) not in current_pos,     current_pos
                    PO[i[0], j] = int(OutputSeq_Mac_Job[(i[0], sequence[j])])
                    if int(OutputSeq_Mac_Job[(i[0], sequence[j])]) not in current_pos:
                        # print 100, i[0], j
                        for st in Invalid_Timings2:
                            if st[0] == i[0] and str(t) == str(st[3]):
                                Begin_Time[i[0], j] = st[1]
                                End_Time[i[0], j] = st[1] + st[2]
                                valid_shutdown_comb.append((i[0], j))
                                # print 100,i[0],j, Begin_Time[i[0],j], End_Time[i[0],j]
                                continue
                    else:
                        Begin_Time[i[0], j] = int(collector.Value(0, t.SafeStartExpr(-1).Var()))
                        End_Time[i[0], j] = int(collector.Value(0, t.SafeEndExpr(-1).Var()))
                        SeqID[(i[0], j)] = Seq_ID[(i[0], sequence[j])]
                        RouteID[(i[0], j)] = Route_ID[(i[0], sequence[j])]
                        OperID[(i[0], j)] = Oper_ID[(i[0], sequence[j])]
                        ProdID[(i[0], j)] = Prod_ID[(i[0], sequence[j])]
                        temp2 = temp2 + End_Time[i[0], j] - Begin_Time[i[0], j]
                        # print 200,i[0],j,Begin_Time[i[0],j],End_Time[i[0],j]
                        valid_tasks.append((i[0], j, Begin_Time[i[0], j], End_Time[i[0], j]))
                        valid_comb.append((i[0], j))

                Processing_Time1[i[0]] = temp2
                #print 1
                Down_Time[i[0]] = temp1
                Up_Time[i[0]] = ip_data.Machine_Avai_End_Time[i[0]] - ip_data.Machine_Avai_Start_Time[i[0]]
                #print 2
                Idle_Time[i[0]] = Up_Time[i[0]] - Processing_Time1[i[0]] - Down_Time[i[0]]
                #print 3
                Utilization[i[0]] = float(Processing_Time1[i[0]] / float(Up_Time[i[0]]))
                #print 4

        valid_tasks_sorted = sorted(valid_tasks, key=lambda x: (x[0], -x[3]))

        # Valid tasks sorted new with multi shift planning (task will be split with a holiday or off-time in between
        global valid_tasks_sorted_new
        valid_tasks_sorted_new=[]

        # for (m, seq, b, e) in valid_tasks_sorted:
        #     if m in ip_data.Task_Split_Allowed_Machines:
        #         for t in Invalid_Timings2:
        #             if m == t[0]:
        #                 if b <= t[1] and e >= t[1] + t[2]:
        #                     print 1, m, PO[(m, seq)], b,t[1]
        #                     print 2, m, PO[(m, seq)], t[1] + t[2], e
        #                     valid_tasks_sorted_new.append((int(m), int(seq), int(b),int(t[1])))
        #                     valid_tasks_sorted_new.append((int(m), int(seq), int(t[1]) + int(t[2]), int(e)))
        #     else:
        #         valid_tasks_sorted_new.append((int(m), int(seq), int(b), int(e)))
        valid_tasks_sorted_new = valid_tasks_sorted
        print "valid_tasks_sorted_new: ", valid_tasks_sorted_new


        if all_pos_iteration == 0:
            for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
                if i in current_pos and active_var[(i, j, ar, s, alt)] == 1:
                    High_Priority_Comb.append((i, j, ar, s, alt))
                    High_Priority_Begin_Time[(i, j, ar, s, alt)] = int(fixed_start_time[(i, j, ar, s, alt)])
                    High_Priority_End_Time[(i, j, ar, s, alt)] = int(fixed_end_time[(i, j, ar, s, alt)])
                    High_Priority_Active_Status[(i, j, ar, s, alt)] = int(active_var[(i, j, ar, s, alt)])

            print "High_Priority_Comb: ", High_Priority_Comb
            print "High_Priority_Begin_Time: ", High_Priority_Begin_Time
            print "High_Priority_End_Time: ", High_Priority_End_Time
            print "High_Priority_Active_Status: ", High_Priority_Active_Status

        # for i in ip_data.allSetuptime_with_Holiday_Inbetweenr.Value(0, t.SafeEndExpr(-1).Var()), ")"

        currenttime = datetime.datetime.now()
        print "Step27:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        all_machine_ends1 = {}
        all_ends_machine1 = {}
        all_machine_ends_unsorted = []
        Avg_Operation_End_Time = {}
        k = {}
        PO_TASK_AR_SEQ_ALT_COMB_New_unsorted = []
        global Total_Iteration_Set
        Total_Iteration_Set = []
        # PO_Set=[]


        for (op, m) in ip_data.Operations_Machines_New:
            all_ends_machine1[m] = next((e for (i, j, b, e) in valid_tasks_sorted_new if i == m), 0)
            all_machine_ends_unsorted.append((op, m, all_ends_machine1[m]))

        all_machine_ends1 = sorted(all_machine_ends_unsorted, key=operator.itemgetter(0, 2, 1))

        currenttime = datetime.datetime.now()
        print "Step28:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        print "iter_solve: ", iter_solve

        if first_solve == 1 or loop == Iteration_Set_Length or iter_solve == 1:
            print "Inside Avg End Times Calculation"
            for op in ip_data.all_operations:
                for po in current_pos:
                    k[op[0], po] = 0
                    Avg_End_Time_Comb = []
                    for (i, j, alt) in ip_data.PO_TASK_ALT_COMB:
                        if i == po and j == op[0]:
                            k[op[0], po] = k[op[0], po] + 1
                            Avg_End_Time_Comb.append((i, j, alt))

                            ##          k[op[0],po[0]] = sum(1 for (i,j,alt) in PO_TASK_ALT_COMB if i==po[0] and j==op[0])
                    if k[op[0], po] > 1:
                        Avg_Operation_End_Time[op[0], po] \
                            = sum(all_ends_machine1[m[0]] for (i, j, alt) in Avg_End_Time_Comb for m in ip_data.all_machines if \
                                  i in current_pos and m[0] == ip_data.Machine_ID2[(i, j, alt)]) / k[op[0], po]

            currenttime = datetime.datetime.now()
            print "Step29:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            count = 98
            MaxSeq = max(getLastSequence[i, ar] for (i, ar) in ip_data.PO_AR_COMB if i in current_pos)
            print "MaxSeq : ", MaxSeq
            while count >= 1 and current_seq <= MaxSeq:
                count = 0
                ##        print "current_seq, Last_Failed_Seq", current_seq,last_failed_seq
                if iter_solve == 1 and current_seq < last_failed_seq and last_failed_seq <> 0:
                    current_seq = last_failed_seq + 1
                ##          print "next current_seq with last failed: ", current_seq
                if current_seq > last_failed_seq and last_failed_seq <> 0 and current_seq <> 1 and count == 0:
                    current_seq = current_seq + 1
                ##          print "next current_seq: ", current_seq
                if current_seq == last_failed_seq:
                    current_seq = current_seq + 1
                ##          print "incremented current_seq: ", current_seq
                for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
                    if i in current_pos and k[(j, i)] > 1 and sol1.Value(all_ends_task[(i, ar, s)]) > \
                            Avg_Operation_End_Time[(j, i)] and active_var[(i, j, ar, s, alt)] > 0:
                        Total_Iteration_Set.append((i, j, ar, s, alt))
                        if s == current_seq:
                            count = count + 1
                            PO_TASK_AR_SEQ_ALT_COMB_New_unsorted.append((i, j, ar, s, alt))
                print "count: ", count
                if count == 0:
                    current_seq = current_seq + 1
                    ##          print "current_seq increased if count=0: ", current_seq
                    if current_seq > MaxSeq:
                        count = 0
                    count = 99
                else:
                    print "element_count: ", count
                    count = 0
            print "next_seq: ", current_seq
            currenttime = datetime.datetime.now()
            print "Step30:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            ##      print "Total_Iteration_Set: ", Total_Iteration_Set
            if first_solve == 1:
                out = int(ip_data.First_Solution_Time_Limit) + len(Total_Iteration_Set) * int(ip_data.Iteration_Time_Limit)

                f1 = open(ip_data.LogFile, 'a')
                f1.write("\n")
                f1.write("Recommendation : ")
                f1.write("For best results Solver Run Time Limit (Sec) should be greater than :  %s" % out)
                f1.write(" seconds")
                f1.write("\n")
                f1.write("\n")

            PO_TASK_AR_SEQ_ALT_COMB_New = sorted(PO_TASK_AR_SEQ_ALT_COMB_New_unsorted, key=lambda x: (x[3], -x[0]))
            currenttime = datetime.datetime.now()
            print "Step31:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        else:
            PO_TASK_AR_SEQ_ALT_COMB_New = []
        currenttime = datetime.datetime.now()
        print "Step31_0:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        return (True, collector.ObjectiveValue(0), active_var, ip_data.PO_TASK_AR_SEQ_ALT_COMB, PO_TASK_AR_SEQ_ALT_COMB_New,
                fixed_decisions, all_machine_ends1, fixed_start_time, fixed_end_time, make_span)
    else:
        if first_solve == 1:
            return (False, obj_value, [], ip_data.PO_TASK_AR_SEQ_ALT_COMB, [], [], [], [], [], makespan_value)
        else:
            active_var_new2 = {}
            for (i, j, ar, s, alt) in ip_data.PO_TASK_AR_SEQ_ALT_COMB:
                if i in current_pos:
                    active_var_new2[(i, j, ar, s, alt)] = active_var_new[(i, j, ar, s, alt)]

            return (False, obj_value, active_var_new2, ip_data.PO_TASK_AR_SEQ_ALT_COMB, [], [], [], [], [], makespan_value)


def get_cum_qty(to_plot):
    to_plot_x = {}
    to_plot_y = {}
    levels_ = []
    products = set([to_plot[i][0] for i in range(len(to_plot))])
    print products
    for p in products:
        to_plot_x[p] = [to_plot[i][1] for i in range(len(to_plot)) if to_plot[i][0] == p]
        to_plot_y[p] = [to_plot[i][2] for i in range(len(to_plot)) if to_plot[i][0] == p]
        to_plot_y[p] = np.cumsum(to_plot_y[p])
        levels_ = levels_ + list((np.column_stack(
            ([to_plot[i][0] for i in range(len(to_plot)) if to_plot[i][0] == p], to_plot_x[p], to_plot_y[p])).tolist()))
    return levels_


def current_time():
  return time()

def getProdOrderCount():
  return int(len(current_pos))

def getMachineSequence(PO,Task,AR):
  for i in range(0,len(ip_data.Sequence_Time)):
    if(int(ip_data.Sequence_Time[i][0]) == PO and int(ip_data.Sequence_Time[i][1] )== Task  and int(ip_data.Sequence_Time[i][2] )== AR ):
      return int(ip_data.Sequence_Time[i][3])

def getOrderQuantity(PO):
##  print 1,PO
  for i in range(0,len(ip_data.Sequence_Time)):
    if(int(ip_data.Sequence_Time[i][0]) == int(PO)):
##      print 2,PO, int(Sequence_Time[i][9])
      return int(ip_data.Sequence_Time[i][9])

def getnMachine(PO,Task,ar):
  for i in range(0,len(ip_data.op_op_ar)):
    if(int(ip_data.op_op_ar[i][0]) == PO and int(ip_data.op_op_ar[i][1]) == Task and int(ip_data.op_op_ar[i][2]) == ar):
      return int(ip_data.op_op_ar[i][3])
  return 0
def getOrderDueDate(PO):
  for i in range(0,len(ip_data.ORDER_DUE_DATE)):
    if(int(ip_data.ORDER_DUE_DATE[i][0]) == PO):
      return int(ip_data.ORDER_DUE_DATE[i][1])

def getOrderPriority(PO):
  for i in range(0,len(ip_data.ORDER_DUE_DATE)):
    if(int(ip_data.ORDER_DUE_DATE[i][0]) == PO):
      return ip_data.ORDER_DUE_DATE[i][2]

def getProductID(PO):
  for i in range(0,len(ip_data.ORDER_DUE_DATE)):
    if(int(ip_data.ORDER_DUE_DATE[i][0]) == int(PO)):
      return int(ip_data.ORDER_DUE_DATE[i][3])

def getMachineAvailableEndTime(Task, Alt):
  for i in range(0,len(ip_data.Machine_Details)):
    if(int(ip_data.Machine_Details[i][1]) == Task and int(ip_data.Machine_Details[i][2]) == Alt):
      return int(ip_data.Machine_Details[i][4])

def getAttachRate(op,ip,m):
  for i in range(0,len(ip_data.BOM_DETAILS)):
    if(ip_data.BOM_DETAILS[i][0] == op and ip_data.BOM_DETAILS[i][1] == ip and ip_data.BOM_DETAILS[i][2] == m):
      return (ip_data.BOM_DETAILS[i][3])

def getYield(op,ip,m):
  for i in range(0,len(ip_data.BOM_DETAILS)):
    if(ip_data.BOM_DETAILS[i][0] == op and ip_data.BOM_DETAILS[i][1] == ip and ip_data.BOM_DETAILS[i][2] == m):
      return (ip_data.BOM_DETAILS[i][4])

def find_invalid_ranges(machine_shutdown, machine_holidays):
    #print machine_shutdown, machine_holidays
    valid_range_array = np.zeros(int(ip_data.PlanningHorizon))
    for i in machine_holidays:
        if i[1]<int(ip_data.PlanningHorizon) :
            valid_range_array[i[0] + 1: i[1] + 1] = 1
    for i in machine_shutdown:
        if i[1] < int(ip_data.PlanningHorizon):
            valid_range_array[i[0] + 1: i[1] + 1] = 1
      ## Find where value chages. This point is start of invalid range, as index 0 is always 0.
    change_index = np.where(valid_range_array[:-1] != valid_range_array[1:])[0]
    # print change_index
    invalid_ranges = []
    if valid_range_array[0] == 0:
          index = 0
          while index < len(change_index):
              invalid_ranges.append((change_index[index], change_index[index + 1]))
              index = index + 2
    return invalid_ranges


def valid_po_comb_calculation():
    valid_po_comb = []
    valid_po_comb_truncated_sfg=[]
    fg_of_sfglotscompleted = []
    fg_lots_with_no_sfg_lots = []  # Lots which doesn't have relevant SFG lots
    # for i in range(0, len(ip_data.SFG_Lots)):
    #     ip_data.SFG_Order_Qty[ip_data.SFG_Lots[i][0]] = int(ip_data.SFG_Lots[i][2])

    for (i, p, q,fg_lot) in ip_data.FG_Lots:  # production_order_index,pm.product_index, order_quantity
        if i in current_pos:
            temp = 0  # to find FG lots which doesn't have enough SFG lots
            for (m, n) in ip_data.SFG_FG_Domain_Comb:  # ip, op  or sfg,fg
                if n == p:
                    FG_bal = 0
                    # print fg_lot
                    for (i1, p1, q1,sfg_lot) in ip_data.SFG_Lots:  # production_order_index,pm.product_index,order_quantity
                        if i1 in current_pos and m == p1:  # and (i1,n) in SFG_PO_FG_Lot_Relation:
                            if FG_bal == 0:
                                fg_current = round(ip_data.FG_Order_Qty[i] *ip_data.AR[(p1, p)])
                                FG_bal = 99999999
                                # print i, fg_current, SFG_Order_Qty
                            # print i,i1,p,p1, (i, i1) in PO_Relation , (p1,p) in SFG_FG_Domain_Comb
                            if (i, i1) in ip_data.PO_Relation:
                                if fg_current >= ip_data.SFG_Order_Qty[i1] and ip_data.SFG_Order_Qty[i1] > 0:
                                    fg_current = fg_current - ip_data.SFG_Order_Qty[i1]
                                    ip_data.SFG_Order_Qty[i1] = 0
                                    if i1 in ip_data.Lots_Completed:
                                        fg_of_sfglotscompleted.append(i)
                                        valid_po_comb_truncated_sfg.append((i,i1))
                                    else:
                                        valid_po_comb.append((i, i1))
                                    temp = 1
                                elif fg_current > 0 and fg_current < ip_data.SFG_Order_Qty[i1]:
                                    ip_data.SFG_Order_Qty[i1] = ip_data.SFG_Order_Qty[i1] - fg_current
                                    fg_current = 0
                                    if i1 in ip_data.Lots_Completed:
                                        fg_of_sfglotscompleted.append(i)
                                        valid_po_comb_truncated_sfg.append((i, i1))
                                    else:
                                        valid_po_comb.append((i, i1))
                                    temp = 2

            if temp == 0:
                fg_lots_with_no_sfg_lots.append(i)
    return  valid_po_comb,fg_lots_with_no_sfg_lots,fg_of_sfglotscompleted,valid_po_comb_truncated_sfg


def Domain_Calculation():
    global Dom_IP
    global Dom_OP
    Dom_IP = {}
    Dom_OP = {}
    global PO_OP_SFG_Comb
    PO_OP_SFG_Comb = []
    getLastSequence = {}
    for (i, ar) in ip_data.PO_AR_COMB:
        if i in current_pos:
            getLastSequence[i, ar] = max(s for (i1, j, ar1, s) in ip_data.PO_TASK_AR_SEQ_COMB if i == i1 and ar1 == ar)

    print "current_pos count: ", len(current_pos)
    print "current_pos: ", current_pos
    for (i1, ar1) in ip_data.PO_AR_COMB:
        if i1 in current_pos:
            for s in range(getLastSequence[i1, ar1], 0, -1):
                for (i2, j2, ar2, s2) in ip_data.PO_TASK_AR_SEQ_COMB:
                    # if i2 == 23 and i1==i2:
                    #     print "s2:",s2,i2,ar2
                    if i1 == i2 and s == s2 and ar1 == ar2:
                        for (i, j, ar, alt, ip, op) in ip_data.PO_BOM_COMB:
                            # if i == 23 and i==i2:
                            #     print i,j,ar,alt,ip,op
                            if i == i1 and ar == ar1 and j == j2:
                                # if i1 == 23:
                                #     print "s: ", i,j,ar,ip,op
                                if ip_data.Product_Type_ID[int(op)] == 3:
                                    Dom_IP[(i, j, ip)] = int(ip_data.Order_Quantity[i] * getAttachRate(op, ip, j))
                                    PO_OP_SFG_Comb.append((i, j, ip))
                                    Dom_OP[(i, j, op)] = int(ip_data.Order_Quantity[i])
                                    # if i1 == 23:
                                    #     print 1,s,j,ip,op, Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)]
                                    for (i3, j3, j4, ar3, s3, s4, p) in ip_data.MAT_BALANCE_CONST:
                                        if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
                                            if ip_data.Product_Type_ID[int(ip)] != 1:
                                                Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]
                                elif ip_data.Product_Type_ID[int(op)] == 2 and getLastSequence[i1, ar1] == s:
                                    Dom_IP[(i, j, ip)] = int(ip_data.Order_Quantity[i] * getAttachRate(op, ip, j))
                                    PO_OP_SFG_Comb.append((i, j, ip))
                                    Dom_OP[(i, j, op)] = int(ip_data.Order_Quantity[i])
                                    # if i1 == 23:
                                    #     print 1,s,j,ip,op, Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)]
                                    for (i3, j3, j4, ar3, s3, s4, p) in ip_data.MAT_BALANCE_CONST:
                                        if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
                                            if ip_data.Product_Type_ID[int(ip)] != 1:
                                                Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]
                                elif ip_data.Product_Type_ID[int(op)] == 2:
                                    # if i1 == 23:
                                    #     print 100, s,j,ar,ip,op
                                    if ip_data.Product_Type_ID[int(ip)] == 1:
                                        Dom_IP[(i, j, ip)] = Dom_OP[(i, j, op)] * getAttachRate(op, ip, j)
                                        PO_OP_SFG_Comb.append((i, j, ip))
                                        if Dom_IP[(i, j, ip)] == 0:
                                            ##                            print i,j,ip,op,Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
                                            Dom_IP[(i, j, ip)] = 1
                                    else:
                                        Dom_IP[(i, j, ip)] = int(Dom_OP[(i, j, op)] * getAttachRate(op, ip, j))
                                        PO_OP_SFG_Comb.append((i, j, ip))
                                        if Dom_IP[(i, j, ip)] == 0:
                                            ##                            print i,j,ip,op,Dom_IP[(i,j,ip)], Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
                                            Dom_IP[(i, j, ip)] = 1
                                            ##                      print 2,s,j,ip,op, Dom_IP[(i,j,ip)] , Dom_OP[(i,j,op)], getAttachRate(op,ip,j)
                                    for (i3, j3, j4, ar3, s3, s4, p) in ip_data.MAT_BALANCE_CONST:
                                        if i3 == i1 and j4 == j and ar3 == ar1 and s4 == s and s3 == s - 1 and ip == p:
                                            if ip_data.Product_Type_ID[int(ip)] != 1:
                                                Dom_OP[(i3, j3, p)] = Dom_IP[(i, j, p)]
    return Dom_IP, Dom_OP,PO_OP_SFG_Comb

def Duration_Pre_Compute_Task_Split(Invalid_Timings2):
    Duration1 = []
    Duration = []
    global Duration2
    Duration2 = []
    for (p, mac) in ip_data.PROD_MAC_Comb:
        if int(mac) in ip_data.Task_Split_Allowed_Machines and len(Invalid_Timings2) > 0:
            original_task_duration = int(ip_data.PROD_MAC_Time[(p, mac)])
            Duration = [(p, mac, h, -1) for t in Invalid_Timings2 if int(mac) == t[0] for h in range(t[1], t[1] + t[2])]
            Duration1 = Duration1 + Duration
            # print "len: ", len(Duration)

    for (p, mac) in ip_data.PROD_MAC_Comb:
        if int(mac) in ip_data.Task_Split_Allowed_Machines and len(Invalid_Timings2) > 0:
            original_task_duration = int(ip_data.PROD_MAC_Time[(p, mac)])
            Invalid_Timings2 = set(Invalid_Timings2)
            Invalid_Timings31 = [(t1, t1 + t2) for (t0, t1, t2) in Invalid_Timings2 if t0 == int(mac)]
            Invalid_Timings31 = sorted(Invalid_Timings31, key=lambda x: (x[0], x[1]))  #Sorting required to handle all subsequent breaks, do not delete
            maxt = max(t2 for (t1, t2) in Invalid_Timings31)
            mint = min(t1 for (t1, t2) in Invalid_Timings31)
            Full_Range = []
            # for (t1,t2) in  Invalid_Timings31:
            #     for loop in range(0,t2-t1):
            #         Full_Range.append(t1+loop)
            Full_Range = set([t1 + loop for (t1, t2) in Invalid_Timings31 for loop in range(0, t2 - t1)])
            # Full_Range=set(Full_Range)
            # print Full_Range
            currenttime = datetime.datetime.now()
            print "Step_x :  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
            # print p, mac, original_task_duration
            # print p, mac, Invalid_Timings31
            for h in xrange(0, int(ip_data.PlanningHorizon)):
                k = 0
                dur = original_task_duration
                if h >= maxt or h < mint - dur:
                    Duration2.append((p, mac, h, dur))
                if h not in Full_Range:
                    for t in Invalid_Timings31:
                        ts, te = t
                        if ts - dur <= h <= ts:
                            k = 1
                            dur += te - ts
                    # Duration2.append((p, mac, h, dur))
                if k != 0:
                    Duration2.append((p, mac, h, dur))

                    # print "Duration2: ", mac, Duration2
    # print "Duration1 : ", len(Duration1)
    # print "Duration1: " ,Duration1
    #     if p==378 and mac==8:
    #         print "Duration2: " ,Duration2
    return Duration1 + Duration2



# def Duration_Pre_Compute_Task_Split(Invalid_Timings2):
#         print "Invalid_Timings2: ", Invalid_Timings2
#         Duration1 = []
#         Duration = []
#         Duration2 = []
#         for (p,mac) in ip_data.PROD_MAC_Comb:
#             if int(mac) in ip_data.Task_Split_Allowed_Machines and len(Invalid_Timings2)>0:
#                 original_task_duration = int(ip_data.PROD_MAC_Time[(p,mac)])
#                 Duration = [(p, mac, h, -1) for t in Invalid_Timings2 if int(mac) == t[0] \
#                                             for h in range(t[1], t[1] + t[2])]
#                 Duration1 = Duration1 + Duration
#
#         currenttime = datetime.datetime.now()
#         print "Step_x :  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')
#
#         for (p, mac) in ip_data.PROD_MAC_Comb:
#             if int(mac) in ip_data.Task_Split_Allowed_Machines and len(Invalid_Timings2) > 0:
#                 original_task_duration = int(ip_data.PROD_MAC_Time[(p, mac)])
#                 Invalid_Timings31 = [(t1,t1+t2) for (t0,t1,t2) in Invalid_Timings2 if t0==int(mac)]
#                 print p,mac, original_task_duration
#                 # print p,mac, Invalid_Timings31
#                 for h in xrange(0, int(ip_data.PlanningHorizon)):
#                     k = 0
#                     dur = original_task_duration
#                     Duration2.append((p, mac, h, dur))
#                     if any(h not in range(l,u) for (l,u) in Invalid_Timings31):
#                         for t in Invalid_Timings31:
#                             ts,te = t
#                             if ts - dur <= h <= ts:# and h < t1 + t2:    #and h not in range(t1, t1 + t2)
#                                 k = 1
#                                 dur += te-ts
#                     if k != 0:
#                         Duration2.remove((p, mac, h, original_task_duration))
#                         Duration2.append((p, mac, h, dur))
#                     # print "Duration2: ", mac, Duration2
#         # print "Duration1 : ", len(Duration1)
#
#         return Duration1+Duration2

# def Duration_Pre_Compute_Task_Split(Invalid_Timings2):
#     Duration1 = []
#     for (p,mac) in ip_data.PROD_MAC_Comb:
#         if int(mac) in ip_data.Task_Split_Allowed_Machines and len(Invalid_Timings2)>0 and mac==10:
#             original_task_duration = int(ip_data.PROD_MAC_Time[(p,mac)])
#             # original_task_duration = 5
#             # start with list assuming no delays
#             Duration1 = [original_task_duration] * 86400
#
#             # work backward, updating all durations based on each invalid timing
#             for t in reversed(Invalid_Timings2):
#                 if int(t[0])==int(mac):
#                     # unpack once (thanks Brian)
#                     t0, stt, end, t3 = t
#                     delay = end
#
#                     # any duration between stt/end -> invalid
#                     Duration1[stt:end] = [-1] * delay
#
#                     # any duration before stt -> delayed
#                     # (everything from 0 to stt is the same, so just use the first element)
#                     Duration1[:stt] = [Duration1[0] + delay] * stt
#
#     return Duration1