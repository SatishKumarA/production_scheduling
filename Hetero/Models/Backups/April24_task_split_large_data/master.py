import input_data as ip_data
# import output_data as op_data
import sys
import os
import mysql.connector
import scheduler as ps
# import local_search as ls
# from input_data import *
# from local_search import *
# from scheduler import *


try:
    global JOB_ID
    JOB_ID = str(sys.argv[1])
except Exception as x:
    JOB_ID = 11665
    print "Using user inputted JOB_ID: ", JOB_ID

print " ************************ Database Details ****************************** "
global  DB_String
global  Host_String
DB_String = 'saddlepointv6_prod2'
Host_String = '127.0.0.1'

cnx = mysql.connector.connect(user='root', password='saddlepoint', host=Host_String, database=DB_String,buffered=True)
cursor = cnx.cursor()

cursor.execute('SELECT distinct Client_ID, Scenario_ID FROM SPAPP_JOB where JOb_ID ='  + str(JOB_ID))
ClientID = cursor.fetchall()
Client_ID  = ClientID[0][0]
Scenario_ID = ClientID[0][1]
PROCESS_ID = os.getpid()
CONNECTION_ID = cnx.connection_id
print "----------------------- System Parameters (Master python file) ---------------------- "

print "JOB ID:            ", JOB_ID
print "ClientID:          ", Client_ID
print "Scenario_ID:       ", Scenario_ID
print "PROCESS_ID:        ", PROCESS_ID
print "DB CONNECTION_ID : ", cnx.connection_id

cursor.execute('update SPAPP_JOB set PID = ' + str(PROCESS_ID) + ' where JOB_ID = '  + str(JOB_ID))
cursor.execute('update SPAPP_JOB set DB_CONNECTION_ID = ' + str(CONNECTION_ID) + ' where JOB_ID ='  + str(JOB_ID))
cursor.close()
cnx.commit()
cnx.close()


if __name__ == '__main__':
    # try:
    # f = open("console.log", 'w')
    # sys.stdout = f
    if Client_ID == 3:
        print "Hetero Model is invoked"
        print ip_data.input_data_reading(JOB_ID,DB_String,Host_String)
        print ps.PriorityIterationSolve()
    else:
        print "General Model is invoked"
        import PS_IBP_Model_General as generalmodel
        print  generalmodel.PriorityIterationSolve()
    #     cnx1 = mysql.connector.connect(user='root', password='saddlepoint', host=Host_String, database=DB_String,buffered=True)
    #     cursor1 = cnx1.cursor()
    #     cursor1.execute('update SPAPP_JOB set JOB_STATUS="COMPLETED",COMMENTS="No Comments", INFO="Job completed" where JOB_ID = ' + str(JOB_ID))
    #     cursor1.close()
    #     cnx1.commit()
    #     cnx1.close()
    # except Exception as x:
    #     print x
    #     cnx = mysql.connector.connect(user='root', password='saddlepoint', host=Host_String, database=DB_String,buffered=True)
    #     cursor = cnx.cursor()
    #     cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
    #     cursor.close()
    #     cnx.commit()
    #     cnx.close()