

# import operator
# from operator import itemgetter
# from ortools.constraint_solver import pywrapcp
import mysql.connector
import os
import datetime
from datetime import date, timedelta
from time import time
import numpy as np
import math
from decimal import *
from itertools import groupby

print 'Production Scheduler is Invoked'


def print_time(message):
    pass
    currenttime = datetime.datetime.now()
    print message,"--- %s ---" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

currenttime = datetime.datetime.now()
print "Step0: %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

def input_data_reading(JOB_ID,DB_String,Host_String):

    global Process_Start_Time
    Process_Start_Time = time()

    cnx = mysql.connector.connect(user='root', password='saddlepoint', host=Host_String, database=DB_String)
    cursor = cnx.cursor()

    print "JOB_ID:  ", JOB_ID
    cursor.execute('SELECT distinct Client_ID, Scenario_ID,Exception_log_link FROM SPAPP_JOB where JOb_ID =' + str(JOB_ID))
    ClientID = cursor.fetchall()
    global Client_ID
    global Scenario_ID
    Client_ID = ClientID[0][0]
    Scenario_ID = ClientID[0][1]
    global LogFile
    LogFile = ClientID[0][2]

    CONNECTION_ID = cnx.connection_id
    print " DB CONNECTION_ID :", cnx.connection_id
    cursor.execute('update SPAPP_JOB set DB_CONNECTION_ID = ' + str(CONNECTION_ID) + ' where JOB_ID =' + str(JOB_ID))

    #cursor.execute('update xlps_route_solver_ready set MAX_BATCH_SIZE=100000,MIN_BATCH_SIZE=0')
    # cursor.execute('delete from  xlps_po_solver_ready where production_order_id="3002011_3002013-Lot0"')
    cnx.commit()
    currenttime = datetime.datetime.now()
    global f1
    f1 = open(LogFile, 'a')
    f1.truncate
    f1.write("-------- Production Scheduler Log ( JOB_ID - %s " % str(JOB_ID))
    f1.write(") -------- \n")
    f1.write("Data reading started at %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
    f1.write("\n")

    print ' ----------------------------------- Solver Parameters -----------------------------------------------------------'
    cursor.execute('select distinct Parameter_ID, Parameter_Value FROM spapp_parameter where module_id=6 and CLIENT_ID= ' + str(
            Client_ID) + ' and Scenario_ID=' + str(Scenario_ID));
    Parameters = cursor.fetchall()

    for i in range(0, len(Parameters)):
        if Parameters[i][0] == 608:
            global Solver_Time_Limit
            Solver_Time_Limit = Parameters[i][1]
            print "Solver_Time_Limit:               ", Solver_Time_Limit
        elif Parameters[i][0] == 614:
            global First_Solution_Time_Limit
            First_Solution_Time_Limit = Parameters[i][1]
            print "First_Solution_Time_Limit:       ", First_Solution_Time_Limit
        elif Parameters[i][0] == 615:
            global Iteration_Time_Limit
            Iteration_Time_Limit = Parameters[i][1]
            print "Iteration_Time_Limit:            ", Iteration_Time_Limit
        elif Parameters[i][0] == 605:
            global ObjectiveType
            ObjectiveType = Parameters[i][1]
            print "ObjectiveType:                   ", ObjectiveType
        elif Parameters[i][0] == 610:
            global RMConstraint
            RMConstraint = Parameters[i][1]
            print "RMConstraint:                    ", RMConstraint
        elif Parameters[i][0] == 602:
            global PlanningBucket
            PlanningBucket = Parameters[i][1]
            print "PlanningBucket:                  ", PlanningBucket
        elif Parameters[i][0] == 603:
            global PlanningHorizon
            PlanningHorizon = Parameters[i][1]
            print "PlanningHorizon:                 ", PlanningHorizon
        elif Parameters[i][0] == 611:
            global LDHorizon
            LDHorizon = Parameters[i][1]
            print "LDHorizon:                       ", LDHorizon
        elif Parameters[i][0] == 612:
            global SchedulingType
            SchedulingType = Parameters[i][1]
            print "SchedulingType:                  ", SchedulingType
        elif Parameters[i][0] == 617:
            global Freeze_Time_Fence
            Freeze_Time_Fence = int(Parameters[i][1])
            print "Freeze_Time_Fence:               ", Freeze_Time_Fence
        elif Parameters[i][0] == 607:
            global Sequence_Dependent_Setup_Time
            Sequence_Dependent_Setup_Time = Parameters[i][1]
            print "Sequence_Dependent_Setup_Time:   ", Sequence_Dependent_Setup_Time


    print ' ----------------------------------- Solver App Parameters -----------------------------------------------------------'

    cursor.execute('select distinct Solver_Parameter_ID, Solver_Parameter_Value FROM spapp_solver_parameter where module_id=6 and CLIENT_ID= ' + str(Client_ID));
    Solver_Parameters = cursor.fetchall()

    for i in range(0, len(Solver_Parameters)):
        if Solver_Parameters[i][0] == 601:
            global Setuptime_with_Holiday_Inbetween
            Setuptime_with_Holiday_Inbetween = Solver_Parameters[i][1]
            print "Setuptime_with_Holiday_Inbetween: ", Setuptime_with_Holiday_Inbetween
        if Solver_Parameters[i][0] == 602:
            global SDST_Split
            SDST_Split = Solver_Parameters[i][1]
            print "SDST_Split:                       ", SDST_Split
        if Solver_Parameters[i][0] == 603:
            global Parallel_Op_End_At_End
            Parallel_Op_End_At_End = Solver_Parameters[i][1]
            print "Parallel_Op_End_At_End:           ", Parallel_Op_End_At_End
        if Solver_Parameters[i][0] == 604:
            global Parallel_Op_Start_At_Start
            Parallel_Op_Start_At_Start = Solver_Parameters[i][1]
            print "Parallel_Op_Start_At_Start:       ", Parallel_Op_Start_At_Start
        if Solver_Parameters[i][0] == 605:
            global Produce_SFG_Lot_on_FG_Lot_production
            Produce_SFG_Lot_on_FG_Lot_production = Solver_Parameters[i][1]
            print "Make_SFG_Lot_if_FG_Lot_made:      ", Produce_SFG_Lot_on_FG_Lot_production
        if Solver_Parameters[i][0] == 606:
            global Task_Split_Allowed
            Task_Split_Allowed = Solver_Parameters[i][1]
            print "Task_Split_Allowed:               ", Task_Split_Allowed
        if Solver_Parameters[i][0] == 607:
            global Local_Search
            Local_Search = Solver_Parameters[i][1]
            print "Local_Search:                     ", Local_Search



    ##  print ' ----------------------------------- Planning Start Date -----------------------------------'
    cursor.execute( 'select distinct STR_TO_DATE(Parameter_Value,"%m/%d/%Y hh:mm:ss") FROM spapp_parameter where module_id=6 and parameter_id=601 and CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID));
    global PlanningStartDate
    PlanningStartDate = cursor.fetchall()
    PlanningStartDate = PlanningStartDate[0][0]
    print 'PlanningStartDate : ', PlanningStartDate
    print 'Freeze_Time_Fence : ', Freeze_Time_Fence

    cursor.execute('select distinct PRODUCTION_ORDER_INDEX from xlps_po_solver_ready  where level <3 and  client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
        Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX')
    global Level2_available
    Level2_available = cursor.fetchall()

    global Lots_Completed
    Lots_Completed=[]
    cursor.execute('select distinct production_order_index FROM xlps_po_lots_completed where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID));
    global Lots_Completed1
    Lots_Completed1 = cursor.fetchall()

    for i in range(0,len(Lots_Completed1)):
        Lots_Completed.append(Lots_Completed1[i][0])
    print "Lots_Completed: ", Lots_Completed

    # global SDST_Split
    # global Setuptime_with_Holiday_Inbetween
    global Subsequent_Op_with_allowed_gap
    # global Parallel_Op_End_At_End
    # global Parallel_Op_Start_At_Start
    global Parallel_Op_with_allowed_gap
    # global Produce_SFG_Lot_on_FG_Lot_production
    # global Local_Search
    # global Task_Split_Allowed
    global High_Priority_Solution_Time_Limit
    # Local_Search = "Disable" # Enable local search to balance the resource loading
    # Setuptime_with_Holiday_Inbetween = "Disable" # If setup is to be maintained between two tasks with a holiday/shift off/maintenance in between them, Enable this
    # SDST_Split = "Allowed"  # If setup can be split when there is a holiday/shift off/maintenance in between two tasks and there a setup time required, Allow this
    # print 'SDST_Split with holiday in between : ', SDST_Split
    # Parallel_Op_End_At_End="Enable" # Parallel operations  should end at the same time
    # Parallel_Op_Start_At_Start="Disable" # Parallel operations  should start at the same time
    Parallel_Op_with_allowed_gap = "Disable" # Parallel operations with allowed gap
    # if len(Level2_available)>0:
    #     Produce_SFG_Lot_on_FG_Lot_production = "Enable"     # Only if FG lot is produced, then SFG lot should be produced"
    # else:
    #     Produce_SFG_Lot_on_FG_Lot_production = "Disable"  # Only if FG lot is produced, then SFG lot should be produced"

    Subsequent_Op_with_allowed_gap = "Disable"  # Subsequent operations with allowed gap
    # Task_Split_Allowed = "Enable"
    global Holding_Resource_Function
    Holding_Resource_Function = "Enable"
    Decimal_Handler=1
    High_Priority_Solution_Time_Limit = 10

    global Task_Split_Allowed_Machines
    global Task_Split_Allowed_Machines1
    Task_Split_Allowed_Machines = []

    if Task_Split_Allowed=="Enable":
        cursor.execute('select distinct ms.MACHINE_INDEX from XLPS_OPERATION_SOLVER_READY om inner join XLPS_MACHINE_SOLVER_READY ms on       \
            om.operation_id=ms.operation_id and om.client_id=ms.client_id and om.scenario_id=ms.scenario_id                \
            inner join xlps_route_solver_ready rsr on rsr.machine_index=ms.machine_index and rsr.operation_index=om.operation_index     \
            and rsr.client_id=om.client_id and rsr.scenario_id=om.scenario_id  where     \
            om.operation_ID="Compression" and om.CLIENT_ID=' + str(Client_ID) + ' and om.Scenario_ID=' + str(Scenario_ID))
        Task_Split_Allowed_Machines1 = cursor.fetchall() #Define a column in xlps_machine_solver_ready where we can specify whether task can overlap over multiple shifts
        for i in range(0,len(Task_Split_Allowed_Machines1)):
            Task_Split_Allowed_Machines.append(Task_Split_Allowed_Machines1[i][0])
        print "Task_Split_Allowed_Machines: ", Task_Split_Allowed_Machines

        cursor.execute('select distinct po.product_id, msr.machine_index, operation_setup_time+processing_time+teardown_time from xlps_route_solver_ready rsr    \
            inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX               \
            inner join xlps_machine_solver_ready msr on rsr.MACHINE_INDEX=msr.MACHINE_INDEX                \
            inner join xlps_operation_solver_ready osr on osr.OPERATION_INDEX=rsr.OPERATION_INDEX and osr.OPERATION_ID=msr.OPERATION_ID   \
            where osr.operation_id="Compression"  ');
        global PROD_MAC
        PROD_MAC = cursor.fetchall()
        global PROD_MAC_Comb
        global PROD_MAC_Time
        PROD_MAC_Comb=[]
        PROD_MAC_Time={}
        for i in range(0,len(PROD_MAC)):
            PROD_MAC_Comb.append((int(PROD_MAC[i][0]),int(PROD_MAC[i][1])))
            PROD_MAC_Time[int(PROD_MAC[i][0]),int(PROD_MAC[i][1])] = int(PROD_MAC[i][2])

        print "PROD_MAC_Comb: ", PROD_MAC_Comb

    global Ops_Renewablw_res1
    global Ops_Renewablw_res
    Ops_Renewablw_res=[]
    cursor.execute('select distinct operation_index from xlps_operation_solver_ready where Operation_ID="Compression"  and CLIENT_ID= ' + str(Client_ID) + ' \
        and Scenario_ID= ' + str(Scenario_ID))
    Ops_Renewablw_res1 = cursor.fetchall()

    for i in range(0,len(Ops_Renewablw_res1)):
        Ops_Renewablw_res.append(Ops_Renewablw_res1[i][0])

    ##print ' ----------------------------------- PlantID -----------------------------------'
    cursor.execute('select distinct location_id FROM xlps_product_master where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID));
    global PlantID
    PlantID = cursor.fetchall()
    PlantID = PlantID[0][0]
    print 'PlantID : ', PlantID

    cursor.execute('select distinct MACHINE_INDEX from XLPS_MACHINE_SOLVER_READY sr where sr.CLIENT_ID= ' + str(Client_ID) + ' \
        and sr.Scenario_ID= ' + str(Scenario_ID) + ' order by MACHINE_INDEX')
    global all_machines
    all_machines = cursor.fetchall()

    cursor.execute('select distinct OPERATION_INDEX from XLPS_MACHINE_SOLVER_READY sr \
    inner join xlps_operation_solver_ready osr on osr.operation_id=sr.operation_id and osr.client_id=sr.client_id and \
    osr.scenario_id=sr.scenario_id where  osr.OPERATION_TYPE=3 and sr.CLIENT_ID= ' + str(Client_ID) + ' \
        and sr.Scenario_ID= ' + str(Scenario_ID) + ' order by MACHINE_INDEX')
    global Form_QC_ID1
    global Form_QC_ID
    Form_QC_ID1 = cursor.fetchall()
    print  "Form_QC_ID1; ", Form_QC_ID1
    Form_QC_ID = []
    for l in range(0, len(Form_QC_ID1)):
        Form_QC_ID.append(Form_QC_ID1[l][0])
    print  "Form_QC_ID; ", Form_QC_ID

    cursor.execute('select distinct MACHINE_INDEX,AVAILABLE_START_TIME_INDEX, AVAILABLE_END_TIME_INDEX from XLPS_MACHINE_SOLVER_READY sr where sr.CLIENT_ID= ' + str(Client_ID) + ' \
        and sr.Scenario_ID= ' + str(Scenario_ID) + ' order by MACHINE_INDEX')
    all_machines_times = cursor.fetchall()
    global Machine_Avai_Start_Time
    global Machine_Avai_End_Time
    Machine_Avai_Start_Time = {}
    Machine_Avai_End_Time = {}
    for l in range(0, len(all_machines_times)):
        Machine_Avai_Start_Time[int(all_machines_times[l][0])] = int(all_machines_times[l][1])
        Machine_Avai_End_Time[int(all_machines_times[l][0])] = int(all_machines_times[l][2])

    ##  print ' ----------------------------------- POs for gobal constraint of renewable resources/tools -----------------------------------'
    cursor.execute('select po.production_order_index from xlps_po_solver_ready po inner join xlps_product_master pm on pm.PRODUCT_INDEX=po.PRODUCT_ID \
    and pm.CLIENT_ID=po.CLIENT_ID and pm.SCENARIO_ID=po.SCENARIO_ID where pm.product_id ="3002013"');
    global POs_Renewablw_res1
    global POs_Renewablw_res
    POs_Renewablw_res=[]
    POs_Renewablw_res1 = cursor.fetchall()

    for i in range(0,len(POs_Renewablw_res1)):
        POs_Renewablw_res.append(POs_Renewablw_res1[i][0])

    # #  print ' ----------------------------------- Ops requiring tools for gobal constraint of renewable resources/tools -----------------------------------'
    # cursor.execute('select operation_index from xlps_operation_solver_ready where operation_id="Compression"')
    # # global Ops_Renewablw_res1
    # # global Ops_Renewablw_res
    # Ops_Renewablw_res=[]
    # Ops_Renewablw_res1 = cursor.fetchall()
    #
    # for i in range(0,len(Ops_Renewablw_res1)):
    #     Ops_Renewablw_res.append(Ops_Renewablw_res1[i][0])

    ##  print ' ----------------------------------- Products -----------------------------------'
    cursor.execute('select distinct product_index from xlps_product_master where CLIENT_ID= ' + str(
        Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
    Products1 = cursor.fetchall()
    global Products
    Products = np.zeros([len(Products1)], dtype=int)
    for l in range(0, len(Products1)):
        Products[int(Products1[l][0])] = Products1[l][0]

    ##  print ' ----------------------------------- ProductType -----------------------------------'
    cursor.execute('select distinct product_index,product_type_id,convert(shelf_life, UNSIGNED INTEGER) from xlps_product_master where CLIENT_ID= ' + str(Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
    ProductType = cursor.fetchall()
    global Product_Type_ID
    Product_Type_ID = np.zeros([len(ProductType)], dtype=int)
    global Product_Shelf_Life
    Product_Shelf_Life = {}
    for l in range(0, len(ProductType)):
        Product_Type_ID[ProductType[l][0]] = (ProductType[l][1])
        Product_Shelf_Life[ProductType[l][0]] = (ProductType[l][2])

    ##  print ' ----------------------------------- ProductionOrders -----------------------------------'

    cursor.execute('select distinct PRODUCTION_ORDER_INDEX from xlps_po_solver_ready  where \
              client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX')

    all_pos1 = cursor.fetchall()
    global all_pos
    all_pos = []
    for l in range(0, len(all_pos1)):
        all_pos.append(int(all_pos1[l][0]))

    if len(all_pos)==0:
        f1.write(" Error: Number of production orders given are zero")
        f1.write("\n")
        f1.write(" Please check the due date of each production order and make sure it is within planning horizon given in parameters")
        f1.write("\n")
        f1.close()
        cursor.execute('update SPAPP_JOB set JOB_STATUS="ERROR", INFO="Job failed", END_TIME=NOW() where JOB_ID = ' + str(JOB_ID))
        cursor.close()
        cnx.commit()
        cnx.close()
        exit()

    cursor.execute('select distinct PRODUCTION_ORDER_INDEX from xlps_route_solver_ready  where \
        client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX')
    all_pos_from_route_table = cursor.fetchall()

    if len(all_pos)!=len(all_pos_from_route_table) :
        f1.write(" Error: Number of production orders are no tmatching from PO List and Route List")
        f1.write("\n")

    ##  print ' ----------------------------------- ProductionOrders -----------------------------------'

    cursor.execute('select distinct PRODUCTION_ORDER_INDEX from xlps_po_solver_ready  where \
             priority="High" and client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
        Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX')

    hp_pos1 = cursor.fetchall()
    global hp_pos
    hp_pos = []
    for l in range(0, len(hp_pos1)):
        hp_pos.append(int(hp_pos1[l][0]))

    print "hp_pos: ", hp_pos

    print ' ----------------------------------- Operations_Machines -----------------------------------'

    cursor.execute(
        'select distinct om.OPERATION_INDEX,om.NO_MACHINES,om.operation_type from XLPS_OPERATION_SOLVER_READY om where om.CLIENT_ID= ' + str(
            Client_ID) + ' \
            and Scenario_ID=' + str(Scenario_ID))
    Operations_Machines = cursor.fetchall()
    # print "Operations_Machines: ", Operations_Machines
    global Op_Type
    Op_Type = {}
    for l in range(0, len(Operations_Machines)):
        Op_Type[Operations_Machines[l][0]] = int(Operations_Machines[l][2])
    # print "Op_Type: ", Op_Type

    ##  print ' ----------------------------------- Operations_Machines_New -----------------------------------'

    cursor.execute('select distinct om.OPERATION_INDEX,MACHINE_INDEX from XLPS_OPERATION_SOLVER_READY om inner join XLPS_MACHINE_SOLVER_READY ms on \
            om.operation_id=ms.operation_id and om.client_id=ms.client_id and om.scenario_id=ms.scenario_id where \
            om.CLIENT_ID= ' + str(Client_ID) + ' \
            and om.Scenario_ID=' + str(Scenario_ID))
    global Operations_Machines_New
    Operations_Machines_New = cursor.fetchall()

    ##  print ' ----------------------------------- Operations_Machines -----------------------------------'

    cursor.execute('select PRODUCTION_ORDER_INDEX,operation_index,ROUTING_ID,count(alternate_index) from xlps_route_solver_ready  \
            where alternate_index != -1 and \
              client_id= ' + str(Client_ID) + ' and Scenario_ID= ' + str(
        Scenario_ID) + ' group by PRODUCTION_ORDER_INDEX,operation_index,ROUTING_ID ')
    global op_op_ar
    op_op_ar = cursor.fetchall()

    currenttime = datetime.datetime.now()
    print "Step0:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------- Route_Master-----------------------------------'
    cursor.execute('select distinct sr.PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ROUTING_ID,SEQUENCE_ID,ALTERNATE_INDEX,MACHINE_INDEX,OPERATION_SETUP_TIME, \
                   PROCESSING_TIME, TEARDOWN_TIME, ORDER_QUANTITY, MIN_BATCH_SIZE, MAX_BATCH_SIZE, sr.OUTPUT_PRODUCT_INDEX \
                     From XLPS_ROUTE_SOLVER_READY sr inner join XLPS_PO_SOLVER_READY po on po.PRODUCTION_ORDER_INDEX = sr.PRODUCTION_ORDER_INDEX \
                    and sr.CLIENT_ID = po.CLIENT_ID and sr.Scenario_ID=po.Scenario_ID where sr.CLIENT_ID= ' + str(
        Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
                   order by sr.PRODUCTION_ORDER_INDEX ');
    global Sequence_Time
    Sequence_Time = cursor.fetchall()
    # print Sequence_Time
    global Machine_ID
    global Order_Quantity
    global Machine_ID
    global OP_Setup_Time
    global Processing_Time
    global Teardown_Time
    global Sequence_ID
    global Min_Batch_Size
    Machine_ID = {}
    OP_Setup_Time = {}
    Processing_Time = {}
    Teardown_Time = {}
    Sequence_ID = {}
    Order_Quantity = {}
    global Max_Batch_Size
    Min_Batch_Size = {}
    Max_Batch_Size = {}
    global Product_Index
    Product_Index = {}
    for l in range(0, len(Sequence_Time)):
        Machine_ID[
            int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
                Sequence_Time[l][4])] = int(Sequence_Time[l][5])
        OP_Setup_Time[
            int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
                Sequence_Time[l][4])] = int(Sequence_Time[l][6])
        Processing_Time[
            int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
                Sequence_Time[l][4])] = Sequence_Time[l][7]
        Teardown_Time[
            int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(
                Sequence_Time[l][4])] = int(Sequence_Time[l][8])
        Sequence_ID[int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2])] = int(Sequence_Time[l][3])
        Order_Quantity[int(Sequence_Time[l][0])] = int(Sequence_Time[l][9])
        Min_Batch_Size[int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][4])] = int(Sequence_Time[l][10]*Decimal_Handler)
        Max_Batch_Size[int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][4])] = int(Sequence_Time[l][11]*Decimal_Handler)
        Product_Index[int(Sequence_Time[l][0]), int(Sequence_Time[l][1]), int(Sequence_Time[l][2]), int(Sequence_Time[l][3]), int(Sequence_Time[l][4])] = int(Sequence_Time[l][12])

    ##  print ' ----------------------------------- Route_Master2-----------------------------------'
    cursor.execute('select distinct sr.PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ALTERNATE_INDEX,MACHINE_INDEX \
                     From XLPS_ROUTE_SOLVER_READY sr inner join XLPS_PO_SOLVER_READY po on po.PRODUCTION_ORDER_INDEX = sr.PRODUCTION_ORDER_INDEX \
                    and sr.CLIENT_ID = po.CLIENT_ID and sr.Scenario_ID=po.Scenario_ID where sr.CLIENT_ID= ' + str(
        Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
                   order by sr.PRODUCTION_ORDER_INDEX');
    Sequence_Time2 = cursor.fetchall()

    cursor.execute('select distinct sr.PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ALTERNATE_INDEX \
                    From XLPS_ROUTE_SOLVER_READY sr inner join XLPS_PO_SOLVER_READY po on po.PRODUCTION_ORDER_INDEX = sr.PRODUCTION_ORDER_INDEX \
                    and sr.CLIENT_ID = po.CLIENT_ID and sr.Scenario_ID=po.Scenario_ID where sr.CLIENT_ID= ' + str(
        Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
                   order by sr.PRODUCTION_ORDER_INDEX');
    global PO_TASK_ALT_COMB
    PO_TASK_ALT_COMB = cursor.fetchall()

    currenttime = datetime.datetime.now()
    print "Step1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')
    global Machine_ID2
    Machine_ID2 = {}
    for l in range(0, len(Sequence_Time2)):
        Machine_ID2[int(Sequence_Time2[l][0]), int(Sequence_Time2[l][1]), int(Sequence_Time2[l][2])] = int(
            Sequence_Time2[l][3])

    ##  print ' ----------------------------------- PO_AR_COMB -----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, ROUTING_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
                   where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
            and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,ROUTING_ID ');
    global PO_AR_COMB
    PO_AR_COMB = cursor.fetchall()

    ##  print ' ----------------------------------- PO_TASK_AR_SEQ_ALT_COMB-----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX From XLPS_ROUTE_SOLVER_READY_NEW rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
                   where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
            and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by rsr.level, po.PRODUCTION_ORDER_INDEX,SEQUENCE_ID'); # po.PRODUCTION_ORDER_ID
    global PO_TASK_AR_SEQ_ALT_COMB
    PO_TASK_AR_SEQ_ALT_COMB = cursor.fetchall()

          ##  print ' ----------------------------------- PO_TASK_AR_SEQ_ALT_TASK_SPLIT_COMB-----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, rsr.OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, rsr.ALTERNATE_INDEX,msr.MACHINE_INDEX \
                   From XLPS_ROUTE_SOLVER_READY_NEW rsr inner join xlps_po_solver_ready po      \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id  \
			inner join xlps_machine_solver_ready msr on rsr.MACHINE_INDEX=msr.MACHINE_INDEX and msr.ALTERNATE_INDEX=rsr.ALTERNATE_INDEX  \
            and rsr.client_id=msr.client_id and rsr.scenario_id=msr.scenario_id                    \
            inner join xlps_operation_solver_ready osr on osr.OPERATION_INDEX=rsr.OPERATION_INDEX and osr.OPERATION_ID=msr.OPERATION_ID     \
            and rsr.client_id=osr.client_id and rsr.scenario_id=osr.scenario_id         \
            where osr.operation_id="Compression" and rsr.CLIENT_ID= ' + str(Client_ID) + ' \
            and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by rsr.level, po.PRODUCTION_ORDER_INDEX,msr.MACHINE_INDEX');
    global PO_TASK_AR_SEQ_ALT_TASK_SPLIT_COMB
    PO_TASK_AR_SEQ_ALT_TASK_SPLIT_COMB = cursor.fetchall()

    print ' ----------------------------------- PRECEDENCE_CONST-----------------------------------'
    cursor.execute('select distinct rsr.PRODUCTION_ORDER_INDEX, rsr.OPERATION_INDEX,rsr1.OPERATION_INDEX, rsr.ROUTING_ID, \
        rsr.SEQUENCE_ID,rsr1.SEQUENCE_ID, rsr.ALTERNATE_INDEX,rsr1.ALTERNATE_INDEX From XLPS_ROUTE_SOLVER_READY_NEW rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
        inner join XLPS_ROUTE_SOLVER_READY_NEW rsr1 on rsr.PRODUCTION_ORDER_INDEX=rsr1.PRODUCTION_ORDER_INDEX and rsr.routing_id=rsr1.routing_id \
        and rsr1.sequence_id=rsr.sequence_id+1 and rsr.client_id=rsr1.client_id and rsr.scenario_id=rsr1.scenario_id  where rsr.CLIENT_ID= ' + str(
        Client_ID) + '   \
        and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by  rsr.PRODUCTION_ORDER_INDEX,rsr.SEQUENCE_ID');
    global PRECEDENCE_CONST_COMB
    PRECEDENCE_CONST_COMB = cursor.fetchall()
    ##print "PRECEDENCE_CONST_COMB: ", PRECEDENCE_CONST_COMB

    print ' ----------------------------------- Parallel_Op_Gap -----------------------------------'
    cursor.execute('select distinct rsr.PRODUCTION_ORDER_INDEX, rsr.OPERATION_INDEX,rsr1.OPERATION_INDEX, rsr.ROUTING_ID, \
        rsr.SEQUENCE_ID, rsr.ALTERNATE_INDEX,rsr1.ALTERNATE_INDEX From XLPS_ROUTE_SOLVER_READY_NEW rsr inner join xlps_po_solver_ready po  \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id           \
        inner join XLPS_ROUTE_SOLVER_READY_NEW rsr1 on rsr.PRODUCTION_ORDER_INDEX=rsr1.PRODUCTION_ORDER_INDEX and rsr.routing_id=rsr1.routing_id      \
        and rsr1.sequence_id=rsr.sequence_id and rsr.OPERATION_INDEX<rsr1.OPERATION_INDEX and rsr.client_id=rsr1.client_id and rsr.scenario_id=rsr1.scenario_id  \
        where rsr.CLIENT_ID =' + str(Client_ID) + ' and rsr.Scenario_ID=' + str(Scenario_ID) + '  order by  rsr.PRODUCTION_ORDER_INDEX,rsr.SEQUENCE_ID  ');

    global Parallel_Op_Gap
    Parallel_Op_Gap = cursor.fetchall()
    Parallel_Op_Gap_Comb = []
    for l in range(0, len(Parallel_Op_Gap)):
        Parallel_Op_Gap_Comb.append((int(Parallel_Op_Gap[l][0]), int(Parallel_Op_Gap[l][4])))
        Parallel_Op_Gap_Comb=list(set(Parallel_Op_Gap_Comb))

    # print "Parallel_Op_Gap_Comb: ", Parallel_Op_Gap_Comb

    print ' ----------------------------------- SHELF_LIFE_CONST-----------------------------------'
    cursor.execute('select distinct rsr.PRODUCTION_ORDER_INDEX, rsr.OPERATION_INDEX,rsr1.OPERATION_INDEX, rsr.ROUTING_ID, \
        rsr.SEQUENCE_ID,rsr1.SEQUENCE_ID, rsr.ALTERNATE_INDEX,rsr1.ALTERNATE_INDEX, sl.OUTPUT_PRODUCT_INDEX From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
        inner join XLPS_ROUTE_SOLVER_READY rsr1 on rsr.PRODUCTION_ORDER_INDEX=rsr1.PRODUCTION_ORDER_INDEX and rsr.routing_id=rsr1.routing_id \
            and rsr.client_id=rsr1.client_id and rsr.scenario_id=rsr1.scenario_id  \
        inner join xlps_shelf_life2 sl on sl.production_order_index=rsr.production_order_index and sl.min_sequence=rsr.SEQUENCE_ID \
        and sl.max_sequence=rsr1.SEQUENCE_ID and rsr.CLIENT_ID=sl.client_id and rsr.SCENARIO_ID=sl.scenario_id \
        where rsr.CLIENT_ID=' + str(Client_ID) + ' and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by  rsr.SEQUENCE_ID');

    SHELF_LIFE_COMB1 = cursor.fetchall()

    global SHELF_LIFE_COMB2
    global SHELF_LIFE_COMB
    SHELF_LIFE_COMB2 = []
    for l in range(0, len(SHELF_LIFE_COMB1)):
        SHELF_LIFE_COMB2.append((int(SHELF_LIFE_COMB1[l][0]), int(SHELF_LIFE_COMB1[l][1]), int(SHELF_LIFE_COMB1[l][2]),
                                 int(SHELF_LIFE_COMB1[l][3]), int(SHELF_LIFE_COMB1[l][4]), int(SHELF_LIFE_COMB1[l][5]),
                                 int(SHELF_LIFE_COMB1[l][6]), int(SHELF_LIFE_COMB1[l][7])))

    SHELF_LIFE_COMB = set(SHELF_LIFE_COMB2)
    global Shelf_Life_Prod_Index
    Shelf_Life_Prod_Index = {}
    for l in range(0, len(SHELF_LIFE_COMB1)):
        Shelf_Life_Prod_Index[int(SHELF_LIFE_COMB1[l][0]), int(SHELF_LIFE_COMB1[l][1]), int(SHELF_LIFE_COMB1[l][2]), int(
            SHELF_LIFE_COMB1[l][3]), int(SHELF_LIFE_COMB1[l][4]), int(SHELF_LIFE_COMB1[l][5]), int(
            SHELF_LIFE_COMB1[l][6]), int(SHELF_LIFE_COMB1[l][7])] = int(SHELF_LIFE_COMB1[l][8])

    ##print "SHELF_LIFE_COMB: ", SHELF_LIFE_COMB
    currenttime = datetime.datetime.now()
    print "Step2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------- PO_AR_SEQ_COMB-----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, ROUTING_ID, SEQUENCE_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
                   where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
            and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX, SEQUENCE_ID');
    global PO_AR_SEQ_COMB
    PO_AR_SEQ_COMB = cursor.fetchall()

    ##  print ' ----------------------------------- PO_AR_SEQ_OUTP_COMB-----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, rsr.operation_index, rsr.ROUTING_ID, SEQUENCE_ID, bm.OUTPUT_PRODUCT_INDEX From XLPS_ROUTE_SOLVER_READY_NEW rsr \
    inner join xlps_po_solver_ready po on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id  \
    inner join xlps_bom bm on bm.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and bm.ROUTING_ID=rsr.ROUTING_ID \
    and bm.OPERATION_INDEX=rsr.OPERATION_INDEX and rsr.ALTERNATE_INDEX=bm.ALTERNATE_INDEX and rsr.CLIENT_ID=bm.client_id and rsr.SCENARIO_ID=bm.SCENARIO_ID         \
                   where rsr.CLIENT_ID= ' + str(Client_ID) + '   and rsr.Scenario_ID=' + str(
        Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX, SEQUENCE_ID');
    global PO_TASK_AR_SEQ_OUTP_COMB
    PO_TASK_AR_SEQ_OUTP_COMB = cursor.fetchall()

    ##  print ' ----------------------------------- PO_TASK_AR_SEQ_COMB-----------------------------------'
    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
            where rsr.CLIENT_ID= ' + str(Client_ID) + ' \
            and rsr.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,SEQUENCE_ID');
    global PO_TASK_AR_SEQ_COMB
    PO_TASK_AR_SEQ_COMB = cursor.fetchall()

    ##  print ' ----------------------------------- PO_BOM_COMB -----------------------------------'

    cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, ROUTING_ID,alternate_index, INPUT_PRODUCT_INDEX,bm.OUTPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
          where bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ');
    global PO_BOM_COMB
    PO_BOM_COMB = cursor.fetchall()
    # print "PO_BOM_COMB: ", PO_BOM_COMB
    print ' ----------------------------------- Alternate_BOM -----------------------------------'

    cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, ROUTING_ID, alternate_index, INPUT_PRODUCT_INDEX,bm.OUTPUT_PRODUCT_INDEX, \
                bm.alternate_bom_flag from xlps_bom bm inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
          where bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
        Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ');
    global Alternate_BOM
    Alternate_BOM = cursor.fetchall()
    ##Alternate_BOM={}

    # print "Alternate_BOM: ", Alternate_BOM
    global Alt_BOM_Comb
    global Alt_BOM_Flag
    Alt_BOM_Comb = []
    Alt_BOM_Flag = {}
    for l in range(0, len(Alternate_BOM)):
        Alt_BOM_Flag[
            int(Alternate_BOM[l][0]), int(Alternate_BOM[l][1]), int(Alternate_BOM[l][2]), int(Alternate_BOM[l][3]), int(
                Alternate_BOM[l][4]), int(Alternate_BOM[l][5])] = int(Alternate_BOM[l][6])
        Alt_BOM_Comb.append((int(Alternate_BOM[l][0]), int(Alternate_BOM[l][1]), int(Alternate_BOM[l][2]),
                             int(Alternate_BOM[l][3]), int(Alternate_BOM[l][4]), int(Alternate_BOM[l][5])))

    print ' ----------------------------------- Alt_BOM_Start_Value -----------------------------------'

    cursor.execute(
        'select distinct production_order_index, ALTERNATE_BOM_FLAG from xlps_bom where ALTERNATE_BOM_FLAG mod 10 = 1 and client_id= ' + str(
            Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX ');
    global Alt_BOM_Start_Value
    Alt_BOM_Start_Value = cursor.fetchall()

    # print "Alt_BOM_Start_Value: ", Alt_BOM_Start_Value

    print ' ----------------------------------- Alternate_BOM_COMB -----------------------------------'

    cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, bm.OUTPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
          where  bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
        Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX');
    global Alternate_BOM_COMB
    Alternate_BOM_COMB = cursor.fetchall()
    ##Alternate_BOM_COMB={}

    print ' ----------------------------------- Alternate_BOM_Products -----------------------------------'

    cursor.execute('select distinct INPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
          where bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX');
    global Alternate_BOM_Products1
    Alternate_BOM_Products1 = cursor.fetchall()
    ##Alternate_BOM_Products1={}
    global Alternate_BOM_Products
    Alternate_BOM_Products = []
    for l in range(0, len(Alternate_BOM_Products1)):
        Alternate_BOM_Products.append(int(Alternate_BOM_Products1[l][0]))

    # print "Alternate_BOM_Products: ", Alternate_BOM_Products

    print ' ----------------------------------- PO_Alternate_BOM_Products -----------------------------------'
    cursor.execute('  select distinct po.production_order_index, INPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id  \
          where bm.Alternate_bom_flag > 0 and bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID=' + str(Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ');
    global po_products_alt_bom_comb
    po_products_alt_bom_comb = cursor.fetchall()

    currenttime = datetime.datetime.now()
    print "Step3:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    if Sequence_Dependent_Setup_Time == "Include":

        cursor.execute('select production_order_index,product_id  from  xlps_po_solver_ready  where  CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))
        po_products_comb = cursor.fetchall()

        cursor.execute('select distinct MACHINE_INDEX, product_from, product_to, convert(SETUP_TIME, UNSIGNED INTEGER) \
                                From XLPS_SETUP_SOLVER_READY  where  CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))
        setup_all_data = cursor.fetchall()
        global setup_all_data_comb
        setup_all_data_comb=[]
        for l in range(0,len(setup_all_data)):
            setup_all_data_comb.append((setup_all_data[l][0],setup_all_data[l][1],setup_all_data[l][2]))
        currenttime = datetime.datetime.now()
        print "Step3_1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')
        SETUP_TIME_NEW0 = []
        SETUP_TIME_NEW = []
        SETUP_TIME_NEW2 = []
        SETUP_TIME_NEW3 = []
        for s in range(0, len(setup_all_data)):
            # if int(setup_all_data[s][0])==1:
            for p in range(0, len(po_products_comb)):
                if int(setup_all_data[s][1]) == int(po_products_comb[p][1]):
                    for p1 in range(0, len(po_products_comb)):
                        if int(setup_all_data[s][2]) == int(po_products_comb[p1][1]) and int(po_products_comb[p][0]) != int(po_products_comb[p1][0]):
                            SETUP_TIME_NEW0.append((setup_all_data[s][0], po_products_comb[p][0], po_products_comb[p1][0],setup_all_data[s][3]))
                            if int(po_products_comb[p][0]) < int(po_products_comb[p1][0]):
                                if (setup_all_data[s][0],po_products_comb[p1][1], po_products_comb[p][1]) in setup_all_data_comb:
                                    SETUP_TIME_NEW.append((setup_all_data[s][0], po_products_comb[p][0], po_products_comb[p1][0],setup_all_data[s][3]))
                                SETUP_TIME_NEW3.append((setup_all_data[s][0], po_products_comb[p][0],po_products_comb[p1][0], setup_all_data[s][3]))
                            if int(po_products_comb[p][0]) > int(po_products_comb[p1][0]):
                                SETUP_TIME_NEW2.append((setup_all_data[s][0], po_products_comb[p][0],po_products_comb[p1][0], setup_all_data[s][3]))

        currenttime = datetime.datetime.now()
        print "Step3_2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        global SETUP_TIME_NEW_COMB_ALL
        SETUP_TIME_NEW_COMB_ALL = []

        for l in range(0, len(SETUP_TIME_NEW0)):
            SETUP_TIME_NEW_COMB_ALL.append((int(SETUP_TIME_NEW0[l][0]), int(SETUP_TIME_NEW0[l][1]), int(SETUP_TIME_NEW0[l][2])))
        ##  SETUP_TIME_NEW_COMB_ALL = set(SETUP_TIME_NEW_COMB_ALL)

        global SDST
        SDST = {}
        print 100

        for l in range(0, len(SETUP_TIME_NEW_COMB_ALL)):
            SDST[int(SETUP_TIME_NEW_COMB_ALL[l][0]), int(SETUP_TIME_NEW_COMB_ALL[l][1]), int(SETUP_TIME_NEW_COMB_ALL[l][2])] = 0

        for l in range(0, len(SETUP_TIME_NEW0)):
            SDST[int(SETUP_TIME_NEW0[l][0]), int(SETUP_TIME_NEW0[l][1]), int(SETUP_TIME_NEW0[l][2])] = int(SETUP_TIME_NEW0[l][3])

        currenttime = datetime.datetime.now()
        print "Step3_1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        global SETUP_TIME_NEW_COMB
        SETUP_TIME_NEW_COMB = []
        for l in range(0, len(SETUP_TIME_NEW)):
            SETUP_TIME_NEW_COMB.append((int(SETUP_TIME_NEW[l][0]), int(SETUP_TIME_NEW[l][1]), int(SETUP_TIME_NEW[l][2])))

        SETUP_TIME_NEW_COMB = set(SETUP_TIME_NEW_COMB)

        currenttime = datetime.datetime.now()
        print "Step3_2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        print ' ----------------------------------- SETUP_TIME_NEW2 -----------------------------------'

        global SETUP_TIME_NEW_COMB2
        SETUP_TIME_NEW_COMB2 = []

        for l in range(0, len(SETUP_TIME_NEW2)):
            SETUP_TIME_NEW_COMB2.append((int(SETUP_TIME_NEW2[l][0]), int(SETUP_TIME_NEW2[l][1]), int(SETUP_TIME_NEW2[l][2])))

        SETUP_TIME_NEW_COMB2 = set(SETUP_TIME_NEW_COMB2)

        currenttime = datetime.datetime.now()
        print "Step3_3:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        print ' ----------------------------------- SETUP_TIME_NEW3 -----------------------------------'
        #
        # cursor.execute('select distinct st.MACHINE_INDEX, po.production_order_index,po1.production_order_index \
        # From XLPS_SETUP_SOLVER_READY st  \
        # inner join xlps_po_products po on po.product_index=st.product_from and po.client_id=st.client_id and po.scenario_id=st.scenario_id \
        # inner join xlps_po_products po1 on po1.product_index=st.product_to and po1.client_id=st.client_id and po1.scenario_id=st.scenario_id  \
        # where st.CLIENT_ID=3  and st.CLIENT_ID= ' + str(Client_ID)+ ' and st.Scenario_ID=' + str(Scenario_ID) + ' and po.production_order_index<po1.production_order_index')
        #
        # SETUP_TIME_NEW3 = cursor.fetchall()
        global SETUP_TIME_NEW_COMB3
        SETUP_TIME_NEW_COMB3 = []

        for l in range(0, len(SETUP_TIME_NEW3)):
            SETUP_TIME_NEW_COMB3.append((int(SETUP_TIME_NEW3[l][0]), int(SETUP_TIME_NEW3[l][1]), int(SETUP_TIME_NEW3[l][2])))

        SETUP_TIME_NEW_COMB3 = set(SETUP_TIME_NEW_COMB3)

        currenttime = datetime.datetime.now()
        print "Step3_4:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

        print ' ----------------------------------- SETUP_MACHINES-----------------------------------'

        cursor.execute('select distinct MACHINE_INDEX from XLPS_SETUP_SOLVER_READY st where st.CLIENT_ID= ' + str(
            Client_ID) + ' and st.Scenario_ID=' + str(Scenario_ID))
        global SETUP_MACHINES
        SETUP_MACHINES = cursor.fetchall()

        ##for l in range(0,len(SETUP_MACHINES)):
        ##  print SETUP_MACHINES[l][0]

    print ' ----------------------------------- ORDER_DUE_DATE-----------------------------------'
    cursor.execute('select distinct PRODUCTION_ORDER_INDEX, ORDER_DUE_DATE_INDEX, PRIORITY, PRODUCT_ID From XLPS_PO_SOLVER_READY \
    where CLIENT_ID= ' + str(Client_ID) + ' \
        and Scenario_ID=' + str(Scenario_ID) + ' order by PRODUCTION_ORDER_INDEX ');
    global ORDER_DUE_DATE
    ORDER_DUE_DATE = cursor.fetchall()

    # print "ORDER_DUE_DATE: ", ORDER_DUE_DATE

    print ' ----------------------------------- MACHINE_DETAILS-----------------------------------'
    cursor.execute('select distinct MACHINE_INDEX, OPERATION_INDEX, ALTERNATE_INDEX, AVAILABLE_START_TIME_INDEX, AVAILABLE_END_TIME_INDEX From \
                    XLPS_MACHINE_SOLVER_READY msd inner join XLPS_OPERATION_SOLVER_READY osd on msd.OPERATION_ID=osd.OPERATION_ID and \
                    msd.CLIENT_ID=osd.CLIENT_ID and msd.Scenario_ID=osd.Scenario_ID where msd.CLIENT_ID= ' + str(
        Client_ID) + ' and msd.Scenario_ID=' + str(Scenario_ID))
    Machine_Details = cursor.fetchall()
    global Machine_Available_Start_Time
    global Machine_Available_End_Time
    # Machine_Available_Start_Time = np.zeros([len(Machine_Details), len(Machine_Details)], dtype=int)
    # Machine_Available_End_Time = np.zeros([len(Machine_Details), len(Machine_Details)], dtype=int)
    Machine_Available_Start_Time={}
    Machine_Available_End_Time={}
    ##Max_Batch_Size={}
    for l in range(0, len(Machine_Details)):
        Machine_Available_Start_Time[int(Machine_Details[l][1]), int(Machine_Details[l][2])] = int(Machine_Details[l][3])
        Machine_Available_End_Time[int(Machine_Details[l][1]), int(Machine_Details[l][2])] = int(Machine_Details[l][4])
    ##  Max_Batch_Size[int(Machine_Details[l][1]),int(Machine_Details[l][2])] = int(Machine_Details[l][5])

    ##  print ' ----------------------------------- MACHINE_SHIFT_DETAILS-----------------------------------'
    cursor.execute('select sr.MACHINE_INDEX, SHIFT_ID, START_TIME_INDEX, END_TIME_INDEX From xlps_machine_valid_ranges vr \
                inner join XLPS_MACHINE_SOLVER_READY sr on vr.machine_id=sr.machine_id and sr.CLIENT_ID=vr.CLIENT_ID and vr.scenario_id=sr.scenario_id \
                where sr.CLIENT_ID=' + str(Client_ID) + ' and sr.Scenario_ID=' + str(Scenario_ID) + ' \
                order by  sr.MACHINE_INDEX, START_TIME_INDEX')
    global Machine_Shift_Details
    Machine_Shift_Details = cursor.fetchall()

    # print "Machine_Shift_Details : ", Machine_Shift_Details
    ##  print ' ----------------------------------- MACHINE_SHUTDOWN_DETAILS -----------------------------------'
    cursor.execute('select distinct msd.MACHINE_INDEX, ms.SHUTDOWN_START_TIME_INDEX, ms.SHUTDOWN_END_TIME_INDEX From XLPS_MACHINE_SOLVER_READY \
                    msd inner join XLPS_OPERATION_SOLVER_READY osd on msd.OPERATION_ID=osd.OPERATION_ID and msd.CLIENT_ID=osd.CLIENT_ID \
                    and msd.Scenario_ID=osd.Scenario_ID inner join XLPS_Machine_Shutdown ms on  ms.Machine_ID=msd.Machine_ID and \
                    msd.CLIENT_ID=ms.CLIENT_ID and msd.Scenario_ID=ms.Scenario_ID where msd.CLIENT_ID = ' + str(Client_ID) + ' \
                    and msd.Scenario_ID=' + str(Scenario_ID) + ' union \
                   select distinct ms.MACHINE_INDEX, ms.HOLIDAY_START_TIME_INDEX, ms.HOLIDAY_END_TIME_INDEX From XLPS_MACHINE_HOLIDAY ms \
                   where ms.CLIENT_ID = ' + str(Client_ID) + ' and ms.Scenario_ID=' + str(Scenario_ID))
    global Machine_Shutdown_Details
    Machine_Shutdown_Details = cursor.fetchall()
    # print "Machine_Shutdown_Details : ", Machine_Shutdown_Details
    ##  print ' ----------------------------------- BOM_DETAILS-----------------------------------'

    cursor.execute('select distinct OUTPUT_PRODUCT_INDEX, INPUT_PRODUCT_INDEX, OPERATION_INDEX, attach_rate, yield from xlps_bom  where ALTERNATE_INDEX <>-1 and \
                 CLIENT_ID= ' + str(Client_ID) + ' and scenario_id= ' + str(Scenario_ID))
    global BOM_DETAILS
    BOM_DETAILS = cursor.fetchall()

    currenttime = datetime.datetime.now()
    print "Step4:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------- PO_AR_BOM_COMB -----------------------------------'

    cursor.execute('select distinct po.production_order_index, OPERATION_INDEX,routing_id, alternate_index, INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT_INDEX from xlps_bom bm inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
          where bm.client_id=' + str(Client_ID) + ' and bm.Scenario_ID= ' + str(
        Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,routing_id ');
    global PO_AR_BOM_COMB
    PO_AR_BOM_COMB = cursor.fetchall()

    currenttime = datetime.datetime.now()
    print "Step4_1:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------- MAT_BALANCE_CONST -----------------------------------'
    cursor.execute('select distinct sr.production_order_index, bm.operation_index, bm1.operation_index, sr.routing_ID,sr.sequence_id,sr1.sequence_id, \
          bm1.input_product_index from xlps_bom bm inner join xlps_route_solver_ready sr on bm.production_order_index=sr.production_order_index \
          and sr.operation_index=bm.operation_index and sr.alternate_index=bm.alternate_index and bm.routing_id=sr.routing_id and bm.client_id=sr.client_id \
          and bm.scenario_id=sr.scenario_id inner join xlps_bom bm1 on bm.PRODUCTION_ORDER_INDEX=bm1.PRODUCTION_ORDER_INDEX and \
          bm1.INPUT_PRODUCT_INDEX=bm.OUTPUT_PRODUCT_INDEX and bm.ROUTING_ID=bm1.routing_id and bm.client_id=bm1.client_id and bm.scenario_id=bm1.scenario_id \
          inner join xlps_route_solver_ready sr1 on bm1.production_order_index=sr1.production_order_index and sr1.routing_id=bm1.routing_id \
          and sr1.operation_index=bm1.operation_index and sr1.alternate_index=bm1.alternate_index and bm1.client_id=sr1.client_id and bm1.scenario_id=sr1.scenario_id \
          and sr.sequence_id=sr1.sequence_id - 1 inner join xlps_po_solver_ready po on po.production_order_index=sr1.production_order_index and po.client_id=sr1.client_id \
          and po.scenario_id=sr1.scenario_id where sr.client_id=' + str(Client_ID) + ' and sr.scenario_id= ' + str(
        Scenario_ID) + ' and sr.alternate_index<>-1 and sr1.alternate_index<>-1 \
          order by sr.production_order_index')
    global MAT_BALANCE_CONST
    MAT_BALANCE_CONST = cursor.fetchall()

    currenttime = datetime.datetime.now()
    print "Step4_2:  %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##print ' ----------------------------------- PO_TASK_AR_SEQ_ALT_IP_COMB -----------------------------------'

    cursor.execute('select distinct sr.production_order_index, sr.operation_index, sr.routing_id,sr.sequence_id, bm.alternate_index, \
    bm.input_product_index from xlps_bom bm inner join xlps_route_solver_ready sr on bm.production_order_index=sr.production_order_index \
    and sr.operation_index=bm.operation_index and sr.alternate_index=bm.alternate_index and bm.client_id=sr.client_id and bm.scenario_id=sr.scenario_id \
    inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
    where sr.client_id=' + str(Client_ID) + ' and sr.scenario_id= ' + str(
        Scenario_ID) + ' and sr.alternate_index<>-1 order by  sr.production_order_index,sr.sequence_id')
    global PO_TASK_AR_SEQ_ALT_IP_COMB
    PO_TASK_AR_SEQ_ALT_IP_COMB = cursor.fetchall()

    cursor.execute('select distinct sr.production_order_index, sr.operation_index, sr.routing_ID,sr.sequence_id,bm.input_product_index, \
    bm.output_product_index from xlps_bom bm inner join xlps_route_solver_ready sr on bm.production_order_index=sr.production_order_index \
    and sr.operation_index=bm.operation_index and sr.alternate_index=bm.alternate_index and bm.client_id=sr.client_id and bm.scenario_id=sr.scenario_id \
    where sr.client_id=' + str(Client_ID) + ' and sr.scenario_id= ' + str(
        Scenario_ID) + ' and sr.alternate_index<>-1 order by sr.production_order_index ,sr.sequence_id')
    IP_OP_COMB = cursor.fetchall()

    PO_TASK_AR_SEQ_OP_COMB1 = []
    PO_TASK_AR_SEQ_IP_COMB1 = []

    for l in range(0, len(IP_OP_COMB)):
        PO_TASK_AR_SEQ_OP_COMB1.append((int(IP_OP_COMB[l][0]), int(IP_OP_COMB[l][1]), int(IP_OP_COMB[l][2]),
                                        int(IP_OP_COMB[l][3]), int(IP_OP_COMB[l][5])))
        PO_TASK_AR_SEQ_IP_COMB1.append((int(IP_OP_COMB[l][0]), int(IP_OP_COMB[l][1]), int(IP_OP_COMB[l][2]),
                                        int(IP_OP_COMB[l][3]), int(IP_OP_COMB[l][4])))
    global PO_TASK_AR_SEQ_OP_COMB
    PO_TASK_AR_SEQ_OP_COMB = set(PO_TASK_AR_SEQ_OP_COMB1)
    PO_TASK_AR_SEQ_IP_COMB = set(PO_TASK_AR_SEQ_IP_COMB1)

    currenttime = datetime.datetime.now()
    print "Step5:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    ##  print ' ----------------------------------- PO_AR_BOM_IP_QTY -----------------------------------'

    cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, routing_id,alternate_index, INPUT_PRODUCT_INDEX,1 from xlps_bom  bm \
        inner join xlps_po_products po on po.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.INPUT_PRODUCT_INDEX=po.product_index \
        and bm.client_id=po.client_id and bm.SCENARIO_ID=po.scenario_id \
        inner join xlps_po_solver_ready po1 \
            on po1.production_order_index=bm.production_order_index and po1.client_id=bm.client_id and po1.scenario_id=bm.scenario_id \
        where po.client_id=' + str(Client_ID) + '  and po.Scenario_ID= ' + str(
        Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX ,routing_id ');
    PO_AR_BOM_IP_QTY = cursor.fetchall()
    global PO_AR_BOM_IP_COMB
    PO_AR_BOM_IP_COMB = []
    for l in range(0, len(PO_AR_BOM_IP_QTY)):
        PO_AR_BOM_IP_COMB.append([int(PO_AR_BOM_IP_QTY[l][0]), int(PO_AR_BOM_IP_QTY[l][1]), int(PO_AR_BOM_IP_QTY[l][2]),
                                  int(PO_AR_BOM_IP_QTY[l][3]), int(PO_AR_BOM_IP_QTY[l][4])])

    ##  print ' ----------------------------------- PO_AR_BOM_OP_QTY -----------------------------------'

    cursor.execute('select distinct po.production_order_index, OPERATION_INDEX, routing_id,alternate_index, OUTPUT_PRODUCT_INDEX,1 from xlps_bom  bm \
        inner join xlps_po_products po on po.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.OUTPUT_PRODUCT_INDEX=po.product_index \
        and bm.client_id=po.client_id and bm.SCENARIO_ID=po.scenario_id \
        inner join xlps_po_solver_ready po1 \
            on po1.production_order_index=bm.production_order_index and po1.client_id=bm.client_id and po1.scenario_id=bm.scenario_id \
        where  po.client_id=' + str(Client_ID) + '  and po.Scenario_ID= ' + str(
        Scenario_ID) + ' order by po.PRODUCTION_ORDER_INDEX,routing_id ');
    PO_AR_BOM_OP_QTY = cursor.fetchall()
    global PO_AR_BOM_OP_COMB
    PO_AR_BOM_OP_COMB = []
    for l in range(0, len(PO_AR_BOM_OP_QTY)):
        PO_AR_BOM_OP_COMB.append([int(PO_AR_BOM_OP_QTY[l][0]), int(PO_AR_BOM_OP_QTY[l][1]), int(PO_AR_BOM_OP_QTY[l][2]),
                                  int(PO_AR_BOM_OP_QTY[l][3]), int(PO_AR_BOM_OP_QTY[l][4])])

    cursor.execute('select distinct production_order_index, OPERATION_INDEX, routing_id, OUTPUT_PRODUCT_INDEX,1 from xlps_op_output_count \
        where client_id=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID) + ' group by production_order_index, \
        OPERATION_INDEX, routing_id,OUTPUT_PRODUCT_INDEX ')
    Multiple_Output_Domain = cursor.fetchall()
    Output_Count = {}

    Multiple_Output_Domain_COMB1 = []
    Multiple_Output_Domain_COMB2 = []

    for l in range(0, len(Multiple_Output_Domain)):
        Multiple_Output_Domain_COMB1.append(
            [int(Multiple_Output_Domain[l][0]), int(Multiple_Output_Domain[l][1]), int(Multiple_Output_Domain[l][2])])
        Multiple_Output_Domain_COMB2.append(
            [int(Multiple_Output_Domain[l][0]), int(Multiple_Output_Domain[l][1]), int(Multiple_Output_Domain[l][2]),
             int(Multiple_Output_Domain[l][3])])
        Output_Count[
            int(Multiple_Output_Domain[l][0]), int(Multiple_Output_Domain[l][1]), int(Multiple_Output_Domain[l][2]), int(
                Multiple_Output_Domain[l][3])] = int(Multiple_Output_Domain[l][4])

    currenttime = datetime.datetime.now()
    print "Step6:  %s " % currenttime.strftime('%Y-%m-%d %H:%M:%S')

    cursor.execute('select distinct operation_index From xlps_route_solver_ready where CLIENT_ID=' + str(Client_ID) + ' and \
          Scenario_ID=' + str(Scenario_ID) + ' order by operation_index')
    global all_operations
    all_operations = cursor.fetchall()

    cursor.execute('select distinct po.PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po \
            on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id \
                   where rsr.CLIENT_ID=' + str(Client_ID) + ' and rsr.operation_index in (22,21,5,12) \
            and rsr.Scenario_ID= ' + str(Scenario_ID) + ' order by SEQUENCE_ID desc');

    PO_TASK_AR_SEQ_ALT_together_COMB = cursor.fetchall()

    ##  print ' ----------------------------------- BOH -----------------------------------'
    cursor.execute('select distinct product_id,time_index, convert(cumul_quantity, UNSIGNED INTEGER) from xlps_material_receipts \
                where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(Scenario_ID) + ' order by  product_id,time_index')
    global BOH_T
    BOH_T = cursor.fetchall()
    global BOH_Comb
    global BOH
    BOH_Comb = []
    BOH = {}
    for l in range(0, len(BOH_T)):
        BOH_Comb.append([int(BOH_T[l][0]), int(BOH_T[l][1])])
        BOH[int(BOH_T[l][0]), int(BOH_T[l][1])] = int(BOH_T[l][2])

    # ##  print ' ----------------------------------- BOH_SFG -----------------------------------'
    # cursor.execute('select distinct product_id,time_index, convert(cumul_quantity, UNSIGNED INTEGER) from xlps_material_receipts_sfg \
    #             where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(
    #     Scenario_ID) + ' order by product_id,time_index')
    # BOH_SFG_T = cursor.fetchall()
    # global BOH_SFG
    # global BOH_SFG_Comb
    # BOH_SFG_Comb = []
    # BOH_SFG = {}
    # for l in range(0, len(BOH_SFG_T)):
    #     BOH_SFG_Comb.append([int(BOH_SFG_T[l][0]), int(BOH_SFG_T[l][1])])
    #     BOH_SFG[int(BOH_SFG_T[l][0]), int(BOH_SFG_T[l][1])] = int(BOH_SFG_T[l][2])
    # print "BOH_SFG:", BOH_SFG

    ##  print ' ----------------------------------- Material_Profile -----------------------------------'
    cursor.execute('select distinct product_id,time_index, convert(quantity, UNSIGNED INTEGER) from xlps_material_receipts \
                where quantity>=0 and client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(
        Scenario_ID) + ' order by  product_id,time_index')
    global Material_Profile_Comb
    Material_Profile_Comb = cursor.fetchall()

    ##global Material_Profile_Comb
    ##Material_Profile_Comb=[]
    ##Material_Profile={}
    ##for l in range(0,len(BOH_T2)):
    ##  Material_Profile_Comb.append([int(BOH_T2[l][0]),int(BOH_T2[l][1]),int(BOH_T2[l][2])])
    ##  Material_Profile[int(BOH_T2[l][0]),int(BOH_T2[l][1])] = int(BOH_T2[l][2])

    ##  print ' ----------------------------------- PO_RELATION -----------------------------------'
    cursor.execute('select production_order_index_fg,production_order_index_sfg from xlps_po_relation \
    where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(Scenario_ID))
    global PO_Relation
    PO_Relation = cursor.fetchall()

    ##  print ' ----------------------------------- FG_Lots -----------------------------------'
    cursor.execute('select production_order_index,pm.product_index,order_quantity,production_order_id from xlps_po_solver_ready po \
        inner join xlps_product_master pm on po.PRODUCT_ID=pm.PRODUCT_INDEX and po.SCENARIO_ID=pm.SCENARIO_ID \
        where PRODUCT_TYPE_ID=3 and po.client_id=' + str(Client_ID) + ' and pm.client_id=' + str(
        Client_ID) + ' and po.Scenario_ID= ' + str(Scenario_ID) + ' order by order_due_date_index, production_order_id')
    global FG_Lots
    FG_Lots = cursor.fetchall()
    global FG_Order_Qty
    FG_Order_Qty = {}
    for i in range(0, len(FG_Lots)):
        FG_Order_Qty[FG_Lots[i][0]] = int(FG_Lots[i][2])

    ##  print ' ----------------------------------- SFG_Lots -----------------------------------'
    cursor.execute('select production_order_index,pm.product_index,order_quantity,production_order_id from xlps_po_solver_ready po \
        inner join xlps_product_master pm on po.PRODUCT_ID=pm.PRODUCT_INDEX and po.SCENARIO_ID=pm.SCENARIO_ID \
        where PRODUCT_TYPE_ID=2 and po.client_id=' + str(Client_ID) + ' and pm.client_id=' + str(
        Client_ID) + ' and po.Scenario_ID= ' + str(Scenario_ID) + ' order by order_due_date_index,production_order_id')
    global SFG_Lots
    SFG_Lots = cursor.fetchall()
    global SFG_Order_Qty
    SFG_Order_Qty = {}
    for i in range(0, len(SFG_Lots)):
        SFG_Order_Qty[SFG_Lots[i][0]] = int(SFG_Lots[i][2])

    # cursor.execute('update xlps_sfg_fg_domain fg inner join xlps_product_master pm on pm.product_id=fg.input_product_id and  fg.client_id=pm.client_id\
    #                 and fg.scenario_id=pm.scenario_id set fg.INPUT_PRODUCT_ID = pm.product_id  where pm.client_id = ' + str(Client_ID))
    # cursor.execute('update xlps_sfg_fg_domain fg inner join xlps_product_master pm on pm.product_index=fg.output_product_index and  fg.client_id=pm.client_id\
    #     and fg.scenario_id=pm.scenario_id set fg.OUTPUT_PRODUCT_ID = pm.product_id  where pm.client_id = ' + str(
    #     Client_ID) + ' and pm.scenario_id = ' + str(Scenario_ID))

    ##  print ' ----------------------------------- SFG_FG_Domain -----------------------------------'
    # cursor.execute('select pm.product_index,pm1.product_index, ip_qty,op_qty,attach_rate from xlps_sfg_fg_domain fg \
    #                inner join xlps_product_master pm on pm.product_id= input_product_id and pm.client_id=fg.client_id and pm.scenario_id=fg.scenario_id \
    # inner join xlps_product_master pm1 on pm1.product_id= output_product_id and pm1.client_id=fg.client_id and pm1.scenario_id=fg.scenario_id \
    #     where fg.client_id=' + str(Client_ID) + ' and fg.Scenario_ID= ' + str(Scenario_ID))
    global SFG_FG_Domain
    global SFG_FG_Domain_Comb
    global FG_Qty
    global SFG_Qty
    global AR

    # SFG_FG_Domain = cursor.fetchall()
    #
    FG_Qty = {}
    SFG_Qty = {}
    AR = {}
    SFG_FG_Domain_Comb = []
    # for i in range(0, len(SFG_FG_Domain)):
    #     SFG_FG_Domain_Comb.append((SFG_FG_Domain[i][0], SFG_FG_Domain[i][1]))
    #     FG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][3]
    #     SFG_Qty[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][2]
    #     AR[(SFG_FG_Domain[i][0], SFG_FG_Domain[i][1])] = SFG_FG_Domain[i][4]

    FG_Lots_sorted = sorted(FG_Lots, key=lambda x: (-x[2]))
    SFG_Lots_sorted = sorted(SFG_Lots, key=lambda x: (-x[2]))
    # print  "FG_Lots_sorted ", FG_Lots_sorted
    # print  "SFG_Lots_sorted ", SFG_Lots_sorted

    cursor.execute(' select po.production_order_index,pm.product_index from xlps_production_order xl \
                inner join xlps_po_solver_ready po on po.production_order_id=xl.production_order_id  \
                and po.client_id=xl.client_id and po.scenario_id=xl.scenario_id  \
                inner join xlps_product_master pm on pm.product_id=xl.output_product and pm.scenario_id=xl.scenario_id \
                and pm.client_id=xl.client_id  where xl.level=2 ');
    global SFG_PO_FG_Lot_Relation
    SFG_PO_FG_Lot_Relation = cursor.fetchall()

    ##  print ' ----------------------------------- BOH_Total -----------------------------------'
    cursor.execute('select distinct product_id, convert(sum(quantity), UNSIGNED INTEGER) from xlps_material_receipts \
               where client_id=' + str(Client_ID) + '  and Scenario_ID= ' + str(Scenario_ID) + '  group by product_id ')
    BOH_Total = cursor.fetchall()

    global BOH_Total_Comb
    global BOH_Total_Value
    BOH_Total_Comb = []
    BOH_Total_Value = {}
    for l in range(0, len(BOH_Total)):
        BOH_Total_Comb.append(int(BOH_Total[l][0]))
        BOH_Total_Value[int(BOH_Total[l][0])] = int(BOH_Total[l][1])

    global Initial_Schedule_Comb
    Initial_Schedule_Comb = []
    if int(Freeze_Time_Fence) > 0:
        print ' ----------------------------------- Initial Schedule -----------------------------------'
        if PlanningBucket == "Seconds":
            cursor.execute('select distinct PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX, ACTIVE_STATUS \
                From XLPS_SCHEDULE_TIMES where ACTIVE_STATUS = 1 and BEGIN_TIME_INDEX <=' + str(
                Freeze_Time_Fence) + ' *24*3600 and CLIENT_ID=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));
        elif PlanningBucket == "Minutes":
            cursor.execute('select distinct PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX, ACTIVE_STATUS \
                From XLPS_SCHEDULE_TIMES where ACTIVE_STATUS = 1 and BEGIN_TIME_INDEX <=' + str(
                Freeze_Time_Fence) + ' *24*60 and CLIENT_ID=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));
        elif PlanningBucket == "Hours":
            cursor.execute('select distinct PRODUCTION_ORDER_INDEX, OPERATION_INDEX, ROUTING_ID, SEQUENCE_ID, ALTERNATE_INDEX, BEGIN_TIME_INDEX, END_TIME_INDEX, ACTIVE_STATUS \
                From XLPS_SCHEDULE_TIMES where ACTIVE_STATUS = 1 and BEGIN_TIME_INDEX <=' + str(
                Freeze_Time_Fence) + ' *24 and CLIENT_ID=' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));

        Initial_Schedule = cursor.fetchall()
        Task_Begin_Time = {}
        Task_End_Time = {}
        Task_Active_Status = {}

        for l in range(0, len(Initial_Schedule)):
            Initial_Schedule_Comb.append((int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]),
                                          int(Initial_Schedule[l][2]), int(Initial_Schedule[l][3]),
                                          int(Initial_Schedule[l][4])))
            Task_Begin_Time[int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]), int(Initial_Schedule[l][2]), int(
                Initial_Schedule[l][3]), int(Initial_Schedule[l][4])] = int(Initial_Schedule[l][5])
            Task_End_Time[int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]), int(Initial_Schedule[l][2]), int(
                Initial_Schedule[l][3]), int(Initial_Schedule[l][4])] = int(Initial_Schedule[l][6])
            Task_Active_Status[int(Initial_Schedule[l][0]), int(Initial_Schedule[l][1]), int(Initial_Schedule[l][2]), int(
                Initial_Schedule[l][3]), int(Initial_Schedule[l][4])] = int(Initial_Schedule[l][7])

    ##print ' ----------------------------------- HOLDING_TIME -----------------------------------'

    cursor.execute('select distinct bm.production_order_index, bm.operation_index, bm.machine_index,min_holding_time,max_holding_time from xlps_holding_time bm\
          inner join xlps_po_solver_ready po \
            on po.production_order_index=bm.production_order_index and po.client_id=bm.client_id and po.scenario_id=bm.scenario_id \
    where bm.client_id=' + str(Client_ID) + ' and bm.scenario_id= ' + str(Scenario_ID) + ' and bm.machine_index<>-1 order by bm.production_order_index, \
    bm.operation_index,bm.machine_index')
    Prod_Res_Holding_Time = cursor.fetchall()

    if Holding_Resource_Function == "Enable":
        global Prod_Res_Holding_Time_Comb
        global Min_Holding_Time
        global Max_Holding_Time

        Min_Holding_Time = {}
        Max_Holding_Time = {}
        Prod_Res_Holding_Time_Comb = []

        for l in range(0, len(Prod_Res_Holding_Time)):
            Prod_Res_Holding_Time_Comb.append(
                [int(Prod_Res_Holding_Time[l][0]), int(Prod_Res_Holding_Time[l][1]), int(Prod_Res_Holding_Time[l][2])])
            Min_Holding_Time[
                int(Prod_Res_Holding_Time[l][0]), int(Prod_Res_Holding_Time[l][1]), int(Prod_Res_Holding_Time[l][2])] = int(
                Prod_Res_Holding_Time[l][3])
            Max_Holding_Time[
                int(Prod_Res_Holding_Time[l][0]), int(Prod_Res_Holding_Time[l][1]), int(Prod_Res_Holding_Time[l][2])] = int(
                Prod_Res_Holding_Time[l][4])

    currenttime = datetime.datetime.now()
    f1.write("Data reading completed at %s" % currenttime.strftime('%Y-%m-%d %H:%M:%S'))
    f1.write("\n")
    f1.close()

    # cursor.execute('delete from spps_schedule_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));
    # cursor.execute('delete from spps_schedule_new_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));
    # cursor.execute('delete from spps_schedule_qty_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))  ;
    # cursor.execute('delete from spps_po_resource_qty_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID));
    # cursor.execute('delete from spps_setup_report where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID= ' + str(Scenario_ID))    ;
    # cursor.execute('Delete from SPPS_MATERIAL_STATUS where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID)) ;
    #
    # cursor.execute('Delete from XLPS_SCHEDULE_TIMES where CLIENT_ID= ' + str(Client_ID) + ' and Scenario_ID=' + str(Scenario_ID))   ;
    active_connection = -1
    cursor.close()
    cnx.commit()
    cnx.close()

    return True
