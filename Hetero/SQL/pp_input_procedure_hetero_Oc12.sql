DELIMITER $$
DROP procedure if exists SPPP_InputDataTransformation $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SPPP_InputDataTransformation`(ClientID INT, ScenarioID INT)
BEGIN
		BEGIN

			Delete from xlps_planning_resources where client_id=ClientID  and Scenario_ID=ScenarioID;
     
			insert into xlps_planning_resources(Client_ID, Scenario_ID,Resource_ID,Bottleneck_Flag)
			select ClientID,ScenarioID, df3.df_value, df2.df_value from spps_relation sr1 
            inner join spps_df_input df3 on sr1.CLIENT_ID=df3.CLIENT_ID and df3.relation_id=sr1.relation_id
			inner join spps_df_input df2 on sr1.CLIENT_ID=df2.CLIENT_ID and df2.scenario_id=df3.scenario_id
            inner join spps_relation sr on sr.CLIENT_ID=df2.CLIENT_ID and df2.relation_id=sr.relation_id and sr.operation_id=sr1.operation_id
            where df3.df_id=2060 and df2.df_id=674 and df2.df_value in ("Planning","Both")
            and sr.CLIENT_ID=ClientID and df2.scenario_id=ScenarioID;
						
		END;
                        
		BEGIN

			DECLARE MACHINEID,planningstartdate VARCHAR(100);
			DECLARE ShutdownStartTime, ShutdownEndTime DATETIME;
            DECLARE Planning_Start_Date DATE;
			DECLARE k5,k6,k1,k2,shutdowntime,Duration,i1,temp1,temp2,temp3,temp4 INT;
 			DECLARE done BOOLEAN DEFAULT FALSE;
			DECLARE planningduration int;
            DECLARE periodid,dateid double;
            DECLARE latedeliveryhorizon,shutdown_count int;
			DECLARE planningbucket varchar(100);
            DECLARE schedulingbucket varchar(100);
   
   

			DECLARE cur2 CURSOR FOR SELECT distinct ps.Resource_ID, STR_TO_DATE(df1.df_value,'%m/%d/%Y %H:%i'), STR_TO_DATE(df2.df_value,'%m/%d/%Y %H:%i') FROM spps_df_input df1
            inner join spps_df_input df2 on df1.RELATION_ID=df2.RELATION_ID and df1.CLIENT_ID=df2.CLIENT_ID and df1.SCENARIO_ID=df2.SCENARIO_ID and df1.increment_id=df2.increment_id
            inner join spps_relation sr on sr.RELATION_ID=df1.RELATION_ID and sr.CLIENT_ID=df1.CLIENT_ID
            inner join xlps_planning_resources ps on ps.resource_id=sr.resource_id and ps.client_id=sr.CLIENT_ID and ps.scenario_id=df2.scenario_id
			where df1.client_id=ClientID and df1.df_id=604 and df2.df_id=623  and df1.Scenario_ID=ScenarioID;
            
 	        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;  
            
           
            

SELECT Parameter_VALUE INTO planningbucket FROM SPAPP_PARAMETER where module_id = 6 and parameter_id = 619
        and client_id = ClientID and Scenario_ID = ScenarioID;
SELECT Parameter_VALUE INTO schedulingbucket FROM SPAPP_PARAMETER where module_id = 6 and parameter_id = 602
        and client_id = ClientID and Scenario_ID = ScenarioID;
SELECT  Parameter_VALUE INTO planningstartdate FROM SPAPP_PARAMETER where module_id = 6 and parameter_id = 601 and client_id = ClientID
        and Scenario_ID = ScenarioID;
SELECT Parameter_VALUE INTO planningduration FROM SPAPP_PARAMETER where  module_id = 6 and parameter_id = 618 and client_id = ClientID
        and Scenario_ID = ScenarioID;
SELECT Parameter_VALUE INTO latedeliveryhorizon FROM SPAPP_PARAMETER where module_id = 6 and parameter_id = 620 and client_id = ClientID
        and Scenario_ID = ScenarioID;

			SELECT Parameter_VALUE INTO schedulingbucket FROM SPAPP_PARAMETER where module_id = 6 and parameter_id = 602
				and client_id = ClientID and Scenario_ID = ScenarioID;
  
SELECT  STR_TO_DATE(Parameter_VALUE,'%m/%d/%Y %H:%i') INTO Planning_Start_Date FROM SPAPP_PARAMETER where module_id = 6 and parameter_id = 601 and client_id = ClientID
        and Scenario_ID = ScenarioID;         

SELECT period_id INTO periodid FROM SPAPP_PERIOD where client_id = ClientID   and period_value = planningstartdate
        and period_bucket = planningbucket;
        
  SELECT planningstartdate;
   SELECT planningbucket;     
   SELECT schedulingbucket;     
   SELECT periodid;
   SELECT planningduration;

			SELECT count(*) into shutdown_count FROM spps_df_input df1
            inner join spps_df_input df2 on df1.RELATION_ID=df2.RELATION_ID and df1.CLIENT_ID=df2.CLIENT_ID and df1.SCENARIO_ID=df2.SCENARIO_ID and df1.increment_id=df2.increment_id
            inner join spps_relation sr on sr.RELATION_ID=df1.RELATION_ID and sr.CLIENT_ID=df1.CLIENT_ID
            inner join xlps_planning_resources ps on ps.resource_id=sr.resource_id and ps.client_id=sr.CLIENT_ID and ps.scenario_id=df2.scenario_id
			where df1.client_id=ClientID and df1.df_id=604 and df2.df_id=623  and df1.Scenario_ID=ScenarioID;
            
            
			Delete from xlps_machine_capacity_hours where client_id=ClientID  and Scenario_ID=ScenarioID;
			SELECT 1;
        
			if shutdown_count>0 Then
            
			if schedulingbucket = "Hours" Then
            SELECT 2;
				OPEN cur2; 
				read_loop:LOOP 
					
					FETCH cur2 INTO MachineID, ShutdownStartTime,ShutdownEndTime;
                    
					IF done = 1 THEN
						LEAVE read_loop;
					END IF;                
					
					SET k5 = Datediff(SHUTDOWNSTARTTIME,Planning_Start_Date) * 24 + Hour(SHUTDOWNSTARTTIME);
					SET k6 = Datediff(SHUTDOWNENDTIME,Planning_Start_Date) * 24 + Hour(SHUTDOWNENDTIME);				
					
					SET k1=0;
					SET k2=0;
					SET Duration =planningduration;

					 while i1 < Duration do
						SET k1 = i1 * 24;
						SET k2 = (i1 + 1) * 24;
						  SET temp1 = 0;
						  SET temp2 = 0;
						  SET temp3 = 0;
						  SET temp4 = 0;
						  if k5 >= k1 and k5 <= k2 and k6 <= k2 Then
								  SET temp1 = temp1 + k6 - k5;            
						  elseif k5 >= k1 and k5 <= k2 and k6 > k2 Then
								  SET temp2 = temp2 + k2 - k5;
						  elseif k5 < k1 and k6 >= k1 and k6 <=k2 Then
								  SET temp3 = temp3 + k6 - k1;
						  elseif k5 < k1 and k6 > k2 Then
								  SET temp4 = temp4 + k2 - k1;
                                  
						ENd if;
						  SET shutdowntime = temp1 + temp2 + temp3 + temp4;
                          
                         
						  
						insert into XLPS_Machine_capacity_hours (Client_ID, Scenario_ID,Resource_ID,OPERATION_ID,PERIOD_ID,TOTAL_HRS,SHUTDOWN_HRS,AVAILABLE_HRS) 
						values(ClientID,ScenarioID, MachineID,MachineID,DATE_ADD(Planning_Start_Date,INTERVAL i1 DAY),24,shutdowntime,24-shutdowntime);
                    
					end while;
				END LOOP; 
				CLOSE cur2; 		
				End If;

			if schedulingbucket = "Minutes" Then
            SELECT "inside minutes";
				OPEN cur2; 
				read_loop:LOOP 
					
                    FETCH cur2 INTO MachineID, ShutdownStartTime,ShutdownEndTime;
                    -- SELECT MachineID, ShutdownStartTime,ShutdownEndTime;
					IF done = 1 THEN
						LEAVE read_loop;
					END IF;
				  
					SET k5 = Datediff(SHUTDOWNSTARTTIME,Planning_Start_Date) *24*60 + Hour(SHUTDOWNSTARTTIME) * 60 + Minute(SHUTDOWNSTARTTIME);
					SET k6 = Datediff(SHUTDOWNENDTIME,Planning_Start_Date) *24*60 + Hour(SHUTDOWNENDTIME) * 60 + Minute(SHUTDOWNENDTIME);
					-- SELECT k5,k6;
					SET k1=0;
					SET k2=0;
                    SET i1=0;

					SET Duration = planningduration;
                     SELECT Duration;
					 while i1 < Duration do

						SET k1 = i1 * 24*60;
						SET k2 = (i1 + 1) * 24*60;
						  SET temp1 = 0;
						  SET temp2 = 0;
						  SET temp3 = 0;
						  SET temp4 = 0;
                          -- SELECT k1,k2,k5,k6;
						  if k5 >= k1 and k5 <= k2 and k6 <= k2 Then
								  SET temp1 = temp1 + k6 - k5;
                                  -- SELECT 9999,  k1,k2,k5,k6;
						  elseif k5 >= k1 and k5 <= k2 and k6 > k2 Then
								  SET temp2 = temp2 + k2 - k5;
						  elseif k5 < k1 and k6 >= k1 and k6 <=k2 Then
								  SET temp3 = temp3 + k6 - k1;
						  elseif k5 < k1 and k6 > k2 Then
								  SET temp4 = temp4 + k2 - k1;
                          ENd if;
						  SET shutdowntime = (temp1 + temp2 + temp3 + temp4)/60;
                          -- SELECT  temp1 , temp2 , temp3 , temp4;
						  
						-- SELECT 'shutdowntime:', shutdowntime;
						insert into XLPS_Machine_capacity_hours (Client_ID, Scenario_ID,Resource_ID,OPERATION_ID,PERIOD_ID,TOTAL_HRS,SHUTDOWN_HRS,AVAILABLE_HRS) 
						values(ClientID,ScenarioID, MachineID,MachineID,DATE_ADD(Planning_Start_Date,INTERVAL i1 DAY),24,shutdowntime,24-shutdowntime);
                    SET i1=i1+1;
					end while;
	 
				END LOOP; 
				CLOSE cur2; 	
			End If;


			if schedulingbucket = "Seconds" Then
             SELECT "inside seconds";
				OPEN cur2; 
				read_loop:LOOP 
					FETCH cur2 INTO MachineID, ShutdownStartTime,ShutdownEndTime;
					IF done = 1 THEN
						LEAVE read_loop;
					END IF;
			
					SET k5 = Datediff(SHUTDOWNSTARTTIME,Planning_Start_Date) *24*60*60 + Hour(SHUTDOWNSTARTTIME) * 3600 + Minute(SHUTDOWNSTARTTIME) *60 + Second(SHUTDOWNSTARTTIME) ;
					SET k6 = Datediff(SHUTDOWNENDTIME,Planning_Start_Date) *24*60*60 + Hour(SHUTDOWNENDTIME) * 3600 + Minute(SHUTDOWNENDTIME) * 60 + Second(SHUTDOWNENDTIME) ;

					SET k1=0;
					SET k2=0;
                     SET i1=0;
				  SET Duration = planningduration;
                  SELECT 'Dur: ',Duration;


					 while i1 < Duration do
						SET k1 = i1 * 24*60*60;
						SET k2 = (i1 + 1) * 24*60*60;

						  SET temp1 = 0;
						  SET temp2 = 0;
						  SET temp3 = 0;
						  SET temp4 = 0;
						  if k5 >= k1 and k5 <= k2 and k6 <= k2 Then
								  SET temp1 = temp1 + k6 - k5;            
						  elseif k5 >= k1 and k5 <= k2 and k6 > k2 Then
								  SET temp2 = temp2 + k2 - k5;
						  elseif k5 < k1 and k6 >= k1 and k6 <=k2 Then
								  SET temp3 = temp3 + k6 - k1;
						  elseif k5 < k1 and k6 > k2 Then
								  SET temp4 = temp4 + k2 - k1;

								ENd if;
						 SET shutdowntime = (temp1 + temp2 + temp3 + temp4)/3600;
                         -- SELECT 'shutdowntime:', shutdowntime;
                                         
						insert into XLPS_Machine_capacity_hours (Client_ID, Scenario_ID,Resource_ID,OPERATION_ID,PERIOD_ID,TOTAL_HRS,SHUTDOWN_HRS,AVAILABLE_HRS) 
						values(ClientID,ScenarioID, MachineID,MachineID,DATE_ADD(Planning_Start_Date,INTERVAL i1 DAY),24,shutdowntime,24-shutdowntime);
                    SET i1=i1+1;
					end while;
	 
				END LOOP; 
				CLOSE cur2; 
			End If;	
		End If;
        

			Delete from xlps_machine_batch_time where client_ID = ClientID and Scenario_ID=ScenarioID;
            
			select 99;
            
			INSERT INTO  xlps_machine_batch_time (Client_ID, Scenario_ID, LOCATION_ID, PRODUCT_ID, OPERATION_ID, Resource_ID,
            Min_Batch_Size, Max_Batch_Size, SETUP_TIME, PROCESSING_TIME, TEARDOWN_TIME,Period_ID, CAPACITY_KGS)  
			SELECT distinct ClientID,SCENARIOID,sr.LOCATION_FROM_ID,sr1.OUTPUT_PRODUCT_ID,sr.OPERATION_ID,sr.Resource_ID,df1.df_value,df2.df_value,
            df3.df_value,df4.df_value,df5.df_value,str_to_date(period_value,'%m/%d/%Y'),0
			from spps_df_input df1 inner join spps_relation sr on sr.relation_id=df1.relation_ID and sr.client_id=df1.client_id
            inner join spps_df_input df2 on sr.relation_id=df2.relation_ID and sr.client_id=df2.client_id and df2.SCENARIO_ID=df1.SCENARIO_ID
            inner join spps_df_input df3 on sr.relation_id=df3.relation_ID and sr.client_id=df3.client_id and df3.SCENARIO_ID=df1.SCENARIO_ID
            inner join spps_df_input df4 on sr.relation_id=df4.relation_ID and sr.client_id=df4.client_id and df4.SCENARIO_ID=df1.SCENARIO_ID
            inner join spps_df_input df5 on sr.relation_id=df5.relation_ID and sr.client_id=df5.client_id and df5.SCENARIO_ID=df1.SCENARIO_ID
			inner join spapp_period sp on sr.client_id=sp.client_id
            inner join xlps_planning_resources pr on pr.resource_id=sr.resource_id and sr.client_id=pr.client_id and df5.SCENARIO_ID=pr.SCENARIO_ID
            inner join spps_relation sr1 on sr1.CLIENT_ID=sr.client_id and sr.operation_ID=sr1.operation_ID and sr.LOCATION_FROM_ID=sr1.LOCATION_FROM_ID 
			inner join spps_df_input df on df.relation_id=sr1.relation_id and sr1.client_id=sr.client_id and df.SCENARIO_ID=df1.SCENARIO_ID
			where df1.df_id =675 and df2.df_id =670 and df3.df_id =608 and df4.df_id =609  and df5.df_id =610  and df.df_id=602
            and sp.period_id between periodid and periodid+planningduration+latedeliveryhorizon-1 and sp.PERIOD_BUCKET='Days'
			and df1.client_id=ClientID and df1.Scenario_ID=ScenarioID order by OPERATION_ID, Resource_ID,df2.df_value desc;
            
            SELECT 100;
            
			if schedulingbucket = "Seconds" Then
            
				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                set bt.capacity_kgs=24*3600*Max_Batch_Size/(SETUP_TIME + PROCESSING_TIME + TEARDOWN_TIME)
                where df.df_id=2077 and df.df_value=2;
 
 				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                set bt.capacity_kgs=24*3600/PROCESSING_TIME
                where df.df_id=2077 and df.df_value=1;
                

				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                inner join  xlps_machine_capacity_hours mc on bt.client_id=mc.client_id and bt.scenario_id=mc.scenario_id
				and bt.resource_id=mc.resource_id and bt.period_id=mc.period_id
				set bt.capacity_kgs=mc.AVAILABLE_HRS*3600*Max_Batch_Size/(SETUP_TIME+ PROCESSING_TIME+ TEARDOWN_TIME)
                where df.df_id=2077 and df.df_value=2;

				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                inner join  xlps_machine_capacity_hours mc on bt.client_id=mc.client_id and bt.scenario_id=mc.scenario_id
				and bt.resource_id=mc.resource_id and bt.period_id=mc.period_id
				set bt.capacity_kgs=mc.AVAILABLE_HRS*3600/PROCESSING_TIME
                where df.df_id=2077 and df.df_value=1;
                
			end if;
 
 
			if schedulingbucket = "Minutes" Then
            
				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                set bt.capacity_kgs=24*60*Max_Batch_Size/(SETUP_TIME + PROCESSING_TIME + TEARDOWN_TIME)
                where df.df_id=2077 and df.df_value=2;
 
 				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                set bt.capacity_kgs=24*60/PROCESSING_TIME
                where df.df_id=2077 and df.df_value=1;
                

				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                inner join  xlps_machine_capacity_hours mc on bt.client_id=mc.client_id and bt.scenario_id=mc.scenario_id
				and bt.resource_id=mc.resource_id and bt.period_id=mc.period_id
				set bt.capacity_kgs=mc.AVAILABLE_HRS*60*Max_Batch_Size/(SETUP_TIME+ PROCESSING_TIME+ TEARDOWN_TIME)
                where df.df_id=2077 and df.df_value=2;

				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                inner join  xlps_machine_capacity_hours mc on bt.client_id=mc.client_id and bt.scenario_id=mc.scenario_id
				and bt.resource_id=mc.resource_id and bt.period_id=mc.period_id
				set bt.capacity_kgs=mc.AVAILABLE_HRS*60/PROCESSING_TIME
                where df.df_id=2077 and df.df_value=1;
                
			end if;
            
			if schedulingbucket = "Hours" Then
            
				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                set bt.capacity_kgs=24*Max_Batch_Size/(SETUP_TIME + PROCESSING_TIME + TEARDOWN_TIME)
                where df.df_id=2077 and df.df_value=2;
 
 				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                set bt.capacity_kgs=24/PROCESSING_TIME
                where df.df_id=2077 and df.df_value=1;
                

				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                inner join  xlps_machine_capacity_hours mc on bt.client_id=mc.client_id and bt.scenario_id=mc.scenario_id
				and bt.resource_id=mc.resource_id and bt.period_id=mc.period_id
				set bt.capacity_kgs=mc.AVAILABLE_HRS*Max_Batch_Size/(SETUP_TIME+ PROCESSING_TIME+ TEARDOWN_TIME)
                where df.df_id=2077 and df.df_value=2;

				update xlps_machine_batch_time bt inner join spps_relation sr on bt.OPERATION_ID=sr.OPERATION_ID
                and bt.LOCATION_ID=sr.LOCATION_FROM_ID and bt.CLIENT_ID=sr.client_id
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                inner join  xlps_machine_capacity_hours mc on bt.client_id=mc.client_id and bt.scenario_id=mc.scenario_id
				and bt.resource_id=mc.resource_id and bt.period_id=mc.period_id
				set bt.capacity_kgs=mc.AVAILABLE_HRS/PROCESSING_TIME
                where df.df_id=2077 and df.df_value=1;
                
			end if;            
            
            
            -- update capacity based on Capacity Conversion Factor

			update xlps_machine_batch_time xl inner join spps_relation sr
                on xl.operation_id=sr.operation_id and xl.RESOURCE_ID=sr.RESOURCE_ID  and xl.client_id=sr.client_id
                and xl.location_id=sr.LOCATION_FROM_ID
                inner join spps_df_input df on df.relation_id=sr.relation_id and df.client_id=sr.client_id
                and df.scenario_id=xl.scenario_id 
 				set xl.capacity_kgs = xl.capacity_kgs * df.df_value
 				where xl.client_id=ClientID and xl.scenario_id=ScenarioID and df.df_id=680;
                
            SELECT 101;
   			Delete from XLPS_CAPACITY where client_ID = ClientID and Scenario_ID=ScenarioID;
            
			INSERT INTO  XLPS_CAPACITY (Client_ID,Scenario_ID, LOCATION_ID,PRODUCT_ID,RESOURCE_ID,Period_ID,CAPACITY)  
			SELECT distinct ClientID,SCENARIOID,LOCATION_ID,PRODUCT_ID,OPERATION_ID,sp.PERIOD_ID,sum(capacity_kgs)
			from xlps_machine_batch_time bt
            inner join spapp_period sp on bt.client_id=sp.client_id and bt.period_id=STR_TO_DATE(sp.PERIOD_VALUE,'%m/%d/%Y') 
			where sp.period_id>= periodid and sp.period_id<=periodid+planningduration+latedeliveryhorizon+1 and  bt.client_id=ClientID and bt.Scenario_ID=ScenarioID 
            and sp.PERIOD_BUCKET='Days'
            group by bt.Client_ID,bt.SCENARIO_ID,LOCATION_ID,PRODUCT_ID,OPERATION_ID,sp.period_id;            
		
            
  -- updating the capacity data based on holiday calendar
SELECT 102;
                update XLPS_CAPACITY xl inner join spps_Holiday_Calendar hc
                on xl.client_id=hc.client_id and xl.scenario_id=hc.scenario_id
                inner join spapp_period p on STR_TO_DATE(p.PERIOD_VALUE,'%m/%d/%Y') =hc.holiday_date
                and p.client_id=hc.client_id 
                and p.period_id=xl.period_id 				
 				set xl.capacity = 0 
 				where xl.client_id=ClientID and p.period_bucket=planningbucket;

-- updating the capacity data based on workday calendar
				SELECT 103;
  			IF planningbucket = "Days" THEN
 				update XLPS_CAPACITY xl inner join spps_workday_Calendar wc
 			    on xl.client_id=wc.client_id and xl.scenario_id=wc.scenario_id
				inner join spapp_period p on xl.Period_ID=p.Period_ID and p.client_id=xl.client_id
 			    and DAYNAME(STR_TO_DATE(p.PERIOD_VALUE,'%m/%d/%Y')) = wc.DAY_NAME 
 				set xl.capacity = 0 where xl.client_id=ClientID and wc.day_status="Holiday";
 			END IF;



  SELECT 200;
            
			INSERT INTO  XLPS_MODES (Client_ID,Scenario_ID, LOCATION_FROM_ID,LOCATION_TO_ID,OUTPUT_PRODUCT_ID,RESOURCE_ID,RESOURCE_INDEX)  
			SELECT distinct ClientID,SCENARIOID,sr.LOCATION_FROM_ID,sr.LOCATION_TO_ID,sr1.OUTPUT_PRODUCT_ID,'mode',0
			from spps_relation sr inner join spps_df_input df on df.client_id=sr.client_id and df.relation_id = sr.RELATION_ID 
            inner join spps_df_input df2 on df2.client_id=sr.client_id and df2.scenario_id=df.scenario_id
            inner join spps_relation sr1 on sr1.relation_id=df2.RELATION_ID and sr1.CLIENT_ID=df2.CLIENT_ID
            where df.df_id=2051 and df2.df_id=2003 and df2.df_value =1 and sr.client_id=ClientID and df2.scenario_id=SCENARIOID;
  
  			INSERT INTO  XLPS_MODES (Client_ID,Scenario_ID, LOCATION_FROM_ID,LOCATION_TO_ID,OUTPUT_PRODUCT_ID,RESOURCE_ID,RESOURCE_INDEX)  
			SELECT distinct ClientID,SCENARIOID,sr.LOCATION_TO_ID,'Customer',sr1.OUTPUT_PRODUCT_ID,'mode',0
			from spps_relation sr inner join spps_df_input df on df.client_id=sr.client_id and df.relation_id = sr.RELATION_ID 
            inner join spps_df_input df2 on df2.client_id=sr.client_id and df2.scenario_id=df.scenario_id
            inner join spps_relation sr1 on sr1.relation_id=df2.RELATION_ID and sr1.CLIENT_ID=df2.CLIENT_ID
            where df.df_id=2051 and df2.df_id=2003 and df2.df_value =3 and sr.client_id=ClientID and df2.scenario_id=SCENARIOID;
            

			DELETE from spps_solver_df_output where client_id=ClientID and Scenario_ID=ScenarioID;
			DELETE from spps_solver_ts_output where client_id=ClientID and Scenario_ID=ScenarioID;
    

        
SELECT "Done";
END;
       END$$
DELIMITER ;
