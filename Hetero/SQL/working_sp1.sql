DELIMITER $$
drop procedure SPPS_InputDataTransformation $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SPPS_InputDataTransformation`(ClientID INT, ScenarioID INT)
BEGIN

	DECLARE time_stamp int;
	DECLARE nPO, nOperations, nAlt, sAltIndex,OrderQty,lotsize INT;
	DECLARE sPOID, sProductID, sOperationID,sIPProductID,pID,prodID,sMachineID  VARCHAR(100);
	DECLARE sOperationIndex, i, j, k, OpCounter, sTK, count,max_setup_time,setuptime,x,POs INT;
	DECLARE sNextOpID, sPrevOpID, Planning_Bucket VARCHAR(100);
	DECLARE Planning_Start_Date,Planning_End_Date DATE; 
    DECLARE Last_Order_Due_Date DATETIME;
    DECLARE Planning_Horizon,OrderQuantity,IP_Qty,AttachRate,Late_Delivery_Horizon INT; 
    DECLARE check_start_time,check_last_time BIGINT;
    DECLARE periodid double;
    DECLARE Hetero_Decimal_Handler INT;
    
    SET Hetero_Decimal_Handler = 1;

	SELECT STR_TO_DATE(Parameter_Value,'%m/%d/%Y')  INTO Planning_Start_Date FROM spapp_parameter where module_id=6 and parameter_id=601 
    and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

	delete from spps_relation_temp where client_id=ClientID;
	delete from spps_df_input_temp where CLIENT_ID = ClientID and SCENARIO_ID = ScenarioID and DF_ID in (608,609,610,620,661,670,675);
 	SELECT Parameter_Value  INTO Late_Delivery_Horizon FROM spapp_parameter where module_id=6 and parameter_id=611 
    and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
    
	SELECT Parameter_Value INTO Planning_Horizon FROM spapp_parameter where module_id=6 and parameter_id=603
    and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;  
    
  
  	SELECT Parameter_Value INTO Planning_Bucket FROM spapp_parameter where module_id=6 and parameter_id=602
    and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

    
	SELECT period_id INTO periodid FROM SPAPP_PERIOD where client_id = ClientID   and str_to_date(period_value,'%m/%d/%Y') = Planning_Start_Date
        and period_bucket = 'Days';
    
    SELECT Planning_Bucket;    
   
	SELECT Planning_Start_Date;
    SELECT periodid;
    
    
    SELECT "Step1 - XLPS_PRODUCT_MASTER";
    select CURTIME() into time_stamp;
    Delete from XLPS_PRODUCT_MASTER where client_id=ClientID  and Scenario_ID=ScenarioID;

	if Planning_Bucket = "Hours" Then
    
		INSERT INTO XLPS_PRODUCT_MASTER (CLIENT_ID, Scenario_ID,LOCATION_ID,PRODUCT_ID,PRODUCT_NAME,PRODUCT_TYPE_ID,SHELF_LIFE)
		select distinct df1.CLIENT_ID,df1.Scenario_ID,sr.location_from_id,df1.df_value,df4.df_value,df2.df_value,df3.df_value*24 from spps_df_input df1 
		inner join spps_relation sr on sr.relation_id =df1.relation_id 
		and sr.client_id=df1.client_id 
		inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id 
		and df1.scenario_id=df2.scenario_id 
		inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id 
		and df1.scenario_id=df3.scenario_id 
		inner join spps_df_input df4 on df1.relation_id=df4.relation_id and df1.client_id=df4.client_id 
		and df1.scenario_id=df4.scenario_id         
		where df1.df_id=2000 and df2.df_id=2003 and df3.df_id=673 and df4.df_id=2001 and df1.CLIENT_ID= ClientID and df1.scenario_id= ScenarioID ;
	
    elseif Planning_Bucket = "Minutes" Then
    
		INSERT INTO XLPS_PRODUCT_MASTER (CLIENT_ID, Scenario_ID,LOCATION_ID,PRODUCT_ID,PRODUCT_NAME,PRODUCT_TYPE_ID,SHELF_LIFE)
		select distinct df1.CLIENT_ID,df1.Scenario_ID,sr.location_from_id,df1.df_value,df4.df_value,df2.df_value,df3.df_value*24*60 from spps_df_input df1 
		inner join spps_relation sr on sr.relation_id =df1.relation_id 
		and sr.client_id=df1.client_id 
		inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id 
		and df1.scenario_id=df2.scenario_id 
		inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id 
		and df1.scenario_id=df3.scenario_id 
		inner join spps_df_input df4 on df1.relation_id=df4.relation_id and df1.client_id=df4.client_id 
		and df1.scenario_id=df4.scenario_id         
		where df1.df_id=2000 and df2.df_id=2003 and df3.df_id=673 and df4.df_id=2001 and df1.CLIENT_ID= ClientID and df1.scenario_id= ScenarioID ;

    
    else 
		INSERT INTO XLPS_PRODUCT_MASTER (CLIENT_ID, Scenario_ID,LOCATION_ID,PRODUCT_ID,PRODUCT_NAME,PRODUCT_TYPE_ID,SHELF_LIFE)
		select distinct df1.CLIENT_ID,df1.Scenario_ID,sr.location_from_id,df1.df_value,df4.df_value,df2.df_value,df3.df_value*24*60*60 from spps_df_input df1 
		inner join spps_relation sr on sr.relation_id =df1.relation_id 
		and sr.client_id=df1.client_id 
		inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id 
		and df1.scenario_id=df2.scenario_id 
		inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id 
		and df1.scenario_id=df3.scenario_id 
		inner join spps_df_input df4 on df1.relation_id=df4.relation_id and df1.client_id=df4.client_id 
		and df1.scenario_id=df4.scenario_id         
		where df1.df_id=2000 and df2.df_id=2003 and df3.df_id=673 and df4.df_id=2001 and df1.CLIENT_ID= ClientID and df1.scenario_id= ScenarioID ;

	end if;
    

    SET @i:=-1;
	UPDATE XLPS_PRODUCT_MASTER SET PRODUCT_INDEX = @i:=@i+1 where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID  order by PRODUCT_TYPE_ID; 
                
	SELECT "Step2 - XLPS_MACHINE_SOLVER_READY",CURTIME() - time_stamp;                
         select CURTIME() into time_stamp;       
	Delete from XLPS_MACHINE_SOLVER_READY where client_id=ClientID  and Scenario_ID=ScenarioID;

	INSERT INTO XLPS_MACHINE_SOLVER_READY
	(CLIENT_ID, Scenario_ID,MACHINE_INDEX, MACHINE_ID,MACHINE_NAME, OPERATION_ID,MAXIMUM_BATCH_SIZE,TASK_SPLIT_ALLOWED)  
	select distinct sr1.CLIENT_ID,df1.Scenario_ID, 1,sr1.resource_ID,df2.df_value,sr1.OPERATION_ID,1000,df3.DF_VALUE from spps_relation sr1
	inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id 
	inner join spps_df_input df2 on sr1.relation_ID= df2.relation_ID and sr1.client_id=df2.client_id and df1.SCENARIO_ID=df2.scenario_id
    inner join spps_df_input df3 on sr1.relation_ID= df3.relation_ID and sr1.client_id=df3.client_id and df1.SCENARIO_ID=df3.scenario_id
	where df1.df_id = 2060 and df2.df_id = 2061 and df3.df_id = 696 and  df1.Scenario_ID=ScenarioID and df1.client_id=ClientID ;

	SET @i:=-1;
	UPDATE XLPS_MACHINE_SOLVER_READY SET ALT_INDEX_KEY = @i:=@i+1 where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID order by operation_id; 

	if Planning_Bucket = "Hours" Then
		update XLPS_MACHINE_SOLVER_READY xl
		SET AVAILABLE_START_TIME_INDEX =0, -- Hour(Planning_Start_Date),
		AVAILABLE_END_TIME_INDEX = AVAILABLE_START_TIME_INDEX + Planning_Horizon + Late_Delivery_Horizon where client_id=ClientID  and Scenario_ID=ScenarioID;
	End If;
   
   	if Planning_Bucket = "Minutes" Then
		update XLPS_MACHINE_SOLVER_READY xl
		SET AVAILABLE_START_TIME_INDEX = 0, -- Hour(Planning_Start_Date) * 60 + Minute(Planning_Start_Date),
		AVAILABLE_END_TIME_INDEX = AVAILABLE_START_TIME_INDEX + Planning_Horizon + Late_Delivery_Horizon
		where client_id=ClientID  and Scenario_ID=ScenarioID;        
	End If;

   	if Planning_Bucket = "Seconds" Then
		update XLPS_MACHINE_SOLVER_READY xl
		SET AVAILABLE_START_TIME_INDEX = 0, -- Hour(Planning_Start_Date) * 3600 + Minute(Planning_Start_Date) *60 + Second(Planning_Start_Date),
		AVAILABLE_END_TIME_INDEX = AVAILABLE_START_TIME_INDEX + Planning_Horizon + Late_Delivery_Horizon
		where client_id=ClientID  and Scenario_ID=ScenarioID;        
	End If;
    COMMIT;
        
    SELECT "Step3 - xlps_machine_shutdown",CURTIME() - time_stamp; 
	select CURTIME() into time_stamp;
	Delete from xlps_machine_shutdown where client_id=ClientID  and Scenario_ID=ScenarioID;

 		BEGIN

			DECLARE MACHINEID VARCHAR(100);
			DECLARE ShutdownStartTime, ShutdownEndTime DATETIME;
			DECLARE k5,k6 INT;
 			DECLARE done BOOLEAN DEFAULT FALSE;
 
 			
			DECLARE cur2 CURSOR FOR SELECT distinct Resource_ID, STR_TO_DATE(df1.df_value,'%m/%d/%Y %H:%i'), STR_TO_DATE(df2.df_value,'%m/%d/%Y %H:%i') FROM spps_df_input df1
            inner join spps_df_input df2 on df1.RELATION_ID=df2.RELATION_ID and df1.CLIENT_ID=df2.CLIENT_ID and df1.SCENARIO_ID=df2.SCENARIO_ID and df1.increment_id=df2.increment_id
            inner join spps_relation sr on sr.RELATION_ID=df1.RELATION_ID and sr.CLIENT_ID=df1.CLIENT_ID
			where df1.client_id=ClientID and df1.df_id=604 and df2.df_id=623  and df1.Scenario_ID=ScenarioID;
            
 	        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;  			
 
 			if Planning_Bucket = "Hours" Then
				OPEN cur2; 
				read_loop:LOOP 
					
					FETCH cur2 INTO MachineID, ShutdownStartTime,ShutdownEndTime;
					IF done = 1 THEN
						LEAVE read_loop;
					END IF;                
					
					SET k5 = Datediff(SHUTDOWNSTARTTIME,Planning_Start_Date) * 24 + Hour(SHUTDOWNSTARTTIME);
					SET k6 = Datediff(SHUTDOWNENDTIME,Planning_Start_Date) * 24 + Hour(SHUTDOWNENDTIME);				
					
					insert into XLPS_Machine_Shutdown (Client_ID, Scenario_ID,Machine_ID,Shutdown_Start_Time_Index,Shutdown_End_Time_Index) 
					values(ClientID,ScenarioID, MachineID,k5,k6);
	 
				END LOOP; 
				CLOSE cur2; 		
				End If;

			if Planning_Bucket = "Minutes" Then
				OPEN cur2; 
				read_loop:LOOP 
					
                    FETCH cur2 INTO MachineID, ShutdownStartTime,ShutdownEndTime;
					IF done = 1 THEN
						LEAVE read_loop;
					END IF;
				  
					SET k5 = Datediff(SHUTDOWNSTARTTIME,Planning_Start_Date) *24*60 + Hour(SHUTDOWNSTARTTIME) * 60 + Minute(SHUTDOWNSTARTTIME);
					SET k6 = Datediff(SHUTDOWNENDTIME,Planning_Start_Date) *24*60 + Hour(SHUTDOWNENDTIME) * 60 + Minute(SHUTDOWNENDTIME);
				
					insert into XLPS_Machine_Shutdown (Client_ID, Scenario_ID,Machine_ID,Shutdown_Start_Time_Index,Shutdown_End_Time_Index) 
					values(ClientID,ScenarioID, MachineID,k5,k6);
	 
				END LOOP; 
				CLOSE cur2; 	
			End If;


			if Planning_Bucket = "Seconds" Then
				OPEN cur2; 
				read_loop:LOOP 
					FETCH cur2 INTO MachineID, ShutdownStartTime,ShutdownEndTime;
					IF done = 1 THEN
						LEAVE read_loop;
					END IF;
			
					SET k5 = Datediff(SHUTDOWNSTARTTIME,Planning_Start_Date) *24*60*60 + Hour(SHUTDOWNSTARTTIME) * 3600 + Minute(SHUTDOWNSTARTTIME) *60 + Second(SHUTDOWNSTARTTIME) ;
					SET k6 = Datediff(SHUTDOWNENDTIME,Planning_Start_Date) *24*60*60 + Hour(SHUTDOWNENDTIME) * 3600 + Minute(SHUTDOWNENDTIME) * 60 + Second(SHUTDOWNENDTIME) ;

					insert into XLPS_Machine_Shutdown (Client_ID, Scenario_ID,Machine_ID,Shutdown_Start_Time_Index,Shutdown_End_Time_Index) 
					values(ClientID,ScenarioID, MachineID,k5,k6);
	 
				END LOOP; 
				CLOSE cur2; 
			End If;	
 		END;
 

        
 	SELECT "Step4 - xlps_machine_valid_ranges",CURTIME() - time_stamp; 
    select CURTIME() into time_stamp;

    Delete from xlps_machine_valid_ranges where client_id=ClientID  and Scenario_ID=ScenarioID;

 		BEGIN

			DECLARE MACHINEID VARCHAR(100);
			DECLARE ShiftStartTime, ShiftEndTime TIME;
			DECLARE k3,k4 DATETIME;
			DECLARE k5,k6,ShiftID,i1,k1,PH,LH INT;
 			DECLARE done BOOLEAN DEFAULT FALSE;
 			
			DECLARE cur1 CURSOR FOR SELECT distinct Resource_ID, df1.Increment_ID,df1.df_value, df2.df_value FROM spps_df_input df1
            inner join spps_df_input df2 on df1.RELATION_ID=df2.RELATION_ID and df1.CLIENT_ID=df2.CLIENT_ID and df1.SCENARIO_ID=df2.SCENARIO_ID
            and df1.increment_id=df2.increment_id
            inner join spps_relation sr on sr.RELATION_ID=df1.RELATION_ID and sr.CLIENT_ID=df1.CLIENT_ID
			where df1.client_id=ClientID and df1.df_id=625 and df2.df_id=626  and df1.Scenario_ID=ScenarioID;

 	        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1; 
 			
            if Planning_Bucket = "Hours" Then
					SET PH = Planning_Horizon/24;
                    SET LH = Late_Delivery_Horizon/24;
			End If;
 
			if Planning_Bucket = "Minutes" Then
					SET PH = Planning_Horizon/(60*24);
                    SET LH = Late_Delivery_Horizon/(60*24);
			End If;

			if Planning_Bucket = "Seconds" Then
					SET PH = Planning_Horizon/(3600*24);
                    SET LH = Late_Delivery_Horizon/(3600*24);
			End If;
                
			SELECT Planning_Horizon, Late_Delivery_Horizon, PH,LH;            
				
 			if Planning_Bucket = "Hours" Then
			
				OPEN cur1; 
				read_loop:LOOP
	 
					FETCH cur1 INTO MachineID, ShiftID, ShiftStartTime, ShiftEndTime;

					IF done = 1 THEN
						LEAVE read_loop;
					END IF;

					SET i1=0;
					WHILE i1 <= PH -1  DO    
					
						if ShiftStartTime < ShiftEndTime Then
						SET k3 = CONCAT(Planning_Start_Date, " ", ShiftStartTime);
						SET k4 = CONCAT(Planning_Start_Date, " ", ShiftEndTime);				
						else
							SET k3 = CONCAT(Planning_Start_Date," ", ShiftStartTime);
							SET k4 = CONCAT(DATE_ADD(Planning_Start_Date, INTERVAL 1 DAY)," ", ShiftEndTime);      
						end if;   
						SET k1 = i1 * 24 ;
						SET k5 = k1 + Datediff(k3,Planning_Start_Date) * 24 + Hour(k3);
						SET k6 = k1 + Datediff(k4,Planning_Start_Date) * 24 + Hour(k4);					
						
						insert into xlps_machine_valid_ranges (Client_ID, Scenario_ID,Machine_ID, Shift_ID, Start_Time_Index, End_Time_Index) 
						values(ClientID, ScenarioID , MachineID, ShiftID, k5, k6); 
                       
						
                        SET i1 = i1 + 1;
						
					END WHILE;
				END LOOP;
				CLOSE cur1;
			End If;



			if Planning_Bucket = "Minutes" Then
			
				OPEN cur1;
				read_loop:LOOP
	 
					FETCH cur1 INTO MachineID, ShiftID, ShiftStartTime, ShiftEndTime;

					IF done = 1 THEN
						LEAVE read_loop;
					END IF;

					SET i1=0;
					WHILE i1 <= PH -1  DO    
					
						if ShiftStartTime < ShiftEndTime Then
						SET k3 = CONCAT(Planning_Start_Date, " ", ShiftStartTime);
						SET k4 = CONCAT(Planning_Start_Date, " ", ShiftEndTime);							
						else
							SET k3 = CONCAT(Planning_Start_Date," ", ShiftStartTime);
							SET k4 = CONCAT(DATE_ADD(Planning_Start_Date, INTERVAL 1 DAY)," ", ShiftEndTime);      
						end if;   
                        SET k1 = i1 * 24*60;
						SET k5 = k1 + Datediff(k3,Planning_Start_Date) * 24*60 + Hour(k3) * 60 + Minute(k3);
						SET k6 = k1 + Datediff(k4,Planning_Start_Date) * 24*60 + Hour(k4) * 60 + Minute(k4);                            

						insert into xlps_machine_valid_ranges (Client_ID, Scenario_ID,Machine_ID, Shift_ID, Start_Time_Index, End_Time_Index) 
						values(ClientID, ScenarioID , MachineID, ShiftID, k5, k6); 
						
                        SET i1 = i1 + 1;
						
					END WHILE;
				END LOOP;
				CLOSE cur1;
			End If;
            

			
            if Planning_Bucket = "Seconds" Then
				OPEN cur1;
	 
				read_loop:LOOP
	 
					FETCH cur1 INTO MachineID, ShiftID, ShiftStartTime, ShiftEndTime;

					IF done = 1 THEN
						LEAVE read_loop;
					END IF;

					SET i1=0;
					WHILE i1 <= PH -1  DO    
					
						if ShiftStartTime < ShiftEndTime Then
							SET k3 = CONCAT(Planning_Start_Date, " ", ShiftStartTime);
							SET k4 = CONCAT(Planning_Start_Date, " ", ShiftEndTime);
						else
							SET k3 = CONCAT(Planning_Start_Date," ", ShiftStartTime);
							SET k4 = CONCAT(DATE_ADD(Planning_Start_Date, INTERVAL 1 DAY)," ", ShiftEndTime);      
						end if;   
						
                        SET k1 = i1 * 24*60*60;
						SET k5 = k1 + Datediff(k3,Planning_Start_Date) * 24*60*60 + Hour(k3) * 3600 + Minute(k3) * 60 + Second(k3);
						SET k6 = k1 + Datediff(k4,Planning_Start_Date) * 24*60*60 + Hour(k4) * 3600 + Minute(k4) * 60 + Second(k4);
							
						insert into xlps_machine_valid_ranges (Client_ID, Scenario_ID,Machine_ID, Shift_ID, Start_Time_Index, End_Time_Index) 
						values(ClientID, ScenarioID , MachineID, ShiftID, k5, k6);
					
						SET i1 = i1 + 1;
						
					END WHILE;
				END LOOP;
				CLOSE cur1;
			End If;
 		END;
 
 
  		BEGIN

			DECLARE MACHINEID VARCHAR(100);
			DECLARE MaxShiftEndTime INT;
 			DECLARE done BOOLEAN DEFAULT FALSE;
 			
			DECLARE cur1 CURSOR FOR SELECT distinct MACHINE_ID, max(End_time_index) FROM xlps_machine_valid_ranges            
            where client_id=ClientID and Scenario_ID= ScenarioID group by MACHINE_ID;

 	        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;  			
 
 			OPEN cur1;
 
 			read_loop:LOOP
 
 				FETCH cur1 INTO MachineID, MaxShiftEndTime;

 				IF done = 1 THEN
 					LEAVE read_loop;
 				END IF;
				
				-- if Planning_Bucket = "Seconds" then
					-- insert into XLPS_Machine_Shutdown (Client_ID, Scenario_ID,Machine_ID,Shutdown_Start_Time_Index,Shutdown_End_Time_Index) 
					-- values(ClientID, ScenarioID, MachineID,MaxShiftEndTime, Planning_horizon);
				
                if Planning_Bucket = "Minutes" then
					insert into XLPS_Machine_Shutdown (Client_ID, Scenario_ID,Machine_ID,Shutdown_Start_Time_Index,Shutdown_End_Time_Index) 
					values(ClientID, ScenarioID, MachineID,MaxShiftEndTime, Planning_horizon);
				
                elseif Planning_Bucket = "Hours" then					
					insert into XLPS_Machine_Shutdown (Client_ID, Scenario_ID,Machine_ID,Shutdown_Start_Time_Index,Shutdown_End_Time_Index) 
					values(ClientID, ScenarioID, MachineID,MaxShiftEndTime, Planning_horizon);
				end if;
 			END LOOP; 
 			CLOSE cur1;
 		
 		END;
        

 
	SELECT "Step6 - XLPS_OPERATION_SOLVER_READY",CURTIME() - time_stamp; 
	select CURTIME() into time_stamp;
	
    Delete from XLPS_OPERATION_SOLVER_READY where client_id=ClientID and Scenario_ID=ScenarioID;

	INSERT INTO XLPS_OPERATION_SOLVER_READY
	(CLIENT_ID,Scenario_ID, OPERATION_INDEX, OPERATION_ID,NO_MACHINES, OPERATION_TYPE)
	select distinct sr1.CLIENT_ID,ScenarioID, 1,sr1.OPERATION_ID,df2.df_value,df1.df_value from spps_relation sr1
    inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id 
    inner join spps_df_input df2 on df1.relation_ID= df2.relation_ID and df1.CLIENT_ID=df2.client_id and df1.scenario_id=df2.SCENARIO_ID
    where df1.df_id = 2077 and df2.df_id = 616 and df1.CLIENT_ID=ClientID and df1.Scenario_ID=ScenarioID;    

	SET @i:=-1;
	UPDATE XLPS_OPERATION_SOLVER_READY SET OPERATION_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;   
	
    Delete from XLPS_PO_SOLVER_READY_TEMP where client_id=ClientID and Scenario_ID=ScenarioID;
    
	INSERT INTO XLPS_PO_SOLVER_READY_TEMP(CLIENT_ID, Scenario_ID,PRODUCTION_ORDER_INDEX, PRODUCTION_ORDER_ID,PRODUCT_ID,ORDER_QUANTITY,PRIORITY,LEVEL)
   	select distinct sr1.CLIENT_ID,ScenarioID,1,sr1.ORDER_ID,pm.product_index,df1.df_value, df3.df_value,pm.PRODUCT_TYPE_ID from spps_relation sr1
    inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id
    inner join spps_df_input df2 on sr1.relation_ID= df2.relation_ID and sr1.client_id=df2.client_id  and df1.scenario_id=df2.SCENARIO_ID
    inner join spps_df_input df3 on sr1.relation_ID= df3.relation_ID and sr1.client_id=df3.client_id  and df1.scenario_id=df3.SCENARIO_ID    
    inner join xlps_product_master pm on pm.product_id=df2.df_value and pm.client_id=df2.client_id and pm.scenario_id=df2.scenario_id
    inner join spps_relation sr2 on sr2.OUTPUT_PRODUCT_ID=pm.PRODUCT_ID and sr2.CLIENT_ID=pm.CLIENT_ID    
    where df1.df_id = 605 and df2.df_id = 636 and df3.df_id = 637  and df1.df_value>0
    and df1.CLIENT_ID=ClientID and df1.Scenario_ID=ScenarioID;
    

	if Planning_Bucket = "Hours" Then
		update XLPS_PO_SOLVER_READY_TEMP xl
		inner join spps_relation sr on sr.order_id=xl.production_order_id and xl.client_id=sr.client_id
		inner join spps_df_input df on sr.relation_ID= df.relation_ID and sr.client_id=df.client_id
		SET ORDER_DUE_DATE_INDEX = Datediff(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s'),Planning_Start_Date) * 24 + Hour(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')),
		ORDER_DUE_DATE = STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')
		where df.df_id = 606 and df.CLIENT_ID=ClientID and df.Scenario_ID=ScenarioID;    
	End If;
 
	if Planning_Bucket = "Minutes" Then
		update XLPS_PO_SOLVER_READY_TEMP xl
		inner join spps_relation sr on sr.order_id=xl.production_order_id and xl.client_id=sr.client_id
		inner join spps_df_input df on sr.relation_ID= df.relation_ID and sr.client_id=df.client_id
		SET ORDER_DUE_DATE_INDEX = Datediff(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s'),Planning_Start_Date) * 24*60 + Hour(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')) * 60
        + Minute(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')),
		ORDER_DUE_DATE = STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')
		where df.df_id = 606 and df.CLIENT_ID=ClientID and df.Scenario_ID=ScenarioID;    
	End If;
 
 	if Planning_Bucket = "Seconds" Then
		update XLPS_PO_SOLVER_READY_TEMP xl
		inner join spps_relation sr on sr.order_id=xl.production_order_id and xl.client_id=sr.client_id
		inner join spps_df_input df on sr.relation_ID= df.relation_ID and sr.client_id=df.client_id
		SET ORDER_DUE_DATE_INDEX = Datediff(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s'),Planning_Start_Date) * 24*60*60 + Hour(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')) *60*60
        + Minute(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s'))*60 + Second(STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')),
		ORDER_DUE_DATE = STR_TO_DATE(df_value,'%m/%d/%Y %H:%i:%s')
		where df.df_id = 606 and df.CLIENT_ID=ClientID and df.Scenario_ID=ScenarioID;    
	End If;
    
	SET @i:=-1;
	UPDATE XLPS_PO_SOLVER_READY_TEMP SET PRODUCTION_ORDER_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID
    order by Priority, ORDER_DUE_DATE_INDEX; 
 
	Delete from XLPS_PO_SOLVER_READY where client_id=ClientID and Scenario_ID=ScenarioID;
   
	INSERT INTO XLPS_PO_SOLVER_READY
	( CLIENT_ID, Scenario_ID,PRODUCTION_ORDER_INDEX, PRODUCTION_ORDER_ID,PRODUCT_ID,ORDER_QUANTITY,PRIORITY, ORDER_DUE_DATE,ORDER_DUE_DATE_INDEX,LEVEL)
	select distinct po.CLIENT_ID, ScenarioID,1,po.PRODUCTION_ORDER_ID,po.product_id,po.ORDER_QUANTITY, po.priority,po.ORDER_DUE_DATE,po.ORDER_DUE_DATE_INDEX,LEVEL from 
	XLPS_PO_SOLVER_READY_TEMP po where po.ORDER_DUE_DATE_INDEX <= Planning_Horizon and
    po.CLIENT_ID=ClientID and po.Scenario_ID=ScenarioID;
            
	SET @i:=-1;
	UPDATE XLPS_PO_SOLVER_READY SET PRODUCTION_ORDER_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID
    order by Priority, ORDER_DUE_DATE_INDEX;    


 	SET i=0;
 	SELECT count(*) INTO count FROM XLPS_MACHINE_SOLVER_READY where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
	SELECT MIN(ALT_INDEX_KEY) INTO sTK FROM XLPS_MACHINE_SOLVER_READY where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
    SELECT OPERATION_ID INTO sPrevOpID FROM XLPS_MACHINE_SOLVER_READY where ALT_INDEX_KEY=sTK and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

 	SET OpCounter=0;
     WHILE i < count DO
		SELECT OPERATION_ID INTO sNextOpID FROM XLPS_MACHINE_SOLVER_READY where ALT_INDEX_KEY=sTK+i and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
 		if sNextOpID=sPrevOpID then
 			SET OpCounter=OpCounter+1;
 		else
 			SET OpCounter=1;
 		end if;
        SET sPrevOpID=sNextOpID;
         UPDATE XLPS_MACHINE_SOLVER_READY
         SET ALTERNATE_INDEX=OpCounter-1 where ALT_INDEX_KEY=sTK+i and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
			SET i=i+1;
 	end while;





   
    SET @i:=-1;
	UPDATE XLPS_MACHINE_SOLVER_READY SET MACHINE_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;  

	SET @i:=-1;
	UPDATE XLPS_MACHINE_SOLVER_READY SET ALT_INDEX_KEY = @i:=@i+1 where CLIENT_ID = ClientID  and Scenario_ID=ScenarioID order by operation_id; 

	-- to handle single resources performing multiple operations, all jobs ,all operations on that machine should be added to machine jobs set
	update xlps_machine_solver_ready m1 join 
    (select CLIENT_ID, Scenario_ID, machine_id, min(machine_index) as minindex from xlps_machine_solver_ready  
       where CLIENT_ID = ClientID  and Scenario_ID=ScenarioID group by machine_id) m2
       on m1.machine_id = m2.machine_id and m1.CLIENT_ID=m2.CLIENT_ID and m1.Scenario_ID=m2.Scenario_ID
       set m1.machine_index = minindex 
       where m1.CLIENT_ID = ClientID  and m1.Scenario_ID=ScenarioID;
       

    SELECT "Step7 - XLPS_ROUTE_SOLVER_READY",CURTIME() - time_stamp;
	select CURTIME() into time_stamp;
    
    -- SELECT 9;
  
    insert into spps_relation_temp (RELATION_ID,OUTPUT_PRODUCT,OPERATION_ID,RESOURCE_ID,CLIENT_ID)
	select distinct RELATION_ID,OUTPUT_PRODUCT_ID,OPERATION_ID,RESOURCE_ID,CLIENT_ID from spps_relation sr
	where sr.client_id=ClientID;
    
    -- SELECT 10;

	insert into spps_df_input_temp (TABLE_KEY,RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID)
	select TABLE_KEY,RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID from spps_df_input df
	where df.CLIENT_ID = ClientID and df.SCENARIO_ID = ScenarioID and DF_ID in (608,609,610,620,661,670,675);
	-- SELECT 20;
	
    Delete from XLPS_ROUTE_SOLVER_READY where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

	SET i=0;
	SELECT max(production_order_index) INTO nPO FROM XLPS_PO_SOLVER_READY where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
    
    if ClientID=31 then
		-- WHILE i < nPO DO
    
		 -- SELECT i;
		
		-- SELECT PRODUCTION_ORDER_ID INTO sPOID FROM XLPS_PO_SOLVER_READY where PRODUCTION_ORDER_INDEX=i and CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
	
		-- SELECT PRODUCT_Index INTO sProductID FROM XLPS_PO_SOLVER_READY sr
        -- inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.scenario_id=sr.scenario_id
        -- where PRODUCTION_ORDER_INDEX=i and pm.CLIENT_ID=ClientID  and pm.Scenario_ID=ScenarioID;
        
        INSERT INTO XLPS_ROUTE_SOLVER_READY(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ROUTING_ID,SEQUENCE_ID,ALTERNATE_INDEX,MACHINE_INDEX,
											OPERATION_SETUP_TIME,PROCESSING_TIME,TEARDOWN_TIME,MIN_BATCH_SIZE, MAX_BATCH_SIZE, OUTPUT_PRODUCT_ID,OUTPUT_PRODUCT_INDEX,LEVEL) 
                              
				select distinct ClientID,ScenarioID,production_order_index, osr.operation_index, df6.df_value ,df5.df_value , osr1.alternate_index,osr1.machine_index,
                df1.df_value,df3.df_value,IF(df4.df_value="",0, CONVERT(df4.df_value,UNSIGNED)), REPLACE(df7.df_value, ",", ""), REPLACE(df7.df_value, ",", ""),
                sr1.OUTPUT_PRODUCT,pm.product_index,pm.PRODUCT_TYPE_ID from spps_df_input df1
				inner join spps_relation_temp sr on sr.relation_id=df1.relation_id and sr.client_id=df1.client_id and sr.CLIENT_ID=ClientID and df1.SCENARIO_ID=ScenarioID 
                inner join spps_df_input_temp df3 on df1.RELATION_ID=df3.relation_id and df1.INCREMENT_ID=df3.INCREMENT_ID
                and df3.SCENARIO_ID=ScenarioID and df3.CLIENT_ID=ClientID and df3.df_id =609
                inner join spps_df_input_temp df4 
                on df1.RELATION_ID=df4.relation_id and df1.INCREMENT_ID=df4.INCREMENT_ID and df1.SCENARIO_ID=ScenarioID and df4.CLIENT_ID=ClientID and df4.df_id =610
                inner join xlps_product_master pm
                on pm.product_id=sr.OUTPUT_PRODUCT and pm.client_id=ClientID and pm.scenario_id=ScenarioID
				inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=sr.operation_id and osr.client_id=sr.CLIENT_ID and osr.scenario_id=df1.scenario_id
                inner join xlps_machine_solver_ready osr1 on osr1.OPERATION_ID=sr.operation_id and osr1.machine_id= sr.resource_id and osr1.client_id=sr.CLIENT_ID and osr1.scenario_id=df1.scenario_id
                inner join spps_relation_temp  sr1 
                on sr1.OUTPUT_PRODUCT=sr.OUTPUT_PRODUCT and sr1.OPERATION_ID=sr.OPERATION_ID and sr1.client_id=ClientID
                inner join spps_df_input_temp df5 
                on sr1.RELATION_ID=df5.relation_id and sr1.client_id=ClientID and df5.scenario_id=ScenarioID and df5.df_id =620
                inner join spps_df_input_temp df6 
                on sr1.RELATION_ID=df6.relation_id and sr1.client_id=ClientID and df6.scenario_id=ScenarioID and df6.df_id =661
                and df5.INCREMENT_ID=df6.increment_id
				inner join spps_df_input_temp df7 
                on sr.RELATION_ID=df7.relation_id and df7.increment_id=df1.increment_id and sr1.client_id=ClientID and df7.scenario_id=ScenarioID  and df7.df_id =675  
				inner join xlps_po_solver_ready po
                on pm.product_index=po.PRODUCT_id  and po.order_quantity=REPLACE(df7.df_value, ",", "") and po.client_id=ClientID and po.scenario_id=ScenarioID  -- and production_order_index=i
				where df1.df_id= 608 and sr.client_id=ClientID and df1.Scenario_ID=ScenarioID  and production_order_index between 0 and nPO;

		-- SET  i=i+1;
	-- END WHILE;    
	else 
    
        
        INSERT INTO XLPS_ROUTE_SOLVER_READY(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ROUTING_ID,SEQUENCE_ID,ALTERNATE_INDEX,MACHINE_INDEX,
											OPERATION_SETUP_TIME,PROCESSING_TIME,TEARDOWN_TIME,MIN_BATCH_SIZE, MAX_BATCH_SIZE,OUTPUT_PRODUCT_ID,OUTPUT_PRODUCT_INDEX,LEVEL) 
       
       				select ClientID,ScenarioID,po.PRODUCTION_ORDER_INDEX, osr.operation_index, df6.df_value ,df5.df_value , osr1.alternate_index,osr1.machine_index,df1.df_value,df3.df_value,
                IF(df4.df_value="",0, CONVERT(df4.df_value,UNSIGNED)),
                 REPLACE(df7.df_value, ",", ""),  REPLACE(df7.df_value, ",", ""),
                sr1.OUTPUT_PRODUCT,pm.product_index,pm.PRODUCT_TYPE_ID from spps_df_input df1
				inner join (select client_id,relation_id,OUTPUT_PRODUCT,OPERATION_ID,resource_id from spps_relation_temp where client_ID=Clientid) sr on sr.relation_id=df1.relation_id
                inner join (select relation_id, increment_id,df_value from spps_df_input_temp where SCENARIO_ID=SCENARIOID and CLIENT_ID=CLIENTID and df_id =609) df3 
                on df1.RELATION_ID=df3.relation_id and df1.INCREMENT_ID=df3.INCREMENT_ID
                inner join (select relation_id, increment_id,df_value from spps_df_input_temp where SCENARIO_ID=SCENARIOID and CLIENT_ID=CLIENTID and df_id =610) df4 
                on df1.RELATION_ID=df4.relation_id and df1.INCREMENT_ID=df4.INCREMENT_ID
                inner join (select product_id,product_index,product_type_id from xlps_product_master where client_id=clientid and scenario_id=scenarioid) pm on pm.product_id=sr.OUTPUT_PRODUCT 
				inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=sr.operation_id and osr.client_id=sr.CLIENT_ID and osr.scenario_id=df1.scenario_id
                inner join xlps_machine_solver_ready osr1 on osr1.OPERATION_ID=sr.operation_id and osr1.machine_id= sr.resource_id and osr1.client_id=sr.CLIENT_ID and osr1.scenario_id=df1.scenario_id
                inner join (select relation_id,output_product, operation_id from spps_relation_temp  where client_id=CLIENTID) sr1 
                on sr1.OUTPUT_PRODUCT=sr.OUTPUT_PRODUCT and sr1.OPERATION_ID=sr.OPERATION_ID 
                inner join (select relation_id, increment_id,df_value from spps_df_input_temp where client_id=clientid and scenario_id=scenarioid and df_id =620) df5 on sr1.RELATION_ID=df5.relation_id
                inner join (select relation_id, increment_id,df_value from spps_df_input_temp where client_id=clientid and scenario_id=scenarioid and df_id =661) df6 on sr1.RELATION_ID=df6.relation_id
                and df5.INCREMENT_ID=df6.increment_id
                 inner join (select relation_id, increment_id,df_value from spps_df_input_temp where client_id=clientid and scenario_id=scenarioid  and df_id =675  ) df7 
                 on sr.RELATION_ID=df7.relation_id and df7.increment_id=df1.increment_id
--                 inner join (select relation_id, increment_id,df_value from spps_df_input_temp where client_id=clientid and scenario_id=scenarioid and df_id =670) df8 on sr.RELATION_ID=df8.relation_id 
--                 and df7.INCREMENT_ID=df8.increment_id 
				-- inner join (select production_order_index, product_index from  xlps_po_products where client_id=clientid and scenario_id=scenarioid ) pop 
                -- on pop.PRODUCTION_ORDER_INDEX=i and pm.product_index=pop.PRODUCT_INDEX
				inner join (select production_order_index, product_id,order_quantity from  xlps_po_solver_ready where client_id=CLIENTID and scenario_id=scenarioid ) po
                on pm.product_index=po.PRODUCT_id 
				where df1.df_id= 608 and sr.client_id=ClientID and df1.Scenario_ID=ScenarioID and po.PRODUCTION_ORDER_INDEX between 0 and nPO ;  
                
                
                
                -- 
-- 				select ClientID,ScenarioID,i, osr.operation_index, df6.df_value ,df5.df_value , osr1.alternate_index,osr1.machine_index,df1.df_value,df3.df_value,
--                 IF(df4.df_value="",0, CONVERT(df4.df_value,UNSIGNED)),df7.df_value,df8.df_value,
--                 sr1.OUTPUT_PRODUCT,pm.product_index,pm.PRODUCT_TYPE_ID from spps_df_input df1
-- 				inner join spps_relation_temp sr on sr.relation_id=df1.relation_id and sr.client_ID=df1.Client_id
--                 inner join spps_df_input_temp df3 on df1.RELATION_ID=df3.relation_id and df1.SCENARIO_ID=df3.SCENARIO_ID and df1.CLIENT_ID=df3.CLIENT_ID and df1.INCREMENT_ID=df3.INCREMENT_ID
--                 inner join spps_df_input_temp df4 on df1.RELATION_ID=df4.relation_id and df1.SCENARIO_ID=df4.SCENARIO_ID and df1.CLIENT_ID=df4.CLIENT_ID and df1.INCREMENT_ID=df4.INCREMENT_ID
--                 inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT and pm.client_id=sr.client_id and pm.scenario_id=df1.scenario_id
-- 
-- 				inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=sr.operation_id and osr.client_id=sr.CLIENT_ID and osr.scenario_id=df1.scenario_id
--                 inner join xlps_machine_solver_ready osr1 on osr1.OPERATION_ID=sr.operation_id and osr1.machine_id= sr.resource_id and osr1.client_id=sr.CLIENT_ID and osr1.scenario_id=df1.scenario_id
--                 inner join spps_relation_temp sr1 on sr1.OUTPUT_PRODUCT=sr.OUTPUT_PRODUCT and sr1.OPERATION_ID=sr.OPERATION_ID and sr1.client_id=sr.CLIENT_ID
--                 inner join spps_df_input_temp df5 on sr1.RELATION_ID=df5.relation_id and df1.SCENARIO_ID=df5.SCENARIO_ID and df1.CLIENT_ID=df5.CLIENT_ID
--                 inner join spps_df_input_temp df6 on sr1.RELATION_ID=df6.relation_id and df1.SCENARIO_ID=df6.SCENARIO_ID and df1.CLIENT_ID=df6.CLIENT_ID 
--                 and df5.INCREMENT_ID=df6.increment_id
--                 inner join spps_df_input_temp df7 on sr.RELATION_ID=df7.relation_id and df1.SCENARIO_ID=df7.SCENARIO_ID and df1.CLIENT_ID=df7.CLIENT_ID
--                 inner join spps_df_input_temp df8 on sr.RELATION_ID=df8.relation_id and df1.SCENARIO_ID=df8.SCENARIO_ID and df1.CLIENT_ID=df8.CLIENT_ID 
--                 and df7.INCREMENT_ID=df8.increment_id 
--                  inner join xlps_po_products pop on pop.PRODUCTION_ORDER_INDEX=i and pm.product_index=pop.PRODUCT_INDEX  and pop.scenario_id=pm.SCENARIO_ID and pop.client_id=pm.client_id
-- 				where df1.df_id= 608 and df3.df_id =609 and df4.df_id =610  and df5.df_id =620  and df6.df_id =661
--                  and df7.df_id =675  and df8.df_id =670
--                 and sr.client_id=ClientID and df1.Scenario_ID=ScenarioID;  

  
    
    end if;

	if ClientID=31 then
				truncate table xlps_routing_temp;
				truncate table xlps_routing_temp2;

--                INSERT INTO xlps_routing_temp(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,ROUTING_ID,Seq_Count)                 
-- 				select distinct ClientID,ScenarioID,po.production_order_index, df6.df_value ,count(df5.df_value) from spps_df_input df5
-- 				inner join (select relation_id,output_product, operation_id from spps_relation_temp  where client_id=ClientID) sr1 
--                 on sr1.relation_id =df5.relation_id and df5.scenario_id=ScenarioID
--                 inner join (select relation_id, increment_id,df_value from spps_df_input_temp where client_id=ClientID and scenario_id=ScenarioID and df_id =661) df6 
--                 on sr1.RELATION_ID=df6.relation_id and df5.INCREMENT_ID=df6.increment_id
--                 inner join (select product_id,product_index,product_type_id from xlps_product_master where client_id=ClientID and scenario_id=ScenarioID) pm 
--                 on pm.product_id=sr1.OUTPUT_PRODUCT
-- 				inner join (select production_order_index, product_id,order_quantity from  xlps_po_solver_ready where client_id=ClientID and scenario_id=ScenarioID ) po
--                 on pm.product_index=po.PRODUCT_id
-- 				inner join (select production_order_index, product_index from  xlps_po_products where client_id=ClientID and scenario_id=ScenarioID ) pop 
--                 on pop.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pm.product_index=pop.PRODUCT_INDEX
-- 				where df5.df_id= 620 and df5.client_id=ClientID and df5.Scenario_ID=ScenarioID
--                 group  by CLIENT_ID,Scenario_ID,po.production_order_index, df6.df_value;  
				
 
                
                INSERT INTO xlps_routing_temp2(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,ROUTING_ID,Seq_Count)
                select distinct client_id, scenario_id, production_order_index, routing_id, count(operation_index)  from xlps_route_solver_ready
                where client_id=ClientID and Scenario_ID=ScenarioID
                 group by  production_order_index,routing_id;
                 
                 -- select * from xlps_routing_temp2;
                 
				INSERT INTO xlps_routing_temp(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,ROUTING_ID,Seq_Count)  
                select distinct ClientID,ScenarioID,po.production_order_index, df6.df_value ,count(osr.operation_index) from spps_df_input df6
				inner join (select relation_id,output_product, operation_id from spps_relation_temp  where client_id=ClientID) sr1 
                on sr1.relation_id =df6.relation_id and df6.scenario_id=ScenarioID
                inner join (select product_id,product_index,product_type_id from xlps_product_master where client_id=ClientID and scenario_id=ScenarioID) pm 
                on pm.product_id=sr1.OUTPUT_PRODUCT
				inner join (select production_order_index, product_id,order_quantity from  xlps_po_solver_ready where client_id=ClientID and scenario_id=ScenarioID ) po
                on pm.product_index=po.PRODUCT_id
				-- inner join (select production_order_index, product_index from  xlps_po_products where client_id=ClientID and scenario_id=ScenarioID ) pop 
                -- on pop.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX and pm.product_index=pop.PRODUCT_INDEX
                inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=sr1.operation_id
				where df6.df_id= 661 and df6.client_id=ClientID and df6.Scenario_ID=ScenarioID
                 group  by df6.CLIENT_ID,df6.Scenario_ID,po.production_order_index, df6.df_value; 
                 
                -- select * from xlps_routing_temp; 
                 
                 delete from xlps_route_solver_ready where 
                 (production_order_index,routing_id) in (
                 select rt.production_order_index,rt.routing_id from xlps_routing_temp rt inner join
                 xlps_routing_temp2 rt2 on rt.PRODUCTION_ORDER_INDEX=rt2.PRODUCTION_ORDER_INDEX
                 and rt.routing_id=rt2.ROUTING_ID and rt.SCENARIO_ID=rt2.scenario_id and rt.CLIENT_ID=rt2.CLIENT_ID
                 where rt.seq_count>rt2.seq_count and rt.scenario_id=ScenarioID and rt.client_id=clientID);
        End if;        

  
        
    
    
        

	
	SELECT "Step9 - xlps_po_products",CURTIME() - time_stamp;     
    select CURTIME() into time_stamp;
    
	Delete from xlps_bom_temp where client_id=ClientID and Scenario_ID=ScenarioID;

	insert into xlps_bom_temp(CLIENT_ID ,Scenario_ID, INPUT_PRODUCT,OUTPUT_PRODUCT, ATTACH_RATE)
	select sr.client_id,df.Scenario_ID,sr.INPUT_PRODUCT_ID,sr.OUTPUT_PRODUCT_ID, df_value from spps_relation sr inner join
	spps_df_input df on df.client_id=sr.client_id and df.relation_id=sr.relation_id
	where df.df_id=602 and sr.client_id=ClientID and df.Scenario_ID=ScenarioID;
		
	update xlps_bom_temp bm
	inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT
	set bm.DESIGNATION='CHILD' 
	where pm.product_type_id=1 and pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;

	update xlps_bom_temp bm
	inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT
	set bm.DESIGNATION='PARENT' 
	where pm.product_type_id=2 or pm.product_type_id=3 and pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;

	update xlps_bom_temp bm
	inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.OUTPUT_PRODUCT
	set bm.OUTPUT_PRODUCT_INDEX=pm.PRODUCT_INDEX 
	where pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;
            

		SET i=0;
        SET j=9999;
		SELECT count(*) INTO nPO FROM XLPS_PO_SOLVER_READY where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
		WHILE i < nPO DO
			SET k = 0;
            
            if k=0 then
				SELECT distinct PRODUCT_INDEX,pm.PRODUCT_ID INTO sProductID,prodID FROM XLPS_PO_SOLVER_READY sr
				inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.SCENARIO_ID=sr.SCENARIO_ID
				where PRODUCTION_ORDER_INDEX=i and pm.CLIENT_ID=ClientID  and pm.Scenario_ID=ScenarioID;
	
				INSERT INTO xlps_bom_temp(CLIENT_ID ,Scenario_ID,INPUT_PRODUCT,INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT,OUTPUT_PRODUCT_INDEX,DESIGNATION) 
				VALUES (ClientID,ScenarioID, prodID,j, prodID,j,'ROOT');

				INSERT INTO xlps_bom_temp(CLIENT_ID ,SCENARIO_ID,INPUT_PRODUCT,OUTPUT_PRODUCT,OUTPUT_PRODUCT_INDEX,DESIGNATION) 
				VALUES (ClientID,ScenarioID, prodID, prodID,j,'PARENT');
            
				SET k=k+1;
            End If;
            
			SET j = j+1;
			SET i = i+1;
        END WHILE;

			update xlps_bom_temp bm
			inner join xlps_product_master pm on pm.client_id=bm.client_id  and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT
			set bm.INPUT_PRODUCT_INDEX=pm.PRODUCT_INDEX
			where bm.designation<>'ROOT' and pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;
          
             delete from xlps_bom_temp 
             where INPUT_PRODUCT_INDEX = OUTPUT_PRODUCT_INDEX
             and designation='PARENT' and client_id=ClientID and scenario_ID=ScenarioID;
            

	Delete from xlps_po_products where client_id=ClientID and Scenario_ID=ScenarioID;
	Delete from xlps_tree_table where client_id=ClientID and Scenario_ID=ScenarioID;
    
    insert into xlps_tree_table(CLIENT_ID,Scenario_ID,FG,Components)
	SELECT distinct CLIENT_ID,Scenario_ID,INPUT_PRODUCT_INDEX, GetFamilyTree2(INPUT_PRODUCT_INDEX,ClientID,ScenarioID) FROM xlps_bom_temp 
	WHERE Designation = 'PARENT'  and CLIENT_ID=ClientID and Scenario_ID=ScenarioID order by ID desc;
	
    SET i=0;
	SELECT count(*) INTO nPO FROM XLPS_PO_SOLVER_READY where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
	WHILE i < nPO DO
		SELECT distinct PRODUCT_INDEX INTO sProductID FROM XLPS_PO_SOLVER_READY sr
        inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.SCENARIO_ID=sr.SCENARIO_ID
        where PRODUCTION_ORDER_INDEX=i and pm.CLIENT_ID=ClientID  and pm.Scenario_ID=ScenarioID;
        

		BEGIN
			DECLARE FG1, Components1 VARCHAR(100);  
            DECLARE a, Quantity BIGINT Default 0 ;
			DECLARE str VARCHAR(255);
			DECLARE done2 BOOLEAN DEFAULT FALSE;  
           
           DECLARE cur2 CURSOR FOR SELECT ID, Components FROM xlps_bom_temp bo 
			inner join xlps_tree_table tr on tr.CLIENT_ID = bo.CLIENT_ID and tr.Scenario_ID = bo.Scenario_ID and tr.FG = bo.INPUT_PRODUCT_INDEX
			WHERE Designation = 'PARENT' and bo.INPUT_PRODUCT_INDEX=sProductID and bo.CLIENT_ID=ClientID and bo.Scenario_ID=ScenarioID order by ID desc;             
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = 1;              
 			          
            OPEN cur2; 
 			read_loop:LOOP 
            
				
 				FETCH cur2 INTO FG1,Components1;
		
 				IF done2 = 1 THEN
 					LEAVE read_loop;
 				END IF;
 				SET a=0;
                
				select distinct PRODUCTION_ORDER_INDEX into POs from xlps_po_products where  client_id=ClientID and scenario_ID=ScenarioID
				and production_order_index=i;
                        
				if ifnull(POs,1) = 1 Then
							
					insert into xlps_po_products(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,PRODUCT_ID) 
					select client_id,ScenarioID,i,INPUT_PRODUCT from xlps_bom_temp
					where ID = trim(FG1) and client_id = ClientID and scenario_ID=ScenarioID;
					select INPUT_PRODUCT_INDEX into sProductID from xlps_bom_temp where ID = trim(FG1) and
					 client_id = ClientID and scenario_ID=ScenarioID;
	  
					simple_loop: LOOP
							 SET a=a+1;
							 SET str=SPLIT_STR(Components1,",",a);
							
							 IF str='' THEN
								LEAVE simple_loop;
							 END IF;

							insert into xlps_po_products(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,PRODUCT_ID) 
							select client_id,ScenarioID,i,INPUT_PRODUCT from xlps_bom_temp
							where ID = trim(str)   and client_id=ClientID and scenario_ID=ScenarioID;                            
							
					END LOOP simple_loop;
                End If;
 			END LOOP read_loop;  
 			CLOSE cur2; 		
 		END; 
        SET i = i+ 1;
	END WHILE;
 

	update xlps_po_products bm
	inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.PRODUCT_ID=bm.PRODUCT_ID and pm.scenario_id=bm.scenario_id
	set bm.PRODUCT_INDEX=pm.PRODUCT_INDEX
	where pm.client_id=ClientID and pm.scenario_id=ScenarioID;
	
    
    
    

        

	SELECT "Step8 - XLPS_SETUP_SOLVER_READY",CURTIME() - time_stamp; 
    select CURTIME() into time_stamp;
	
    Delete from XLPS_SETUP_SOLVER_READY where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

    INSERT INTO XLPS_SETUP_SOLVER_READY(CLIENT_ID,Scenario_ID, MACHINE_ID, PRODUCT_FROM, PRODUCT_TO,SETUP_TIME)        
    select sr.CLIENT_ID,df.Scenario_ID,sr.RESOURCE_ID,pm1.PRODUCT_INDEX,pm.PRODUCT_INDEX, df.df_value from spps_relation sr                
	INNER JOIN spps_df_input df ON sr.CLIENT_ID=df.CLIENT_ID and df.relation_id=sr.relation_id		
    inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT_ID and pm.client_id=sr.client_id and pm.SCENARIO_ID=df.SCENARIO_ID
    inner join xlps_product_master pm1 on pm1.product_id=sr.INPUT_PRODUCT_ID and pm1.client_id=sr.client_id and pm1.SCENARIO_ID=df.SCENARIO_ID
	where df.CLIENT_ID = ClientID and df.df_id=622  and df.Scenario_ID=ScenarioID;   
 
	update XLPS_SETUP_SOLVER_READY xl
	INNER JOIN xlps_machine_solver_ready msr ON xl.MACHINE_ID=msr.MACHINE_ID and xl.CLIENT_ID=msr.CLIENT_ID  and xl.Scenario_ID=msr.Scenario_ID
	set xl.MACHINE_INDEX= msr.machine_index where xl.CLIENT_ID = ClientID and xl.Scenario_ID=ScenarioID;  
    
	SELECT "Step10 - xlps_material_receipts",CURTIME() - time_stamp; 
    select CURTIME() into time_stamp;
    
	Delete from xlps_material_receipts where client_id=ClientID and Scenario_ID=ScenarioID;
    Delete from xlps_boh where client_id=ClientID and Scenario_ID=ScenarioID;
  

  START TRANSACTION;
    
    INSERT INTO xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID, ARRIVAL_TIME, QUANTITY,BOH_Update_Flag)
	select distinct  df1.CLIENT_ID, df1.Scenario_ID, pm.PRODUCT_INDEX,  STR_TO_DATE(df1.df_value,'%m/%d/%Y') ,df2.df_value,0 
	from spps_relation sr  
	inner join spps_df_input df1 on sr.client_id=df1.client_id and sr.relation_id=df1.relation_id
    inner join spps_df_input df2 on sr.client_id=df2.client_id and sr.relation_id=df2.relation_id and df1.scenario_id=df2.scenario_id 
    and df1.increment_id=df2.increment_id
    inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT_ID and pm.client_id=sr.client_id and pm.SCENARIO_ID=df1.SCENARIO_ID
    where df1.CLIENT_ID=ClientID and df1.Scenario_ID=ScenarioID 
    and df1.df_id=638 and df2.df_id=639;
    

    if Planning_Bucket = "Hours" Then    
		update xlps_material_receipts xl
		SET TIME_INDEX = Datediff(ARRIVAL_TIME,Planning_Start_Date) * 24 + Hour(ARRIVAL_TIME)
		where client_id=ClientID  and Scenario_ID=ScenarioID;
	End If;
    
    if Planning_Bucket = "Minutes" Then
		update xlps_material_receipts xl
		SET TIME_INDEX = Datediff(ARRIVAL_TIME,Planning_Start_Date) * 24*60 + Hour(ARRIVAL_TIME) *60 + Minute(ARRIVAL_TIME)
		where client_id=ClientID  and Scenario_ID=ScenarioID;
	End If;
 
     if Planning_Bucket = "Seconds" Then    
		update xlps_material_receipts xl
		SET TIME_INDEX = Datediff(ARRIVAL_TIME,Planning_Start_Date) * 24*60*60 + Hour(ARRIVAL_TIME) *60*60 + Minute(ARRIVAL_TIME)*60 + Second(ARRIVAL_TIME)
		where client_id=ClientID  and Scenario_ID=ScenarioID;
	End If;
    
    
	INSERT INTO xlps_boh(CLIENT_ID, Scenario_ID, PRODUCT_INDEX, ARRIVAL_TIME, TIME_INDEX,QUANTITY)
	select distinct  df1.CLIENT_ID, df1.Scenario_ID, pm.PRODUCT_INDEX,  Planning_Start_Date,0, df1.df_value 
	from spps_relation sr  
	inner join spps_df_input df1 on sr.client_id=df1.client_id and sr.relation_id=df1.relation_id
    inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT_ID and pm.client_id=sr.client_id and pm.SCENARIO_ID=df1.SCENARIO_ID
    and pm.LOCATION_ID=sr.LOCATION_FROM_ID
    where df1.CLIENT_ID=ClientID and df1.Scenario_ID=ScenarioID and df1.df_id=677;
   
	update xlps_material_receipts xl inner join xlps_boh bh on xl.product_id=bh.product_index and xl.Time_Index=bh.Time_Index
    and xl.client_id=bh.client_id and xl.Scenario_ID=bh.Scenario_ID
    SET xl.Quantity = xl.Quantity + bh.Quantity, BOH_Update_Flag =1
    where xl.time_index = 0 and xl.client_id=ClientID  and xl.Scenario_ID=ScenarioID;
    
	update   xlps_material_receipts xl inner join xlps_material_receipts xl1 
     on xl.product_id=xl1.product_id and xl.client_id=xl1.client_id and xl.Scenario_ID=xl1.Scenario_ID
     set xl1.BOH_Update_Flag=1 where xl.BOH_Update_Flag=1 and xl.client_id=ClientID  and xl.Scenario_ID=ScenarioID;
     
    -- SELECT 10;   
     drop table if exists xlps_boh_temp;
     
     create temporary table xlps_boh_temp(product_index int(11), Quantity int(11));    
 	INSERT INTO xlps_boh_temp(PRODUCT_INDEX, QUANTITY)
 	select distinct  PRODUCT_INDEX, bh.quantity 
 	from xlps_boh bh left join xlps_material_receipts mr on mr.PRODUCT_ID=bh.PRODUCT_INDEX and mr.CLIENT_ID=bh.CLIENT_ID
     and mr.SCENARIO_ID=bh.SCENARIO_ID 
     where bh.time_index=0 and mr.BOH_Update_Flag!=1 and bh.CLIENT_ID=CLIENTID and bh.Scenario_ID=ScenarioID;
     -- SELECT 20;
     
 	 INSERT INTO xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID,ARRIVAL_TIME, TIME_INDEX, QUANTITY)
 	 select distinct  CLIENTID, ScenarioID, PRODUCT_INDEX, Planning_Start_Date, 0,quantity from xlps_boh_temp ;
   
   COMMIT; 
     -- select * from xlps_material_receipts;
    
    
    BEGIN
            DECLARE done5 BOOLEAN DEFAULT FALSE;
            DECLARE cur5 CURSOR FOR select distinct product_id from xlps_material_receipts sr 						
			where client_id=ClientID and Scenario_ID=ScenarioID order by product_id;
            
 	        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done5 = 1; 
                        
			set @csum := 0;
 			if Planning_Bucket = "Hours" Then
				OPEN cur5; 
				read_loop:LOOP 
					
					FETCH cur5 INTO pid;
					IF done5 = 1 THEN
						LEAVE read_loop;
					END IF;                    

					update xlps_material_receipts
					set CUMUL_QUANTITY = (@csum := @csum + QUANTITY)
					where client_id=ClientID  and Scenario_ID=ScenarioID
					and product_id =pid
					order by product_id, time_index;
					
					SET check_start_time = (SELECT time_index from xlps_material_receipts where time_index = 0 and product_id=pid and 
					client_id=ClientID and scenario_Id=ScenarioID and Quantity is not NULL);                     
								
					 if check_start_time!=0 or ifnull(check_start_time,1) = 1 Then
						 insert into xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID, TIME_INDEX, Cumul_QUANTITY)
						 values(CLIENTID, ScenarioID,pid,0,0);
					 End if;					                 
		
					SET  check_last_time = (SELECT time_index from xlps_material_receipts 
					where time_index = Planning_Horizon + Hour(Planning_Start_Date) and product_id=pid and 
					client_id=ClientID and scenario_Id=ScenarioID);
					
                    if ifnull(check_last_time,1) = 1 Then
                        SELECT pid, check_last_time;
						insert into xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID, TIME_INDEX, Cumul_QUANTITY)
						values(CLIENTID, ScenarioID,pid,Planning_Horizon + Hour(Planning_Start_Date),10000000);
					End IF;					
					
					set @csum = 0;					
				END LOOP; 				
				CLOSE cur5;     
			End If;

			if Planning_Bucket = "Minutes" Then
				OPEN cur5; 
				read_loop:LOOP 
					
					FETCH cur5 INTO pid;
					IF done5 = 1 THEN
						LEAVE read_loop;
					END IF;

					update xlps_material_receipts
					set CUMUL_QUANTITY = (@csum := @csum + QUANTITY)
					where client_id=ClientID  and Scenario_ID=ScenarioID
					and product_id =pid
					order by product_id, time_index;
					
					SET check_start_time = (SELECT time_index from xlps_material_receipts where time_index = 0 and product_id=pid and 
					client_id=ClientID and scenario_Id=ScenarioID and Quantity is not NULL);                     
								
					 if check_start_time!=0 or ifnull(check_start_time,1) = 1 Then
						 insert into xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID, TIME_INDEX, Cumul_QUANTITY)
						 values(CLIENTID, ScenarioID,pid,0,0);
					 End if;
					
					SET check_last_time = (SELECT time_index from xlps_material_receipts 
					where time_index = Planning_Horizon + Hour(Planning_Start_Date) * 60 + Minute(Planning_Start_Date) and product_id=pid and 
					client_id=ClientID and scenario_Id=ScenarioID);
					
                    if ifnull(check_last_time,1) = 1 Then
						insert into xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID, TIME_INDEX, Cumul_QUANTITY)
						values(CLIENTID, ScenarioID,pid,Planning_Horizon + Hour(Planning_Start_Date) * 60 + Minute(Planning_Start_Date),10000000);   
					End IF;
                    set @csum = 0;
					
				END LOOP; 				
				CLOSE cur5; 
			End If;

			if Planning_Bucket = "Seconds" Then
				OPEN cur5; 
				read_loop:LOOP 
					
					FETCH cur5 INTO pid;
					IF done5 = 1 THEN
						LEAVE read_loop;
					END IF;

					-- SELECT pid;
					update xlps_material_receipts
					set CUMUL_QUANTITY = (@csum := @csum + QUANTITY)
					where client_id=ClientID  and Scenario_ID=ScenarioID
					and product_id =pid
					order by product_id, time_index;
                    
					
					SET check_start_time = (SELECT time_index from xlps_material_receipts where time_index = 0 and product_id=pid and 
					client_id=ClientID and scenario_Id=ScenarioID and Quantity is not NULL);                     
					-- SELECT check_start_time;
					 if check_start_time!=0 or ifnull(check_start_time,1) = 1 Then
						 insert into xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID, TIME_INDEX, Cumul_QUANTITY)
						 values(CLIENTID, ScenarioID,pid,0,0);
					 End if;		
						
					SET check_last_time = (SELECT time_index from xlps_material_receipts 
					where time_index = Planning_Horizon + Hour(Planning_Start_Date) * 3600 + Minute(Planning_Start_Date) *60 + Second(Planning_Start_Date) and 
					product_id=pid and  client_id=ClientID and scenario_Id=ScenarioID);
			
					if ifnull(check_last_time,1) = 1 Then
						insert into xlps_material_receipts(CLIENT_ID, Scenario_ID, PRODUCT_ID, TIME_INDEX, Cumul_QUANTITY)
						values(CLIENTID, ScenarioID,pid,Planning_Horizon + Hour(Planning_Start_Date) * 3600 + Minute(Planning_Start_Date) *60 + Second(Planning_Start_Date),10000000);
					End If;	
					
					set @csum = 0;					
				END LOOP; 				
				CLOSE cur5;  
			End If;       
		END;
            

    
    
		SELECT "Step11 - xlps_bom",CURTIME() - time_stamp;

		DELETE from xlps_bom where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;	 

		insert into xlps_bom (client_id, scenario_id, PRODUCTION_ORDER_INDEX, OPERATION_INDEX,ROUTING_ID, ALTERNATE_INDEX,  INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT_INDEX,ATTACH_RATE,YIELD, ALTERNATE_BOM_FLAG)
		select distinct df1.client_id,df1.Scenario_ID,rsr.production_order_index,rsr.OPERATION_INDEX, rsr.ROUTING_ID,rsr.alternate_index, 
        sp.PRODUCT_INDEX, sp1.product_index, df1.df_value, df2.df_value, CASE WHEN df3.df_value ='' THEN 0 ELSE df3.df_value END 
		from spps_df_input df1 inner join spps_relation sr on sr.relation_id =df1.relation_id and sr.client_id=df1.client_id 		
        inner join XLPS_OPERATION_SOLVER_READY xl on sr.OPERATION_ID=xl.OPERATION_ID and sr.CLIENT_ID=xl.CLIENT_ID and df1.scenario_id=xl.scenario_id 
		inner join XLPS_ROUTE_SOLVER_READY rsr on rsr.OPERATION_INDEX=xl.OPERATION_INDEX and rsr.CLIENT_ID=xl.CLIENT_ID and df1.scenario_id=rsr.scenario_id 
		inner join xlps_po_products sp 
        on sp.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and sr.INPUT_PRODUCT_ID =sp.PRODUCT_ID and sp.client_id=rsr.client_id and sp.scenario_id=rsr.scenario_id 
        inner join xlps_po_products sp1 on sp1.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX 
		and sr.OUTPUT_PRODUCT_ID =sp1.product_id and sp1.client_id=rsr.client_id and sp1.scenario_id=rsr.scenario_id  		
		inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id and df1.scenario_id=df2.scenario_id
        inner join spps_df_input df3 on df1.relation_id=df3.relation_id and df1.client_id=df3.client_id and df1.scenario_id=df3.scenario_id
		where  df1.df_id=602 and df2.df_id=621 and df3.df_id=687 and  df1.client_id=ClientID and df1.Scenario_ID= ScenarioID; 

                  
       SELECT "Step12 - Deleting Data from output tables",CURTIME() - time_stamp; 

		DELETE from xlps_po_routing where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
  
		insert into xlps_po_routing (CLIENT_ID,SCENARIO_ID,PRODUCTION_ORDER_INDEX, ROUTING_ID)
		select distinct rsr.CLIENT_ID,rsr.Scenario_ID,po.PRODUCTION_ORDER_INDEX, ROUTING_ID From XLPS_ROUTE_SOLVER_READY rsr inner join xlps_po_solver_ready po
		on po.production_order_index=rsr.production_order_index and po.client_id=rsr.client_id and po.scenario_id=rsr.scenario_id 
		where rsr.CLIENT_ID= ClientID and rsr.Scenario_ID=ScenarioID order by po.PRODUCTION_ORDER_INDEX,routing_id;

		SET @i:=-1;
		UPDATE xlps_po_routing SET PO_ROUTE_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
    	
	delete from  XLPS_HOLDING_TIME where Scenario_ID=ScenarioID and client_id=ClientID;
    
    INSERT INTO XLPS_HOLDING_TIME(CLIENT_ID, Scenario_ID,PRODUCTION_ORDER_INDEX, OPERATION_INDEX, MACHINE_INDEX, MIN_HOLDING_TIME, MAX_HOLDING_TIME)
	select distinct sr.CLIENT_ID,df1.Scenario_ID, rsr.production_order_index, rsr.OPERATION_INDEX,rsr.ALTERNATE_INDEX,df2.df_value,df1.df_value
    from spps_relation sr
    inner join spps_df_input df1 on sr.relation_ID= df1.relation_ID and sr.client_id=df1.client_id 
    inner join xlps_product_master pm on pm.PRODUCT_ID=sr.output_product_id and pm.CLIENT_ID=sr.CLIENT_ID and pm.SCENARIO_ID=df1.SCENARIO_ID
    inner join xlps_po_products po on po.PRODUCT_INDEX=pm.product_index and po.client_id=sr.client_id and po.SCENARIO_ID=df1.scenario_id
    inner join xlps_machine_solver_ready msr on msr.MACHINE_ID=sr.resource_id and msr.OPERATION_ID=sr.OPERATION_ID
    and msr.CLIENT_ID=sr.CLIENT_ID and msr.SCENARIO_ID=df1.SCENARIO_ID
    inner join xlps_route_solver_ready rsr on rsr.MACHINE_INDEX=msr.MACHINE_INDEX and rsr.PRODUCTION_ORDER_INDEX=po.PRODUCTION_ORDER_INDEX
    and rsr.CLIENT_ID=sr.CLIENT_ID and rsr.SCENARIO_ID=df1.SCENARIO_ID   
    inner join spps_df_input df2 on sr.relation_ID= df2.relation_ID and sr.client_id=df2.client_id  and df1.SCENARIO_ID=df2.SCENARIO_ID    
    where df1.df_id = 671 and df2.df_id = 672
    and df1.Scenario_ID=ScenarioID and df1.client_id=ClientID;
    
    DELETE from xlps_op_output_count where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
    
    insert into xlps_op_output_count(Client_id,Scenario_ID,production_order_index, OPERATION_INDEX, routing_id,OUTPUT_PRODUCT_INDEX)
	select distinct CLIENT_ID,Scenario_ID,production_order_index, OPERATION_INDEX, routing_id,OUTPUT_PRODUCT_INDEX
	from xlps_bom  bm  where client_id=ClientID  and Scenario_ID= ScenarioID 
     group by production_order_index, OPERATION_INDEX, routing_id,OUTPUT_PRODUCT_INDEX;
     
    Delete from xlps_shelf_life where  CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
    Delete from xlps_shelf_life2 where  CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
    
	insert into xlps_shelf_life (CLIENT_ID,SCENARIO_ID,OUTPUT_PRODUCT_INDEX,MIN_SEQUENCE,MAX_SEQUENCE)
	select ClientID,ScenarioID,bm.output_product_index, min(sequence_id), max(sequence_id)+1 from xlps_bom bm 
    inner join xlps_route_solver_ready rsr
    on bm.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and bm.ALTERNATE_INDEX=rsr.ALTERNATE_INDEX
    and bm.ROUTING_ID=rsr.ROUTING_ID and bm.OPERATION_INDEX=rsr.OPERATION_INDEX
    and bm.CLIENT_ID=rsr.CLIENT_ID and bm.SCENARIO_ID=rsr.SCENARIO_ID
    where  rsr.CLIENT_ID=ClientID and rsr.Scenario_ID=ScenarioID
    group by bm.OUTPUT_PRODUCT_INDEX;
 
 	insert into xlps_shelf_life2 (CLIENT_ID,SCENARIO_ID,PRODUCTION_ORDER_INDEX, OUTPUT_PRODUCT_INDEX,MIN_SEQUENCE,MAX_SEQUENCE)
	select ClientID,ScenarioID,po.production_order_index,sl.output_product_index,min_sequence, max_sequence 
    from xlps_shelf_life sl 
    inner join xlps_po_products po
    on po.PRODUCT_INDEX=sl.OUTPUT_PRODUCT_INDEX and po.CLIENT_ID=sl.CLIENT_ID and po.SCENARIO_ID=sl.SCENARIO_ID
    where po.CLIENT_ID= ClientID and po.scenario_ID=ScenarioID;
    
    
    Delete from XLPS_Machine_Holiday where Client_ID=CLIENTID and SCENARIO_ID=ScenarioID;
    
	if Planning_Bucket = "Seconds" Then

		insert into XLPS_Machine_Holiday (Client_ID, Scenario_ID,Machine_Index,Machine_ID,Holiday_Start_Time,Holiday_End_Time,Holiday_Start_Time_Index,Holiday_End_Time_Index) 
		select CLIENTID,ScenarioID,msr.machine_index,msr.machine_id, holiday_date, Date_ADD(holiday_date, INTERVAL 1 DAY),
        Datediff(holiday_date,Planning_Start_Date) *24*60*60, 
        Datediff(Date_ADD(holiday_date, INTERVAL 1 DAY),Planning_Start_Date) *24*60*60
		from xlps_machine_solver_ready msr inner join
		spps_holiday_calendar hc on msr.CLIENT_ID=hc.CLIENT_ID  and msr.SCENARIO_ID = hc.SCENARIO_ID 
        inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=msr.OPERATION_ID and osr.CLIENT_ID=msr.CLIENT_ID
        and msr.SCENARIO_ID = osr.SCENARIO_ID
        where msr.Client_ID=CLIENTID and msr.SCENARIO_ID=ScenarioID and osr.OPERATION_TYPE<>3;


		insert into XLPS_Machine_Holiday (Client_ID, Scenario_ID,Machine_Index,Machine_ID,Holiday_Start_Time,Holiday_End_Time,Holiday_Start_Time_Index,Holiday_End_Time_Index) 
		select CLIENTID,ScenarioID,msr.machine_index,msr.machine_id, str_to_date(p.period_value,'%m/%d/%Y'), Date_ADD(str_to_date(p.period_value,'%m/%d/%Y'), INTERVAL 1 DAY),
        Datediff(str_to_date(p.period_value,'%m/%d/%Y'),Planning_Start_Date)  *24*60*60, 
        Datediff(Date_ADD(str_to_date(p.period_value,'%m/%d/%Y'), INTERVAL 1 DAY),Planning_Start_Date) *24*60*60 from spps_workday_calendar wc
        inner join spapp_period p on p.client_id=wc.CLIENT_ID and DAYNAME(STR_TO_DATE(p.PERIOD_VALUE,'%m/%d/%Y')) = wc.DAY_NAME 
        inner join xlps_machine_solver_ready msr on msr.CLIENT_ID=wc.CLIENT_ID  and msr.SCENARIO_ID = wc.SCENARIO_ID 
		inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=msr.OPERATION_ID and osr.CLIENT_ID=msr.CLIENT_ID
         and msr.SCENARIO_ID = osr.SCENARIO_ID
        where p.period_bucket='Days' and p.period_id>= periodid and str_to_date(p.period_value,'%m/%d/%Y')>=Planning_Start_Date and wc.day_status="Holiday" and osr.OPERATION_TYPE<>3
        and  Datediff(str_to_date(p.period_value,'%m/%d/%Y'),Planning_Start_Date) <= Planning_Horizon/86400 and msr.Client_ID=CLIENTID and msr.SCENARIO_ID=ScenarioID;
  

	End If;

	if Planning_Bucket = "Minutes" Then
   	insert into XLPS_Machine_Holiday (Client_ID, Scenario_ID,Machine_Index,Machine_ID,Holiday_Start_Time,Holiday_End_Time,Holiday_Start_Time_Index,Holiday_End_Time_Index) 
		select CLIENTID,ScenarioID,msr.machine_index,msr.machine_id, holiday_date, Date_ADD(holiday_date, INTERVAL 1 DAY),
        Datediff(holiday_date,Planning_Start_Date) *24*60, 
        Datediff(Date_ADD(holiday_date, INTERVAL 1 DAY),Planning_Start_Date) *24*60
		from xlps_machine_solver_ready msr inner join
		spps_holiday_calendar hc on msr.CLIENT_ID=hc.CLIENT_ID and msr.SCENARIO_ID = hc.SCENARIO_ID 
        inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=msr.OPERATION_ID and osr.CLIENT_ID=msr.CLIENT_ID
        and msr.SCENARIO_ID = osr.SCENARIO_ID
        where msr.Client_ID=CLIENTID and msr.SCENARIO_ID=ScenarioID and osr.OPERATION_TYPE<>3;
        
   	insert into XLPS_Machine_Holiday (Client_ID, Scenario_ID,Machine_Index,Machine_ID,Holiday_Start_Time,Holiday_End_Time,Holiday_Start_Time_Index,Holiday_End_Time_Index) 
		select CLIENTID,ScenarioID,msr.machine_index,msr.machine_id, str_to_date(p.period_value,'%m/%d/%Y'), Date_ADD(str_to_date(p.period_value,'%m/%d/%Y'), INTERVAL 1 DAY),
        Datediff(str_to_date(p.period_value,'%m/%d/%Y'),Planning_Start_Date)  *24*60, 
        Datediff(Date_ADD(str_to_date(p.period_value,'%m/%d/%Y'), INTERVAL 1 DAY),Planning_Start_Date) *24*60 from spps_workday_calendar wc
        inner join spapp_period p on p.client_id=wc.CLIENT_ID and DAYNAME(STR_TO_DATE(p.PERIOD_VALUE,'%m/%d/%Y')) = wc.DAY_NAME 
        inner join xlps_machine_solver_ready msr on msr.CLIENT_ID=wc.CLIENT_ID  and msr.SCENARIO_ID = wc.SCENARIO_ID 
		inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=msr.OPERATION_ID and osr.CLIENT_ID=msr.CLIENT_ID
         and msr.SCENARIO_ID = osr.SCENARIO_ID
        where p.period_bucket='Days' and p.period_id>= periodid and str_to_date(p.period_value,'%m/%d/%Y')>=Planning_Start_Date and wc.day_status="Holiday" and osr.OPERATION_TYPE<>3
        and  Datediff(str_to_date(p.period_value,'%m/%d/%Y'),Planning_Start_Date) <= Planning_Horizon/1440 and msr.Client_ID=CLIENTID and msr.SCENARIO_ID=ScenarioID;
        
	End If;
    
	if Planning_Bucket = "Hours" Then
   	insert into XLPS_Machine_Holiday (Client_ID, Scenario_ID,Machine_Index,Machine_ID,Holiday_Start_Time,Holiday_End_Time,Holiday_Start_Time_Index,Holiday_End_Time_Index) 
		select CLIENTID,ScenarioID,msr.machine_index,msr.machine_id, holiday_date, Date_ADD(holiday_date, INTERVAL 1 DAY),
        Datediff(holiday_date,Planning_Start_Date) *24, 
        Datediff(Date_ADD(holiday_date, INTERVAL 1 DAY),Planning_Start_Date) *24
		from xlps_machine_solver_ready msr inner join
		spps_holiday_calendar hc on msr.CLIENT_ID=hc.CLIENT_ID and msr.SCENARIO_ID = hc.SCENARIO_ID 
        inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=msr.OPERATION_ID and osr.CLIENT_ID=msr.CLIENT_ID
        and msr.SCENARIO_ID = osr.SCENARIO_ID
        where msr.Client_ID=CLIENTID and msr.SCENARIO_ID=ScenarioID and osr.OPERATION_TYPE<>3;
        
     	insert into XLPS_Machine_Holiday (Client_ID, Scenario_ID,Machine_Index,Machine_ID,Holiday_Start_Time,Holiday_End_Time,Holiday_Start_Time_Index,Holiday_End_Time_Index) 
		select CLIENTID,ScenarioID,msr.machine_index,msr.machine_id, str_to_date(p.period_value,'%m/%d/%Y'), Date_ADD(str_to_date(p.period_value,'%m/%d/%Y'), INTERVAL 1 DAY),
        Datediff(str_to_date(p.period_value,'%m/%d/%Y'),Planning_Start_Date)  *24, 
        Datediff(Date_ADD(str_to_date(p.period_value,'%m/%d/%Y'), INTERVAL 1 DAY),Planning_Start_Date) *24 from spps_workday_calendar wc
        inner join spapp_period p on p.client_id=wc.CLIENT_ID and DAYNAME(STR_TO_DATE(p.PERIOD_VALUE,'%m/%d/%Y')) = wc.DAY_NAME 
        inner join xlps_machine_solver_ready msr on msr.CLIENT_ID=wc.CLIENT_ID  and msr.SCENARIO_ID = wc.SCENARIO_ID 
		inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=msr.OPERATION_ID and osr.CLIENT_ID=msr.CLIENT_ID
         and msr.SCENARIO_ID = osr.SCENARIO_ID
        where p.period_bucket='Days' and p.period_id>= periodid and str_to_date(p.period_value,'%m/%d/%Y')>=Planning_Start_Date and wc.day_status="Holiday" and osr.OPERATION_TYPE<>3
        and  Datediff(str_to_date(p.period_value,'%m/%d/%Y'),Planning_Start_Date) <= Planning_Horizon/24 and msr.Client_ID=CLIENTID and msr.SCENARIO_ID=ScenarioID;
        
	End If;
		Delete from XLPS_PO_RELATION where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
        
    	INSERT INTO XLPS_PO_RELATION
		(CLIENT_ID, Scenario_ID,PRODUCTION_ORDER_INDEX_FG, PRODUCT_ID_FG, PRODUCTION_ORDER_INDEX_SFG,PRODUCT_ID_SFG)
		select po.CLIENT_ID, po.Scenario_ID, po.production_order_index,po.product_id,po1.production_order_index,po1.product_id 
		from xlps_po_solver_ready po
		inner join xlps_po_solver_ready po1 on po1.CLIENT_ID=po.client_id and po1.Scenario_ID=po.Scenario_ID
		inner join xlps_po_products pop on pop.production_order_index=po.production_order_index
		and pop.product_index=po1.product_id and po1.CLIENT_ID=pop.client_id and po1.Scenario_ID=pop.Scenario_ID
		where po.level=3 and po1.level=2 and po1.CLIENT_ID=CLIENTID and po1.Scenario_ID=ScenarioID
        order  by po.ORDER_DUE_DATE_INDEX;
        
        	 	  
		Delete from xlps_route_solver_ready_new  where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
        insert into xlps_route_solver_ready_new select * from xlps_route_solver_ready;
    
     SELECT "Step13 - UPDATING OPERATIONS COMPLETED";
   
			Delete from xlps_po_lots_completed  where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
	  
   
   BEGIN
			DECLARE PO,OP,ROUTE,seq,max_seq INT Default 0 ;
            DECLARE count_seq_total_op,count_seq_same_op INT;
			DECLARE done2 BOOLEAN DEFAULT FALSE;  
           
 
			DECLARE cur2 CURSOR FOR  SELECT distinct po.PRODUCTION_ORDER_INDEX,op.OPERATION_INDEX,df.df_value  FROM xlps_po_solver_ready po
			
            inner join spps_relation sr on sr.order_id=po.PRODUCTION_ORDER_ID and sr.client_id=po.client_id 
			inner join spps_df_input df on sr.relation_ID= df.relation_ID and sr.client_id=df.client_id  and df.scenario_id=po.SCENARIO_ID
            inner join xlps_operation_solver_ready op on po.client_ID=op.client_id and po.scenario_id=op.scenario_id
            inner join spps_df_input df1 on sr.relation_ID= df1.relation_ID and sr.client_id=df1.client_id  and df1.scenario_id=po.SCENARIO_ID
             and op.operation_id=df1.df_value
            where df.df_id=663 and df1.df_id=662 and df.CLIENT_ID=ClientID and df.Scenario_ID=ScenarioID;
            
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = 1;      

            OPEN cur2; 
 			read_loop:LOOP             
				
 				FETCH cur2 INTO PO,OP,ROUTE;
		
 				IF done2 = 1 THEN
 					LEAVE read_loop;
 				END IF;

				SELECT PO,OP,ROUTE;
  				select distinct SEQUENCE_ID into seq from xlps_route_solver_ready_new where  production_order_index=PO 
                and operation_index=OP and routing_id=ROUTE and client_id=ClientID and scenario_ID=ScenarioID;
                
                select count(*) into count_seq_same_op from xlps_route_solver_ready_new where production_order_index=PO 
                and routing_id=ROUTE and operation_index=OP and client_id=ClientID and scenario_ID=ScenarioID and sequence_id=seq;

                select count(*) into count_seq_total_op from xlps_route_solver_ready_new where production_order_index=PO 
                and routing_id=ROUTE and client_id=ClientID and scenario_ID=ScenarioID and SEQUENCE_ID=seq;
                
				select max(SEQUENCE_ID) into max_seq from xlps_route_solver_ready_new where production_order_index=PO 
                and routing_id=ROUTE and client_id=ClientID and scenario_ID=ScenarioID;
                
                SELECT seq, count_seq_same_op, count_seq_total_op, max_seq;
                
                if seq=max_seq then
					insert into xlps_po_lots_completed(Client_ID, Scenario_ID, production_order_index)
					values (ClientID, ScenarioID,PO) ;
                    
                     Delete from xlps_route_solver_ready_new where production_order_index=PO and routing_id=ROUTE
					and client_id=ClientID and scenario_ID=ScenarioID and sequence_id=seq;                
                end if;
                
                Delete from xlps_route_solver_ready_new where production_order_index=PO and routing_id=ROUTE
                and client_id=ClientID and scenario_ID=ScenarioID and sequence_id<seq;
                
                if count_seq_total_op-count_seq_same_op>=1 then            
				
					Delete from xlps_route_solver_ready_new where production_order_index=PO and routing_id=ROUTE
					and operation_index=OP and client_id=ClientID and scenario_ID=ScenarioID and sequence_id=seq;
                else
					
                    Delete from xlps_route_solver_ready_new where production_order_index=PO and routing_id=ROUTE
					and operation_index=OP and client_id=ClientID and scenario_ID=ScenarioID and sequence_id=seq;
                    
					update xlps_route_solver_ready_new rsr
					set SEQUENCE_ID = SEQUENCE_ID - seq  where  production_order_index=PO and routing_id=ROUTE
					and client_id=ClientID and scenario_ID=ScenarioID; 
                    
                end if;
                        
				
 			END LOOP read_loop;  
 			CLOSE cur2; 		
 		END; 
           
    
        
        DELETE from SPPS_SCHEDULE_REPORT where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from SPPS_SCHEDULE_NEW_REPORT where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from SPPS_SCHEDULE_qty_REPORT where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from SPPS_RESOURCE_UTILIZATION where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from SPPS_SHORTAGE_REPORT where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from spps_material_requirement where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from spps_PO_RESOURCE_qty_report where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from spps_machine_daily_capacity_report where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
        DELETE from spps_solver_df_output where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		DELETE from spps_setup_report where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
        DELETE from xlps_machine_invalid_ranges where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
        Delete from SPPS_MATERIAL_STATUS where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
		-- select * from xlps_po_solver_ready  where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
        
        
        -- Delete  xlps_po_solver_ready.* from  xlps_po_solver_ready inner join  po_delete_lots_completed on
--         xlps_po_solver_ready.production_order_index=po_delete_lots_completed.production_order_index
--         where xlps_po_solver_ready.client_id=ClientID and xlps_po_solver_ready.scenario_ID=ScenarioID;


END$$
DELIMITER ;
