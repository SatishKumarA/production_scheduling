DELIMITER $$
drop procedure if exists SPPS_ROUTING_GRAPH $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SPPS_ROUTING_GRAPH`(ClientID INT, ScenarioID INT)
BEGIN

	DECLARE time_stamp int;
	DECLARE nPO, nOperations, nAlt, sAltIndex,OrderQty,lotsize INT;
	DECLARE sPOID, sProductID, sOperationID,sIPProductID,pID,prodID,sMachineID  VARCHAR(100);
	DECLARE sOperationIndex, i, j, k, OpCounter, sTK, count,max_setup_time,setuptime,x,POs INT;
	DECLARE sNextOpID, sPrevOpID, Planning_Bucket VARCHAR(100);
	DECLARE Planning_Start_Date,Planning_End_Date DATETIME; 
    DECLARE Last_Order_Due_Date DATETIME;
    DECLARE Planning_Horizon,OrderQuantity,IP_Qty,AttachRate,Late_Delivery_Horizon INT; 
    DECLARE check_start_time,check_last_time BIGINT;
    DECLARE enable_routing_diagram,enable_populating_route_solver_ready int;

	SET enable_routing_diagram =1 ;
    SET enable_populating_route_solver_ready =1 ;
    
    if enable_routing_diagram=1 and ClientID!=10 then
    
		truncate table xlps_machine_solver_ready;
		truncate table xlps_route_solver_ready;
		truncate table xlps_operation_solver_ready;
		truncate table xlps_po_solver_ready;
		truncate table xlps_po_products;
		truncate table xlps_material_receipts;
		truncate table xlps_setup_solver_ready;
		truncate table xlps_bom;
		truncate table xlps_bom_temp;
		truncate table xlps_product_master;	
		truncate table spps_df_input_temp;
		truncate table spps_relation_temp;

		SELECT STR_TO_DATE(Parameter_Value,'%m/%d/%Y')  INTO Planning_Start_Date FROM spapp_parameter where module_id=6 and parameter_id=601 
		and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		
		delete from spps_relation_temp where client_id=ClientID;
		delete from spps_df_input_temp where CLIENT_ID = ClientID and SCENARIO_ID = ScenarioID and DF_ID in (608,609,610,620,661,670,675);
		SELECT Parameter_Value  INTO Late_Delivery_Horizon FROM spapp_parameter where module_id=6 and parameter_id=611 
		and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		
		SELECT Parameter_Value INTO Planning_Horizon FROM spapp_parameter where module_id=6 and parameter_id=603
		and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

		SELECT Parameter_Value INTO Planning_Bucket FROM spapp_parameter where module_id=6 and parameter_id=602
		and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		
	  
		SELECT "Step1 - XLPS_PRODUCT_MASTER";
		select CURTIME() into time_stamp;
		Delete from XLPS_PRODUCT_MASTER where client_id=ClientID  and Scenario_ID=ScenarioID;

		INSERT INTO XLPS_PRODUCT_MASTER
		( CLIENT_ID, Scenario_ID,LOCATION_ID,PRODUCT_ID,PRODUCT_TYPE_ID)
		select distinct df1.CLIENT_ID,df1.Scenario_ID,sr.location_from_id,df1.df_value,df2.df_value from spps_df_input df1 
		inner join spps_relation sr on sr.relation_id =df1.relation_id 
		and sr.client_id=df1.client_id 
		inner join spps_df_input df2 on df1.relation_id=df2.relation_id and df1.client_id=df2.client_id 
		and df1.scenario_id=df2.scenario_id where df1.df_id=2000 and df2.df_id=2003 and df1.CLIENT_ID= ClientID 
		and df1.scenario_id= ScenarioID ;

		SET @i:=-1;
		UPDATE XLPS_PRODUCT_MASTER SET PRODUCT_INDEX = @i:=@i+1 where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID  order by PRODUCT_TYPE_ID; 
					
		SELECT "Step2 - XLPS_MACHINE_SOLVER_READY",CURTIME() - time_stamp;                
			 select CURTIME() into time_stamp;       
		Delete from XLPS_MACHINE_SOLVER_READY where client_id=ClientID  and Scenario_ID=ScenarioID;

		INSERT INTO XLPS_MACHINE_SOLVER_READY
		( CLIENT_ID, Scenario_ID,MACHINE_INDEX, MACHINE_ID,OPERATION_ID)
		select distinct sr1.CLIENT_ID,df1.Scenario_ID, 1,sr1.resource_ID,sr1.OPERATION_ID from spps_relation sr1
		inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id 
		where df1.df_id = 2060  and df1.Scenario_ID=ScenarioID and df1.client_id=ClientID ;
		

		SET @i:=-1;
		UPDATE XLPS_MACHINE_SOLVER_READY SET ALT_INDEX_KEY = @i:=@i+1 where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID order by operation_id; 

		SELECT "Step6 - XLPS_OPERATION_SOLVER_READY",CURTIME() - time_stamp; 
		select CURTIME() into time_stamp;
		
		Delete from XLPS_OPERATION_SOLVER_READY where client_id=ClientID and Scenario_ID=ScenarioID;

		INSERT INTO XLPS_OPERATION_SOLVER_READY
		(CLIENT_ID,Scenario_ID, OPERATION_INDEX, OPERATION_ID,NO_MACHINES, OPERATION_TYPE)
		select distinct sr1.CLIENT_ID,ScenarioID, 1,sr1.OPERATION_ID,df2.df_value,df1.df_value from spps_relation sr1
		inner join spps_df_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id 
		inner join spps_df_input df2 on df1.relation_ID= df2.relation_ID and df1.CLIENT_ID=df2.client_id and df1.scenario_id=df2.SCENARIO_ID
		where df1.df_id = 2077 and df2.df_id = 616 and df1.CLIENT_ID=ClientID and df1.Scenario_ID=ScenarioID;    
		
		SET @i:=-1;
		UPDATE XLPS_OPERATION_SOLVER_READY SET OPERATION_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;   
		
		Delete from XLPS_PO_SOLVER_READY_TEMP where client_id=ClientID and Scenario_ID=ScenarioID;
		
		INSERT INTO XLPS_PO_SOLVER_READY_TEMP(CLIENT_ID, Scenario_ID,PRODUCTION_ORDER_INDEX, PRODUCTION_ORDER_ID,PRODUCT_ID,ORDER_QUANTITY)
		select distinct CLIENTID,ScenarioID,1,concat(pm.PRODUCT_ID,'-Lot0'),pm.product_index,0 from xlps_product_master pm 
		where pm.product_type_id = 3 and pm.CLIENT_ID=ClientID and pm.Scenario_ID=ScenarioID;  
	  
		update XLPS_PO_SOLVER_READY_TEMP po 
		inner join xlps_product_master pm on po.PRODUCT_ID=pm.PRODUCT_Index and pm.client_id=po.client_id and pm.scenario_id=po.scenario_id
		inner join spps_relation sr1 on sr1.OUTPUT_PRODUCT_ID=pm.PRODUCT_ID and sr1.client_id=pm.client_id
		inner join spps_ts_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id    
		set order_quantity = df1.ts_value
		where df1.sP_ts_id = 601  and df1.ts_value>0
		and df1.CLIENT_ID=ClientID and df1.Scenario_ID=ScenarioID;
		
			delete from xlps_po_solver_ready where order_quantity<=0;
	  
	-- 	select distinct sr1.CLIENT_ID,df1.Scenario_ID,1,concat(pm.PRODUCT_ID,'-Lot0'),pm.product_index,df1.ts_value from spps_relation sr1
	--     inner join spps_ts_input df1 on sr1.relation_ID= df1.relation_ID and sr1.client_id=df1.client_id
	--     inner join xlps_product_master pm on sr1.OUTPUT_PRODUCT_ID=pm.PRODUCT_ID and pm.client_id=df1.client_id and pm.scenario_id=df1.scenario_id
	--     where df1.sP_ts_id = 601  and df1.ts_value>0
	--     and df1.CLIENT_ID=ClientID and df1.Scenario_ID=ScenarioID;

		SET @i:=-1;
		UPDATE XLPS_PO_SOLVER_READY_TEMP SET PRODUCTION_ORDER_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID; 
	 
		Delete from XLPS_PO_SOLVER_READY where client_id=ClientID and Scenario_ID=ScenarioID;
	   
		INSERT INTO XLPS_PO_SOLVER_READY
		( CLIENT_ID, Scenario_ID,PRODUCTION_ORDER_INDEX, PRODUCTION_ORDER_ID,PRODUCT_ID,ORDER_QUANTITY)
		select distinct po.CLIENT_ID, ScenarioID,1,po.PRODUCTION_ORDER_ID,po.product_id,ORDER_QUANTITY from 
		XLPS_PO_SOLVER_READY_TEMP po where po.CLIENT_ID=ClientID and po.Scenario_ID=ScenarioID;
				
		SET @i:=-1;
		UPDATE XLPS_PO_SOLVER_READY SET PRODUCTION_ORDER_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

		SET i=0;
		SELECT count(*) INTO count FROM XLPS_MACHINE_SOLVER_READY where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		SELECT MIN(ALT_INDEX_KEY) INTO sTK FROM XLPS_MACHINE_SOLVER_READY where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		SELECT OPERATION_ID INTO sPrevOpID FROM XLPS_MACHINE_SOLVER_READY where ALT_INDEX_KEY=sTK and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;

		SET OpCounter=0;
		 WHILE i < count DO
			SELECT OPERATION_ID INTO sNextOpID FROM XLPS_MACHINE_SOLVER_READY where ALT_INDEX_KEY=sTK+i and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
			if sNextOpID=sPrevOpID then
				SET OpCounter=OpCounter+1;
			else
				SET OpCounter=1;
			end if;
			SET sPrevOpID=sNextOpID;
			 UPDATE XLPS_MACHINE_SOLVER_READY
			 SET ALTERNATE_INDEX=OpCounter-1 where ALT_INDEX_KEY=sTK+i and CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
				SET i=i+1;
		end while;

	   SELECT 20;
		SET @i:=-1;
		UPDATE XLPS_MACHINE_SOLVER_READY SET MACHINE_INDEX = @i:=@i+1 where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;  

		SET @i:=-1;
		UPDATE XLPS_MACHINE_SOLVER_READY SET ALT_INDEX_KEY = @i:=@i+1 where CLIENT_ID = ClientID  and Scenario_ID=ScenarioID order by operation_id; 

		update xlps_machine_solver_ready m1 join (select CLIENT_ID, Scenario_ID, machine_id, min(machine_index) as minindex from xlps_machine_solver_ready  group by machine_id) m2
		   on m1.machine_id = m2.machine_id and m1.CLIENT_ID=m2.CLIENT_ID and m1.Scenario_ID=m2.Scenario_ID
		   set m1.machine_index = minindex 
		   where m1.CLIENT_ID = ClientID  and m1.Scenario_ID=ScenarioID;
		   
		  SELECT 30;

		
		SELECT "Step9 - xlps_po_products",CURTIME() - time_stamp;     
		select CURTIME() into time_stamp;
		
		Delete from xlps_bom_temp where client_id=ClientID and Scenario_ID=ScenarioID;

		insert into xlps_bom_temp(CLIENT_ID ,Scenario_ID, INPUT_PRODUCT,OUTPUT_PRODUCT, ATTACH_RATE)
		select sr.client_id,df.Scenario_ID,sr.INPUT_PRODUCT_ID,sr.OUTPUT_PRODUCT_ID, df_value from spps_relation sr inner join
		spps_df_input df on df.client_id=sr.client_id and df.relation_id=sr.relation_id
		where df.df_id=602 and sr.client_id=ClientID and df.Scenario_ID=ScenarioID;
			
		update xlps_bom_temp bm
		inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT
		set bm.DESIGNATION='CHILD' 
		where pm.product_type_id=1 and pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;

		update xlps_bom_temp bm
		inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT
		set bm.DESIGNATION='PARENT' 
		where pm.product_type_id=2 or pm.product_type_id=3 and pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;
		SELECT 40;
		update xlps_bom_temp bm
		inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.OUTPUT_PRODUCT
		set bm.OUTPUT_PRODUCT_INDEX=pm.PRODUCT_INDEX 
		where pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;
				
		SELECT 50;
			SET i=0;
			SET j=9999;
			SELECT count(*) INTO nPO FROM XLPS_PO_SOLVER_READY where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
			WHILE i < nPO DO
				SET k = 0;
				
				if k=0 then
					SELECT distinct PRODUCT_INDEX,pm.PRODUCT_ID INTO sProductID,prodID FROM XLPS_PO_SOLVER_READY sr
					inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.SCENARIO_ID=sr.SCENARIO_ID
					where PRODUCTION_ORDER_INDEX=i and pm.CLIENT_ID=ClientID  and pm.Scenario_ID=ScenarioID;
		
					INSERT INTO xlps_bom_temp(CLIENT_ID ,Scenario_ID,INPUT_PRODUCT,INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT,OUTPUT_PRODUCT_INDEX,DESIGNATION) 
					VALUES (ClientID,ScenarioID, prodID,j, prodID,j,'ROOT');

					INSERT INTO xlps_bom_temp(CLIENT_ID ,SCENARIO_ID,INPUT_PRODUCT,OUTPUT_PRODUCT,OUTPUT_PRODUCT_INDEX,DESIGNATION) 
					VALUES (ClientID,ScenarioID, prodID, prodID,j,'PARENT');
				
					SET k=k+1;
				End If;
				
				SET j = j+1;
				SET i = i+1;
			END WHILE;
			SELECT 60;
				update xlps_bom_temp bm
				inner join xlps_product_master pm on pm.client_id=bm.client_id  and pm.Scenario_ID=bm.Scenario_ID and pm.PRODUCT_ID=bm.INPUT_PRODUCT
				set bm.INPUT_PRODUCT_INDEX=pm.PRODUCT_INDEX
				where bm.designation<>'ROOT' and pm.client_id=ClientID and pm.Scenario_ID=ScenarioID;
			  
				 delete from xlps_bom_temp 
				 where INPUT_PRODUCT_INDEX = OUTPUT_PRODUCT_INDEX
				 and designation='PARENT' and client_id=ClientID and scenario_ID=ScenarioID;
				
			
		Delete from xlps_po_products where client_id=ClientID and Scenario_ID=ScenarioID;
		Delete from xlps_tree_table where client_id=ClientID and Scenario_ID=ScenarioID;
		
		insert into xlps_tree_table(CLIENT_ID,Scenario_ID,FG,Components)
		SELECT distinct CLIENT_ID,Scenario_ID,INPUT_PRODUCT_INDEX, GetFamilyTree2(INPUT_PRODUCT_INDEX,ClientID,ScenarioID) FROM xlps_bom_temp 
		WHERE Designation = 'PARENT'  and CLIENT_ID=ClientID and Scenario_ID=ScenarioID order by ID desc;
		SELECT 70;
		SET i=0;
		SELECT count(*) INTO nPO FROM XLPS_PO_SOLVER_READY where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
		WHILE i < nPO DO
			SELECT distinct PRODUCT_INDEX INTO sProductID FROM XLPS_PO_SOLVER_READY sr
			inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.SCENARIO_ID=sr.SCENARIO_ID
			where PRODUCTION_ORDER_INDEX=i and pm.CLIENT_ID=ClientID  and pm.Scenario_ID=ScenarioID;
			

			BEGIN
				DECLARE FG1, Components1 VARCHAR(100);  
				DECLARE a, Quantity BIGINT Default 0 ;
				DECLARE str VARCHAR(255);
				DECLARE done2 BOOLEAN DEFAULT FALSE;  
			   
			   DECLARE cur2 CURSOR FOR SELECT ID, Components FROM xlps_bom_temp bo 
				inner join xlps_tree_table tr on tr.CLIENT_ID = bo.CLIENT_ID and tr.Scenario_ID = bo.Scenario_ID and tr.FG = bo.INPUT_PRODUCT_INDEX
				WHERE Designation = 'PARENT' and bo.INPUT_PRODUCT_INDEX=sProductID and bo.CLIENT_ID=ClientID and bo.Scenario_ID=ScenarioID order by ID desc;             
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET done2 = 1;              
						  
				OPEN cur2; 
				read_loop:LOOP 
				
					
					FETCH cur2 INTO FG1,Components1;
			
					IF done2 = 1 THEN
						LEAVE read_loop;
					END IF;
					SET a=0;
					
					select distinct PRODUCTION_ORDER_INDEX into POs from xlps_po_products where  client_id=ClientID and scenario_ID=ScenarioID
					and production_order_index=i;
							
					if ifnull(POs,1) = 1 Then
								
						insert into xlps_po_products(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,PRODUCT_ID) 
						select client_id,ScenarioID,i,INPUT_PRODUCT from xlps_bom_temp
						where ID = trim(FG1) and client_id = ClientID and scenario_ID=ScenarioID;
						select INPUT_PRODUCT_INDEX into sProductID from xlps_bom_temp where ID = trim(FG1) and
						 client_id = ClientID and scenario_ID=ScenarioID;
		  
						simple_loop: LOOP
								 SET a=a+1;
								 SET str=SPLIT_STR(Components1,",",a);
								
								 IF str='' THEN
									LEAVE simple_loop;
								 END IF;

								insert into xlps_po_products(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,PRODUCT_ID) 
								select client_id,ScenarioID,i,INPUT_PRODUCT from xlps_bom_temp
								where ID = trim(str)   and client_id=ClientID and scenario_ID=ScenarioID;                            
								
						END LOOP simple_loop;
					End If;
				END LOOP read_loop;  
				CLOSE cur2; 		
			END; 
			SET i = i+ 1;
		END WHILE;
	 
		SELECT 80;
		update xlps_po_products bm
		inner join xlps_product_master pm on pm.client_id=bm.client_id and pm.PRODUCT_ID=bm.PRODUCT_ID and pm.scenario_id=bm.scenario_id
		set bm.PRODUCT_INDEX=pm.PRODUCT_INDEX
		where pm.client_id=ClientID and pm.scenario_id=ScenarioID;

	if enable_populating_route_solver_ready = 1 then
	SELECT "Step7 - XLPS_ROUTE_SOLVER_READY",CURTIME() - time_stamp;
		select CURTIME() into time_stamp;
	  
		insert into spps_relation_temp (RELATION_ID,OUTPUT_PRODUCT,OPERATION_ID,RESOURCE_ID,CLIENT_ID)
		select distinct RELATION_ID,OUTPUT_PRODUCT_ID,OPERATION_ID,RESOURCE_ID,CLIENT_ID from spps_relation sr
		where sr.client_id=ClientID;    
		SELECT 199;
		insert into spps_df_input_temp (TABLE_KEY,RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID)
		select TABLE_KEY,RELATION_ID,DF_ID,DF_VALUE,INCREMENT_ID,CLIENT_ID,SCENARIO_ID from spps_df_input df
		where df.CLIENT_ID = ClientID and df.SCENARIO_ID = ScenarioID and DF_ID in (608,609,610,620,661,670,675);
		SELECT 200;
		Delete from XLPS_ROUTE_SOLVER_READY where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;
		SELECT 201;
		select * from XLPS_PO_SOLVER_READY;
		select 202;
		SET i=0;
		SELECT count(*) INTO nPO FROM XLPS_PO_SOLVER_READY where CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
		SELECT nPO;
		WHILE i < nPO DO
			
			SELECT PRODUCTION_ORDER_ID INTO sPOID FROM XLPS_PO_SOLVER_READY where PRODUCTION_ORDER_INDEX=i and CLIENT_ID=ClientID  and Scenario_ID=ScenarioID;
		
			SELECT PRODUCT_Index INTO sProductID FROM XLPS_PO_SOLVER_READY sr
			inner join xlps_product_master pm on pm.product_index=sr.product_id and pm.client_id=sr.client_id and pm.scenario_id=sr.scenario_id
			where PRODUCTION_ORDER_INDEX=i and pm.CLIENT_ID=ClientID  and pm.Scenario_ID=ScenarioID;
			
			INSERT INTO XLPS_ROUTE_SOLVER_READY(CLIENT_ID,Scenario_ID,PRODUCTION_ORDER_INDEX,OPERATION_INDEX,ROUTING_ID,SEQUENCE_ID,ALTERNATE_INDEX,MACHINE_INDEX) 
					
				select ClientID,ScenarioID,i, osr.operation_index, df6.df_value ,df5.df_value , osr1.alternate_index,osr1.machine_index from spps_df_input df1
				inner join spps_relation_temp sr on sr.relation_id=df1.relation_id and sr.client_ID=df1.Client_id
                inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT and pm.client_id=sr.client_id and pm.scenario_id=df1.scenario_id
				inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=sr.operation_id and osr.client_id=sr.CLIENT_ID and osr.scenario_id=df1.scenario_id
                inner join xlps_machine_solver_ready osr1 on osr1.OPERATION_ID=sr.operation_id and osr1.machine_id= sr.resource_id and osr1.client_id=sr.CLIENT_ID and osr1.scenario_id=df1.scenario_id
		
                inner join spps_relation_temp sr1 on sr1.OUTPUT_PRODUCT=sr.OUTPUT_PRODUCT and sr1.OPERATION_ID=sr.OPERATION_ID and sr1.client_id=sr.CLIENT_ID
                inner join spps_df_input_temp df5 on sr1.RELATION_ID=df5.relation_id and df1.SCENARIO_ID=df5.SCENARIO_ID and df1.CLIENT_ID=df5.CLIENT_ID
                inner join spps_df_input_temp df6 on sr1.RELATION_ID=df6.relation_id and df1.SCENARIO_ID=df6.SCENARIO_ID and df1.CLIENT_ID=df6.CLIENT_ID 
                and df5.INCREMENT_ID=df6.increment_id
                inner join xlps_po_solver_ready pop on pop.PRODUCTION_ORDER_INDEX=i and pm.product_index=pop.PRODUCT_ID
                
				where df1.df_id= 608 and df5.df_id =620  and df6.df_id =661
                and sr.client_id=ClientID and df1.Scenario_ID=ScenarioID;                      

-- 				select ClientID,ScenarioID,i, osr.operation_index, df6.df_value ,df5.df_value , osr1.alternate_index,osr1.machine_index 
-- 					from spps_df_input df1
-- 					inner join (select relation_id,OUTPUT_PRODUCT, operation_id, resource_id, client_id from spps_relation_temp where client_id=ClientID ) sr on sr.relation_id=df1.relation_id
-- 					inner join xlps_product_master pm on pm.product_id=sr.OUTPUT_PRODUCT and pm.client_id=sr.client_id and pm.scenario_id=df1.scenario_id
-- 					inner join ( select production_order_index, product_index, client_id, scenario_id from xlps_po_products where     
-- 					client_id =ClientID and scenario_id = ScenarioID) as xlpm on xlpm.PRODUCTION_ORDER_INDEX=i and pm.product_index=xlpm.PRODUCT_INDEX 
-- 					and pm.client_id=xlpm.client_id and pm.scenario_id=xlpm.scenario_id
-- 					inner join xlps_operation_solver_ready osr on osr.OPERATION_ID=sr.operation_id and osr.client_id=sr.CLIENT_ID and osr.scenario_id=df1.scenario_id
-- 					inner join xlps_machine_solver_ready osr1 on osr1.OPERATION_ID=sr.operation_id and osr1.machine_id= sr.resource_id 
-- 					and osr1.client_id=sr.CLIENT_ID and osr1.scenario_id=df1.scenario_id		
-- 					inner join ( select relation_id,OUTPUT_PRODUCT, OPERATION_ID, client_id from spps_relation_temp where client_id =ClientID) as srt 
-- 					on srt.OUTPUT_PRODUCT=sr.OUTPUT_PRODUCT and srt.OPERATION_ID=sr.OPERATION_ID
-- 					inner join ( select relation_id,client_id, scenario_id ,df_value,df_id,increment_id from spps_df_input_temp 
-- 					where client_id =ClientID and scenario_id=ScenarioID and df_id =620) as df5 on srt.RELATION_ID=df5.relation_id                    
-- 					inner join ( select relation_id,client_id, scenario_id ,df_value,df_id,increment_id from spps_df_input_temp 
-- 					where client_id =ClientID and scenario_id=ScenarioID and df_id =661) as df6 on srt.RELATION_ID=df6.relation_id  and df5.INCREMENT_ID=df6.increment_id  
-- 					inner join (select production_order_index, product_id from  xlps_po_solver_ready where client_id=ClientID and scenario_id=ScenarioID ) po
-- 					on po.PRODUCTION_ORDER_INDEX=i and pm.product_index=po.PRODUCT_id 
-- 					where df1.df_id= 608 and sr.client_id=ClientID and df1.Scenario_ID=ScenarioID; 
                    
                    
			SET  i=i+1;
		END WHILE;    
end if;

		
			SELECT "Step11 - xlps_bom",CURTIME() - time_stamp;

			DELETE from xlps_bom where CLIENT_ID=ClientID and Scenario_ID=ScenarioID;	 

			insert into xlps_bom (client_id, scenario_id, PRODUCTION_ORDER_INDEX, OPERATION_INDEX,ROUTING_ID, ALTERNATE_INDEX,  INPUT_PRODUCT_INDEX,OUTPUT_PRODUCT_INDEX,ATTACH_RATE)
	-- 		select distinct df1.client_id,df1.Scenario_ID,rsr.production_order_index,rsr.OPERATION_INDEX, rsr.ROUTING_ID,rsr.alternate_index, 
	--         sp.PRODUCT_INDEX, sp1.product_index,df1.df_value
	-- 		from spps_df_input df1 inner join spps_relation sr on sr.relation_id =df1.relation_id and sr.client_id=df1.client_id 		
	--         inner join XLPS_OPERATION_SOLVER_READY xl on sr.OPERATION_ID=xl.OPERATION_ID and sr.CLIENT_ID=xl.CLIENT_ID and df1.scenario_id=xl.scenario_id 
	-- 		inner join XLPS_ROUTE_SOLVER_READY rsr on rsr.OPERATION_INDEX=xl.OPERATION_INDEX and rsr.CLIENT_ID=xl.CLIENT_ID and df1.scenario_id=rsr.scenario_id 
	-- 		inner join xlps_po_products sp 
	--         on sp.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and sr.INPUT_PRODUCT_ID =sp.PRODUCT_ID and sp.client_id=rsr.client_id and sp.scenario_id=rsr.scenario_id 
	--         inner join xlps_po_products sp1 on sp1.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX 
	-- 		and sr.OUTPUT_PRODUCT_ID =sp1.product_id and sp1.client_id=rsr.client_id and sp1.scenario_id=rsr.scenario_id  		
	-- 		where  df1.df_id=602 and  df1.client_id=ClientID and df1.Scenario_ID= ScenarioID; 
			
			select distinct df1.client_id,df1.Scenario_ID,rsr.production_order_index,rsr.OPERATION_INDEX, rsr.ROUTING_ID,rsr.alternate_index, 
			sp.PRODUCT_INDEX, sp1.product_index,df1.df_value
			from spps_df_input df1 
			inner join (select OPERATION_ID,relation_id,INPUT_PRODUCT_ID,OUTPUT_PRODUCT_ID, client_id from  spps_relation where client_id=ClientID) sr on sr.relation_id =df1.relation_id	
			inner join XLPS_OPERATION_SOLVER_READY xl on sr.OPERATION_ID=xl.OPERATION_ID and sr.CLIENT_ID=xl.CLIENT_ID and df1.scenario_id=xl.scenario_id 
			inner join 
			(select production_order_index,operation_index,routing_id,alternate_index, client_id,scenario_id from XLPS_ROUTE_SOLVER_READY where client_id =ClientID and scenario_id=ScenarioID) rsr 
			on rsr.OPERATION_INDEX=xl.OPERATION_INDEX 
			inner join 
			(select  production_order_index, product_index, product_id,client_id, scenario_id from xlps_po_products  where client_id =ClientID and scenario_id=ScenarioID) sp 
			on sp.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and sr.INPUT_PRODUCT_ID =sp.PRODUCT_ID
			inner join 
			(select  production_order_index, product_index, product_id, client_id, scenario_id from xlps_po_products  where client_id =ClientID and scenario_id=ScenarioID) sp1 
			on sp1.PRODUCTION_ORDER_INDEX=rsr.PRODUCTION_ORDER_INDEX and sr.OUTPUT_PRODUCT_ID =sp1.product_id 		
			where  df1.df_id=602 and  df1.client_id=ClientID and df1.Scenario_ID= ScenarioID; 
			

			update xlps_bom bm inner join
			xlps_product_master pm
			on bm.input_product_index=pm.product_index and bm.client_id = pm.client_id and bm.Scenario_ID = pm.Scenario_ID
			set bm.input_product=pm.product_id
			where  bm.client_id = ClientId and bm.Scenario_ID = ScenarioID;

			update xlps_bom bm inner join
			xlps_product_master pm
			on bm.output_product_index=pm.product_index and bm.client_id = pm.client_id and bm.Scenario_ID = pm.Scenario_ID
			set bm.output_product=pm.product_id
			where  bm.client_id = ClientId and bm.Scenario_ID = ScenarioID;

			delete from spps_routing_temp where client_id=ClientID and Scenario_ID=ScenarioID;
			delete from spps_routing where client_id=ClientID and Scenario_ID=ScenarioID;

			INSERT INTO `spps_routing_temp`(`FINISHED_GOOD`,`OUTPUT_PRODUCT_ID`,`INPUT_PRODUCT_ID`,`OPERATION_ID`,`SEQUENCE_ID`,`ROUTING_ID`,`CLIENT_ID`,`SCENARIO_ID`)
			select distinct pm.PRODUCT_ID, bm.output_product, bm.input_product, op.operation_id, rsr.SEQUENCE_ID, bm.ROUTING_ID,bm.client_id,bm.Scenario_ID
			from xlps_bom bm
			 inner join xlps_po_solver_ready po on po.PRODUCTION_ORDER_INDEX=bm.PRODUCTION_ORDER_INDEX and bm.client_id = po.client_id and bm.Scenario_ID = po.Scenario_ID
			 inner join xlps_operation_solver_ready op on op.OPERATION_INDEX=bm.OPERATION_INDEX and bm.client_id = op.client_id and bm.Scenario_ID = op.Scenario_ID
			 inner join xlps_route_solver_ready rsr on rsr.PRODUCTION_ORDER_INDEX = bm.PRODUCTION_ORDER_INDEX and rsr.OPERATION_INDEX=bm.OPERATION_INDEX
			 and rsr.ROUTING_ID=bm.ROUTING_ID and rsr.client_id = po.client_id and rsr.Scenario_ID = po.Scenario_ID
			 inner join xlps_product_master pm on pm.PRODUCT_INDEX=po.PRODUCT_ID and pm.client_id = po.client_id and pm.Scenario_ID = po.Scenario_ID
			where bm.client_id=ClientID and bm.Scenario_ID =ScenarioID;

			INSERT INTO `spps_routing`(`FINISHED_GOOD`,`OUTPUT_PRODUCT_ID`,`INPUT_PRODUCT_ID`,`OPERATION_ID`,`SEQUENCE_ID`,`ROUTING_ID`,`CLIENT_ID`,`SCENARIO_ID`)
			SELECT `FINISHED_GOOD`,`OUTPUT_PRODUCT_ID`,GROUP_CONCAT(`INPUT_PRODUCT_ID` SEPARATOR ', '),`OPERATION_ID`,`SEQUENCE_ID`,`ROUTING_ID`,`CLIENT_ID`,`SCENARIO_ID`
			
			FROM spps_routing_temp 
			where client_id=ClientID and Scenario_ID =ScenarioID
			GROUP BY `FINISHED_GOOD`,`OUTPUT_PRODUCT_ID`,`OPERATION_ID`,`SEQUENCE_ID`,`ROUTING_ID`,`CLIENT_ID`,`SCENARIO_ID`;

		end if;
    
    

END$$
DELIMITER ;
