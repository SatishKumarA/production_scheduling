DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `GetFamilyTree2`(GivenName varchar(1024),ClientID int, ScenarioID int) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv,q,queue,queue_children,queue_names VARCHAR(1024);
    DECLARE queue_length,pos INT;
    DECLARE GivenSSN,front_ssn VARCHAR(1024);

    SET rv = '';

    SELECT distinct INPUT_PRODUCT_INDEX INTO GivenSSN
    FROM xlps_bom_temp
    WHERE INPUT_PRODUCT_INDEX = GivenName
    AND Designation <> 'ROOT' and client_id=ClientID and scenario_ID=ScenarioID;
    IF ISNULL(GivenSSN) THEN
        RETURN ev;
    END IF;

    SET queue = GivenSSN;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        IF queue_length = 1 THEN
            SET front_ssn = queue;
            SET queue = '';
        ELSE
            SET pos = LOCATE(',',queue);
            SET front_ssn = LEFT(queue,pos - 1);
            SET q = SUBSTR(queue,pos + 1);
            SET queue = q;
        END IF;
        SET queue_length = queue_length - 1;
        SELECT IFNULL(qc,'') INTO queue_children
        FROM
        (
            SELECT GROUP_CONCAT(INPUT_PRODUCT_INDEX) qc FROM xlps_bom_temp
            WHERE OUTPUT_PRODUCT_INDEX = front_ssn AND Designation <> 'ROOT'  and client_id=ClientID and scenario_ID=ScenarioID
        ) A;
        SELECT IFNULL(qc,'') INTO queue_names
        FROM
        (
            SELECT GROUP_CONCAT(ID) qc FROM xlps_bom_temp
            WHERE OUTPUT_PRODUCT_INDEX = front_ssn AND Designation <> 'ROOT'  and client_id=ClientID and scenario_ID=ScenarioID
        ) A;
        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_names;
            ELSE
                SET rv = CONCAT(rv,',',queue_names);                
            END IF;
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;
    RETURN rv;

END$$
DELIMITER ;
